
import argparse
from datetime import datetime, timedelta
import time
import copy
import subprocess
from collections import defaultdict
import os
from random import randint

# Defines the ids for the various experiments to be run
BURSTY_SVD_VARY_VN_AR = 'b_svd_ar' # Bursty VN model. Static vs. Dynamic physical network. Vary the average arrival rate of VNs.
BURSTY_SVD_VARY_SIZE = 'b_svd_size' # Bursty VN model. Static vs. Dynamic physical network. Vary the size of the physical network.
BURSTY_ILP_VARY_VN_AR = 'b_ilp_ar' # Bursty VN model. Compare with ILP/LP. Vary the average arrival rate of VNs.
BURSTY_ILP_VARY_SIZE = 'b_ilp_size' # Bursty VN model. Compare with ILP/LP. Vary the size of the physical network.
BURSTY_LMS_VARY_VN_AR = 'b_lms_ar' # Bursty VN model. Compares Static and Dynamic embeddings. Vary the average arrival rate of VNs.
BURSTY_LMS_VARY_SIZE = 'b_lms_size' # Bursty VN model. Compares Static and Dynamic embeddings. Vary the size of the physical network.
BURSTY_LMS_VARY_VN_AR_MULTI_SLACK = 'b_lms_ar_multi_slack' # Bursty VN model. Compares Static and Dynamic embeddings. Vary the average arrival rate of VNs. Two different slack factors.
BURSTY_LMS_VARY_SIZE_MULTI_SLACK = 'b_lms_size_multi_slack' # Bursty VN model. Compares Static and Dynamic embeddings. Vary the size of the physical network. Two different slack factors.

BURSTY_SVD_LARGE = 'b_svd_large'

BURSTY_SVD_VARY_VN_AR_SMALL = 'b_svd_ar_small' # Bursty VN model. Static vs. Dynamic physical network. Vary the average arrival rate of VNs. On a small physical network (16 racks)
BURSTY_SVD_VARY_SIZE_SMALL = 'b_svd_size_small' # Bursty VN model. Static vs. Dynamic physical network. Vary the size of the physical network. On smaller networks only.

BURSTY_LMS_VARY_SIZE_SMALL = 'b_lms_size_small' # Bursty VN model. Compares Static and Dynamic embeddings. Vary the size of the physical network.

# Experiments for the INFOCOMM paper
ALL_TESTS = 'all'
LP_TESTS = 'lp'
SMALL_TESTS = 'small'
LARGE_TESTS = 'large'
LARGE_AR_TESS = 'large_ar'
LARGE_HIGH_AR_TESTS = 'large_high_ar'

HIGH_AR_150_TESS = 'high_ar_150'
HIGH_AR_200_TESS = 'high_ar_200'

TEST_REVERSE_NODE_RANK = 'test_rnr'

VARY_VN_SIZE = 'vary_vn_size'
VARY_REAL_VN_SIZE = 'vary_real_vn_size'
VARY_NUM_LINKS = 'vary_num_links'

PROFILE = 'profile'
ALL_EXPERIMENT_IDS = [BURSTY_SVD_VARY_VN_AR, BURSTY_SVD_VARY_SIZE, BURSTY_SVD_LARGE,
						BURSTY_ILP_VARY_VN_AR, BURSTY_ILP_VARY_SIZE,
						BURSTY_LMS_VARY_VN_AR, BURSTY_LMS_VARY_SIZE, BURSTY_LMS_VARY_SIZE_SMALL,
						BURSTY_LMS_VARY_VN_AR_MULTI_SLACK, BURSTY_LMS_VARY_SIZE_MULTI_SLACK,
						BURSTY_SVD_VARY_VN_AR_SMALL, BURSTY_SVD_VARY_SIZE_SMALL,
						ALL_TESTS, LP_TESTS, SMALL_TESTS, LARGE_TESTS, LARGE_AR_TESS, LARGE_HIGH_AR_TESTS, HIGH_AR_150_TESS, HIGH_AR_200_TESS,
						TEST_REVERSE_NODE_RANK,
						PROFILE,
						VARY_VN_SIZE, VARY_REAL_VN_SIZE, VARY_NUM_LINKS]

GUROBI_EXPERIMENTS = [BURSTY_ILP_VARY_VN_AR, BURSTY_ILP_VARY_SIZE]

VN_TOPOLOGY_FILES = ['real_world_data/emulab_data.vn_top', 'real_world_data/deterlab_data.vn_top']
FLOW_DATA_FILES = ['real_world_data/vl2_approx.flow_iat_hist']

# Class to hold the values. When initialized with cmd line arguments, holds defualt values for tests.
class ExperimentParams:
	def __init__(self, args):
		# Default Values
		# Physical Network
		num_racks = 64
		num_edge_sets_per_rack = 8
		num_servers_per_rack = 8
		num_edges_per_edge_set = 8
		self.physical_network_size = [(num_racks, num_edge_sets_per_rack, num_servers_per_rack, num_edges_per_edge_set)]

		self.server_cpu = 1.0
		self.rack_to_rack_bandwidth = 10.0 # In Gb/s
		self.server_to_rack_bandwidth = None # For now this is just ignored
		self.number_of_backbone_trees = [3]

		self.physical_out_file = '' # This stops the physical network from being outputted

		# Virtual Network
		self.vn_duration = (0.0, 100.0)
		self.vn_arrival_rate = [.5]
		self.vn_cpu = (0.0, 0.1)
		self.vn_bw = (0.0, 10.0) # virtual edges generate between 0 and 10 Gb/s
		self.vn_test_duration = 5000.0

		self.vn_out_file = ''

		self.vn_data_file = args.real_vn_data_file[0]
		if self.vn_data_file not in VN_TOPOLOGY_FILES:
			raise Exception('Invalid VN Topology file')

		self.real_vn_sizes = [None]

		self.max_physical_edge_collisions = args.slack_factor
		if any(max_physical_edge_collision <= 0.0 or max_physical_edge_collision > 1.0 for max_physical_edge_collision in self.max_physical_edge_collisions):
			raise Exception('Invalid value given for slack factor')

		self.num_virtual_nodes = [None]
		self.link_percent = None

		self.embedding_scheme = 'backtracking'
		self.link_mapping_scheme = ['dyn']
		self.node_mapping_scheme = ['nr']
		self.path_weighting_scheme = ['spread']
		self.preemption_scheme = ['none']
		self.ilp_scheme = ['no_ilp']
		self.ilp_max_num_racks = None
		self.mip_gap = None
		self.compute_max_arrival_rate_scheme = 'queue'
		self.longest_path_length = [3] # could maybe try 2 and see how much it improves things

		self.schedule_out_file = ''

		self.file_name_id = args.file_name_identifier[0]
		
		self.avoid_impossible_edge_sets = True

		self.routing_scheme = 'map'

		self.flow_data_file = args.real_flow_data_file[0]
		if self.flow_data_file not in FLOW_DATA_FILES:
			raise Exception('Invalid flow data file')

		self.no_flow_completion_times = False
		self.profile = False 

		self.num_tests = args.num_tests[0]

class ExperimentRunner:
	def __init__(self, num_cores = 1, filter_exp = []):
		self.num_cores = num_cores
		self.filter_exp = filter_exp

		self.overall_params = {}
		self.tests_per_experiment = defaultdict(list)

	def addExperiment(self, experiment, params):
		self.overall_params[experiment] = params

		if self.num_cores > 1:
			for i in range(params.num_tests):
				for vn_ar in params.vn_arrival_rate:
					for pns in params.physical_network_size:
						for ilps in params.ilp_scheme:
							for lms in params.link_mapping_scheme:
								for mpec in params.max_physical_edge_collisions:
									for nvn in params.num_virtual_nodes:
										for real_nvn in params.real_vn_sizes:
											this_params = copy.deepcopy(params)
											this_params.vn_arrival_rate = [vn_ar]
											this_params.physical_network_size = [pns]
											this_params.ilp_scheme = [ilps]
											this_params.link_mapping_scheme = [lms]
											this_params.max_physical_edge_collisions = [mpec]
											this_params.num_virtual_nodes = [nvn]
											this_params.real_vn_sizes = [real_nvn]
											this_params.num_tests = 1
											this_params.file_name_id += '-' + str(len(self.tests_per_experiment[experiment]))
											if self.filter_exp == None or len(self.filter_exp) == 0 or len(self.tests_per_experiment[experiment]) in self.filter_exp:
												self.tests_per_experiment[experiment].append(this_params)

												print experiment, len(self.tests_per_experiment[experiment]) - 1, vn_ar, pns, ilps, lms, mpec, nvn, real_nvn
											else:
												self.tests_per_experiment[experiment].append(None)

	def run(self):
		if self.num_cores <= 1:
			for exp in self.overall_params:
				runBurstyExperiment(self.overall_params[exp])
		else:
			start_per_test = {}
			duration_per_tests = {}
			total_tests = sum(len([k for k in self.tests_per_experiment[x] if k != None]) for x in self.tests_per_experiment)

			active_processes = []
			completed_processes = {x: [False for p in self.tests_per_experiment[x] if p != None] for x in self.tests_per_experiment}
			for experiment in self.tests_per_experiment:
				print 'Starting:', experiment
				for i, param in enumerate([k for k in self.tests_per_experiment[experiment] if k != None]):
					# Wait until the number of active_processes is les than self.num_cores
					while len(active_processes) >= self.num_cores:

						# Wait while all are running
						while all(process.poll() == None for process, _, _ in active_processes):
							time.sleep(1.0)

						# Mark which tests have finished, and remove those from the activ_process list
						finished_processes = [(process, exp, j) for process, exp, j in active_processes if process.poll() != None]
						for process, exp, j in finished_processes:
							print 'Ending test:', '(' + exp + ', ' + str(j) + ')'
							duration_per_tests[(exp, j)] = (datetime.now() - start_per_test[(exp, j)])
							print 'Length of test:', duration_per_tests[(exp, j)]
							print 'Average test duration:', sum((duration_per_tests[k] for k in duration_per_tests), timedelta()) / len(duration_per_tests)
							print 'Percent tests complete:', str(float(len(duration_per_tests)) / total_tests * 100.0) + '%'
							active_processes.remove((process, exp, j))
							completed_processes[exp][j] = True

					# Check if all tests of a single experiment are over, If so merge the files
					print 'Running test:', '(' + experiment + ', ' + str(i) + ')'
					start_per_test[(experiment, i)] = datetime.now()
					active_processes.append((runBurstyExperiment(param, wait = False, verbose = False), experiment, i))

					for exp in self.tests_per_experiment:
						if all(v for v in completed_processes[exp]):
							# merge all files into one
							self.mergeAllFiles(exp)
			print 'All tests started!!!'

			# Once all processes have started wait until they are all done
			while len(active_processes) > 0:

				# Wait while all are running
				while all(process.poll() == None for process, _, _ in active_processes):
					time.sleep(1.0)

				# Mark which tests have finished, and remove those from the activ_process list
				finished_processes = [(process, exp, j) for process, exp, j in active_processes if process.poll() != None]
				for process, exp, j in finished_processes:
					print 'Ending test:', '(' + exp + ', ' + str(j) + ')'
					duration_per_tests[(exp, j)] = (datetime.now() - start_per_test[(exp, j)])
					print 'Length of test:', duration_per_tests[(exp, j)]
					print 'Average test duration:', sum((duration_per_tests[k] for k in duration_per_tests), timedelta()) / len(duration_per_tests)
					print 'Percent tests complete:', str(float(len(duration_per_tests)) / total_tests * 100.0) + '%'
							
					active_processes.remove((process, exp, j))
					completed_processes[exp][j] = True
			
			# Merge any remaining files
			for exp in self.tests_per_experiment:
				if all(v for v in completed_processes[exp]):
					# merge all files into one
					self.mergeAllFiles(exp)

	def mergeAllFiles(self, experiment):
		print 'Merging files:', experiment
		out_file = 'data/simulator_data_' + self.overall_params[experiment].file_name_id + '.txt'
		f = open(out_file, 'w')

		for test in self.tests_per_experiment[experiment]:
			in_file = 'data/simulator_data_' + test.file_name_id + '.txt'
			try:
				fin = open(in_file, 'r')
				f.write(fin.read())
				fin.close()
				os.remove(in_file)
			except Exception, e:
				print 'There was an exception with file', in_file + ':', e 

def runBurstyExperiment(params, wait = True, verbose = True):
	cmd = ['python', 'simulate_bursty_vns.py']

	# Physical Network Params
	cmd += ['-pns'] + map(lambda x: ','.join(map(str, x)), params.physical_network_size)
	cmd += ['-sc', str(params.server_cpu)]
	cmd += ['-pnnbw', str(params.rack_to_rack_bandwidth)]
	cmd += ['-nbb'] + map(str,params.number_of_backbone_trees)
	cmd += ['-pout', params.physical_out_file]

	if params.server_to_rack_bandwidth != None:
		cmd += ['-snbw', str(params.server_to_rack_bandwidth)]

	# Virtual Network Params
	cmd += ['-dur'] + map(str, params.vn_duration)
	cmd += ['-vcpu'] + map(str, params.vn_cpu)
	cmd += ['-vebw'] + map(str, params.vn_bw)
	cmd += ['-td', str(params.vn_test_duration)]
	cmd += ['-vnar'] + map(str, params.vn_arrival_rate)
	cmd += ['-vout', params.vn_out_file]
	
	if params.vn_data_file != None:
		cmd += ['-rvn', params.vn_data_file]
		if params.real_vn_sizes != None and not(len(params.real_vn_sizes) == 1 and params.real_vn_sizes[0] == None):
			cmd += ['-rvn_size'] + map(str, params.real_vn_sizes)
	elif params.num_virtual_nodes != None and params.link_percent != None:
		cmd += ['-vnn'] + map(str, params.num_virtual_nodes)
		cmd += ['-vlp', str(params.link_percent)]
	else:
		raise Exception('Invalid input')

	# Embeding Params
	cmd += ['-mpec'] + map(str, params.max_physical_edge_collisions)
	cmd += ['-ems', params.embedding_scheme]
	cmd += ['-lms'] + params.link_mapping_scheme
	cmd += ['-nms'] + params.node_mapping_scheme
	cmd += ['-pws'] + params.path_weighting_scheme
	cmd += ['-pre'] + params.preemption_scheme
	cmd += ['-ilp'] + params.ilp_scheme

	if params.ilp_max_num_racks != None:
		cmd += ['-ilp_ms', str(params.ilp_max_num_racks)]

	if params.mip_gap != None:
		cmd += ['-mip_gap', str(params.mip_gap)]

	cmd += ['-mas', params.compute_max_arrival_rate_scheme]
	cmd += ['-lpl'] + map(str, params.longest_path_length)
	cmd += ['-sout', params.schedule_out_file]

	# Simulator Params
	cmd += ['-k', params.file_name_id]
	
	if params.avoid_impossible_edge_sets:
		cmd += ['-avoid_impos_es']

	cmd += ['-routing', params.routing_scheme]
	cmd += ['-rwd', params.flow_data_file]

	if params.no_flow_completion_times:
		cmd += ['-no_fct']
	
	if params.profile:
		cmd += ['-profile']

	cmd += ['-nt', str(params.num_tests)]

	if verbose:
		experiment_process = subprocess.Popen(cmd)
	else:
		DEVNULL = open(os.devnull, 'wb')
		experiment_process = subprocess.Popen(cmd, stdout = DEVNULL, stderr = subprocess.STDOUT)
	if wait:
		experiment_process.wait()
	else:
		return experiment_process

def testGurobi():
	try:
		import gurobipy as GB

		GB.Model()
	except Exception, e:
		print 'Gurobipy not properly setup'
		raise e

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description = 'Runs experiments for the paper. Has the parameters for each figure saved, so they don\'t have to be reentered')
	
	current_time = datetime.now()

	parser.add_argument('-exp', '--experiment_identifier', metavar = 'EXPERIMENT_ID', type = str, nargs = '+', default = [], help = 'List of experiments to run. Must be value in (' + ', '.join(ALL_EXPERIMENT_IDS) + ')')
	parser.add_argument('-rvn', '--real_vn_data_file', metavar = 'VN_DATA_FILE', type = str, nargs = 1, default = ['real_world_data/deterlab_data.vn_top'], help = 'File to pull VN topologies from. Must be from (' + ', '.join(VN_TOPOLOGY_FILES) + ')')
	parser.add_argument('-rfd', '--real_flow_data_file', metavar = 'FLOW_DATA_FILE', type = str, nargs = 1, default = ['real_world_data/vl2_approx.flow_iat_hist'], help = 'File to pull flow data from. Must be from (' + ', '.join(FLOW_DATA_FILES) + ')')
	parser.add_argument('-k', '--file_name_identifier', metavar = 'FILE_NAME_ID', type = str, nargs = 1, default = [current_time.strftime('%m_%d-K')], help = 'File to pull flow data from. Must be from (' + ', '.join(FLOW_DATA_FILES) + ')')
	parser.add_argument('-sf', '--slack_factor', metavar = 'SLACK_FACTOR', type = float, nargs = '+', default = [0.1], help = 'Slack factor to use in the Bursty VNE Algorithm. must be in range (0,1]')
	parser.add_argument('-nt', '--num_tests', metavar = 'NUM_TESTS', type = int, nargs = 1, default = [1], help = 'Number of tests to run')
	parser.add_argument('-nc', '--num_cores', metavar = 'NUM_CORES', type = int, nargs = 1, default = [1], help = 'Number of cores to run on (i.e. number of parallel processes to use')
	parser.add_argument('-fil', '--filter_exp', metavar = 'IND', type = int, nargs = '+', default = [], help = 'If given will only run those tests')
	parser.add_argument('-no_run', '--do_not_run_tests', action = 'store_true', help = 'If given, will not run tests, just create the experiments')

	args = parser.parse_args()
	experiments = args.experiment_identifier

	if any(experiment not in ALL_EXPERIMENT_IDS for experiment in experiments):
		raise Exception('Invalid experiment id: ' + ', '.join([experiment for experiment in experiments if experiment not in ALL_EXPERIMENT_IDS]))

	default_params = ExperimentParams(args)

	runner = ExperimentRunner(num_cores = args.num_cores[0], filter_exp = args.filter_exp)

	if any(exp in GUROBI_EXPERIMENTS for exp in experiments):
		testGurobi()

	if default_params.no_flow_completion_times:
		print 'FLOW COMPLETION TIMES ARE NOT BEING RECORDED'

	no_run = args.do_not_run_tests

	for experiment in experiments:
		this_params = copy.deepcopy(default_params)
		this_params.file_name_id = this_params.file_name_id.replace('K', experiment)
		if experiment == BURSTY_SVD_VARY_VN_AR:
			# Parameters specific to this experiment
			this_params.vn_arrival_rate = [.1, .2, .3, .4, .5, .6, .7, .8, .9, 1.0]

			num_racks = 64
			num_edge_sets_per_rack = 8
			num_servers_per_rack = 8
			this_params.physical_network_size = [(num_racks, num_edge_sets_per_rack, num_servers_per_rack, 1), (num_racks, num_edge_sets_per_rack, num_servers_per_rack, 8)]
		if experiment == BURSTY_SVD_VARY_VN_AR_SMALL:
			# Parameters specific to this experiment
			this_params.vn_arrival_rate = [.1, .2, .3, .4, .5, .6, .7, .8, .9, 1.0]

			num_racks = 16
			num_edge_sets_per_rack = 4
			num_servers_per_rack = 4
			this_params.physical_network_size = [(num_racks, num_edge_sets_per_rack, num_servers_per_rack, 1), (num_racks, num_edge_sets_per_rack, num_servers_per_rack, 4)]
		if experiment == BURSTY_SVD_VARY_SIZE:
			num_racks = [4, 8, 16, 32, 64]
			num_edge_sets_per_rack = [2, 3, 4, 6, 8]
			num_servers_per_rack = [2, 3, 4, 6, 8]
			num_dynamic_edges_per_rack = [2, 3, 4, 6, 8]

			this_params.physical_network_size = [(a, b, c, 1) for a, b, c in zip(num_racks, num_edge_sets_per_rack, num_servers_per_rack)] + \
												[(a, b, c, d) for a, b, c, d in zip(num_racks, num_edge_sets_per_rack, num_servers_per_rack, num_dynamic_edges_per_rack)]
		if experiment == BURSTY_SVD_VARY_SIZE_SMALL:
			num_racks = [4, 8, 16]
			num_edge_sets_per_rack = [2, 3, 4]
			num_servers_per_rack = [2, 3, 4]
			num_dynamic_edges_per_rack = [2, 3, 4]

			this_params.physical_network_size = [(a, b, c, 1) for a, b, c in zip(num_racks, num_edge_sets_per_rack, num_servers_per_rack)] + \
												[(a, b, c, d) for a, b, c, d in zip(num_racks, num_edge_sets_per_rack, num_servers_per_rack, num_dynamic_edges_per_rack)]
		if experiment == BURSTY_SVD_LARGE:
			num_racks = [96, 128]
			num_edge_sets_per_rack = [10, 12]
			num_servers_per_rack = [10, 12]
			num_dynamic_edges_per_rack = [10, 12]

			this_params.physical_network_size = [(a, b, c, 1) for a, b, c in zip(num_racks, num_edge_sets_per_rack, num_servers_per_rack)] + \
												[(a, b, c, d) for a, b, c, d in zip(num_racks, num_edge_sets_per_rack, num_servers_per_rack, num_dynamic_edges_per_rack)]
		if experiment == BURSTY_ILP_VARY_VN_AR:
			this_params.vn_arrival_rate = [.1, .2, .3, .4, .5, .6, .7, .8, .9, 1.0]

			# Only use LP
			this_params.ilp_scheme = ['lp']
		if experiment == BURSTY_ILP_VARY_SIZE:
			num_racks = [4, 8, 16]
			num_edge_sets_per_rack = [2, 3, 4]
			num_servers_per_rack = [2, 3, 4]
			num_dynamic_edges_per_rack = [2, 3, 4]

			this_params.physical_network_size = [(a, b, c, d) for a, b, c, d in zip(num_racks, num_edge_sets_per_rack, num_servers_per_rack, num_dynamic_edges_per_rack)]

			this_params.ilp_scheme = ['lp']
			this_params.ilp_max_num_racks = 4
		if experiment == BURSTY_LMS_VARY_VN_AR:
			this_params.vn_arrival_rate = [.1, .2, .3, .4, .5, .6, .7, .8, .9, 1.0]

			this_params.link_mapping_scheme = ['dyn', 'sta']
			
			this_params.vn_bw = (0.0,3.0)
		if experiment == BURSTY_LMS_VARY_SIZE:
			num_racks = [4, 8, 16, 32, 64]
			num_edge_sets_per_rack = [2, 3, 4, 6, 8]
			num_servers_per_rack = [2, 3, 4, 6, 8]
			num_dynamic_edges_per_rack = [2, 3, 4, 6, 8]

			this_params.physical_network_size = [(a, b, c, d) for a, b, c, d in zip(num_racks, num_edge_sets_per_rack, num_servers_per_rack, num_dynamic_edges_per_rack)]

			this_params.link_mapping_scheme = ['dyn', 'sta']

			this_params.vn_bw = (0.0,3.0)
		if experiment == BURSTY_LMS_VARY_SIZE_SMALL:
			num_racks = [4, 8, 16, 32, 64]
			num_edge_sets_per_rack = [2, 3, 4, 6, 8]
			num_servers_per_rack = [2, 3, 4, 6, 8]
			num_dynamic_edges_per_rack = [2, 3, 4, 6, 8]

			this_params.physical_network_size = [(a, b, c, d) for a, b, c, d in zip(num_racks, num_edge_sets_per_rack, num_servers_per_rack, num_dynamic_edges_per_rack)]

			this_params.link_mapping_scheme = ['dyn', 'sta']

			this_params.vn_bw = (0.0,3.0)

			this_params.vn_test_duration = 1000.0
		if experiment == BURSTY_LMS_VARY_VN_AR_MULTI_SLACK:
			this_params.vn_arrival_rate = [.1, .2, .3, .4, .5, .6, .7, .8, .9, 1.0]

			this_params.link_mapping_scheme = ['dyn', 'sta']

			this_params.max_physical_edge_collisions = [.1, .5]
		if experiment == BURSTY_LMS_VARY_SIZE_MULTI_SLACK:
			num_racks = [4, 8, 16, 32, 64]
			num_edge_sets_per_rack = [2, 3, 4, 6, 8]
			num_servers_per_rack = [2, 3, 4, 6, 8]
			num_dynamic_edges_per_rack = [2, 3, 4, 6, 8]

			this_params.physical_network_size = [(a, b, c, d) for a, b, c, d in zip(num_racks, num_edge_sets_per_rack, num_servers_per_rack, num_dynamic_edges_per_rack)]

			this_params.link_mapping_scheme = ['dyn', 'sta']

			this_params.max_physical_edge_collisions = [.1, .5]
		if experiment == PROFILE:
			num_racks = [4]
			num_edge_sets_per_rack = [2]
			num_servers_per_rack = [2]
			num_dynamic_edges_per_rack = [2]
			this_params.physical_network_size = []
			for nr, nes, ns, ne in zip(num_racks, num_edge_sets_per_rack, num_servers_per_rack, num_dynamic_edges_per_rack):
				this_params.physical_network_size += [(nr, nes, ns, 1)]

			this_params.link_mapping_scheme = ['dyn']
			this_params.ilp_scheme = ['mip']

			this_params.vn_bw = (0.0, 3.0)

			this_params.vn_test_duration = 500.0
			this_params.mip_gap = .2
		if experiment == TEST_REVERSE_NODE_RANK:
			num_racks = 32
			num_edge_sets_per_rack = 6
			num_servers_per_rack = 6
			num_edges_per_edge_set = 6
			this_params.physical_network_size = [(num_racks, num_edge_sets_per_rack, num_servers_per_rack, num_edges_per_edge_set)]

			this_params.node_mapping_scheme = ['nr', 'rnr']
		if experiment == ALL_TESTS:
			this_params.vn_test_duration = 1000.0

			num_racks = 64
			num_edge_sets_per_rack = 8
			num_servers_per_rack = 8
			num_dynamic_edges_per_rack = 8
			this_params.physical_network_size = [(num_racks, num_edge_sets_per_rack, num_servers_per_rack, 1),
													(num_racks, num_edge_sets_per_rack, num_servers_per_rack, num_dynamic_edges_per_rack)]

			this_params.link_mapping_scheme = ['dyn', 'sta']

			this_params.vn_bw = (0.0, 3.0)

			this_params.vn_arrival_rate = [.25, .5, .75, 1.0, 1.25, 1.5]
		if experiment == LP_TESTS:
			num_racks = 64
			num_edge_sets_per_rack = 8
			num_servers_per_rack = 8
			num_dynamic_edges_per_rack = 8
			this_params.physical_network_size = [(num_racks, num_edge_sets_per_rack, num_servers_per_rack, 1),
													(num_racks, num_edge_sets_per_rack, num_servers_per_rack, num_dynamic_edges_per_rack)]

			this_params.link_mapping_scheme = ['dyn']
			this_params.ilp_scheme = ['lp']

			this_params.vn_bw = (0.0, 3.0)
			this_params.vn_arrival_rate = [.25, .5, .75, 1.0]
		if experiment == SMALL_TESTS:
			num_racks = [4, 8, 16]
			num_edge_sets_per_rack = [2, 3, 4]
			num_servers_per_rack = [2, 3, 4]
			num_dynamic_edges_per_rack = [2, 3, 4]
			this_params.physical_network_size = []
			for nr, nes, ns, ne in zip(num_racks, num_edge_sets_per_rack, num_servers_per_rack, num_dynamic_edges_per_rack):
				this_params.physical_network_size += [(nr, nes, ns, ne)]

			this_params.link_mapping_scheme = ['dyn']

			this_params.vn_bw = (0.0, 3.0)

			this_params.vn_test_duration = 1000.0
			this_params.mip_gap = 90
		if experiment == LARGE_TESTS:
			num_racks = [96, 128, 192, 256]
			num_edge_sets_per_rack = [10, 12, 14, 16]
			num_servers_per_rack = [10, 12, 14, 16]
			num_dynamic_edges_per_rack = [10, 12, 14, 16]
			this_params.physical_network_size = []
			for nr, nes, ns, ne in zip(num_racks, num_edge_sets_per_rack, num_servers_per_rack, num_dynamic_edges_per_rack):
				this_params.physical_network_size += [(nr, nes, ns, ne)]

			this_params.link_mapping_scheme = ['dyn']

			this_params.vn_bw = (0.0, 3.0)
		if experiment == LARGE_AR_TESS:
			this_params.vn_test_duration = 1000.0

			num_racks = 64
			num_edge_sets_per_rack = 8
			num_servers_per_rack = 8
			num_dynamic_edges_per_rack = 8
			this_params.physical_network_size = [(num_racks, num_edge_sets_per_rack, num_servers_per_rack, 1)]

			this_params.link_mapping_scheme = ['dyn', 'sta']

			this_params.vn_bw = (0.0, 3.0)

			this_params.vn_arrival_rate = [2.5, 5.0, 7.5, 10.0]
		if experiment == HIGH_AR_150_TESS:
			this_params.vn_test_duration = 150.0

			num_racks = 64
			num_edge_sets_per_rack = 8
			num_servers_per_rack = 8
			num_dynamic_edges_per_rack = 8
			this_params.physical_network_size = [(num_racks, num_edge_sets_per_rack, num_servers_per_rack, num_dynamic_edges_per_rack)]

			this_params.link_mapping_scheme = ['dyn', 'sta']

			this_params.vn_bw = (0.0, 3.0)

			this_params.vn_arrival_rate = [7.5, 10.0]
		if experiment == HIGH_AR_200_TESS:
			this_params.vn_test_duration = 200.0

			num_racks = 64
			num_edge_sets_per_rack = 8
			num_servers_per_rack = 8
			num_dynamic_edges_per_rack = 8
			this_params.physical_network_size = [(num_racks, num_edge_sets_per_rack, num_servers_per_rack, num_dynamic_edges_per_rack)]

			this_params.link_mapping_scheme = ['dyn', 'sta']

			this_params.vn_bw = (0.0, 3.0)

			this_params.vn_arrival_rate = [7.5, 10.0]
		if experiment == LARGE_HIGH_AR_TESTS:
			num_racks = [96, 128, 192, 256]
			num_edge_sets_per_rack = [10, 12, 14, 16]
			num_servers_per_rack = [10, 12, 14, 16]
			num_dynamic_edges_per_rack = [10, 12, 14, 16]
			this_params.physical_network_size = []
			for nr, nes, ns, ne in zip(num_racks, num_edge_sets_per_rack, num_servers_per_rack, num_dynamic_edges_per_rack):
				this_params.physical_network_size += [(nr, nes, ns, ne)]

			this_params.link_mapping_scheme = ['dyn']

			this_params.vn_bw = (0.0, 3.0)

			this_params.vn_test_duration = 500.0
			this_params.vn_arrival_rate = [5.0]
		if experiment == VARY_NUM_LINKS:
			this_params.vn_test_duration = 1000.0

			num_racks = 64
			num_edge_sets_per_rack = 8
			num_servers_per_rack = 8
			num_dynamic_edges_per_rack = [1, 2, 4, 6, 8]
			this_params.physical_network_size = [(num_racks, num_edge_sets_per_rack, num_servers_per_rack, k) for k in num_dynamic_edges_per_rack]

			this_params.link_mapping_scheme = ['dyn', 'sta']

			this_params.vn_bw = (0.0, 3.0)

		if experiment == VARY_VN_SIZE:
			this_params.vn_test_duration = 1000.0

			num_racks = 64
			num_edge_sets_per_rack = 8
			num_servers_per_rack = 8
			num_dynamic_edges_per_rack = 8
			this_params.physical_network_size = [(num_racks, num_edge_sets_per_rack, num_servers_per_rack, 1),
													(num_racks, num_edge_sets_per_rack, num_servers_per_rack, num_dynamic_edges_per_rack)]

			this_params.link_mapping_scheme = ['dyn', 'sta']

			this_params.vn_bw = (0.0, 3.0)
			this_params.vn_data_file = None
			this_params.num_virtual_nodes = [2, 5, 10, 25, 50]
			this_params.link_percent = -1.0
		if experiment == VARY_REAL_VN_SIZE:
			this_params.vn_test_duration = 1000.0

			num_racks = 64
			num_edge_sets_per_rack = 8
			num_servers_per_rack = 8
			num_dynamic_edges_per_rack = 8
			this_params.physical_network_size = [(num_racks, num_edge_sets_per_rack, num_servers_per_rack, 1),
													(num_racks, num_edge_sets_per_rack, num_servers_per_rack, num_dynamic_edges_per_rack)]

			this_params.link_mapping_scheme = ['dyn', 'sta']

			this_params.vn_bw = (0.0, 3.0)
			this_params.real_vn_sizes = [2, 5, 10, 25, 50]

		runner.addExperiment(experiment, this_params)

	if not no_run:
		runner.run()
