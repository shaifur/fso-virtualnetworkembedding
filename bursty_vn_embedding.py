
import physical_network as PN
import vn_requests as VNR
try:
	import ilp_benchmark.ilp_bursty as ILPB
except Exception:
	pass
from profiler import Profiler

import random
from collections import defaultdict
from math import log, exp, ceil, sqrt
import numpy as np
import argparse
from itertools import chain
import numpy
import time as time_module

# TODO check the bandwidth between a PhysicalServer and PhysicalNode when embedding a VN
# TODO add a way to profile the algorithm and find bottlenecks

# TODO comment
class ConvertVirtualEdgeCollisionToPhysicalEdgeCollisionAlgorithm:
	independent_probability = 'ind'
	all_algos = [independent_probability]

	def __init__(self, str_value):
		if str_value not in ConvertVirtualEdgeCollisionToPhysicalEdgeCollisionAlgorithm.all_algos:
			raise Exception('Invalid conversion algorithm')
		self.algo_type = str_value

	def convert(self, max_virtual_edge_collision, physical_network, longest_path):
		Profiler.start('ConvertVirtualEdgeCollisionToPhysicalEdgeCollisionAlgorithm.convert')
		if self.algo_type == ConvertVirtualEdgeCollisionToPhysicalEdgeCollisionAlgorithm.independent_probability:
			smallest_path_set = findSmallestPathSet(physical_network, longest_path)
			max_edge_collision = maxEdgeRateForPathCollision(max_virtual_edge_collision, longest_path, smallest_path_set)
			Profiler.end('ConvertVirtualEdgeCollisionToPhysicalEdgeCollisionAlgorithm.convert')
			return max_edge_collision
		else:
			raise Exception('Unhandled conversion algo type: ' + self.algo_type)

	def __str__(self):
		return self.algo_type

# TODO comment
class PathWeightingScheme:
	inverse_path_length = 'ipl'
	shortest_path_first = 'spf'
	min_shortest_path_first = 'min_spf'
	fixed_path_length = 'fpl'
	spread_weighting_scheme = 'spread'
	all_algos = [inverse_path_length, shortest_path_first, min_shortest_path_first, fixed_path_length, spread_weighting_scheme]
	num_val_algos =[inverse_path_length, fixed_path_length]

	def __init__(self, str_value):
		if str_value[:4] in map(lambda x: x + '_', PathWeightingScheme.num_val_algos):
			num_val = int(str_value[4:])
			str_value = str_value[:3]
		else:
			if str_value in PathWeightingScheme.num_val_algos:
				num_val = 1
			else:
				num_val = None
		if str_value not in PathWeightingScheme.all_algos:
			raise Exception('Inavlid path weighting scheme')
		self.algo_type = str_value
		self.num_val = num_val
		self.random_num_paths = None

	def __str__(self):
		return self.algo_type + ('' if self.num_val == None else '_' + str(self.num_val))

	def __eq__(self, other):
		if isinstance(other, self.__class__):
			return self.algo_type == other.algo_type and self.num_val == other.num_val
		else:
			return False

	def __ne__(self, other):
		return not self.__eq__(other)

	def __hash__(self):
		return hash((self.algo_type, self.num_val))

class LinkMappingScheme:
	dynamic_embedding = 'dyn'
	static_embedding = 'sta'
	all_algos = [dynamic_embedding, static_embedding]

	def __init__(self, str_value):
		if str_value not in LinkMappingScheme.all_algos:
			raise Exception('Unexpected link mapping scheme')
		self.algo_type = str_value

	def __str__(self):
		return self.algo_type

	def __eq__(self, other):
		if isinstance(other, self.__class__):
			return self.algo_type == other.algo_type
		else:
			return False

	def __ne__(self, other):
		return not self.__eq__(other)

	def __hash__(self):
		return hash((self.algo_type,))

class NodeMappingScheme:
	random_node_mapping = 'rand'
	node_rank = 'nr'
	reverse_node_rank = 'rnr'

	all_algos = [random_node_mapping, node_rank, reverse_node_rank]
	def __init__(self, str_value):
		if str_value not in NodeMappingScheme.all_algos:
			raise Exception('Invalid node mapping scheme')
		self.algo_type = str_value
		self.virtual_node_ranks = None
		self.physical_node_ranks = None

	def precomputeVirtualNetworkRanks(self, virtual_network):
		# TODO Save the rankings for each virtual network
		if self.algo_type in (NodeMappingScheme.node_rank, NodeMappingScheme.reverse_node_rank):
			self.virtual_node_ranks = computeVirtualNodeRank(virtual_network)

	def precomputePhysicalNetworkRanks(self, physical_network, total_mapping, link_mapping_scheme):
		if self.algo_type in (NodeMappingScheme.node_rank, NodeMappingScheme.reverse_node_rank):
			self.physical_node_ranks = computePhysicalNodeRank(physical_network, total_mapping, link_mapping_scheme = link_mapping_scheme)

	def __str__(self):
		return self.algo_type

	def __eq__(self, other):
		if isinstance(other, self.__class__):
			return self.algo_type == other.algo_type
		else:
			return False

	def __ne__(self, other):
		return not self.__eq__(other)

	def __hash__(self):
		return hash((self.algo_type,))

class PreemptionScheme:
	no_preemption = 'none'
	only_full_vn_reembedding = 'vn_full'

	all_algos = [no_preemption, only_full_vn_reembedding]

	def __init__(self, str_value):
		if str_value not in PreemptionScheme.all_algos:
			raise Exception('Invalid preemption scheme')
		self.algo_type = str_value

	def __str__(self):
		return self.algo_type

	def __eq__(self, other):
		if isinstance(other, self.__class__):
			return self.algo_type == other.algo_type
		else:
			return False

	def __ne__(self, other):
		return not self.__eq__(self, other)

	def __hash__(self):
		return hash((self.algo_type,))

class ComputeMaxInterArrivalTimeScheme:
	analytical = 'ana'
	numerical = 'num'
	queue_analytical = 'queue'
	all_algos = [analytical, numerical, queue_analytical]

	def __init__(self, str_value):
		if str_value not in ComputeMaxInterArrivalTimeScheme.all_algos:
			raise Exception('Inavlid ComputeMaxInterArrivalTimeScheme')
		self.algo_type = str_value

	def __str__(self):
		return self.algo_type

	def __eq__(self, other):
		if isinstance(other, self.__class__):
			return self.algo_type == other.algo_type
		else:
			return False

	def __ne__(self, other):
		return not self.__eq__(other)

	def __hash__(self):
		return hash((self.algo_type,))

class ILPScheme:
	no_ilp = 'no_ilp'
	run_ip = 'ip'
	run_mip = 'mip'
	run_lp = 'lp'
	all_algos = [no_ilp, run_ip, run_mip, run_lp]

	def __init__(self, str_value):
		if str_value not in ILPScheme.all_algos:
			raise Exception('Inavlid ILPScheme')
		self.algo_type = str_value

		self.ilp_model_type = None
		if self.algo_type == ILPScheme.run_ip:
			self.ilp_model_type = ILPB.ILPBurstySolver.IP
		if self.algo_type == ILPScheme.run_mip:
			self.ilp_model_type = ILPB.ILPBurstySolver.MIP
		if self.algo_type == ILPScheme.run_lp:
			self.ilp_model_type = ILPB.ILPBurstySolver.LP

		self.ilp_solver = None
		self.schedule = None

		self.max_size = None
		self.mip_gap = None

		self.status_file = None

	def setMaxSize(self, v):
		self.max_size = v

	def setMIPGap(self, v):
		self.mip_gap = v

	def willRunILP(self, num_racks = None):
		return (self.algo_type in (ILPScheme.run_ip, ILPScheme.run_mip) and (num_racks == None or self.max_size == None or num_racks <= self.max_size)) or self.algo_type == ILPScheme.run_lp

	def runILP(self, physical_network, current_vnrs, new_vnr, k = None):
		if not self.willRunILP(num_racks = physical_network.getNumberOfNodes()):
			raise Exception('ILP shouldn\'t be run!!!')

		if self.ilp_solver == None:
			self.ilp_solver = ILPB.ILPBurstySolver(self.ilp_model_type, self.status_file)

		return self.ilp_solver.computeILPMapping(physical_network, list(current_vnrs) + [new_vnr], k = k, mip_gap = self.mip_gap)

	def shouldRunILPFlowSimulation(self, num_racks = None):
		return False
		# return self.algo_type in (ILPScheme.run_ip, ILPScheme.run_mip) and (num_racks == None or self.max_size == None or num_racks <= self.max_size)

	def __str__(self):
		return self.algo_type

	def __eq__(self, other):
		if isinstance(other, self.__class__):
			return self.algo_type == other.algo_type
		else:
			return False

	def __ne__(self, other):
		return not self.__eq__(other)

	def __hash__(self):
		return hash((self.algo_type,))

class EmbeddingScheme:
	two_stage = 'two_stage'
	backtracking = 'backtracking'
	all_algos = [two_stage, backtracking]

	def __init__(self, str_value):
		if str_value not in EmbeddingScheme.all_algos:
			raise Exception('Inavlid EmbeddingScheme')
		self.algo_type = str_value

	def __str__(self):
		return self.algo_type

	def __eq__(self, other):
		if isinstance(other, self.__class__):
			return self.algo_type == other.algo_type
		else:
			return False

	def __ne__(self, other):
		return not self.__eq__(other)

	def __hash__(self):
		return hash((self.algo_type,))

# Records the schedule of VN Embedding
class VirtualNetworkSchedule:
	def __init__(self, physical_network, vn_requests):
		self.physical_network = physical_network
		self.vn_requests = {vnr.vn_request_id: vnr for vnr in vn_requests}

		# List of 4-tuple (time of event, event type, vn_request_id) where
		#   time of event - the global time that the event happend
		#   event type - either 'reject' (the vnr was rejected due to limited resources),
		#                       'accept' (the vnr was successfully embedded (TODO add a way to include the actual mapping))
		#						'change' (the mapping of the vnr was changed), or
		#                       'end' (the previously accepted vnr's duration ended)
		#   vn_request_id - id of the associated vnr
		#   single_mapping - None for 'reject' and 'end' events, and the mapping of the VN for an 'accept' event
		self.events = []

		self.num_accept = 0
		self.num_change = 0
		self.num_reject = 0

		self.num_node_reject = 0
		self.num_dynamic_reject = 0
		self.num_backbone_reject = 0

		self.num_virtual_edges_accepted = 0
		self.num_virtual_edges_changed = 0
		self.num_virtual_edges_rejected = 0

	# The following three functions add a 'event' for a virtual network request at a given time. They basically just add a string tag
	def addRejectedVnRequest(self, time, vnr):
		if vnr != self.vn_requests[vnr.vn_request_id]:
			raise Exception('Mismatched Virtual Network request')
		self.events.append((time, 'reject', vnr.vn_request_id, None))
		self.num_reject += 1
		self.num_virtual_edges_rejected += vnr.numVirtualEdges()

	def addAcceptedVnRequest(self, time, vnr, single_mapping):
		if vnr != self.vn_requests[vnr.vn_request_id]:
			raise Exception('Mismatched Virtual Network request')
		self.events.append((time, 'accept', vnr.vn_request_id, single_mapping))
		self.num_accept += 1
		self.num_virtual_edges_accepted += vnr.numVirtualEdges()

	# Adds an event where the mapping of the vnr changed
	def addChangedVnRequest(self, time, vnr, single_mapping):
		if vnr != self.vn_requests[vnr.vn_request_id]:
			raise Exception('Mismatched Virtual Network request')
		self.events.append((time, 'change', vnr.vn_request_id, single_mapping))
		self.num_change += 1
		self.num_virtual_edges_changed += vnr.numVirtualEdges()

	def addEndingVnRequest(self, time, vnr):
		if vnr != self.vn_requests[vnr.vn_request_id]:
			raise Exception('Mismatched Virtual Network request')
		self.events.append((time, 'end', vnr.vn_request_id, None))

	def getAverageResourceUsageOfVirtualEdges(self):
		resource_usage = []
		path_count_by_length = {1:[], 2:[], 3:[]}
		for t, et, vnr_id, mapping in self.events:
			if mapping != None:
				for virtual_edge in mapping.virtual_edge_to_physical_paths_mapping:
					if not all(path.length() == 0 for path,weight in mapping.virtual_edge_to_physical_paths_mapping[virtual_edge]):
						this_resource_usage = 0.0
						this_path_count_by_length = {1:0, 2:0, 3:0}
						for path, weight in mapping.virtual_edge_to_physical_paths_mapping[virtual_edge]:
							this_resource_usage += path.length() * weight
							this_path_count_by_length[path.length()] += 1
						total_weight = sum(this_path_count_by_length[k] / float(k) for k in this_path_count_by_length)
						approx_resource_usage = sum(float(k) * this_path_count_by_length[k] / k / total_weight for k in this_path_count_by_length)
						resource_usage.append(this_resource_usage)
						for i in path_count_by_length:
							path_count_by_length[i].append(this_path_count_by_length[i])

		if len(resource_usage) > 0:
			avg_path_count_by_length = {i: float(sum(path_count_by_length[i])) / len(path_count_by_length[i]) for i in path_count_by_length}
			# print 'avg_path_length', avg_path_count_by_length
			avg_resource_usage = sum(resource_usage) / len(resource_usage)
		else:
			avg_resource_usage = 0.0
		# print 'RU:', avg_resource_usage
		return avg_resource_usage

	def getMappingByPathLen(self):
		resource_usage = []
		path_count_by_length = defaultdict(int)
		for t, et, vnr_id, mapping in self.events:
			if mapping != None:
				for virtual_edge in mapping.virtual_edge_to_physical_paths_mapping:
					for k in path_count_by_length:
						if all(path.length() == k for path,weight in mapping.virtual_edge_to_physical_paths_mapping[virtual_edge]):
							path_count_by_length[k] += 1
		return path_count_by_length

	def computeAverageAcceptedRevenue(self, ONLY_AR = False, AR_AND_FS = False, normalize_by_link_rate = None):
		# Ensures that exactly one of them is True and the other is False
		if (not ONLY_AR and not AR_AND_FS) or (ONLY_AR and AR_AND_FS):
			raise Exception('Invalid input')

		total_revenue_by_time = 0.0
		test_duration = 0.0
		for t, event_type, vnr_id, mapping in self.events:
			if t > test_duration:
				test_duration = t
			if event_type == 'accept':
				vnr = self.vn_requests[vnr_id]

				total_revenue_by_time += vnr.computeRevenue(ONLY_AR = ONLY_AR, AR_AND_FS = AR_AND_FS, normalize_by_link_rate = normalize_by_link_rate) * vnr.duration

		return total_revenue_by_time / test_duration

	def computeAverageRevenueToCostRatio(self, normalize_by_link_rate):
		total = 0.0
		duration = 0.0
		for t, event_type, vnr_id, mapping in self.events:
			if event_type == 'accept':
				vnr = self.vn_requests[vnr_id]

				revenue = vnr.computeRevenue(AR_AND_FS = True, normalize_by_link_rate = normalize_by_link_rate)
				cost = mapping.computeCost(normalize_by_link_rate, vnr.real_world_data.all_fs_avg)
				
				total += revenue / cost * vnr.duration
				duration += vnr.duration
		return total / duration

	def __str__(self):
		# TODO print out the physical network (or at least a way to look up their info)
		# TODO print out the virtual network requests (or at least a way to look up their info)

		return '\n'.join('event@ ' + str(t) + ': ' + str(ty) + ' ' + str(vnr_id) + ('' if sm == None else '\n\t' + sm.toString(sep = '\n\t')) for t, ty, vnr_id, sm in self.events)

# Writes VirtualNetworkSchedule
def writeSchedule(schedule, out_file, pn_file, vnr_file, algorithm_version, params):
	if out_file == '':
		return

	Profiler.start('writeSchedule')
	f = open(out_file, 'w')
	f.write('VERSION ' + ' '.join(algorithm_version) + '\n')
	f.write('PARAMS ' + ' '.join(['|'.join([var, params[var][0], params[var][1]]) for var in params]) + '\n')
	f.write('PNF ' + pn_file + '\nVNRF ' + vnr_file +'\n\n')
	f.write(str(schedule))
	f.close()
	Profiler.end('writeSchedule')

# TODO comment
def decodeValue(val_type, value):
	if val_type == 'int':
		return int(value)
	elif val_type == 'float':
		return float(value)
	elif val_type == 'str':
		return value
	elif val_type == 'list(int)':
		return map(int, value.split(','))
	elif val_type == 'list(float)':
		return map(float, value.split(','))
	elif val_type == 'ConvertVirtualEdgeCollisionToPhysicalEdgeCollisionAlgorithm':
		return ConvertVirtualEdgeCollisionToPhysicalEdgeCollisionAlgorithm(value)
	else:
		print val_type
		raise Exception('Unknown value type')

# Reads in VirtualNetworkSchedule. Also returns assosciated physical network, virtual network requests (with virtual networks built), and parameters
def readSchedule(filename):
	Profiler.start('readSchedule')
	f = open(filename)

	schedule = None						# VirtualNetworkSchedule
	physical_network = None				# PhysicalNetwork
	virtual_network_requests = None		# List of VirtualNetworkRequest

	param_description = None
	param_vals = None

	version = None

	this_mapping = None

	for line in f:
		spl = line.split()
		if len(spl) == 0:
			continue
		key, vals = spl[0], spl[1:]
		if key == 'VERSION':
			version = (vals[0], ' '.join(vals[1:]))
		elif key == 'PARAMS':
			vals = map(lambda x: tuple(x.split('|')), vals)
			param_description = {n:(t, v) for n, t, v in vals}
			param_vals = {n:decodeValue(t, v) for n, t, v in  vals}
		elif key == 'PNF' and len(vals) == 1:
			pn_file = vals[0]
			physical_network = PN.readPhysicalNetwork(pn_file)
		elif key == 'VNRF' and len(vals) == 1:
			vnr_file = vals[0]
			virtual_network_requests = VNR.read_vn_requests(vnr_file)
			for vnr in virtual_network_requests:
				vnr.buildVirtualNetwork()
		elif key == 'event@' and len(vals) == 3:
			time, event_type, vnr_id = vals
			time = float(time[:-1])
			vnr_id = int(vnr_id)

			# Gets the vnr
			this_vnr = None
			for vnr in virtual_network_requests:
				if vnr.vn_request_id == vnr_id:
					this_vnr = vnr
					break
			if this_vnr == None:
				raise Exception('Invalid virtual network request ID')

			# Creates mapping for an accept event
			this_mapping = None
			if event_type == 'accept' or event_type == 'change':
				this_mapping = SingleMapping(physical_network, this_vnr.virtual_network)

			# Adds the event tuple to the schedule
			schedule.events.append((time, event_type, vnr_id, this_mapping))
		elif key == 'NM' and len(vals) == 2:
			if this_mapping == None:
				raise Exception('Invalid file format')

			virtual_node_id, physical_server_id = vals
			virtual_node_id = int(virtual_node_id)
			physical_server_id = tuple(map(int,physical_server_id[1:-1].split(',')))
			virtual_node = this_mapping.virtual_network.nodes[virtual_node_id]
			physical_server = this_mapping.physical_network.nodes[physical_server_id[0]].servers[physical_server_id[1]]

			this_mapping.virtual_node_to_physical_server_mapping[virtual_node] = physical_server
			this_mapping.physical_server_to_virtual_node_mapping[physical_server].append(virtual_node)
		elif key == 'EM':
			virtual_edge_id, path_str_and_weight = tuple(map(int, vals[0][1:-1].split(','))), [(vals[i], float(vals[i + 1])) for i in xrange(1,len(vals),2)]
			
			virtual_edge = this_mapping.virtual_network.nodes[virtual_edge_id[0]].edges[virtual_edge_id[1]]
			
			# Create the PhysicalPath objects
			path_and_weight = []
			for path_str, w in path_str_and_weight:
				node_vals, edge_vals = path_str.split('->'), path_str.split('|')
				path = None
				if len(node_vals) >= 2:
					node_ids = map(lambda x: int(x.split(',')[0][1:]), node_vals)
					edge_ids = map(lambda x: tuple(map(lambda y: tuple(map(int, y[1:-1].split(','))),x.split('->'))), edge_vals)

					# TODO instead of making multiple PhysicalPath objects for each virtual edge (between same physical nodes) reuse old objects when possible
					physical_nodes = [this_mapping.physical_network.nodes[nid] for nid in node_ids]
					physical_edges = [this_mapping.physical_network.nodes[e1[0]].edge_sets[e1[1]].getEdge(this_mapping.physical_network.nodes[e2[0]].edge_sets[e2[1]]) for e1, e2 in edge_ids]
					path = PN.PhysicalPath(physical_nodes, physical_edges)
				else:
					path = PN.PhysicalPath([this_mapping.virtual_node_to_physical_server_mapping[virtual_edge.node1].node], [])
				if path == None or str(path) != path_str:
					raise Exception('Unable to make path from file')
				path_and_weight.append((path, w))

			# Add the mapping of virtual edge to physical paths
			this_mapping.virtual_edge_to_physical_paths_mapping[virtual_edge] = path_and_weight

			# Add mapping of physical edges to virtual edge
			for p, w in path_and_weight:
				for e in p.edges:
					this_mapping.physical_edge_to_virtual_edge_mapping[e].append((virtual_edge, w))
		else:
			print 'Unmatched key:', key

		if physical_network != None and virtual_network_requests != None and schedule == None:
			schedule = VirtualNetworkSchedule(physical_network, virtual_network_requests)

	# TODO check that the schedule is 'correct'
	Profiler.end('readSchedule')
	return schedule, physical_network, virtual_network_requests, (param_vals, param_description, version)

# Container of all mappings on one PhysicalNetwork
class TotalMapping:
	def __init__(self, physical_network):
		self.physical_network = physical_network

		self.active_virtual_networks = []
		self.active_single_mappings = []
		self.virtual_network_to_single_mapping = {}

		self.inter_arrival_time_per_edge_set = {edge_set: float('inf') for edge_set in physical_network.getAllPhysicalEdgeSets()}

		# STATS
		# Key is length of shortest path of virtual edge, value is # of virtual edges
		self.shortest_path_accept = defaultdict(int)  # This is for virtual edges that were accepted
		self.shortest_path_reject = defaultdict(int)  # This is for virtual edges that were rejected

		self.reject_because_useless = 0

	def updateEdgeSetInterArrivalTime(self):
		Profiler.start('TotalMapping.updateEdgeSetInterArrivalTime')
		for edge_set in self.inter_arrival_time_per_edge_set:
			inverse_iat = sum(w / ve.large_flow_inter_arrival_time
									for single_mapping in self.active_single_mappings
										for pe in edge_set.edges
												if pe in single_mapping.physical_edge_to_virtual_edge_mapping
											for ve, w in single_mapping.physical_edge_to_virtual_edge_mapping[pe])
			if inverse_iat <= 0.00000000000001:
				self.inter_arrival_time_per_edge_set[edge_set] = float('inf')
			else:
				self.inter_arrival_time_per_edge_set[edge_set] = 1.0 / inverse_iat
		Profiler.end('TotalMapping.updateEdgeSetInterArrivalTime')

	# Returns the total CPU that is reserved on a given physical server
	def getMappedCPU(self, physical_server):
		Profiler.start('TotalMapping.getMappedCPU')
		mapped_cpu = sum(vn.cpu_requirement for single_mapping in self.active_single_mappings for vn in single_mapping.physical_server_to_virtual_node_mapping[physical_server])
		Profiler.end('TotalMapping.getMappedCPU')
		return mapped_cpu

	def getAggregateInterArrivalTime(self, physical_edge, additional_mappings = None, debug = False):
		Profiler.start('TotalMapping.getAggregateInterArrivalTime')

		additional_inter_arrival_time_1 = sum(w / ve.large_flow_inter_arrival_time
													for pe in physical_edge.edge_set1.edges
															if pe in additional_mappings.physical_edge_to_virtual_edge_mapping
														for ve, w in additional_mappings.physical_edge_to_virtual_edge_mapping[pe])

		additional_inter_arrival_time_2 = sum(w / ve.large_flow_inter_arrival_time
													for pe in physical_edge.edge_set2.edges
															if pe in additional_mappings.physical_edge_to_virtual_edge_mapping
														for ve, w in additional_mappings.physical_edge_to_virtual_edge_mapping[pe])

		if self.inter_arrival_time_per_edge_set[physical_edge.edge_set1] == float('inf') and additional_inter_arrival_time_1 <= .00000000001:
			iat1 = float('inf')
		else:
			iat1 = 1.0 / (1.0 / self.inter_arrival_time_per_edge_set[physical_edge.edge_set1] + additional_inter_arrival_time_1)

		if self.inter_arrival_time_per_edge_set[physical_edge.edge_set2] == float('inf') and additional_inter_arrival_time_2 <= .00000000001:
			iat2 = float('inf')
		else:
			iat2 = 1.0 / (1.0 / self.inter_arrival_time_per_edge_set[physical_edge.edge_set2] + additional_inter_arrival_time_2)

		Profiler.end('TotalMapping.getAggregateInterArrivalTime')
		return min([iat1, iat2])

	# TODO rethink name
	def isAggregateInterArrivalTimeBelowMax(self, physical_edge, additional_mappings = None, debug = False):
		Profiler.start('TotalMapping.isAggregateInterArrivalTimeBelowMax')
		if self.physical_network.max_edge_inter_arrival_time == None:
			raise Exception('No max_edge_inter_arrival_time set')
		
		aggregate_inter_arrival_time = self.getAggregateInterArrivalTime(physical_edge, additional_mappings = additional_mappings)
		Profiler.end('TotalMapping.isAggregateInterArrivalTimeBelowMax')
		# This occasionally suffers from rounding errors
		return aggregate_inter_arrival_time >= self.physical_network.max_edge_inter_arrival_time \
					or abs(aggregate_inter_arrival_time - self.physical_network.max_edge_inter_arrival_time) < 0.00001

	def remainingInterArrivalTime(self, edge_set, additional_mappings = None, debug = False):
		# TODO Improve
		Profiler.start('TotalMapping.remainingInterArrivalTime')
		mapped_iat = self.inter_arrival_time_per_edge_set[edge_set]
		if additional_mappings != None:
			additional_ar = sum(w / ve.large_flow_inter_arrival_time
									for pe in edge_set.edges
											if pe in additional_mappings.physical_edge_to_virtual_edge_mapping
										for ve, w in additional_mappings.physical_edge_to_virtual_edge_mapping[pe])
		else:
			additional_ar = 0.0

		if abs(1.0 / mapped_iat + additional_ar) <= .00000000000001:
			iat = float('inf')
		else:
			iat = 1.0 / (1.0 / mapped_iat + additional_ar)
		
		if abs(1.0 / self.physical_network.max_edge_inter_arrival_time - 1.0 / iat) <= 0.00000000000001:
			Profiler.end('TotalMapping.remainingInterArrivalTime')
			return float('inf')
		rv = 1.0 / (1.0 / self.physical_network.max_edge_inter_arrival_time - 1.0 / iat)
		Profiler.end('TotalMapping.remainingInterArrivalTime')
		return rv

	def addSingleMapping(self, single_mapping):
		if single_mapping.virtual_network in self.active_virtual_networks:
			raise Exception('Adding a mapping for a virtual network that already has a mapping')

		self.active_virtual_networks.append(single_mapping.virtual_network)
		self.active_single_mappings.append(single_mapping)
		self.virtual_network_to_single_mapping[single_mapping.virtual_network] = single_mapping

		self.updateEdgeSetInterArrivalTime()

	def removeVirtualNetwork(self, virtual_network):
		self.active_virtual_networks.remove(virtual_network)
		self.active_single_mappings.remove(self.virtual_network_to_single_mapping[virtual_network])
		del self.virtual_network_to_single_mapping[virtual_network]

		self.updateEdgeSetInterArrivalTime()

	def temporarilyRemoveVirtualNetwork(self, virtual_network):
		rv = self.virtual_network_to_single_mapping[virtual_network]

		self.active_virtual_networks.remove(virtual_network)
		self.active_single_mappings.remove(self.virtual_network_to_single_mapping[virtual_network])
		del self.virtual_network_to_single_mapping[virtual_network]

		self.updateEdgeSetInterArrivalTime()

		return rv

	def getAggregateBackboneInterArrivalTime(self, physical_edge, additional_mappings = None, debug = False):
		Profiler.start('TotalMapping.getAggregateBackboneInterArrivalTime')
		inter_arrival_times = [1.0 / ve.small_flow_inter_arrival_time
									for single_mapping in self.active_single_mappings + ([] if additional_mappings == None else [additional_mappings])
										if physical_edge in single_mapping.physical_backbone_edge_to_virtual_edge_mapping
											for ve in single_mapping.physical_backbone_edge_to_virtual_edge_mapping[physical_edge]]
		if len(inter_arrival_times) == 0:
			iat = float('inf')
		else:
			iat = 1.0 / sum(inter_arrival_times)
		Profiler.end('TotalMapping.getAggregateBackboneInterArrivalTime')
		return iat

	def isAggregateBackboneInterArrivalTimeBelowMax(self, physical_edge, additional_mappings = None):
		Profiler.start('TotalMapping.isAggregateBackboneInterArrivalTimeBelowMax')
		if self.physical_network.max_backbone_edge_inter_arrival_time == None:
			raise Exception('No max_backbone_edge_inter_arrival_time set')
		aggregate_backbone_inter_arrival_time = self.getAggregateBackboneInterArrivalTime(physical_edge, additional_mappings = additional_mappings)
		Profiler.end('TotalMapping.isAggregateBackboneInterArrivalTimeBelowMax')
		return aggregate_backbone_inter_arrival_time >= self.physical_network.max_backbone_edge_inter_arrival_time

	# Returns the remaining "available" arrival rate for this edge_set
	# Three options for this value
	#    1) Capacity(edge_set) - arrival rate mapped to THIS edge_set
	#    2) Average of remaining arrival rate over this edge_set's edges
	#    3) Maximum amount of arrival rate that can be added to the edges of this edge_set (in all but extreme cases will be identical to #1)
	# Currently using Option 1
	def getRemainingArrivalRate(self, edge_set):
		remaining_ar = 1.0 / self.remainingInterArrivalTime(edge_set)
		if remaining_ar < 0.0:
			remaining_ar = 0.0
		return remaining_ar

	def checkPhysicalServerInterArrivalConstraint(self, physical_server, new_virtual_node = None):
		Profiler.start('TotalMapping.checkPhysicalServerInterArrivalConstraint')
		if self.physical_network.max_server_to_node_edge_inter_arrival_time == None:
			Profiler.end('TotalMapping.checkPhysicalServerInterArrivalConstraint')
			return True

		sum_arrival_rates = sum(1.0 / vn.getTotalInterArrivalTime()
									for single_mapping in self.active_single_mappings
										for vn in single_mapping.physical_server_to_virtual_node_mapping[physical_server])

		if new_virtual_node != None:
			sum_arrival_rates += 1.0 / new_virtual_node.getTotalInterArrivalTime()

		if sum_arrival_rates <= 0.00000001:
			aggregate_inter_arrival_time = float('inf')
		else:
			aggregate_inter_arrival_time = 1.0 / sum_arrival_rates

		# print aggregate_inter_arrival_time, self.physical_network.max_server_to_node_edge_inter_arrival_time

		Profiler.end('TotalMapping.checkPhysicalServerInterArrivalConstraint')
		return aggregate_inter_arrival_time >= self.physical_network.max_server_to_node_edge_inter_arrival_time \
					or abs(aggregate_inter_arrival_time - self.physical_network.max_server_to_node_edge_inter_arrival_time) < 0.00001

	def getActiveEdgesByEdgeSets(self, additional_mappings = None):
		return {edge_set:physical_edge for single_mapping in self.active_single_mappings + ([additional_mappings] if additional_mappings != None else []) for physical_edge in single_mapping.physical_edge_to_virtual_edge_mapping for edge_set in (physical_edge.edge_set1, physical_edge.edge_set2)}

	def getNumberOfMappedVirtualEdges(self, physical_edge, additional_mappings = None):
		num_e1 = len([ve for single_mapping in self.active_single_mappings + ([additional_mappings] if additional_mappings != None else [])
							for pe in physical_edge.edge_set1.edges
									if pe in single_mapping.physical_edge_to_virtual_edge_mapping
								for ve, w in single_mapping.physical_edge_to_virtual_edge_mapping[pe]])

		num_e2 = len([ve for single_mapping in self.active_single_mappings + ([additional_mappings] if additional_mappings != None else [])
							for pe in physical_edge.edge_set2.edges
									if pe in single_mapping.physical_edge_to_virtual_edge_mapping
								for ve, w in single_mapping.physical_edge_to_virtual_edge_mapping[pe]])

		return max(num_e1, num_e2)

# Container for one in-progress mapping of a VN to a PN
class SingleMapping:
	def __init__(self, physical_network, virtual_network):
		self.physical_network = physical_network
		self.virtual_network = virtual_network

		# Keys are (VirtualNode)
		# Values are (PhysicalServer)
		self.virtual_node_to_physical_server_mapping = {}

		# Keys are (PhysicalServer)
		# Values are list of (VirtualNodes)
		self.physical_server_to_virtual_node_mapping = defaultdict(list)

		# Keys are (VirtualEdge)
		# Values are list of (PhysicalPath, weight)
		self.virtual_edge_to_physical_paths_mapping = defaultdict(list)

		# Keys are (Physical_edge)
		# Values are list of (VirtualEdge, weight) (Can be multiple elements with same VirtualEdge)
		self.physical_edge_to_virtual_edge_mapping = defaultdict(list)

		# Keys are (VirtualEdge)
		# Values are PhysicalPath (with backbone edges only)
		self.virtual_edge_to_physical_backbone_paths_mapping = {}

		# Keys are (Physical_backbone_edge)
		# Values are list of VirtualEdge
		self.physical_backbone_edge_to_virtual_edge_mapping = defaultdict(list)

		# Used in the RoutingScheme prioritize_mapped_paths
		self.mapped_paths = None
		self.unmapped_paths = None

	def isCompleteMapping(self):
		Profiler.start('SingleMapping.isCompleteMapping')
		rv = all(node in self.virtual_node_to_physical_server_mapping for node in self.virtual_network.getAllVirtualNodes()) and \
				all(edge in self.virtual_edge_to_physical_paths_mapping and edge in self.virtual_edge_to_physical_backbone_paths_mapping for edge in self.virtual_network.getAllVirtualEdges())
		Profiler.end('SingleMapping.isCompleteMapping')
		return rv

	def getVirtualNodeFront(self):
		Profiler.start('SingleMapping.getVirtualNodeFront')
		if len(self.virtual_node_to_physical_server_mapping) == 0:
			Profiler.end('SingleMapping.getVirtualNodeFront')
			return self.virtual_network.getAllVirtualNodes()
		rv =  set(other_node for node in self.virtual_node_to_physical_server_mapping for other_node in node.neighborNodes() if other_node not in self.virtual_node_to_physical_server_mapping)
		Profiler.end('SingleMapping.getVirtualNodeFront')
		return rv

	# Returns the list of virtual_edges of this virtual_node where the other end is mapped.
	def getVirtualEdgeFront(self, virtual_node):
		return (edge for edge in virtual_node.getAllVirtualEdges() if edge.getOtherNode(virtual_node) in self.virtual_node_to_physical_server_mapping)

	def computePrioritizedPaths(self):
		Profiler.start('SingleMapping.computePrioritizedPaths')
		self.mapped_paths = {}
		self.unmapped_paths = {}

		for virtual_edge in self.virtual_edge_to_physical_paths_mapping:
			self.mapped_paths[virtual_edge] = [p for p, w in self.virtual_edge_to_physical_paths_mapping[virtual_edge]]

			physical_node1 = self.virtual_node_to_physical_server_mapping[virtual_edge.node1].node
			physical_node2 = self.virtual_node_to_physical_server_mapping[virtual_edge.node2].node
			# Bottleneck for computation!!! Need to cut down the number of times this is called!
			# Precompute these sets for each virtual edge and store it in the mapping. (Need to recompute those if the mapping is changed)
			self.unmapped_paths[virtual_edge] = list(p for p in self.physical_network.findPathsBetween(physical_node1, physical_node2) \
										if not any(p.compare(x) for x in self.mapped_paths[virtual_edge]))
		Profiler.end('SingleMapping.computePrioritizedPaths')

	def computeCost(self, normalize_by_link_rate, average_flow_size):
		node_cost = sum(vn.cpu_requirement for vn in self.virtual_node_to_physical_server_mapping)
		link_cost = sum(sum(p.length() * w for p, w in self.virtual_edge_to_physical_paths_mapping[ve])
							* average_flow_size / ve.large_flow_inter_arrival_time / normalize_by_link_rate
						for ve in self.virtual_edge_to_physical_paths_mapping)
		return node_cost + link_cost

	def toString(self, sep = '\n'):
		# Virtual Node to physical Server
		# NM virtual_node_id (physical_node_id,physical_server_id)
		s = sep.join('NM ' + str(vn.id_num) + ' (' + str(self.virtual_node_to_physical_server_mapping[vn].node.id_num) + ',' + str(self.virtual_node_to_physical_server_mapping[vn].id_num) + ')'  for vn in self.virtual_node_to_physical_server_mapping)
		
		# Virtual edge to physical path (with weight)
		# EM (virtual_node1_id,virtual_node2_id) path_string1 weight1 path_string2 weight2...
		s += sep + sep.join('EM (' + str(ve.node1.id_num) + ',' + str(ve.node2.id_num) + ') ' + ' '.join(str(p) + ' ' + str(w) for p,w in self.virtual_edge_to_physical_paths_mapping[ve]) for ve in self.virtual_edge_to_physical_paths_mapping)
		return s

def mapVirtualNodes(node_mapping_scheme, physical_network, virtual_network, this_mapping, total_mapping):
	if node_mapping_scheme.algo_type == NodeMappingScheme.random_node_mapping:
		return random_node_mapping(physical_network, virtual_network, this_mapping, total_mapping)
	elif node_mapping_scheme.algo_type == NodeMappingScheme.node_rank:
		return mapNodesUsingNodeRank(physical_network, virtual_network, this_mapping, total_mapping, node_mapping_scheme)
	else:
		raise Exception('Not implemented yet')

# Randomly tries to map virtual nodes to physical servers
# Input:
#   physical_network - Network with the physical servers to map the virtual nodes to
#   virtual_network - Network with the virtual nodes to map to physical servers
#   this_mapping - The mapping of virtual_network to physical_network. Updates this with the computed node mapping
#   total_mapping - The mappings of previous virtual networks to physical_network. Does not change.
# Output:
#   If a sucessful mapping is found, returns true (also updates this_mapping with the mapping). Returns false otherwise.
def random_node_mapping(physical_network, virtual_network, this_mapping, total_mapping):
	Profiler.start('random_node_mapping')
	num_tries = 10
	mapped_all_nodes = False
	for this_try in range(num_tries):
		virtual_nodes = [virtual_network.nodes[i] for i in virtual_network.nodes]
		physical_servers = random.sample(list(physical_network.getAllServers()), len(virtual_nodes))
		
		if all(vn.cpu_requirement + total_mapping.getMappedCPU(ps) <= ps.cpu for vn, ps in zip(virtual_nodes, physical_servers)):
			mapped_all_nodes = True
			for vn, ps in zip(virtual_nodes, physical_servers):
				this_mapping.physical_server_to_virtual_node_mapping[ps].append(vn)
				this_mapping.virtual_node_to_physical_server_mapping[vn] = ps
			break
	Profiler.end('random_node_mapping')
	return mapped_all_nodes

def computeVirtualNodeRank(virtual_network, jump_probability = 0.15, forward_probability = 0.85, stopping_tolerance = .0001):
	Profiler.start('computeVirtualNodeRank')
	virtual_nodes = [virtual_network.nodes[vn_id] for vn_id in virtual_network.nodes]
	
	# H(virtual_nodes)
	local_resources = {u: u.cpu_requirement *
								sum(1.0 / u.edges[v].large_flow_inter_arrival_time
										for v in u.edges)
							for u in virtual_nodes}

	virtual_neighbors = {u: [v for v in u.neighborNodes()] for u in virtual_nodes}

	virtual_node_ranks = computeNodeRank(virtual_nodes, local_resources, virtual_neighbors, jump_probability, forward_probability, stopping_tolerance)
	Profiler.end('computeVirtualNodeRank')
	return virtual_node_ranks

def computePhysicalNodeRank(physical_network, total_mapping, jump_probability = 0.15, forward_probability = 0.85, stopping_tolerance = .0001, link_mapping_scheme = None):
	Profiler.start('computePhysicalNodeRank')
	node_rank_target = 'rack'
	if node_rank_target == 'server':
		# List of PhysicalServers that will be our 'nodes'
		physical_nodes = [physical_network.nodes[pn_id].servers[ps_id] for pn_id in physical_network.nodes for ps_id in physical_network.nodes[pn_id].servers]
		
		# Compute Local Resources for each node
		# TODO find the remaining AR for each edge exactly once, find the sum of remaining AR for each PhysicalNode exactly once
		local_resources = {ps: (ps.cpu - total_mapping.getMappedCPU(ps))
								* sum(total_mapping.getRemainingArrivalRate(ps.node.edge_sets[es_id]) for es_id in ps.node.edge_sets)
										for ps in physical_nodes}

		# Compute the neighbors of each server (This should include servers on the same PhysicalNode, and servers on PhysicalNodes that neighbor the server's PhysicalNode)
		physical_neighbors = {ps: ps.getNeighborServers() for ps in physical_nodes}

		physical_server_ranks = computeNodeRank(physical_nodes, local_resources, physical_neighbors, jump_probability, forward_probability, stopping_tolerance)
	elif node_rank_target == 'rack':
		# List of PhysicalNodes will be the 'nodes'
		physical_nodes = [physical_network.nodes[pn_id] for pn_id in physical_network.nodes]

		# Calculates the amount of local resources. This is sum(CPU remaining of servers) * sum(Arrival Rate remaining)
		cpu_remaining_per_node = {pn: sum(ps.cpu - total_mapping.getMappedCPU(ps) for _, ps in pn.servers.iteritems()) for pn in physical_nodes}

		local_resources = {pn: cpu_remaining_per_node[pn]
								* sum(total_mapping.getRemainingArrivalRate(edge_set) for _, edge_set in pn.edge_sets.iteritems())
									for pn in physical_nodes}

		if link_mapping_scheme == None or link_mapping_scheme.algo_type == LinkMappingScheme.dynamic_embedding:
			physical_neighbors = {pn: [edge.getOtherNode(pn) for _, edge_set in pn.edge_sets.iteritems() for edge in edge_set.edges] for pn in physical_nodes}
		elif link_mapping_scheme != None and link_mapping_scheme.algo_type == LinkMappingScheme.static_embedding:
			active_edges_by_edge_sets = total_mapping.getActiveEdgesByEdgeSets()
			physical_neighbors = {pn:[edge.getOtherNode(pn)
											for _, edge_set in pn.edge_sets.iteritems()
												for edge in
													((active_edges_by_edge_sets[edge_set],) if edge_set in active_edges_by_edge_sets
													else (edge for edge in edge_set.edges if edge.getOtherEdgeSet(edge_set) not in active_edges_by_edge_sets))]
										for pn in physical_nodes}

		physical_node_ranks = computeNodeRank(physical_nodes, local_resources, physical_neighbors, jump_probability, forward_probability, stopping_tolerance)

		# Convert the physical_node_ranks from PhysicalNode keyed to PhysicalServer keyed
		physical_server_ranks = {ps: (ps.cpu - total_mapping.getMappedCPU(ps)) / cpu_remaining_per_node[pn] * physical_node_ranks[pn]
										for pn in physical_nodes
											for _, ps in pn.servers.iteritems()}
	Profiler.end('computePhysicalNodeRank')
	return physical_server_ranks

# nodes - list of 'node' - The nodes to compute rank for
# local_resources - dict with key of 'node' and value of float - This defines the amount of local resources of each node
# neighbors - dict with key of 'node' and value of list of 'node' - contains the list of neighbors of each node
# jump_probability & forward_probability - float - Constants used in algorithm. Must sum to 1
# stopping_tolerances - float - algorithm stops when the sum of the change between two consecutive iterations is less than the given value
# Implementation based on https://pdfs.semanticscholar.org/5916/38ee010aceb0192c6cab37fb311900d83914.pdf
def computeNodeRank(nodes, local_resources, neighbors, jump_probability, forward_probability, stopping_tolerance):
	Profiler.start('computeNodeRank')
	if abs(jump_probability + forward_probability - 1.0) > .00000000001:
		raise Exception('The two probabilities must sum to exactly 1')

	sum_lr = sum(local_resources[node] for node in local_resources)

	# p^J_uv (since it is only based on v, we only compute for each v, and not each pair (u,v))
	jump_to_v = {v: (local_resources[v] / sum_lr if sum_lr > 0.0 else 0.0)
						for v in nodes}

	sum_lr_neigh = [sum(local_resources[v] for v in neighbors[u]) for u in nodes]

	forward_from_u_to_v = [[((local_resources[v] / sum_lr_neigh[i] if sum_lr_neigh[i] > 0.0 else 0.0) if v in neighbors[u] else 0.0) for v in nodes] for i, u in enumerate(nodes)]

	cur_node_rank = numpy.transpose(numpy.array([[(local_resources[u] / sum_lr if sum_lr > 0.0 else 0.0) for u in nodes]]))

	factors = numpy.transpose(numpy.array([[jump_to_v[v] * jump_probability + forward_from_u_to_v[i][j] * forward_probability
													for j, v in enumerate(nodes)]
														for i, u in enumerate(nodes)]))
	x = 0
	while True:
		Profiler.start('computeNodeRank_iter')
		x += 1
		# Copy cur_node_rank to prev_node_rank
		prev_node_rank = [nr for nr in cur_node_rank]

		# Update cur_node_rank
		new_node_rank = numpy.dot(factors, cur_node_rank)

		# check the change in cur_node_rank
		difference = sum(abs(cnr[0] - pnr[0]) for cnr, pnr in zip(cur_node_rank, new_node_rank))
		cur_node_rank = new_node_rank
		Profiler.end('computeNodeRank_iter')
		if difference < stopping_tolerance:
			break

	Profiler.end('computeNodeRank')
	return {u: cnr[0] for cnr, u in zip(cur_node_rank, nodes)}

def mapNodesUsingNodeRank(physical_network, virtual_network, this_mapping, total_mapping, node_mapping_scheme):
	Profiler.start('mapNodesUsingNodeRank')
	# Compute node rank for virtual network <--
	if node_mapping_scheme.virtual_node_ranks == None:
		virtual_node_ranks = computeVirtualNodeRank(virtual_network)
	else:
		virtual_node_ranks = node_mapping_scheme.virtual_node_ranks

	ranked_virtual_nodes = [virtual_network.nodes[vn_id] for vn_id in virtual_network.nodes]
	
	# Used to randomly break ties between virtual nodes with the same rank
	virtual_random_tiebreak = {v: random.random() for v in ranked_virtual_nodes}
	
	# Sorts virtual nodes by decreasing ranks
	ranked_virtual_nodes.sort(key = lambda x: (virtual_node_ranks[x], virtual_random_tiebreak[x]), reverse = True)

	# Compute node rank for physical network
	if node_mapping_scheme.physical_node_ranks == None:
		physical_node_ranks = computePhysicalNodeRank(physical_network, total_mapping)
	else:
		physical_node_ranks = node_mapping_scheme.physical_node_ranks

	ranked_physical_nodes = [physical_network.nodes[pn_id].servers[ps_id] for pn_id in physical_network.nodes for ps_id in physical_network.nodes[pn_id].servers]
	physical_random_tiebreak = {p: random.random() for p in ranked_physical_nodes}
	ranked_physical_nodes.sort(key = lambda x: (physical_node_ranks[x], physical_random_tiebreak[x]), reverse = True)

	# Pair up nodes, and check that if it works
	mapped_physical_servers = []
	partial_mapping = []
	all_nodes_mapped = True
	for virtual_node in ranked_virtual_nodes:
		virtual_node_mapped = False
		for physical_node in ranked_physical_nodes:
			if physical_node in mapped_physical_servers:
				continue
			if (virtual_node.cpu_requirement + total_mapping.getMappedCPU(physical_node) <= physical_node.cpu) and \
					total_mapping.checkPhysicalServerInterArrivalConstraint(physical_node, new_virtual_node = virtual_node):
				partial_mapping.append((virtual_node, physical_node))
				mapped_physical_servers.append(physical_node)
				virtual_node_mapped = True
				break
		if not virtual_node_mapped:
			all_nodes_mapped = False
			break

	if all_nodes_mapped:
		for vn, pn in partial_mapping:
			this_mapping.physical_server_to_virtual_node_mapping[pn].append(vn)
			this_mapping.virtual_node_to_physical_server_mapping[vn] = pn

	Profiler.end('mapNodesUsingNodeRank')
	return all_nodes_mapped

def inversePathLength(p, k = 1):
	return 1.0 / pow(float(p.length()), k)

def fixedPathLength(k, path_length, max_path_length):
	return float(pow(k, max_path_length - path_length))

# Based on this algorithm: https://docs.google.com/document/d/1szPqfmj1Gl3sXvwGu2ivBNOCXidbPZfWsVr2PE8ysP4/edit?usp=sharing
# Input:
#   physical_network - Network with the physical servers to map the virtual edges to
#   virtual_network - Network with the virtual edges to map to physical paths
#   this_mapping - The mapping of virtual_network to physical_network. Updates this with the computed edge mapping. Assumed that all virtual nodes are mapped
#   total_mapping - The mappings of previous virtual networks to physical_network. Does not change.
#   path_weighting_scheme - TODO
#   longest_path - The longest path to map a virtual edge to
# Output:
#   If a viable mapping is found, returns true (and updates this_mapping). Otherwise returns false.
def poissonEdgeMapping(physical_network, virtual_network, this_mapping, total_mapping, path_weighting_scheme, longest_path):
	Profiler.start('poissonEdgeMapping')
	# For each virtual edge find a set of paths to map it to, and determine a weight for each
	# Function used to calcuate the 'weight' of a path. Should take exactly one argument that is a PhysicalPath object
	all_virtual_edges = [ve for ve in virtual_network.getAllVirtualEdges()]
	random.shuffle(all_virtual_edges)
	for virtual_edge in all_virtual_edges:
		edge_mapped = poissonSingleEdgeMapping(virtual_edge, physical_network, this_mapping, total_mapping, path_weighting_scheme, longest_path)
		if not edge_mapped:
			Profiler.end('poissonEdgeMapping')
			return False
	# Check that this mapping is valid
	# Only checks physical edges that will have new mappings to it
	all_edges_good = True
	for physical_edge in physical_network.getAllPhysicalEdges():
		if not total_mapping.isAggregateInterArrivalTimeBelowMax(physical_edge, additional_mappings = this_mapping):
			all_edges_good = False
			break
	Profiler.end('poissonEdgeMapping')
	return all_edges_good

# Returns True when virtual_edge is mapped sucessfully, and updates this_mapping appropriately
# Returns False when virtual_edge is unable to be mapped, and makes sure this_mapping is not affected
def poissonSingleEdgeMapping(virtual_edge, physical_network, this_mapping, total_mapping, path_weighting_scheme, longest_path):
	Profiler.start('poissonSingleEdgeMapping')
	mapped_physical_node1 = this_mapping.virtual_node_to_physical_server_mapping[virtual_edge.node1].node
	mapped_physical_node2 = this_mapping.virtual_node_to_physical_server_mapping[virtual_edge.node2].node

	this_paths = physical_network.findPathsBetween(mapped_physical_node1, mapped_physical_node2, longest_path)

	if len(this_paths) <= 0:
		return False

	shortest_path_len = min(this_paths, key = lambda x: x.length()).length()

	if mapped_physical_node1 == mapped_physical_node2:
		if len(this_paths) != 1 or this_paths[0].length() != 0:
			raise Exception('Invalid assumptions about path between same node')

		zero_path = this_paths[0]
		this_mapping.virtual_edge_to_physical_paths_mapping[virtual_edge] = [(zero_path, 1.0)]
	else:
		if path_weighting_scheme.algo_type == PathWeightingScheme.shortest_path_first:
			# Uses only paths of a certain length. Tries with paths of length 1 first then goes up to longest_path.
			this_paths_by_length = {l:[] for l in set(p.length() for p in this_paths)}
			for p in this_paths:
				this_paths_by_length[p.length()].append(p)

			some_mapping_worked = False
			for l in sorted(this_paths_by_length):
				# Try and use only paths of length l
				weight = 1.0 / float(len(this_paths_by_length[l]))
				this_mapping.virtual_edge_to_physical_paths_mapping[virtual_edge] = [(p, weight) for p in this_paths_by_length[l]]
				changed_physical_edges = set()
				for p in this_paths_by_length[l]:
					for physical_edge in p.edges:
						changed_physical_edges.add(physical_edge)
						for pe in physical_edge.getRelatedEdges():
							changed_physical_edges.add(pe)
						this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge].append((virtual_edge, weight))

				# Check if this mapping works
				all_edges_good = True
				for physical_edge in changed_physical_edges:
					if not total_mapping.isAggregateInterArrivalTimeBelowMax(physical_edge, additional_mappings = this_mapping):
						all_edges_good = False
						break

				if all_edges_good:
					some_mapping_worked = True
					break
				else:
					# clear this mapping of what was just added
					del this_mapping.virtual_edge_to_physical_paths_mapping[virtual_edge]
					for p in this_paths_by_length[l]:
						for physical_edge in p.edges:
							this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge].remove((virtual_edge, weight))
							if len(this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge]) == 0:
								del this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge]

			if not some_mapping_worked:
				total_mapping.shortest_path_reject[shortest_path_len] += 1
				Profiler.end('poissonSingleEdgeMapping')
				return False
		elif path_weighting_scheme.algo_type == PathWeightingScheme.min_shortest_path_first:
			# Uses only paths of a certain length (starting at 1 then going up to longest_path).
			# Additionally for each path length tries to find a subset of paths that will work. Finds this subset by sorting paths by remaining resources, then finds range that a) has the smallest starting index and b) is the smallest range, that works.

			this_paths_by_length = {l:[] for l in set(p.length() for p in this_paths)}
			remaining_inter_arrival_time_per_edge_set = {es: total_mapping.remainingInterArrivalTime(es, additional_mappings = this_mapping) for p in this_paths for pe in p.edges for y in pe.getRelatedEdges() for es in (y.edge_set1, y.edge_set2)}
			remaining_inter_arrival_time_per_path = {p:max(max(remaining_inter_arrival_time_per_edge_set[es] for es in (pe.edge_set1, pe.edge_set2)) for pe in p.edges) for p in this_paths}
			for p in this_paths:
				this_paths_by_length[p.length()].append(p)
			for l in this_paths_by_length:
				this_paths_by_length[l].sort(key = lambda p: remaining_inter_arrival_time_per_path[p], reverse = True)

			some_mapping_worked = False
			for l in sorted(this_paths_by_length):
				start_index = None
				num_paths = None
				for i, p in enumerate(this_paths_by_length[l]):
					for k in xrange(1, len(this_paths_by_length[l]) - i):
						expected_weight = 1.0 / float(k)
						used_edge_sets = set(es for x in this_paths_by_length[l][i:(i + k)] for pe in x.edges for y in pe.getRelatedEdges() for es in (y.edge_set1, y.edge_set2))
						num_edge_set_occurences = {es:0 for es in used_edge_sets}
						for x in this_paths_by_length[l][i:(i + k)]:
							for pe in x.edges:
								for y in pe.getRelatedEdges():
									for es in (y.edge_set1, y.edge_set2):
										num_edge_set_occurences[es] += 1
						if all(remaining_inter_arrival_time_per_edge_set[es] <= virtual_edge.large_flow_inter_arrival_time / (expected_weight * num_edge_set_occurences[es]) for es in used_edge_sets):
							start_index = i
							num_paths = k
							break
					if start_index != None:
						break

				# Try and use only paths of length l
				if start_index != None:
					# print l, start_index, len(this_paths_by_length[l]), num_paths
					weight = 1.0 / float(k)
					this_mapping.virtual_edge_to_physical_paths_mapping[virtual_edge] = [(p, weight) for p in this_paths_by_length[l][i:(i + k)]]
					changed_physical_edges = set()
					for p in this_paths_by_length[l][i:(i + k)]:
						for physical_edge in p.edges:
							changed_physical_edges.add(physical_edge)
							for pe in physical_edge.getRelatedEdges():
								changed_physical_edges.add(pe)
							this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge].append((virtual_edge, weight))

					# Check if this mapping works
					all_edges_good = True
					for physical_edge in changed_physical_edges:
						if not total_mapping.isAggregateInterArrivalTimeBelowMax(physical_edge, additional_mappings = this_mapping):
							all_edges_good = False
							break

					if all_edges_good:
						some_mapping_worked = True
						break
					else:
						raise Exception('NO!!!')
						# clear this mapping of what was just added
						del this_mapping.virtual_edge_to_physical_paths_mapping[virtual_edge]
						for p in this_paths_by_length[l]:
							for physical_edge in p.edges:
								this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge].remove((virtual_edge, weight))
								if len(this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge]) == 0:
									del this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge]

			if not some_mapping_worked:
				total_mapping.shortest_path_reject[shortest_path_len] += 1
				Profiler.end('poissonSingleEdgeMapping')
				return False
		elif path_weighting_scheme.algo_type == PathWeightingScheme.inverse_path_length:
			# Assigns a weight of 1/(length(p)^path_weighting_scheme.num_val)
			# TODO Only calculate weight_function(p) once for each p
			sum_weight = sum(inversePathLength(p, k = path_weighting_scheme.num_val) for p in this_paths)
			this_mapping.virtual_edge_to_physical_paths_mapping[virtual_edge] = [(p, inversePathLength(p, k = path_weighting_scheme.num_val) / sum_weight) for p in this_paths]
			changed_physical_edges = set()
			for p in this_paths:
				for physical_edge in p.edges:
					changed_physical_edges.add(physical_Edge)
					this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge].append((virtual_edge, inversePathLength(p, k = path_weighting_scheme.num_val) / sum_weight))
		
			# Check if it worked
			all_edges_good = True
			for physical_edge in changed_physical_edges:
				if not total_mapping.isAggregateInterArrivalTimeBelowMax(physical_edge, additional_mappings = this_mapping):
					all_edges_good = False
					break

			if all_edges_good:
				some_mapping_worked = True
			else:
				# clear this mapping of what was just added
				for p, w in this_mapping.virtual_edge_to_physical_paths_mapping[virtual_edge]:
					for physical_edge in p.edges:
						this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge].remove((virtual_edge, w))
						if len(this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge]):
							del this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge]
				del this_mapping.virtual_edge_to_physical_paths_mapping[virtual_edge]
				total_mapping.shortest_path_reject[shortest_path_len] += 1
				Profiler.end('poissonSingleEdgeMapping')
				return False
		elif path_weighting_scheme.algo_type == PathWeightingScheme.fixed_path_length:
			# Divides the total weight equal between path length, then divides that value equally between paths with that length.
			this_paths_by_length = {l:[] for l in set(p.length() for p in this_paths)}
			for p in this_paths:
				this_paths_by_length[p.length()].append(p)

			weights_by_length = {k:fixedPathLength(path_weighting_scheme.num_val, k, longest_path) for k in this_paths_by_length}
			sum_weight = sum(weights_by_length[k] for k in weights_by_length)
			weights_by_length = {k:weights_by_length[k] / sum_weight / len(this_paths_by_length[k]) for k in weights_by_length}

			this_mapping.virtual_edge_to_physical_paths_mapping[virtual_edge] = [(p, weights_by_length[p.length()]) for p in this_paths]
			changed_physical_edges = set()
			for p in this_paths:
				for physical_edge in p.edges:
					changed_physical_edges.add(physical_edge)
					this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge].append((virtual_edge, weights_by_length[p.length()]))

			# Check if it worked
			all_edges_good = True
			for physical_edge in changed_physical_edges:
				if not total_mapping.isAggregateInterArrivalTimeBelowMax(physical_edge, additional_mappings = this_mapping):
					all_edges_good = False
					break

			if all_edges_good:
				some_mapping_worked = True
			else:
				# clear this mapping of what was just added
				for p, w in this_mapping.virtual_edge_to_physical_paths_mapping[virtual_edge]:
					for physical_edge in p.edges:
						this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge].remove((virtual_edge, w))
						if len(this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge]) == 0:
							del this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge]
				del this_mapping.virtual_edge_to_physical_paths_mapping[virtual_edge]
				total_mapping.shortest_path_reject[shortest_path_len] += 1
				Profiler.end('poissonSingleEdgeMapping')
				return False
		elif path_weighting_scheme.algo_type == PathWeightingScheme.spread_weighting_scheme:
			# Uses only paths of a certain length. Tries with paths of length 1 first then goes up to longest_path.
			this_paths_by_length = {l:[] for l in set(p.length() for p in this_paths)}
			for p in this_paths:
				this_paths_by_length[p.length()].append(p)

			some_mapping_worked = False
			for l in sorted(this_paths_by_length):
				# Try and find the "fairest" distribution of weights

				# Try and use only paths of length at most l
				# Filter out any paths that we know will have a weight of 0
				unmapped_paths_per_edge_set = defaultdict(list)
				full_paths_set = []
				remaining_iat_per_es = {}
				filtered_out_paths = 0
				for x in this_paths_by_length:
					if x <= l:
						for p in this_paths_by_length[x]:
							maxed_out_edge_set = False
							for e in p.edges:
								for es in (e.edge_set1, e.edge_set2):
									if es not in remaining_iat_per_es:
										remaining_iat_per_es[es] = total_mapping.remainingInterArrivalTime(es, additional_mappings = this_mapping)
									if remaining_iat_per_es[es] == float('inf'):
										maxed_out_edge_set = True
										break
								if maxed_out_edge_set:
									break
							if not maxed_out_edge_set:
								full_paths_set.append(p)

				max_rand_iter = 1
				if path_weighting_scheme.random_num_paths == None:
					random_num_paths = 50
				else:
					random_num_paths = path_weighting_scheme.random_num_paths

				for rand_iter in range(max_rand_iter):
					if len(full_paths_set) <= random_num_paths:
						paths_set = list(full_paths_set)
					else:
						paths_set = random.sample(full_paths_set, random_num_paths)

					for p in paths_set:
						for e in p.edges:
							unmapped_paths_per_edge_set[e.edge_set1].append(p)
							unmapped_paths_per_edge_set[e.edge_set2].append(p)

					# Find the weight to be given to unassigned weights.
					# See if there is any edge_sets that can't support that, and then assign the max possible to paths that use that edge_set.
					# If all paths get assigned in this way then fail.
					num_unmapped_paths = len(paths_set)
					unmapped_weight = 1.0

					# When paths are assigned weight, we have to include that iat into the remaining iat calculation
					extra_iat_per_edge_set = defaultdict(lambda: float('inf'))

					# Key is PhysicalPath and value is weight to assign to path. If the value is None, then it will be given an equal share of the remaining unmapped weight.
					weight_per_path = {p: None for p in paths_set}
					found_weight_mapping = False
					while num_unmapped_paths > 0:
						default_weight = unmapped_weight / float(num_unmapped_paths)

						bad_edge_sets = []
						# Find edge_sets that would be overwhelmed
						for es in unmapped_paths_per_edge_set:
							# If setting all paths would violate this edge_set, then must set paths going through this edge_set to something else
							remaining_iat =  remaining_iat_per_es[es]
							if es in extra_iat_per_edge_set and remaining_iat != float('inf'):
								remaining_arrival_rate = 1.0 / remaining_iat - 1.0 / extra_iat_per_edge_set[es]
								if abs(remaining_arrival_rate) <= 0.000000001:
									remaining_iat = float('inf')
								else:
									remaining_iat = 1.0 / remaining_arrival_rate

							this_num_unmapped_paths = len(unmapped_paths_per_edge_set[es])
							if virtual_edge.large_flow_inter_arrival_time / (this_num_unmapped_paths * default_weight) < remaining_iat:
								bad_edge_sets.append((es, virtual_edge.large_flow_inter_arrival_time / (remaining_iat * this_num_unmapped_paths)))

						# If no edge_sets overwhelmed then apply default weight to remaining paths
						if len(bad_edge_sets) == 0:
							for p in weight_per_path:
								if weight_per_path[p] == None:
									weight_per_path[p] = default_weight
							found_weight_mapping = True
							break

						# Find the edge_set with the least remaining_iat per path
						# TODO do this for all bad_edge_sets
						worst_edge_set, weight_to_assign = min(bad_edge_sets, key = lambda x: x[1])
						
						# Assign the highest possible weight to paths going through this edge_set
						assigned_paths = set()
						for worst_edge_set, weight_to_assign in sorted(bad_edge_sets, key = lambda x:x[1]):
							if any(p in assigned_paths for p in unmapped_paths_per_edge_set[worst_edge_set]):
								continue
							for p in list(unmapped_paths_per_edge_set[worst_edge_set]):
								assigned_paths.add(p)
								weight_per_path[p] = weight_to_assign

								# Update data structures
								num_unmapped_paths -= 1
								unmapped_weight -= weight_to_assign

								for e in p.edges:
									unmapped_paths_per_edge_set[e.edge_set1].remove(p)
									unmapped_paths_per_edge_set[e.edge_set2].remove(p)

									new_ar1 = 1.0 / extra_iat_per_edge_set[e.edge_set1] + weight_to_assign / virtual_edge.large_flow_inter_arrival_time
									extra_iat_per_edge_set[e.edge_set1] = (1.0 / new_ar1 if new_ar1 > 0.00000000001 else float('inf'))
									new_ar2 = 1.0 / extra_iat_per_edge_set[e.edge_set2] + weight_to_assign / virtual_edge.large_flow_inter_arrival_time
									extra_iat_per_edge_set[e.edge_set2] = (1.0 / new_ar2 if new_ar2 > 0.00000000001 else float('inf'))

									if len(unmapped_paths_per_edge_set[e.edge_set1]) == 0:
										del unmapped_paths_per_edge_set[e.edge_set1]
									if len(unmapped_paths_per_edge_set[e.edge_set2]) == 0:
										del unmapped_paths_per_edge_set[e.edge_set2]
						
						to_remove_es = set()
						for es in unmapped_paths_per_edge_set:
							if len(unmapped_paths_per_edge_set[es]) == 0:
								to_remove_es.add(es)

						for es in to_remove_es:
							del unmapped_paths_per_edge_set[es]
					
					if found_weight_mapping:
						break

				if found_weight_mapping:
					this_mapping.virtual_edge_to_physical_paths_mapping[virtual_edge] = [(p, weight_per_path[p]) for p in paths_set]
					changed_physical_edges = set()
					for p in paths_set:
						for physical_edge in p.edges:
							changed_physical_edges.add(physical_edge)
							for pe in physical_edge.getRelatedEdges():
								changed_physical_edges.add(pe)
							this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge].append((virtual_edge, weight_per_path[p]))
					some_mapping_worked = True
					break
			if not some_mapping_worked:
				total_mapping.shortest_path_reject[shortest_path_len] += 1
				Profiler.end('poissonSingleEdgeMapping')
				return False
	
	total_mapping.shortest_path_accept[shortest_path_len] += 1

	Profiler.end('poissonSingleEdgeMapping')
	return True

# Returns True when virtual_edge is mapped sucessfully, and updates this_mapping appropriately
# Returns False when virtual_edge is unable to be mapped, and makes sure this_mapping is not affected
def staticLinkMapping(virtual_edge, physical_network, this_mapping, total_mapping, longest_path):
	Profiler.start('staticLinkMapping')
	mapped_physical_node1 = this_mapping.virtual_node_to_physical_server_mapping[virtual_edge.node1].node
	mapped_physical_node2 = this_mapping.virtual_node_to_physical_server_mapping[virtual_edge.node2].node

	this_paths = physical_network.findPathsBetween(mapped_physical_node1, mapped_physical_node2, longest_path)
	path_tiebreak = {p:random.random() for p in this_paths}

	shortest_path_len = float('inf')

	path_to_map_to = None

	active_edges_by_edge_sets = total_mapping.getActiveEdgesByEdgeSets(additional_mappings = this_mapping)
	for path in sorted(this_paths, key = lambda x: (x.length(), path_tiebreak[x])):
		# Check that all edges in path can become active or are active
		if not all((edge_set not in active_edges_by_edge_sets) or (active_edges_by_edge_sets[edge_set] == physical_edge) for physical_edge in path.edges for edge_set in (physical_edge.edge_set1, physical_edge.edge_set2)):
			continue

		shortest_path_len = min(path.length(), shortest_path_len)

		# Try by adding path to this_mapping
		this_mapping.virtual_edge_to_physical_paths_mapping[virtual_edge] = [(path, 1.0)]
		for physical_edge in path.edges:
			this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge].append((virtual_edge, 1.0))

		# Check that using this path with weight one is okay
		all_edges_good = True
		for physical_edge in path.edges:
			if not total_mapping.isAggregateInterArrivalTimeBelowMax(physical_edge, additional_mappings = this_mapping):
					all_edges_good = False
					break

		# Check that no edge_sets are "useless"
		if all_edges_good:
			temp_active_edges_by_edge_sets = dict(active_edges_by_edge_sets)
			for physical_edge in path.edges:
				temp_active_edges_by_edge_sets[physical_edge.edge_set1] = physical_edge
				temp_active_edges_by_edge_sets[physical_edge.edge_set2] = physical_edge

			all_edge_sets_useful = True
			for edge_set in physical_network.getAllPhysicalEdgeSets():
				if edge_set in temp_active_edges_by_edge_sets:
					continue

				if all(pe.getOtherEdgeSet(edge_set) in temp_active_edges_by_edge_sets for pe in edge_set.edges):
					all_edge_sets_useful = False
					break

			if not all_edge_sets_useful:
				all_edges_good = False
				total_mapping.reject_because_useless += 1

		# If all constraints passed, then we found our mapping
		if all_edges_good:
			path_to_map_to = path
			break

		# Else we remove the mapping
		for p, w in this_mapping.virtual_edge_to_physical_paths_mapping[virtual_edge]:
			for physical_edge in p.edges:
				this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge].remove((virtual_edge, 1.0))
				if len(this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge]) == 0:
					del this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge]
		del this_mapping.virtual_edge_to_physical_paths_mapping[virtual_edge]

	if shortest_path_len != float('inf'):
		if path_to_map_to == None:
			total_mapping.shortest_path_reject[shortest_path_len] += 1
		else:
			total_mapping.shortest_path_accept[shortest_path_len] += 1

	Profiler.end('staticLinkMapping')
	return path_to_map_to != None

def backboneEdgeMapping(physical_network, virtual_network, this_mapping, total_mapping):
	if not physical_network.has_backbone:
		return True
	
	# For each virtual_edge in virtual_network choose a backbone path betwen the mapped physical_nodes (either at random, or the one with the most remaining capacity)
	for virtual_edge in virtual_network.getAllVirtualEdges():
		this_edge_mapped = bacboneSingleEdgeMapping(virtual_edge, physical_network, this_mapping, total_mapping)
		if not this_edge_mapped:
			return False

	all_edges_good = True
	for physical_edge in physical_network.getAllPhysicalBackboneEdges():
		if not total_mapping.isAggregateBackboneInterArrivalTimeBelowMax(physical_edge, additional_mappings = this_mapping):
			all_edges_good = False
			break
	# Check that using these paths doesn't violate anything
	return all_edges_good

def bacboneSingleEdgeMapping(virtual_edge, physical_network, this_mapping, total_mapping):
	mapped_physical_node1 = this_mapping.virtual_node_to_physical_server_mapping[virtual_edge.node1].node
	mapped_physical_node2 = this_mapping.virtual_node_to_physical_server_mapping[virtual_edge.node2].node

	backbone_paths = physical_network.findBackbonePathsBetween(mapped_physical_node1, mapped_physical_node2)

	# Choose path at random. TODO choose path with the most remaining capacity
	this_path = random.choice(backbone_paths)

	this_mapping.virtual_edge_to_physical_backbone_paths_mapping[virtual_edge] = this_path
	changed_physical_edges = set()
	for physical_edge in this_path.edges:
		changed_physical_edges.add(physical_edge)
		this_mapping.physical_backbone_edge_to_virtual_edge_mapping[physical_edge].append(virtual_edge)

	# Check that the mapping is valid
	all_edges_good = True
	for physical_edge in changed_physical_edges:
		if not total_mapping.isAggregateBackboneInterArrivalTimeBelowMax(physical_edge, additional_mappings = this_mapping):
			all_edges_good = False
			break

	if all_edges_good:
		return True
	else:
		# If the mapping failed, reset this_mapping
		for physical_edge in this_mapping.virtual_edge_to_physical_backbone_paths_mapping[virtual_edge].edges:
			this_mapping.physical_backbone_edge_to_virtual_edge_mapping[physical_edge].remove(virtual_edge)
			if len(this_mapping.physical_backbone_edge_to_virtual_edge_mapping[physical_edge]) == 0:
				del this_mapping.physical_backbone_edge_to_virtual_edge_mapping[physical_edge]
		del this_mapping.virtual_edge_to_physical_backbone_paths_mapping[virtual_edge]

		return False

# Maps the given virtual_network to the given physical_network.
def mapVnUsingProbabilisticPoissonModel(physical_network, virtual_network, total_mapping, node_mapping_scheme, path_weighting_scheme, longest_path, num_tries = 10):
	Profiler.start('mapVnUsingProbabilisticPoissonModel')
	mapping_sucessful = False

	node_mapping_scheme.precomputeVirtualNetworkRanks(virtual_network)

	for i in range(num_tries):
		# For the multiple tries, the rejection could come from both, but the returned should essentially be random
		reject_from_node = False
		reject_from_dynamic = False
		reject_from_backbone = False
		this_mapping = SingleMapping(physical_network, virtual_network)

		# Map Virtual Nodes
		if not mapVirtualNodes(node_mapping_scheme, physical_network, virtual_network, this_mapping, total_mapping):
			reject_from_node = True
			continue
		
		# Map Virtual Edges
		dynamic_mapping_worked = poissonEdgeMapping(physical_network, virtual_network, this_mapping, total_mapping, path_weighting_scheme, longest_path)
		if not dynamic_mapping_worked:
			reject_from_dynamic = True
			continue

		backbone_mapping_worked = backboneEdgeMapping(physical_network, virtual_network, this_mapping, total_mapping)
		if not backbone_mapping_worked:
			reject_from_backbone = True
			continue

		mapping_sucessful = True
		total_mapping.addSingleMapping(this_mapping)
		break
	Profiler.end('mapVnUsingProbabilisticPoissonModel')
	return mapping_sucessful, reject_from_node, reject_from_dynamic, reject_from_backbone , (this_mapping if mapping_sucessful else None)

class Counter:
	def __init__(self, max_value):
		self.x = 0
		self.max_value = max_value

	def incr(self):
		self.x += 1
		return self.x <= self.max_value

def checkVirtualEdgeMappingCorrepsondsToPhysicalEdgeMapping(virtual_edge, this_mapping, verbose = False):
	if verbose:
		print '-------------------------------------'
	values = defaultdict(lambda: defaultdict(int))
	for path, weight in this_mapping.virtual_edge_to_physical_paths_mapping[virtual_edge]:
		if verbose:
			print path.length(), weight
		for pe in path.edges:
			if pe not in this_mapping.physical_edge_to_virtual_edge_mapping:
				if verbose:
					print 'A'
				return False
			values[pe][weight] += 1

	mapped_elements = []
	for pe in values:
		for weight in values[pe]:
			if this_mapping.physical_edge_to_virtual_edge_mapping[pe].count((virtual_edge, weight)) != values[pe][weight]:
				if verbose:
					print this_mapping.physical_edge_to_virtual_edge_mapping[pe].count((virtual_edge, weight)), values[pe][weight]
					print 'B'
				return False
	return True

def backtrackingBurstyVnMapping(physical_network, virtual_network, total_mapping, link_mapping_scheme, node_mapping_scheme, path_weighting_scheme, longest_path, max_iterations = 10):
	Profiler.start('backtrackingBurstyVnMapping')
	# Computes the ranks of the virtual nodes
	if node_mapping_scheme.algo_type not in (NodeMappingScheme.node_rank, NodeMappingScheme.reverse_node_rank):
		raise Exception('Must use node ranking scheme with backtracking')	
	node_mapping_scheme.precomputeVirtualNetworkRanks(virtual_network)

	this_mapping = SingleMapping(physical_network, virtual_network)

	# Sorts the physical_servers vy rank
	if node_mapping_scheme.physical_node_ranks == None:
		physical_server_ranks = computePhysicalNodeRank(physical_network, total_mapping)
	else:
		physical_server_ranks = node_mapping_scheme.physical_node_ranks
	ranked_physical_servers = [physical_network.nodes[pn_id].servers[ps_id] for pn_id in physical_network.nodes for ps_id in physical_network.nodes[pn_id].servers]
	physical_random_tiebreak = {p: random.random() for p in ranked_physical_servers}
	
	reverse_sort = None
	if node_mapping_scheme.algo_type == NodeMappingScheme.node_rank:
		reverse_sort = True
	if node_mapping_scheme.algo_type == NodeMappingScheme.reverse_node_rank:
		reverse_sort = False

	ranked_physical_servers.sort(key = lambda x: (physical_server_ranks[x], physical_random_tiebreak[x]), reverse = reverse_sort)

	rv = recursiveBackTracking(this_mapping, ranked_physical_servers, [], total_mapping, link_mapping_scheme, node_mapping_scheme, path_weighting_scheme, longest_path, Counter(max_iterations + len(virtual_network.nodes)))
	if rv == True:
		total_mapping.addSingleMapping(this_mapping)
	Profiler.end('backtrackingBurstyVnMapping')
	return rv == True, None, None, None, this_mapping

# Returns True if a Sucessuful Mapping was found, and False if not
def recursiveBackTracking(this_mapping, ranked_physical_servers, mapped_physical_servers, total_mapping, link_mapping_scheme, node_mapping_scheme, path_weighting_scheme, longest_path, counter):
	# If all nodes mapped mapped then this worked
	if this_mapping.isCompleteMapping():
		return True
	# Compute the "Front," virtual nodes that are not mapped and have a link to a virtual node that is mapped. (If no nodes are mapped, then all nodes)
	# TODO iteratively update it instead of recomputing it everytime
	virtual_node_front = list(this_mapping.getVirtualNodeFront())

	# Sorts the nodes in virtual_node front by rank (TODO only do the sort once)
	if node_mapping_scheme.virtual_node_ranks == None:
		virtual_node_ranks = computeVirtualNodeRank(virtual_network)
	else:
		virtual_node_ranks = node_mapping_scheme.virtual_node_ranks

	virtual_random_tiebreak = {v: random.random() for v in virtual_node_front}

	reverse_sort = None
	if node_mapping_scheme.algo_type == NodeMappingScheme.node_rank:
		reverse_sort = True
	if node_mapping_scheme.algo_type == NodeMappingScheme.reverse_node_rank:
		reverse_sort = False

	virtual_node_front.sort(key = lambda x: (virtual_node_ranks[x], virtual_random_tiebreak[x]), reverse = reverse_sort)
	
	# Loop over VNs in non-increasing order of rank:
	for virtual_node in virtual_node_front:
		# Loop over PNs in non-increasing order of rank, when one could potentiallly work then:
		for physical_node in ranked_physical_servers:
			# TODO Filter out possible node mappings by ???

			# Check if virtual_node can possibly be mapped to physical_node
			if physical_node not in mapped_physical_servers and \
						virtual_node.cpu_requirement + total_mapping.getMappedCPU(physical_node) <= physical_node.cpu and \
						total_mapping.checkPhysicalServerInterArrivalConstraint(physical_node, new_virtual_node = virtual_node):
					# Increase Counter! If counter is beyond max_value return False
					if not counter.incr():
						# This should cause a return of False all the way to the orginal call
						return None

					# Add the (virtual_node -> physical_node) mapping to this_mapping
					this_mapping.virtual_node_to_physical_server_mapping[virtual_node] = physical_node
					this_mapping.physical_server_to_virtual_node_mapping[physical_node].append(virtual_node)

					# Try and Map all VEs that go between this vn and already mapped vns
					virtual_edges_to_map = this_mapping.getVirtualEdgeFront(virtual_node)

					all_virtual_edges_mapped = True
					mapped_virtual_edges = [] # list of 2-tuple (virtual_edge, 'large'|'small')
					for virtual_edge in virtual_edges_to_map:
						# Large Flow mapping
						if link_mapping_scheme.algo_type == LinkMappingScheme.dynamic_embedding:
							large_flow_mapped = poissonSingleEdgeMapping(virtual_edge, this_mapping.physical_network, this_mapping, total_mapping, path_weighting_scheme, longest_path)
						elif link_mapping_scheme.algo_type == LinkMappingScheme.static_embedding:
							large_flow_mapped = staticLinkMapping(virtual_edge, this_mapping.physical_network, this_mapping, total_mapping, longest_path)

						if not large_flow_mapped:
							all_virtual_edges_mapped = False
						else:
							mapped_virtual_edges.append((virtual_edge, 'large'))

						# Small Flow mapping
						small_flow_mapped = bacboneSingleEdgeMapping(virtual_edge, this_mapping.physical_network, this_mapping, total_mapping)
						if not small_flow_mapped:
							all_virtual_edges_mapped = False
						else:
							mapped_virtual_edges.append((virtual_edge, 'small'))

					# If this suceeds, recursively call function to map rest of function
					if all_virtual_edges_mapped:
						rv = recursiveBackTracking(this_mapping, ranked_physical_servers, mapped_physical_servers + [physical_node], total_mapping, link_mapping_scheme, node_mapping_scheme, path_weighting_scheme, longest_path, counter)
						if rv == None:
							# This means the counter went over, so this should Fail all the way up the recursive tree
							return None
						if rv == False:
							# This means all possible mappings of the sub-call were exhausted, so we continue to the next possible node mapping
							pass
						if rv == True:
							# This means all nodes and edges were mapped, so we return True all the way up the recursive tree
							return True

					# If not edge mapping or the sub-call fails, then remove all mappings and go onto next PN
					for virtual_edge, map_type in mapped_virtual_edges:
						if map_type == 'large':
							for p, w in this_mapping.virtual_edge_to_physical_paths_mapping[virtual_edge]:
								for physical_edge in p.edges:
									this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge].remove((virtual_edge, w))
									if len(this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge]) == 0:
										del this_mapping.physical_edge_to_virtual_edge_mapping[physical_edge]
							del this_mapping.virtual_edge_to_physical_paths_mapping[virtual_edge]
						elif map_type == 'small':
							for physical_edge in this_mapping.virtual_edge_to_physical_backbone_paths_mapping[virtual_edge].edges:
								this_mapping.physical_backbone_edge_to_virtual_edge_mapping[physical_edge].remove(virtual_edge)
								if len(this_mapping.physical_backbone_edge_to_virtual_edge_mapping[physical_edge]) == 0:
									del this_mapping.physical_backbone_edge_to_virtual_edge_mapping[physical_edge]
							del this_mapping.virtual_edge_to_physical_backbone_paths_mapping[virtual_edge]
						else:
							raise Exception('Invalid map_type')
					
					# Remove the (virtual_node -> physical_node) mapping from this_mapping
					del this_mapping.virtual_node_to_physical_server_mapping[virtual_node]
					this_mapping.physical_server_to_virtual_node_mapping[physical_node].remove(virtual_node)
					if len(this_mapping.physical_server_to_virtual_node_mapping[physical_node]) == 0:
						del this_mapping.physical_server_to_virtual_node_mapping[physical_node]
	# If we have exhausted all possible mappings, we failed
	return False

def currentDistanceBetween(mapped_physical_servers, physical_network, this_mapping, total_mapping, link_mapping_scheme):
	Profiler.start('currentDistanceBetween')
	# If there are no mapped_physical_servers already, then just say all racks are the same distance away
	if len(mapped_physical_servers) == 0:
		Profiler.end('currentDistanceBetween')
		return {rack: 1 for _, rack in physical_network.nodes.iteritems()}

	if link_mapping_scheme.algo_type == LinkMappingScheme.static_embedding:
		active_edges_by_edge_sets = total_mapping.getActiveEdgesByEdgeSets(additional_mappings = this_mapping)
	else:
		# TODO in the dynamic case, can save the results, because we never alter the network
		active_edges_by_edge_sets = dict()

	rack_distances = {rack: None for _, rack in physical_network.nodes.iteritems()}


	rack_front = set(x.node for x in mapped_physical_servers)
	for rack in rack_front:
		rack_distances[rack] = 0

	next_distance = 1
	while True:
		new_rack_front = set()

		for rack in rack_front:
			for _, es in rack.edge_sets.iteritems():
				for edge in ((active_edges_by_edge_sets[es],) if es in active_edges_by_edge_sets else es.edges):
					neighbor = edge.getOtherNode(rack) 
					if rack_distances[neighbor] == None:
						new_rack_front.add(neighbor)

		for rack in new_rack_front:
			rack_distances[rack] = next_distance

		next_distance += 1
		rack_front = new_rack_front
		if len(new_rack_front) == 0:
			break

	if any(dist == None for rack, dist in rack_distances.iteritems()):
		print rack_distances, mapped_physical_servers
		raise Exception('Didn\'t find distance to some rack')

	Profiler.end('currentDistanceBetween')
	return rack_distances

def createVnEmbeddingSchedule(physical_network, vn_requests, max_physical_edge_collision, embedding_scheme, link_mapping_scheme, node_mapping_scheme, path_weighting_scheme, preemption_scheme, ilp_scheme, compute_max_inter_arrival_time_scheme, longest_path, real_world_data, return_data = False, status_file = None):
	Profiler.start('createVnEmbeddingSchedule')
	status_file.write('start createVnEmbeddingSchedule')

	# Builds the VirtualNetwork data structure for each VNR
	for vnr in vn_requests:
		vnr.buildVirtualNetwork()

	schedule = VirtualNetworkSchedule(physical_network, vn_requests)
	total_mapping = TotalMapping(physical_network)

	if ilp_scheme.willRunILP(num_racks = physical_network.getNumberOfNodes()):
		ilp_schedule = VirtualNetworkSchedule(physical_network, vn_requests)

	# Max Arrival Rate for Large Flow Dynamic Network
	max_edge_inter_arrival_time = \
			computeMaxEdgeInterArrivalTime(max_physical_edge_collision, \
											real_world_data.getLongFlowDistribution(physical_network.getLinkRateInBytesPerSecond()), \
											real_world_data.getAverageLongFlowDuration(physical_network.getLinkRateInBytesPerSecond()), \
											compute_max_inter_arrival_time_scheme)

	physical_network.setMaxEdgeInterArrivalTime(max_edge_inter_arrival_time)

	print 'Allowing a maximum of', real_world_data.large_fs_avg / max_edge_inter_arrival_time / 1000.0 / 1000.0 / 1000.0 * 8.0, 'Gb per second on dynamic edges'

	# Max Arrival Rate for Small Flow Backbone Network
	max_backbone_edge_inter_arrival_time = computeMaxEdgeInterArrivalTime(max_physical_edge_collision, \
																			real_world_data.getSmallFlowDistribution(physical_network.getLinkRateInBytesPerSecond()), \
																			real_world_data.getAverageSmallFlowDuration(physical_network.getLinkRateInBytesPerSecond()), \
																			compute_max_inter_arrival_time_scheme)
	physical_network.setMaxBackboneEdgeInterArrivalTime(max_backbone_edge_inter_arrival_time)

	print 'Allowing a maximum of', real_world_data.small_fs_avg / max_backbone_edge_inter_arrival_time / 1000.0 / 1000.0 / 1000.0 * 8.0, 'Gb per second on backbone edges'

	# Max Arrival Rate for Connection between Server and ToR Switch
	server_to_node_bandwidth = physical_network.getServerToRackLinkRateInBytesPerSecond()
	if server_to_node_bandwidth != None:
		max_server_to_node_edge_inter_arrival_time = computeMaxEdgeInterArrivalTime(max_physical_edge_collision, \
																						real_world_data.getAllFlowDistribution(server_to_node_bandwidth), \
																						real_world_data.getAverageFlowDuration(server_to_node_bandwidth), \
																						compute_max_inter_arrival_time_scheme)
		physical_network.setMaxServerToNodeEdgeInterArrivalTime(max_server_to_node_edge_inter_arrival_time)

		print 'Allowing a maximum of', real_world_data.all_fs_avg / max_server_to_node_edge_inter_arrival_time / 1000.0 / 1000.0 / 1000.0 * 8.0, 'Gb per second on server to ToR edges'
	else:
		physical_network.setMaxServerToNodeEdgeInterArrivalTime(None)

	node_mapping_scheme.precomputePhysicalNetworkRanks(physical_network, total_mapping, link_mapping_scheme)

	total_usage_per_node = {physical_node: 0.0 for pn_id, physical_node in physical_network.nodes.iteritems()}
	
	embedding_times = []

	# List of 'events' (Arrival or departure of VN requests) sorted by time (one event starts when the other ends, the one that ends is first (this is handled by the fact that in python 'end' < 'start'))
	events = sorted([(vnr.arrival_time + (0 if t == 'start' else vnr.duration), t, vnr) for vnr in vn_requests for t in ('start', 'end')])
	current_vnrs = set()
	ilp_current_vnrs = set()
	num_event = 0
	for time, type_event, vnr in events:
		status_file.write('event %d of %d at time %d with type %s' % (num_event, len(events), time, type_event))
		num_event += 1
		if type_event == 'end':
			if vnr in current_vnrs:
				# If this VNR was accepted, then remove it from total_mapping
				current_vnrs.remove(vnr)
				schedule.addEndingVnRequest(time, vnr)
				total_mapping.removeVirtualNetwork(vnr.virtual_network)

			if vnr in ilp_current_vnrs:
				ilp_current_vnrs.remove(vnr)
				ilp_schedule.addEndingVnRequest(time, vnr)
		else:
			start_time = time_module.time()
			# Call function to build mapping
			if embedding_scheme.algo_type == EmbeddingScheme.two_stage:
				if link_mapping_scheme.algo_type == LinkMappingScheme.static_embedding:
					raise Exception('Link_mapping_scheme is not implmented for 2-stage algo')
				was_vnr_accepted, reject_from_node, reject_from_dynamic, reject_from_backbone, this_mapping = \
						mapVnUsingProbabilisticPoissonModel(physical_network, vnr.virtual_network, total_mapping, node_mapping_scheme, path_weighting_scheme, longest_path, num_tries = 1)
			elif embedding_scheme.algo_type == EmbeddingScheme.backtracking:
				was_vnr_accepted, reject_from_node, reject_from_dynamic, reject_from_backbone, this_mapping = \
						backtrackingBurstyVnMapping(physical_network, vnr.virtual_network, total_mapping, link_mapping_scheme, node_mapping_scheme, path_weighting_scheme, longest_path)
			if not was_vnr_accepted:
				# Preemption
				if preemption_scheme.algo_type == PreemptionScheme.no_preemption:
					# This algo doesn't have any preemption, so we do nothing
					pass
				if preemption_scheme.algo_type == PreemptionScheme.only_full_vn_reembedding:
					# Uses only 'Phase 3' from https://docs.google.com/document/d/1rzwYa-xL3M72uQP-TrixpOUA1wKdMNAzihzEsEXJ0Zs/edit
					# Essentially assigns each previous VN a score of how congested they are, then in order from most to least congested VN, removes them, retries this vn, then redoes the preempted VN.

					# Order the Currently embedded VNs
					# TODO Change this from a random shuffle to the rnaking based on Butt et. al. 2014
					Profiler.start('createVnEmbeddingSchedule_preemptive-phase3')
					old_virtual_networks = list(total_mapping.active_virtual_networks)
					random.shuffle(old_virtual_networks)					

					for retry_virtual_network in old_virtual_networks:
						# Remove retry_virtual_network
						original_single_mapping = total_mapping.temporarilyRemoveVirtualNetwork(retry_virtual_network)
						node_mapping_scheme.precomputePhysicalNetworkRanks(physical_network, total_mapping, link_mapping_scheme)

						# Map vnr
						if embedding_scheme.algo_type == EmbeddingScheme.two_stage:
							was_vnr_accepted, reject_from_node, reject_from_dynamic, reject_from_backbone, this_mapping = \
									mapVnUsingProbabilisticPoissonModel(physical_network, vnr.virtual_network, total_mapping, node_mapping_scheme, path_weighting_scheme, longest_path, num_tries = 1)
						elif embedding_scheme.algo_type == EmbeddingScheme.backtracking:
							was_vnr_accepted, reject_from_node, reject_from_dynamic, reject_from_backbone, this_mapping = \
									backtrackingBurstyVnMapping(physical_network, vnr.virtual_network, total_mapping, node_mapping_scheme, path_weighting_scheme, longest_path)
						
						if was_vnr_accepted:
							node_mapping_scheme.precomputePhysicalNetworkRanks(physical_network, total_mapping, link_mapping_scheme)
							# If vnr mapped right, then map retry_virtual_network
							if embedding_scheme.algo_type == EmbeddingScheme.two_stage:
								was_retry_accepted, t1, t2, t3, new_retry_mapping = \
										mapVnUsingProbabilisticPoissonModel(physical_network, retry_virtual_network, total_mapping, node_mapping_scheme, path_weighting_scheme, longest_path, num_tries = 1)
							elif embedding_scheme.algo_type == EmbeddingScheme.backtracking:
								was_retry_accepted, t1, t2, t3, new_retry_mapping = \
										backtrackingBurstyVnMapping(physical_network, retry_virtual_network, total_mapping, node_mapping_scheme, path_weighting_scheme, longest_path)
							
							if was_retry_accepted:
								# If retry_virtual_network mapped right, then break and stick with that mapping
								# Add the changed mapping to the schedule
								schedule.addChangedVnRequest(time, retry_virtual_network.virtual_network_request, new_retry_mapping)
								break

						# If either of the previous two steps failed, then reset to beginning of loop and go to next virtual network
						if was_vnr_accepted:
							total_mapping.removeVirtualNetwork(vnr.virtual_network)
						total_mapping.addSingleMapping(original_single_mapping)
						was_vnr_accepted = False
					
					if not was_vnr_accepted:
						node_mapping_scheme.precomputePhysicalNetworkRanks(physical_network, total_mapping, link_mapping_scheme)
					Profiler.end('createVnEmbeddingSchedule_preemptive-phase3')

			if was_vnr_accepted:
				# if this_mapping is complete
				schedule.addAcceptedVnRequest(time, vnr, this_mapping)
				current_vnrs.add(vnr)
				node_mapping_scheme.precomputePhysicalNetworkRanks(physical_network, total_mapping, link_mapping_scheme)

				# TEMP STAT
				for virtual_node, physical_server in this_mapping.virtual_node_to_physical_server_mapping.iteritems():
					total_usage_per_node[physical_server.node] += virtual_node.cpu_requirement * vnr.duration
			else:
				# if this_mapping isn't complete
				schedule.addRejectedVnRequest(time, vnr)
				if reject_from_node:
					schedule.num_node_reject += 1
				if reject_from_dynamic:
					schedule.num_dynamic_reject += 1
				if reject_from_backbone:
					schedule.num_backbone_reject += 1

			end_time = time_module.time()
			delta_time = end_time - start_time
			embedding_times.append(delta_time)

			if ilp_scheme.willRunILP(num_racks = physical_network.getNumberOfNodes()):
				Profiler.start('runningBurstyILP')
				node_mapping, edge_mapping = ilp_scheme.runILP(physical_network, ilp_current_vnrs, vnr)
				Profiler.end('runningBurstyILP')
				if node_mapping != None and edge_mapping != None:
					# If a simulation is going to be run the ILP computed embeddings, then we must construct a proper embedding.
					if ilp_scheme.shouldRunILPFlowSimulation(num_racks = physical_network.getNumberOfNodes()):
						# ILP accepted the new vnr
						# Construct the SingleMappings for all of the VNRs
						new_single_mappings = {vn_request: SingleMapping(physical_network, vn_request.virtual_network) for vn_request in ilp_current_vnrs}
						new_single_mappings[vnr] = SingleMapping(physical_network, vnr.virtual_network)

						# Add the node mappings
						Profiler.start('ilp_constructNodeMappings')
						for virtual_node, physical_server in node_mapping.iteritems():
							this_sm = new_single_mappings[virtual_node.network.virtual_network_request]
							this_sm.virtual_node_to_physical_server_mapping[virtual_node] = physical_server
							this_sm.physical_server_to_virtual_node_mapping[physical_server].append(virtual_node)
						Profiler.end('ilp_constructNodeMappings')

						# From the edge_mapping, compute the Paths for each virtual edge and the associated weights
						Profiler.start('ilp_constructEdgeMappings')
						for virtual_edge, weighted_physical_edges in edge_mapping.iteritems():
							this_sm = new_single_mappings[virtual_edge.node1.network.virtual_network_request]

							# Compute Path weights
							Profiler.start('ilp_computePathWeights')
							mapped_pes = [pe for pe, w in weighted_physical_edges]

							physical_node1 = this_sm.virtual_node_to_physical_server_mapping[virtual_edge.node1].node
							physical_node2 = this_sm.virtual_node_to_physical_server_mapping[virtual_edge.node2].node

							all_paths = physical_network.findPathsBetween(physical_node1, physical_node2)

							pos_paths = [p for p in all_paths if all(pe in mapped_pes for pe in p.edges)]

							path_matrix = [[(1 if pe in p.edges else 0) for p in pos_paths] for pe in mapped_pes]
							weight_matrix = [w for pe, w in weighted_physical_edges]

							# TODO this can throw an exception in the case of a singular matrix
							path_weights, _, _, _ = numpy.linalg.lstsq(path_matrix, weight_matrix)
							Profiler.end('ilp_computePathWeights')

							# Add the mappings
							this_sm.virtual_edge_to_physical_paths_mapping[virtual_edge] = [(p, path_weight) for p, path_weight in zip(pos_paths, path_weights)]
							for pe, w in weighted_physical_edges:
								this_sm.physical_edge_to_virtual_edge_mapping[pe].append((virtual_edge, w))
						
						# Hanle len 0 paths
						for vnr_k in new_single_mappings:
							virtual_network = vnr_k.virtual_network
							for virtual_edge in virtual_network.getAllVirtualEdges():
								if virtual_edge not in new_single_mappings[vnr_k].virtual_edge_to_physical_paths_mapping:
									if new_single_mappings[vnr_k].virtual_node_to_physical_server_mapping[virtual_edge.node1].node != \
												new_single_mappings[vnr_k].virtual_node_to_physical_server_mapping[virtual_edge.node2].node:
										raise Exception('Missing non-len 0 path')
									physical_node1 = new_single_mappings[vnr_k].virtual_node_to_physical_server_mapping[virtual_edge.node1].node
									physical_node2 = new_single_mappings[vnr_k].virtual_node_to_physical_server_mapping[virtual_edge.node2].node
									
									new_single_mappings[vnr_k].virtual_edge_to_physical_paths_mapping[virtual_edge] = [(len0_path, 1.0) for len0_path in physical_network.findPathsBetween(physical_node1, physical_node2)]
									if len(new_single_mappings[vnr_k].virtual_edge_to_physical_paths_mapping[virtual_edge]) != 1 or \
											new_single_mappings[vnr_k].virtual_edge_to_physical_paths_mapping[virtual_edge][0][0].length() != 0:
										raise Exception('Unexpected number or lenght of paths')
						Profiler.end('ilp_constructEdgeMappings')

						# Add the changed mappings to ilp_schedule
						for changed_vnr in ilp_current_vnrs:
							ilp_schedule.addChangedVnRequest(time, changed_vnr, new_single_mappings[changed_vnr])
						
						# Add the new mapping to ilp_schedule
						ilp_schedule.addAcceptedVnRequest(time, vnr, new_single_mappings[vnr])					

						ilp_current_vnrs.add(vnr)
					# If no simulation is going to be run, we just care about the revenue and cost of the embedding
					else:
						# For now don't care about the actually mapping, so just use None. If the cost is needed a simple class should be made to hold the lp mapping

						# Add the changed mappings to ilp_schedule
						for changed_vnr in ilp_current_vnrs:
							ilp_schedule.addChangedVnRequest(time, changed_vnr, None)
						
						# Add the new mapping to ilp_schedule
						ilp_schedule.addAcceptedVnRequest(time, vnr, None)					

						ilp_current_vnrs.add(vnr)
				else:
					# ILP rejected the new vnr
					ilp_schedule.addRejectedVnRequest(time, vnr)

	status_file.write('end createVnEmbeddingSchedule')
	if return_data:
		mapping_by_path_len_str = ','.join(map(lambda x: str(x[0]) + ':' + str(x[1]), [(a, b) for a, b in schedule.getMappingByPathLen().iteritems()]))
		if mapping_by_path_len_str == '':
			mapping_by_path_len_str = 'None'

		shortest_path_accept_str = ','.join([str(a) + ':' + str(b) for a, b in total_mapping.shortest_path_accept.iteritems()])
		if shortest_path_accept_str == '':
			shortest_path_accept_str = 'None'
		shortest_path_reject_str = ','.join([str(a) + ':' + str(b) for a, b in total_mapping.shortest_path_reject.iteritems()])
		if shortest_path_reject_str == '':
			shortest_path_reject_str = 'None'

		average_embedding_times = 0.0
		if len(embedding_times) > 0:
			average_embedding_time = sum(embedding_times) / float(len(embedding_times))
		print average_embedding_time

		schedule_data = {'num_vns_accepted': schedule.num_accept,
							'num_vns_changed': schedule.num_change,
							'num_vns_rejected': schedule.num_reject,
							'num_vns_node_rejected': schedule.num_node_reject,
							'num_vns_dynamic_rejected': schedule.num_dynamic_reject,
							'num_vns_backbone_rejected': schedule.num_backbone_reject,
							'num_virtual_edges_accepted': schedule.num_virtual_edges_accepted,
							'num_virtual_edges_changed': schedule.num_virtual_edges_changed,
							'num_virtual_edges_rejected': schedule.num_virtual_edges_rejected,
							'max_physical_edge_collision': max_physical_edge_collision,
							'max_edge_inter_arrival_time': max_edge_inter_arrival_time,
							'average_accepted_revenue_ar_formula': schedule.computeAverageAcceptedRevenue(ONLY_AR = True),
							'average_accepted_revenue_data_formula': schedule.computeAverageAcceptedRevenue(AR_AND_FS = True),
							'average_accepted_revenue_normalized_data_formula': schedule.computeAverageAcceptedRevenue(AR_AND_FS = True, normalize_by_link_rate = physical_network.getLinkRateInBytesPerSecond()),
							'average_revenue_to_cost_ratio': schedule.computeAverageRevenueToCostRatio(physical_network.getLinkRateInBytesPerSecond()),
							'virtual_edge_resource_usage': schedule.getAverageResourceUsageOfVirtualEdges(),
							'mapping_by_path_len': mapping_by_path_len_str,
							'shortest_path_accept': shortest_path_accept_str,
							'shortest_path_reject': shortest_path_reject_str,
							'total_resource_usage_per_node': ','.join([str(node.id_num) + ':' + str(v) for node, v in total_usage_per_node.iteritems()]),
							'reject_because_useless': total_mapping.reject_because_useless,
							'average_embedding_time_in_seconds': average_embedding_time,
							'average_embedding_time_in_milliseconds': average_embedding_time * 1000.0,
							'average_embedding_time_in_microseconds': average_embedding_time * 1000000.0}
		schedule_data_vals = ['num_vns_accepted', 'num_vns_changed', 'num_vns_rejected',
								'num_vns_node_rejected', 'num_vns_dynamic_rejected', 'num_vns_backbone_rejected',
								'num_virtual_edges_accepted', 'num_virtual_edges_changed', 'num_virtual_edges_rejected',
								'max_physical_edge_collision', 'max_edge_inter_arrival_time',
								'average_accepted_revenue_ar_formula', 'average_accepted_revenue_data_formula', 'average_accepted_revenue_normalized_data_formula', 'average_revenue_to_cost_ratio',
								'virtual_edge_resource_usage', 'mapping_by_path_len', 'shortest_path_accept', 'shortest_path_reject', 'total_resource_usage_per_node', 'reject_because_useless',
								'average_embedding_time_in_seconds', 'average_embedding_time_in_milliseconds', 'average_embedding_time_in_microseconds']

		if ilp_scheme.willRunILP(num_racks = physical_network.getNumberOfNodes()):
			schedule_data[ilp_scheme.algo_type + '_num_vns_accepted'] = ilp_schedule.num_accept
			schedule_data[ilp_scheme.algo_type + '_num_vns_rejected'] = ilp_schedule.num_reject
			schedule_data[ilp_scheme.algo_type + '_num_virtual_edges_accepted'] = ilp_schedule.num_virtual_edges_accepted
			schedule_data[ilp_scheme.algo_type + '_num_virtual_edges_rejected'] = ilp_schedule.num_virtual_edges_rejected

			schedule_data[ilp_scheme.algo_type + '_average_accepted_revenue_ar_formula'] = ilp_schedule.computeAverageAcceptedRevenue(ONLY_AR = True)
			schedule_data[ilp_scheme.algo_type + '_average_accepted_revenue_data_formula'] = ilp_schedule.computeAverageAcceptedRevenue(AR_AND_FS = True)
			schedule_data[ilp_scheme.algo_type + '_average_accepted_revenue_normalized_data_formula'] = ilp_schedule.computeAverageAcceptedRevenue(AR_AND_FS = True, normalize_by_link_rate = physical_network.getLinkRateInBytesPerSecond())
							

			schedule_data_vals.append(ilp_scheme.algo_type + '_num_vns_accepted')
			schedule_data_vals.append(ilp_scheme.algo_type + '_num_vns_rejected')
			schedule_data_vals.append(ilp_scheme.algo_type + '_num_virtual_edges_accepted')
			schedule_data_vals.append(ilp_scheme.algo_type + '_num_virtual_edges_rejected')

			schedule_data_vals.append(ilp_scheme.algo_type + '_average_accepted_revenue_ar_formula')
			schedule_data_vals.append(ilp_scheme.algo_type + '_average_accepted_revenue_data_formula')
			schedule_data_vals.append(ilp_scheme.algo_type + '_average_accepted_revenue_normalized_data_formula')

			ilp_scheme.schedule = ilp_schedule

		Profiler.end('createVnEmbeddingSchedule')
		return schedule, (schedule_data, schedule_data_vals)
	else:
		Profiler.end('createVnEmbeddingSchedule')
		return schedule

def findSmallestPathSet(physical_network, longest_path):
	Profiler.start('findSmallestPathSet')
	smallest_path_set = -1
	for i in physical_network.nodes:
		for j in physical_network.nodes:
			if i < j:
				# Find all paths with length at most longest_path
				paths = physical_network.findPathsBetween(physical_network.nodes[i], physical_network.nodes[j], longest_path)
				if len(paths) > 0:
					if smallest_path_set == -1 or len(paths) < smallest_path_set:
						smallest_path_set = len(paths)
	Profiler.start('findSmallestPathSet')
	return smallest_path_set

def maxEdgeRateForPathCollision(path_collision, longest_path, smallest_path_set):
	# TODO make sure this equation is correct
	return 1 - pow(1 - pow(path_collision, 1.0 / smallest_path_set), 1.0 / longest_path)

def generateFlowStarts(arrival_rate, test_length):
	rv = []
	cur_t = 0.0

	while True:
		delta = -log(1 - random.random()) / arrival_rate
		if cur_t + delta >= test_length:
			break

		rv.append(cur_t + delta)
		cur_t += delta

	return rv

def simulateFlowCongestion(arrival_rate, flow_distribution, test_length):
	Profiler.start('simulateFlowCongestion')
	flow_starts = generateFlowStarts(arrival_rate, test_length)
	flow_starts.append(float(test_length))

	active_flows = []
	time_by_num_active_flows = defaultdict(float)
	cur_flow_rate = float('inf')

	cur_t = 0

	for i, fs in enumerate(flow_starts):
		active_flows.sort(key = lambda x: x[1])

		# End flows that should end before this flow starts
		while True:
			if len(active_flows) > 0 and active_flows[0][1] <= fs:
				if active_flows[0][1] - cur_t < 0:
					raise Exception('Invalid assumption about queue')

				time_by_num_active_flows[len(active_flows)] += active_flows[0][1] - cur_t
				cur_t = active_flows[0][1]

				# Update flow end times
				active_flows = [(flow_index, cur_t + float(len(active_flows) - 1) / float(len(active_flows)) * (cur_end_time - cur_t)) for flow_index, cur_end_time in active_flows[1:]]
			else:
				break

		# Advance time to the arrival of this flow
		if fs - cur_t < 0:
			raise Exception('Invalid assumption about queue')

		time_by_num_active_flows[len(active_flows)] += fs - cur_t
		cur_t = fs

		# Update old Flows
		active_flows = [(flow_index, cur_t + float(len(active_flows) + 1) / float(len(active_flows)) * (cur_end_time - cur_t)) for flow_index, cur_end_time in active_flows]

		duration = flow_distribution()
		active_flows.append((i, cur_t + duration * float(len(active_flows) + 1)))

	Profiler.end('simulateFlowCongestion')
	return sum(time_by_num_active_flows[k] / test_length for k in time_by_num_active_flows if k >= 2)

def computeMaxEdgeInterArrivalTime(max_edge_collision, flow_distribution, average_flow_duration, compute_max_inter_arrival_time_scheme):
	# TODO make it so this has the full flow size distribution!!!!
	Profiler.start('computeMaxEdgeInterArrivalTime')
	if compute_max_inter_arrival_time_scheme.algo_type in (ComputeMaxInterArrivalTimeScheme.analytical, ComputeMaxInterArrivalTimeScheme.numerical):
		# Repeated Doubling approximation
		current_solution = 1.0
		current_floor = 0.0
		current_ceil = None
		num_iters = 100
		stopping_tolerance = .001
		for i in range(num_iters):
			if compute_max_inter_arrival_time_scheme.algo_type == ComputeMaxInterArrivalTimeScheme.analytical:
				current_edge_collision = 1.0 - (1.0 + average_flow_duration / current_solution) * exp(-average_flow_duration / current_solution)
			elif compute_max_inter_arrival_time_scheme.algo_type == ComputeMaxInterArrivalTimeScheme.numerical:
				current_edge_collision = simulateFlowCongestion(1.0 / current_solution, flow_distribution, current_solution * 10000.0)
				analytical_collision = pow(1.0 / current_solution * average_flow_duration , 2.0)
			
			if abs(current_edge_collision - max_edge_collision) <= stopping_tolerance:
				break
			elif current_edge_collision > max_edge_collision and current_ceil == None:
				current_floor = current_solution
				current_solution *= 2.0
			elif current_edge_collision > max_edge_collision and current_ceil != None:
				current_floor = current_solution
				current_solution = (current_ceil + current_floor) / 2.0
			elif current_edge_collision < max_edge_collision:
				current_ceil = current_solution
				current_solution = (current_ceil + current_floor) / 2.0
			else:
				raise Exception('Unexpected case in numerical approximation of max edge arrival rate')
	elif compute_max_inter_arrival_time_scheme.algo_type == ComputeMaxInterArrivalTimeScheme.queue_analytical:
		current_solution = average_flow_duration / sqrt(max_edge_collision)

	# TEMP comparison
	if compute_max_inter_arrival_time_scheme.algo_type == ComputeMaxInterArrivalTimeScheme.numerical:
		queue_analytical_solution = average_flow_duration / sqrt(max_edge_collision)

		print average_flow_duration, sum(flow_distribution() for i in range(1000)) / 1000.0
		print 'Solution difference:', abs(current_solution - queue_analytical_solution), current_solution, queue_analytical_solution
		raise Exception('Quit')

	Profiler.end('computeMaxEdgeInterArrivalTime')
	return current_solution

def debug_main():
	# Creates test networks
	# physical_network = PN.generatePhysicalNetwork(16, 4, 4, 3, 1.0, 1.0, 1.0)
	# vn_requests = VNR.generate_vn_requests(10, 4, 1.0, .5, 3.0, .1, .2, 1.0)

	physical_network1 = PN.makeToyNetwork(1)
	physical_network2 = PN.makeToyNetwork(2)
	vn_requests = VNR.generate_vn_requests(10, 2, 1.0, (0.0,), (10.0,), (.01,), (1.0,), 1.0)
	print vn_requests

	# Parameters for the functions
	max_virtual_edge_collision = .25
	longest_path = 3
	average_flow_duration = 0.05 # All that is needed from flow size distribution and physical link rate

	w, k1 = createVnEmbeddingSchedule(physical_network1, vn_requests, 1.0, PathWeightingScheme('spf'), longest_path, average_flow_duration, return_data = True, temp_var = 1.0)
	w, k2 = createVnEmbeddingSchedule(physical_network2, vn_requests, 1.0, PathWeightingScheme('spf'), longest_path, average_flow_duration, return_data = True, temp_var = 1.0)
	print k1[0]['num_vns_accepted'], k2[0]['num_vns_accepted']

def addScheduleArgs(parser):
	# Allow mvec and mpec to be a list of values
	# Allow longest_path_length to be a list of values

	# Capacity args
	parser.add_argument('-mvec', '--max_virtual_edge_collision', metavar = 'MAX_VIRTUAL_EDGE_COLLISION', type = float, nargs = '+', default = [-1.0], help = 'The maximum chance that a virtual edge can\'t be fufilled')
	parser.add_argument('-con_algo', '--conversion_algorithm', metavar = 'CONVERSION_ALGORITHM', type = ConvertVirtualEdgeCollisionToPhysicalEdgeCollisionAlgorithm, nargs = 1, default = [ConvertVirtualEdgeCollisionToPhysicalEdgeCollisionAlgorithm('ind')], help = 'Algorithm used to conver the given mvec value to a mpec value. Must be a value in (' + ', '.join(ConvertVirtualEdgeCollisionToPhysicalEdgeCollisionAlgorithm.all_algos) + ')')
	parser.add_argument('-mpec', '--max_physical_edge_collision', metavar = 'MAX_PHYSICAL_EDGE_COLLISION', type = float, nargs = '+', default = [-1.0], help = 'The maximum chance that a physical edge will have a collision')
	
	parser.add_argument('-ems', '--embedding_scheme', metavar = 'EMBEDDING_SCHEME', type = EmbeddingScheme, nargs = 1, default = [EmbeddingScheme('two_stage')], help = 'Algorithm used to embed virtual networks onto the physical network. Must be one of the following values (' + ', '.join(EmbeddingScheme.all_algos) + ')')
	parser.add_argument('-lms', '--link_mapping_scheme', metavar = 'LINK_MAPPING_SCHEME', type = LinkMappingScheme, nargs = '+', default = [LinkMappingScheme('dyn')], help = 'Algorithm used map virtual edges to paths. Must be one of the following values (' + ', '.join(LinkMappingScheme.all_algos) + ')')
	parser.add_argument('-nms', '--node_mapping_scheme', metavar = 'NODE_MAPPING_SCHEME', type = NodeMappingScheme, nargs = '+', default = [NodeMappingScheme('rand')], help = 'Algorithm used to map virtual nodes to physical nodes. Must be one of the following values (' + ', '.join(NodeMappingScheme.all_algos) + ')')
	parser.add_argument('-pws', '--path_weighting_scheme', metavar = 'PATH_WEIGHTING_SCHEME', type = PathWeightingScheme, nargs = '+', default = [PathWeightingScheme('spf')], help = 'Algorithm used to assign weights to paths when embedding virtual links. Must be one of the following values (' + ', '.join(PathWeightingScheme.all_algos) + ')')
	parser.add_argument('-pre', '--preemption_scheme', metavar = 'PREEMPTION_SCHEME', type = PreemptionScheme, nargs = '+', default = [PreemptionScheme('none')], help = 'Algorithm used when an VN is rejected. Preemptive variants will alter previous embeddings. Must be one of the following values (' + ', '.join(PreemptionScheme.all_algos) + ')')
	parser.add_argument('-ilp', '--ilp_scheme', metavar = 'ILP_SCHEME', type = ILPScheme, nargs = '+', default = [ILPScheme('no_ilp')], help = 'Algorithm used. Must be one of the following values (' + ', '.join(ILPScheme.all_algos) + ')')
	parser.add_argument('-ilp_ms', '--ilp_max_rack_size', metavar = 'MAX_RACK_SIZE', type = int, nargs = 1, default = [None], help = 'Max physical network size to run the selcted ILP algorithms (Doesn\'t affect LP, only IP and MIP)')
	parser.add_argument('-mip_gap', '--optimality_gap', metavar = 'MIP_GAP', type = float, nargs = 1, default = [None], help = 'Gap between stopping point and optimal solution for ILP')

	parser.add_argument('-mas', '--compute_max_inter_arrival_time_scheme', metavar = 'COMPUTE_MAX_INTER_ARRIVAL_TIME_SCHEME', type = ComputeMaxInterArrivalTimeScheme, nargs = 1, default = [ComputeMaxInterArrivalTimeScheme('queue')], help = 'Algorithm used to calculate the maximum arrival rate from user supplied congestion. Must be one of the following values (' + ', '.join(ComputeMaxInterArrivalTimeScheme.all_algos) + ')')

	# Schedule specific
	parser.add_argument('-lpl', '--longest_path_length', metavar = 'LONGEST_PATH_LENGTH', type = int, nargs = '+', default = [1], help = 'The longest path a virtual edge can be mapped to')
	parser.add_argument('-sout','--schedule_out_file',metavar = 'SCHEDULE_OUT_FILE', type = str, nargs = 1, default = ['data/schedule_data/schedule_X_K.txt'],help = 'File to write the schedule of embeddings. The first \'X\' in the filename is replaced by a unique identifier')

def getScheduleArgs(args):
	# Capacity params (user must supply exactly one of mvec or mpec)
	max_virtual_edge_collision = args.max_virtual_edge_collision
	conversion_algorithm = args.conversion_algorithm[0]
	max_physical_edge_collision = args.max_physical_edge_collision

	embedding_scheme = args.embedding_scheme[0]
	link_mapping_scheme = args.link_mapping_scheme
	node_mapping_scheme = args.node_mapping_scheme
	path_weighting_scheme = args.path_weighting_scheme
	preemption_scheme = args.preemption_scheme
	ilp_scheme = args.ilp_scheme
	ilp_max_size = args.ilp_max_rack_size[0]
	mip_gap = args.optimality_gap[0]

	for ilps in ilp_scheme:
		ilps.setMaxSize(ilp_max_size)
		ilps.setMIPGap(mip_gap)

	compute_max_inter_arrival_time_scheme = args.compute_max_inter_arrival_time_scheme[0]
	
	longest_path_length = args.longest_path_length
	schedule_out_file = args.schedule_out_file[0]

	this_params = {'max_virtual_edge_collision': ('list(float)', ','.join(map(str, max_virtual_edge_collision))),
					'conversion_algorithm': ('ConvertVirtualEdgeCollisionToPhysicalEdgeCollisionAlgorithm', str(conversion_algorithm)),
					'max_physical_edge_collision': ('list(float)', ','.join(map(str, max_physical_edge_collision))),
					'embedding_scheme': ('EmbeddingScheme', str(embedding_scheme)),
					'link_mapping_scheme': ('list(LinkMappingScheme)', ','.join(map(str, link_mapping_scheme))),
					'node_mapping_scheme': ('list(NodeMappingScheme)', ','.join(map(str, node_mapping_scheme))),
					'path_weighting_scheme': ('list(PathWeightingScheme)', ','.join(map(str, path_weighting_scheme))),
					'preemption_scheme': ('list(PreemptionScheme)', ','.join(map(str, preemption_scheme))),
					'ilp_scheme': ('list(ILPScheme)', ','.join(map(str, ilp_scheme))),
					'ilp_max_rack_size': ('int', str(ilp_max_size)),
					'ilp_mip_gap': ('float', str(mip_gap)),
					'compute_max_inter_arrival_time_scheme': ('ComputeMaxInterArrivalTimeScheme', str(compute_max_inter_arrival_time_scheme)),
					'longest_path_length': ('list(int)', ','.join(map(str, longest_path_length))),
					'schedule_out_file': ('str', schedule_out_file)}

	return this_params, max_virtual_edge_collision, conversion_algorithm, max_physical_edge_collision, embedding_scheme, link_mapping_scheme, node_mapping_scheme, path_weighting_scheme, preemption_scheme, ilp_scheme, ilp_max_size, compute_max_inter_arrival_time_scheme, longest_path_length, schedule_out_file

def getMaxPhysicalEdgeCollision(max_virtual_edge_collision, conversion_algorithm, max_physical_edge_collision, physical_network, longest_path_length):
	if max_virtual_edge_collision >= 0.0 and max_physical_edge_collision < 0.0:
		return conversion_algorithm.convert(max_virtual_edge_collision, physical_network, longest_path_length)
	elif max_virtual_edge_collision < 0.0 and max_physical_edge_collision >= 0.0:
		return max_physical_edge_collision
	else:
		raise Exception('Provide only one of mvec and mpec')

algorithm_version = []

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description = 'Embeds a series of virtual network requests oto a shared physical network using a Poisson model based VN')
	
	# Allows for experiments to be repeated (especially in the case of a bug)
	seed = random.randint(1,1000000)
	print 'Random seed is', seed
	random.seed(seed)

	# Physical Network params
	PN.addPhysicalNetworkArgs(parser)

	# Virtual Network Request params
	VNR.addVirtualNetworkRequestArgs(parser)

	# SchedulePArams
	addScheduleArgs(parser)

	# General Params
	parser.add_argument('-nt', '--num_tests', metavar = 'NUM_TESTS', type = int, nargs = 1, default = [1], help = 'The number of tests to run')

	args = parser.parse_args()

	# Physical params
	physical_params, physical_num_nodes, num_servers, num_edge_sets, physical_num_edges, server_cpu, physical_node_to_node_bandwidth, server_to_node_bandwidth, physical_out_file = PN.getPhysicalNetworkArgs(args)

	# Virtual params
	virtual_params, num_vns, virtual_num_nodes, virtual_link_percent, vn_arrival_gap, vn_duration, virtual_cpu_requirement, inter_arrival_time, flow_size, virtual_out_file = VNR.getVirtualNetworkRequestArgs(args)

	# Schedule params
	schedule_params, max_virtual_edge_collision, conversion_algorithm, max_physical_edge_collision, node_mapping_scheme, path_weighting_scheme, longest_path_length, schedule_out_file = getScheduleArgs(args)
	
	# Test params
	num_tests = args.num_tests[0]

	params = {'num_tests': ('int', str(num_tests))}
	params.update(physical_params)
	params.update(virtual_params)
	params.update(schedule_params)

	average_flow_duration = flow_size / physical_node_to_node_bandwidth

	args = [(this_physical_num_edges, this_virtual_num_nodes, this_max_virtual_edge_collision, this_max_physical_edge_collision, this_longest_path_length)
				for this_physical_num_edges in physical_num_edges
				for this_virtual_num_nodes in virtual_num_nodes
				for this_max_virtual_edge_collision in max_virtual_edge_collision
				for this_max_physical_edge_collision in max_physical_edge_collision
				for this_longest_path_length in longest_path_length]

	for i in range(num_tests):
		for this_physical_num_edges, this_virtual_num_nodes, this_max_virtual_edge_collision, this_max_physical_edge_collision, this_longest_path_length in args:
			print 'Running test', i + 1, 'out of', num_tests

			physical_network = PN.generatePhysicalNetwork(physical_num_nodes, num_servers, num_edge_sets, this_physical_num_edges, server_cpu, physical_node_to_node_bandwidth, server_to_node_bandwidth)
			vn_requests = VNR.generate_vn_requests(num_vns, this_virtual_num_nodes, virtual_link_percent, vn_arrival_gap, vn_duration, virtual_cpu_requirement, inter_arrival_time, flow_size)
			
			this_mpec = getMaxPhysicalEdgeCollision(this_max_virtual_edge_collision, conversion_algorithm, this_max_physical_edge_collision, physical_network, this_longest_path_length)

			schedule = createVnEmbeddingSchedule(physical_network, vn_requests, this_mpec, path_weighting_scheme, this_longest_path_length, average_flow_duration)

			# Write physical_network
			this_physical_out_file = physical_out_file.replace('X', str(i + 1))
			PN.writePhysicalNetwork(physical_network, this_physical_out_file)

			# Write vn_requests
			this_virtual_out_file = virtual_out_file.replace('X', str(i + 1))
			VNR.write_vn_requests(vn_requests, this_virtual_out_file)

			# schedule
			this_schedule_out_file = schedule_out_file.replace('X', str(i + 1))
			writeSchedule(schedule, this_schedule_out_file, this_physical_out_file, this_virtual_out_file, algorithm_version, params)

			print 'Num Accept', schedule.num_accept
			print 'Num Reject', schedule.num_reject
