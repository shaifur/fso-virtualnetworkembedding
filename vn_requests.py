
from profiler import Profiler

from random import random, choice
from math import log
import argparse
import re

# Container for a VN request
class VirtualNetworkRequest:
	def __init__(self, vn_request_id, num_nodes, arrival_time, duration):
		# Unique integer identifier
		self.vn_request_id = vn_request_id

		# Number of nodes in the request, refered to by ids in range [0,num_nodes)
		self.num_nodes = num_nodes

		# The time when the VN Request will arrive
		self.arrival_time = arrival_time

		# The amount of time that the VN will be embedded in the physical network
		self.duration = duration

		# CPU requirement for each virtual node, key is id of node in range [0,num_nodes - 1]
		self.cpu_requirements = {}

		# Bi-directional Edges in the VN, list of 2-tuples
		self.edges = []

		# Arrival rate of each edge, keys are 2-tuple edges and values are the float arrival rate
		self.edge_inter_arrival_time = {}
		self.edge_large_flow_inter_arrival_time = {}
		self.edge_small_flow_inter_arrival_time = {}

		# RealWorldData Object (see simulate_bursty_vns)
		self.real_world_data = None

		self.virtual_network = None

	def safeBuildVirtualNetwork(self):
		if self.virtual_network == None:
			self.buildVirtualNetwork()

	def buildVirtualNetwork(self):
		Profiler.start('VirtualNetworkRequest.buildVirtualNetwork')
		self.virtual_network = VirtualNetwork(self, self.real_world_data)
		self.virtual_network.nodes = {i:VirtualNode(self.virtual_network, i, self.cpu_requirements[i]) for i in range(self.num_nodes)}
		for i, j in self.edges:
			edge = VirtualEdge(self.virtual_network.nodes[i], self.virtual_network.nodes[j], self.edge_large_flow_inter_arrival_time[(i,j)], self.edge_small_flow_inter_arrival_time[(i,j)])
			self.virtual_network.nodes[i].edges[j] = edge
			self.virtual_network.nodes[j].edges[i] = edge
		Profiler.end('VirtualNetworkRequest.buildVirtualNetwork')

	def numVirtualEdges(self):
		return len(self.edges)

	# ONLY_AR uses only the large flow component
	# AR_AND_FS uses the entire arrival rate and multilpies it by the average flow size (which is ethe average generated data)
	def computeRevenue(self, ONLY_AR = False, AR_AND_FS = False, node_weight = 1.0, link_weight = 1.0, normalize_by_link_rate = None):
		# Ensures that exactly one is True and the other is False
		if (not ONLY_AR and not AR_AND_FS) or (ONLY_AR and AR_AND_FS):
			raise Exception('Invalid input')

		node_revenue = sum(cpu for _, cpu in self.cpu_requirements.iteritems())

		if ONLY_AR:
			link_revenue = sum(1.0 / iat for _, iat in self.edge_large_flow_inter_arrival_time.iteritems())

		if AR_AND_FS:
			# Average Flow Size in Bytes
			average_flow_size = self.real_world_data.all_fs_avg
			link_revenue = sum(average_flow_size / iat for _, iat in self.edge_inter_arrival_time.iteritems())

			if normalize_by_link_rate != None:
				link_revenue = link_revenue / normalize_by_link_rate

		return node_weight * node_revenue + link_weight * link_revenue

	def __str__(self):
		s = 'Request ' + str(self.vn_request_id) + '\n'
		s += 'num_nodes ' + str(self.num_nodes) + '\n'
		s += 'arrival_time ' + str(self.arrival_time) + '\n'
		s += 'duration ' + str(self.duration) + '\n'
		s += 'node_cpu ' + ' '.join(str(i) + ',' + str(self.cpu_requirements[i]) for i in self.cpu_requirements) + '\n'
		s += 'edges ' + ' '.join(str(a) + ',' + str(b) for a, b in self.edges) + '\n'
		s += 'inter_arrival_time ' + ' '.join(str(a) + ',' + str(b) + ',' + str(self.edge_inter_arrival_time[(a,b)]) for a, b in self.edge_inter_arrival_time) + '\n'
		# s += 'flow_size_distribution ' + ' '.join(str(p) + ',' + str(s) for p, s in self.flow_size_distribution)
		return s

class VirtualNetwork:
	def __init__(self, virtual_network_request, real_world_data):
		self.virtual_network_request = virtual_network_request
		self.nodes = {}
		self.real_world_data = real_world_data

	def getAllVirtualNodes(self):
		Profiler.start('VirtualNetwork.getAllVirtualEdges')
		rv = (node for _, node in self.nodes.iteritems())
		Profiler.end('VirtualNetwork.getAllVirtualEdges')
		return rv

	def getAllVirtualEdges(self):
		Profiler.start('VirtualNetwork.getAllVirtualEdges')
		rv = (self.nodes[node_id].edges[other_node_id] for node_id in self.nodes for other_node_id in self.nodes[node_id].edges if node_id < other_node_id)
		Profiler.end('VirtualNetwork.getAllVirtualEdges')
		return rv

class VirtualNode:
	def __init__(self, network, id_num, cpu_requirement):
		self.network = network
		self.id_num = id_num
		self.cpu_requirement = cpu_requirement

		self.total_inter_arrival_time = None

		# Key is the other node's id, value is the actual edge
		self.edges = {}

	def neighborNodes(self):
		return (self.network.nodes[v] for v in self.edges)

	def hasNeighbor(self, other_node):
		return other_node.id_num in self.edges

	def getConnectingEdge(self, other_node):
		return self.edges[other_node.id_num]

	def getAllVirtualEdges(self):
		return (edge for _, edge in self.edges.iteritems())

	def getTotalInterArrivalTime(self):
		Profiler.start('VirtualNode.getTotalInterArrivalTime')
		if self.total_inter_arrival_time == None:
			# TODO handle extreme case where one of the individual iats is 0.0
			sum_arrival_rates = sum((1.0 / self.edges[other_node_id].large_flow_inter_arrival_time + 1.0 / self.edges[other_node_id].small_flow_inter_arrival_time) for other_node_id in self.edges)

			if sum_arrival_rates <= 0.00000001:
				self.total_inter_arrival_time = float('inf')
			else:
				self.total_inter_arrival_time = 1.0 / sum_arrival_rates

		Profiler.end('VirtualNode.getTotalInterArrivalTime')
		return self.total_inter_arrival_time

class VirtualEdge:
	def __init__(self, node1, node2, large_flow_inter_arrival_time, small_flow_inter_arrival_time):
		self.node1 = node1
		self.node2 = node2

		self.large_flow_inter_arrival_time = large_flow_inter_arrival_time

		self.small_flow_inter_arrival_time = small_flow_inter_arrival_time

	def getOtherNode(self, virtual_node):
		if virtual_node == self.node1:
			return self.node2
		if virtual_node == self.node2:
			return self.node1

	def getLargeFlowArrivalRate(self):
		return 1.0 / self.large_flow_inter_arrival_time

	def getTotalArrivalRate(self):
		return (1.0 / self.large_flow_inter_arrival_time + 1.0 / self.small_flow_inter_arrival_time)

class RealVirtualTopologyData:
	def __init__(self, filename):
		self.filename = filename
		self.virtual_networks = []
		self.virtual_network_weights = [] # list of index of virtual_networks, copies equal to the number of copies in the data

	def useRealData(self):
		return self.filename != ""

	def readInVNData(self, real_vn_size = None):
		vn_re = 'K (?P<num_copies>\d+) N (?P<num_nodes>\d+) E (?P<edges>(?:\d+,\d+,(?:\d+|\\\\N) )*\d+,\d+,(?:\d+|\\\\N))\n'

		f = open(self.filename)
		for line in f:
			vn_match = re.match(vn_re, line)
			if vn_match == None:
				raise Exception('Invalid VN File')

			if real_vn_size != None and real_vn_size > 1 and real_vn_size != int(vn_match.group('num_nodes')):
				continue

			this_index = len(self.virtual_networks)
			self.virtual_networks.append((int(vn_match.group('num_nodes')), map(lambda x:tuple(map(self._int_or_null, x.split(','))) , vn_match.group('edges').split())))
			for k in range(int(vn_match.group('num_copies'))):
				self.virtual_network_weights.append(this_index)

		f.close()

	def _int_or_null(self, v):
		if v =='\\N':
			return None
		else:
			return int(v)

	def getRandomTopology(self):
		vn_index = choice(self.virtual_network_weights)

		return self.virtual_networks[vn_index]

	def throwOutVNData(self):
		self.virtual_networks.clear()
		self.virtual_network_weights.clear()

	def __str__(self):
		return self.filename

# range_vals - iterable - Returns a value uniformly at random between max(range_vals) and min(range_vals)
def chooseRandomValueInRange(range_vals):
	if len(range_vals) == 0:
		raise Exception('Invalid range of values')
	if len(range_vals) == 1:
		return range_vals[0]
	min_range = min(range_vals)
	max_range = max(range_vals)
	if min_range == max_range:
		return min_range
	return min_range + (max_range - min_range) * random()

# TODO update comment to include random ranges
# Generates a series of VN requests
# Input:
# 	Topology Generation:
# 		Can be either (i.e. all values are non-None):
# 			num_nodes - int in [1,inf) - number of nodes per VN (TODO allow for a random range)
# 			link_percent - float in [0,1] - chance that any two nodes in a VN have a link. If less than -1, then it will be set so that the expected number of edges is -link_percent * num_nodes.
# 		Or (which takes priority):
# 			real_vn_data - RealVirtualTopologyData - Contains a way to generate realistic vn topologies. If it's internal value of filename is "" then use the above option.
# 	VN Arrival:
# 		Can be either:
# 			num_vns - int in [1,inf) - number of VN requests to generate
# 			vn_arrival_gap - iterable of float in [0,inf) - the time in between consecutive VN requests (Chooses random value between min and max (TODO make this a Poisson Distribution))
# 		Or (Priority):
# 			test_duration - float in [0,inf) - The duration of the test. Any VNs who run over the test_duration will have truncated durations
# 			vn_arrival_rate - float in (0,inf) - Average arrival rate of VNs. VN arrivals modeled as a Poisson Process with a lambda of this value.
# 	Both cases require:
# 		vn_duration - iterable of float in (0,inf) - length that ALL VN requests will last (Chooses random value between min and max)
# 		cpu_requirement - iterable of float in [0,inf) - the CPU requirement of ALL nodes in the virtual network (Chooses random value between min and max)
# 		virtual_edge_bandwidth - iterable of float in (0,inf) - The amount of data generated by 
# 		real_world_data - RealWorldData Object - contains information about the flow_size information
# Output:
#   List of VirtuanlNetworkRequests
def generate_vn_requests(num_vns = None,
							num_nodes = None,
							link_percent = None,
							vn_arrival_gap = None,
							vn_duration = None,
							cpu_requirement = None,
							virtual_edge_bandwidth = None,
							real_world_data = None,
							verbose = True,
							real_vn_data = None,
							real_vn_size = None,
							test_duration = None,
							vn_arrival_rate = None):
	Profiler.start('generate_vn_requests')

	guarenteee_connected = False

	if link_percent <= -0.01:
		guarenteee_connected = True

		link_percent = -link_percent
		if (num_nodes * float(num_nodes - 1) / 2.0 - (num_nodes - 1)) < 0.0001:
			link_percent = 1.0
		else:
			link_percent = (link_percent * num_nodes - (num_nodes - 1)) / (num_nodes * float(num_nodes - 1) / 2.0 - (num_nodes - 1))

	if any(k == None for k in (vn_duration, cpu_requirement, virtual_edge_bandwidth, real_world_data)) \
			or ((real_vn_data == None or not real_vn_data.useRealData()) and any(k == None for k in (num_nodes, link_percent))) \
			or (any(k == None for k in (num_vns, vn_arrival_gap)) and any(k == None for k in (test_duration, vn_arrival_rate))):
		raise Exception('Invalid input to generate_vn_requests')

	if real_vn_data != None and real_vn_data.useRealData():
		real_vn_data.readInVNData(real_vn_size = real_vn_size)

	# Determines which VN arrival process to use
	if all(k != None for k in (test_duration, vn_arrival_rate)):
		vn_arrival_process = 'poisson'
		print 'Poisson', test_duration, vn_arrival_rate
	elif all(k != None for k in (num_vns, vn_arrival_gap)):
		vn_arrival_process = 'default'

	vns = []
	cur_time = 0.0
	vnr_id = 0
	while (vnr_id < num_vns and vn_arrival_process == 'default') or (vn_arrival_process == 'poisson'):
		# Create an empty container for a VN
		if real_vn_data != None and real_vn_data.useRealData():
			this_num_nodes, this_topology = real_vn_data.getRandomTopology()
		else:
			this_num_nodes = num_nodes

		# Determine the gap between this VN and the previous, and the duration 
		if vn_arrival_process == 'poisson':
			delta = -log(1.0 - random()) / vn_arrival_rate
			cur_time += delta

			# If this VN starts at or after the test_duration, ends the VN generation process
			if cur_time >= test_duration:
				break

			this_duration = chooseRandomValueInRange(vn_duration)

			# Truncates VNs that end after the end of test_duration.
			if cur_time + this_duration >= test_duration:
				this_duration = test_duration - cur_time

		elif vn_arrival_process == 'default':
			cur_time += chooseRandomValueInRange(vn_arrival_gap)
			this_duration = chooseRandomValueInRange(vn_duration)

		this_vn = VirtualNetworkRequest(vnr_id, this_num_nodes, cur_time, this_duration)
		vns.append(this_vn)

		# Create the cpu requirements for the nodes
		this_vn.cpu_requirements = {i:chooseRandomValueInRange(cpu_requirement) for i in range(this_num_nodes)}


		if real_vn_data != None and real_vn_data.useRealData():
			for a, b, bw in this_topology:
				this_vn.edges.append((a, b))
		else:
			# Create edges in the virtual network. Tries k times to create a connected VN, raises error if can't.
			completely_connected = False
			# TODO k is currently a constant 10, make it be a parameter
			for i in range(10):
				this_vn.edges = []
				if guarenteee_connected:
					# Better? randomly 

					groups = []
					out_set = [x for x in range(this_num_nodes)]

					all_edges = [(x, y) for x in range(this_num_nodes) for y in range(this_num_nodes) if x < y]

					while len(out_set) > 0:
						a, b = choice(all_edges)

						all_edges.remove((a, b))
						group_a_inds = [i for i, g in enumerate(groups) if a in g]
						group_b_inds = [i for i, g in enumerate(groups) if b in g]

						if len(group_a_inds) == 0:
							group_a_ind = None
						elif len(group_a_inds) == 1:
							group_a_ind = group_a_inds[0]
						else:
							raise Exception('Invalid VN generation')

						if len(group_b_inds) == 0:
							group_b_ind = None
						elif len(group_b_inds) == 1:
							group_b_ind = group_b_inds[0]
						else:
							raise Exception('Invalid VN generation')

						if group_a_ind != None and group_b_ind != None and group_a_ind == group_b_ind:
							continue

						this_vn.edges.append((a, b))

						if group_a_ind == None and group_b_ind == None:
							groups.append([a, b])
							out_set.remove(a)
							out_set.remove(b)

						if group_a_ind == None and group_b_ind != None:
							groups[group_b_ind].append(a)
							out_set.remove(a)

						if group_a_ind != None and group_b_ind == None:
							groups[group_a_ind].append(b)
							out_set.remove(b)

						if group_a_ind != None and group_b_ind != None:
							groups[group_a_ind] += groups[group_b_ind]
							del groups[group_b_ind]


				# FOR NOW the bw is ignored
				for a, b in [(x, y) for x in range(this_num_nodes) for y in range(this_num_nodes) if x < y]:
					if (a, b) not in this_vn.edges and random() < link_percent:
						this_vn.edges.append((a,b))
				
				# TODO check that the network is connected
				# list of list of node_ids. Each sublist is connected.
				connected_components = []
				for i, j in this_vn.edges:
					comp1_index = find_component(connected_components, i)
					comp2_index = find_component(connected_components, j)
					if comp1_index == None and comp2_index == None:
						connected_components.append([i, j])
					elif comp1_index == comp2_index:
						continue
					else:
						# Remove component 1
						if comp1_index != None:
							comp1 = connected_components.pop(comp1_index)

							# Find the new index of component2 after the removal
							comp2_index = find_component(connected_components, j)
						else:
							comp1 = [i]
						# Remove component 2
						if comp2_index != None:
							comp2 = connected_components.pop(comp2_index)
						else:
							comp2 = [j]
						# Combine them and add them back
						connected_components.append(comp1 + comp2)
				completely_connected = (len(connected_components) == 1 and len(connected_components[0]) == this_num_nodes)
				if completely_connected:
					break
			if not completely_connected:
				raise Exception('Could not create connected Virtual Network')

		# Set the arrival rates for each edge
		percent_large_flows = real_world_data.getPercentLargeFlows()

		edge_bandwidths = {edge:chooseRandomValueInRange(virtual_edge_bandwidth) for edge in this_vn.edges}
		this_vn.edge_inter_arrival_time = {edge:(real_world_data.all_fs_avg / (edge_bandwidth * 1000.0 * 1000.0 * 1000.0 / 8.0) if edge_bandwidth > .00000001 else float('inf')) for edge, edge_bandwidth in edge_bandwidths.iteritems()}
		if percent_large_flows == 0.0:
			this_vn.edge_large_flow_inter_arrival_time = {edge:float('inf') for edge in this_vn.edges}
		else:
			this_vn.edge_large_flow_inter_arrival_time = {edge:this_vn.edge_inter_arrival_time[edge] / percent_large_flows for edge in this_vn.edges}
		if percent_large_flows == 1.0:
			this_vn.edge_small_flow_inter_arrival_time = {edge:float('inf') for edge in this_vn.edges}
		else:
			this_vn.edge_small_flow_inter_arrival_time = {edge:this_vn.edge_inter_arrival_time[edge] / (1.0 - percent_large_flows) for edge in this_vn.edges}

		# Set the flow size distribution
		this_vn.real_world_data = real_world_data

		vnr_id += 1
	if verbose:
		print 'Created', len(vns), 'virtual networks'

		inter_arrival_time = ((real_world_data.all_fs_avg / (max(virtual_edge_bandwidth) * 1000.0 * 1000.0 * 1000.0 / 8.0) if max(virtual_edge_bandwidth) > .00000001 else float('inf')),
								(real_world_data.all_fs_avg / (min(virtual_edge_bandwidth) * 1000.0 * 1000.0 * 1000.0 / 8.0) if min(virtual_edge_bandwidth) > .00000001 else float('inf')))
		
		print 'Range of iat is', min(inter_arrival_time), 'to', max(inter_arrival_time)
		print 'Virtual Edges Generating between', real_world_data.all_fs_avg / min(inter_arrival_time) / 1000.0 / 1000.0 / 1000.0 * 8.0, 'and', real_world_data.all_fs_avg / max(inter_arrival_time)/ 1000.0 / 1000.0 / 1000.0 * 8.0, 'Gb per second for all flows'
		
		if percent_large_flows < 1.0 and percent_large_flows > 0.0:
			print 'Virtual Edges Generating between', real_world_data.large_fs_avg / (min(inter_arrival_time) / percent_large_flows) / 1000.0 / 1000.0 / 1000.0 * 8.0, 'and', real_world_data.large_fs_avg / (max(inter_arrival_time) / percent_large_flows) / 1000.0 / 1000.0 / 1000.0 * 8.0, 'Gb per second for large flows'
			print 'Virtual Edges Generating between', real_world_data.small_fs_avg / (min(inter_arrival_time) / (1.0 - percent_large_flows)) / 1000.0 / 1000.0 / 1000.0 * 8.0, 'and', real_world_data.small_fs_avg / (max(inter_arrival_time) / (1.0 - percent_large_flows)) / 1000.0 / 1000.0 / 1000.0 * 8.0, 'Gb per second for small flows'

	Profiler.end('generate_vn_requests')
	return vns

# Helper function used in generate_vn_requests to ensure that the generated VN are completely connected.
def find_component(connected_components, value):
	for i, component in enumerate(connected_components):
		if value in component:
			return i
	return None

# Input:
#   vns - list of VirtualNetworkRequests - list of VN requests to be written to file
#   filename - str - filename to output vns to
# Output:
#   Writes each VirtualNetworkRequest to file by calling str on each object. Prepends "Request #" and adds a newline after each VN request.
def write_vn_requests(vns, filename):
	if filename == '':
		return

	Profiler.start('write_vn_requests')
	f = open(filename, 'w')
	for i, vn in enumerate(vns):
		f.write(str(vn) + '\n\n')
	f.close()
	Profiler.end('write_vn_requests')

# Reads in the list of VNRequests in the specified file and returns the list
def read_vn_requests(filename):
	Profiler.start('read_vn_requests')
	f = open(filename)
	vns = []
	this_vn = None
	for line in f:
		spl = line.split()
		if len(spl) == 0:
			if this_vn != None:
				vns.append(this_vn)
				this_vn = None
			continue
		if spl[0] == 'Request':
			this_vn = VirtualNetworkRequest(int(spl[1]), None, None, None)
			if len(vns) != int(spl[1]):
				raise Exception('Invalid input file')
		elif spl[0] == 'num_nodes':
			this_vn.num_nodes = int(spl[1])
		elif spl[0] == 'arrival_time':
			this_vn.arrival_time = float(spl[1])
		elif spl[0] == 'duration':
			this_vn.duration = float(spl[1])
		elif spl[0] == 'node_cpu':
			this_vn.cpu_requirements = {int(node_id):float(cpu_requirement) for node_id, cpu_requirement in map(lambda x: tuple(x.split(',')), spl[1:])}
		elif spl[0] == 'edges':
			this_vn.edges = map(lambda x:tuple(map(int,x.split(','))), spl[1:])
		elif spl[0] == 'inter_arrival_time':
			this_vn.edge_inter_arrival_time = {(int(n1_id), int(n2_id)):float(this_inter_arrival_time) for n1_id, n2_id, this_inter_arrival_time in map(lambda x: tuple(x.split(',')), spl[1:])}
		# elif spl[0] == 'flow_size_distribution':
		# 	this_vn.flow_size_distribution = [(float(probability), float(flow_size)) for probability, flow_size in map(lambda x: tuple(x.split(',')), spl[1:])]
	Profiler.end('read_vn_requests')
	return vns

def addVirtualNetworkRequestArgs(parser):
	# Allow virtual_num_nodes to have multiple values
	# Make it so that vn_arrival_gap is either a random range or used to generate a poisson distribution
	# Allow vn_duration to be a range (i.e. two values which will be used to generate a random value between)
	# Allow virtual_cpu_requirement to be a range
	# Allow inter_arrival_time to be a range
	# Allow flow_size to be a full distribution
	parser.add_argument('-nv', '--num_vns', metavar = 'NUM_VNS', type = int, nargs = 1, default = [1], help = 'Number of VN requests to generate')
	parser.add_argument('-vnn', '--virtual_num_nodes', metavar = 'NODES', type = int, nargs = '+', default = [1], help = 'Number of nodes in ALL VNs')
	parser.add_argument('-vlp', '--virtual_link_percent', metavar = 'LINK_PERCENT', type = float, nargs = 1, default = [1.0], help = 'The chance that any two nodes in a VN will have an edge between them')
	parser.add_argument('-ag', '--vn_arrival_gap', metavar = 'ARRIVAL_GAP', type = float, nargs = '+', default = [10.0], help = 'The gap between consecutive VN requests. For each Vn, the arrival gap will chosen randomly between the min and amx value supplied')
	parser.add_argument('-dur', '--vn_duration', metavar = 'DURATION', type = float, nargs = '+', default = [1.0], help = 'The length of time that each VN Request will remain active. For each VN the duration will be randomly chosen between the min and max value supplied')
	parser.add_argument('-vcpu', '--virtual_cpu_requirement', metavar = 'VCPU', type = float, nargs = '+', default = [0.0], help = 'The percent of cpu each virtual node requires')
	parser.add_argument('-vebw', '--virtual_edge_bandwidth', metavar = 'VE_BANDWIDTH', type = float, nargs = '+', default = [1.0], help = 'The range of data each virtual edge will generate. (In Gb/s)')
	parser.add_argument('-fs', '--flow_size', metavar = 'FLOW_SIZE', type = float, nargs = 1, default = [1.0], help = 'The size of flows on each edge')
	parser.add_argument('-rvn', '--real_vn_data', metavar = 'VN_DATA_FILE', type = RealVirtualTopologyData, nargs = 1, default = [RealVirtualTopologyData("")], help = 'File to get real VN Data')
	parser.add_argument('-rvn_size', '--real_vn_size', metavar = 'VN_SIZE', type = int, nargs = '+', default = [-1], help = 'Fixed size to generate real VNs')
	parser.add_argument('-td', '--test_duration', metavar = 'TEST_DURATION', type = float, nargs = 1, default = [None], help = 'Duration to generate VNs over')
	parser.add_argument('-vnar', '--virtual_network_arrival_rate', metavar = 'VN_ARRIVAL_RATE', type = float, nargs = '+', default = [None], help = 'Average arrival rate for Poisson Process to model VN arrivals.')

	parser.add_argument('-vout','--virtual_out_file',metavar = 'VIRTUAL_OUT_FILE',type = str,nargs = 1,default = ['data/vnr_data/virtual_network_request_X_K.txt'],help = 'File to write virtual network requests to. The first \'X\' in the filename is replaced by a unique identifier')

def getVirtualNetworkRequestArgs(args):
	num_vns = args.num_vns[0]
	virtual_num_nodes = args.virtual_num_nodes
	virtual_link_percent = args.virtual_link_percent[0]
	vn_arrival_gap = args.vn_arrival_gap
	vn_duration = args.vn_duration
	virtual_cpu_requirement = args.virtual_cpu_requirement
	virtual_edge_bandwidth = args.virtual_edge_bandwidth
	flow_size = args.flow_size[0]
	real_vn_data = args.real_vn_data[0]
	real_vn_sizes = args.real_vn_size
	test_duration = args.test_duration[0]
	virtual_network_arrival_rate = args.virtual_network_arrival_rate

	virtual_out_file = args.virtual_out_file[0]

	this_params = {'num_vns': ('int', str(num_vns)),
					'virtual_num_nodes': ('list(int)', ','.join(map(str, virtual_num_nodes))),
					'virtual_link_percent': ('float', str(virtual_link_percent)),
					'vn_arrival_gap': ('list(float)', ','.join(map(str, vn_arrival_gap))),
					'vn_duration': ('list(float)', ','.join(map(str, vn_duration))),
					'virtual_cpu_requirement': ('list(float)', ','.join(map(str, virtual_cpu_requirement))),
					'virtual_edge_bandwidth': ('list(float)', ','.join(map(str, virtual_edge_bandwidth))),
					'flow_size': ('float', str(flow_size)),
					'real_vn_data': ('RealVirtualTopologyData', str(real_vn_data)),
					'real_vn_sizes': ('int', ','.join(map(str, real_vn_sizes))),
					'test_duration': ('float', str(test_duration)),
					'virtual_network_arrival_rate': ('list(float)', ','.join(map(str, virtual_network_arrival_rate))),
					'virtual_out_file': ('str', virtual_out_file)}

	return this_params, num_vns, virtual_num_nodes, virtual_link_percent, vn_arrival_gap, vn_duration, virtual_cpu_requirement, virtual_edge_bandwidth, flow_size, real_vn_data, real_vn_sizes, test_duration, virtual_network_arrival_rate, virtual_out_file

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description = 'Generates a list of VN request with bursty links and outputs them to a file')

	# Virtual Network Args
	parser.add_argument('-nv', '--num_vns', metavar = 'NV', type = int, nargs = 1, default = [1], help = 'Number of VN requests to generate')
	parser.add_argument('-nn', '--num_nodes', metavar = 'NN', type = int, nargs = 1, default = [1], help = 'Number of nodes in ALL VNs')
	parser.add_argument('-lp', '--link_percent', metavar = 'LP', type = float, nargs = 1, default = [1.0], help = 'The chance that any two nodes in a VN will have an edge between them')
	parser.add_argument('-ag', '--vn_arrival_gap', metavar = 'AG', type = float, nargs = 1, default = [1.0], help = 'The gap between consecutive VN requests')
	parser.add_argument('-dur', '--vn_duration', metavar = 'DUR', type = float, nargs = 1, default = [1.0], help = 'The length of time that each VN Request will remain active')
	parser.add_argument('-cpu', '--cpu_requirement', metavar = 'CPU', type = float, nargs = 1, default = [1.0], help = 'The percent of cpu each virtual node requires')
	parser.add_argument('-iat', '--inter_arrival_time', metavar = 'IAT', type = float, nargs = 1, default = [1.0], help = 'The average inter arrival time of flows on each edge')
	parser.add_argument('-fs', '--flow_size', metavar = 'FS', type = float, nargs = 1, default = [1.0], help = 'The size of flows on each edge')

	# Output
	parser.add_argument('-out','--out_file',metavar = 'OF',type = str,nargs = 1,default = ['temp.txt'],help = 'File to output vn requests to')

	args = parser.parse_args()

	num_vns = args.num_vns[0]
	num_nodes = args.num_nodes[0]
	link_percent = args.link_percent[0]
	vn_arrival_gap = args.vn_arrival_gap[0]
	vn_duration = args.vn_duration[0]
	inter_arrival_time = args.inter_arrival_time[0]
	flow_size = args.flow_size[0]

	out_file = args.out_file[0]

	vns = generate_vn_requests(num_vns, num_nodes, link_percent, vn_arrival_gap, vn_duration, cpu_requirement, inter_arrival_time, flow_size)
	write_vn_requests(vns, out_file)
