'''
author: Md Shaifur Rahman (shaifur.at.buet@gmail.com)
date: Feb 27, 2017
Project: Virtual Network Embedding in FSO-linked Data-centers
'''
import networkx as nx
import random
import numpy as np
import logging

#-----------------------------------------------------------------------------------------------------------------------------------------------
class VirtualNetworkEmbedding(object):
    '''
    Class Attributes:
    ---------------
    max_deg = maximum node degree per FSO-node, defaults to 2, if a node has n max_degree,
            it can connect to at most n neighbors using one of its k edges,
            currently it's class attribute, i.e same value for each fso-node
    G_isn = (Networkx) Graph for the Initial Substrate Network (ISN), which contains all nodes plus all  possible edges
          Once initialized, this graph is kept unchanged throughout serving Virtual Network Requests
    G_rsn = Graph for Residual Substrate Network(RSN), which is initially copied from RSN, ...complete doc..
    G_rvn = Graph for Requested Virtual Network (RVN)
    rvn_to_rsn_node_mapping = the one-to-one node mapping between rvn (requested) and rsn (residual substrate) graphs, currently a list of lenght = #nodes in G_rvn graph
                                i-th entry k, maps i-th node of RVN to k-th node of RSN
    rvn_to_rsn_link_mapping =  a dict of mapping virtual edges to paths in the residual substrate network graph i.e. G_rsn. The virtual edge (v1, v2) is the key as tuple,
                                and the value is a list of substrate nodes which defines the path
    self.virtual_nodes_covered = a temporary list of virtual list which has at least one edge covered in the substrate graph, not used yet...
    logger = object created for log output, used for debugging/error messages instead of print method
            log-level set to DEBUG in the constructor method
    '''
    def __init__(self, ext_logger):
        self.G_isn = None
        self.G_rsn = None
        self.G_rvn = None
        self.rvn_to_rsn_node_mapping = None
        self.rvn_to_rsn_link_mapping = None
        self.virtual_nodes_covered = None
        self.logger = ext_logger

        self.logger.info('Object initalized')



    def createSubstrateNetworkInstance(self, G_in = None):
        '''
        creates a substrate network and initialized isn with it
        :param : None
        :return: None
        '''
        if G_in:
            self.G_isn = nx.Graph(G_in)
            self.G_rsn = nx.Graph(self.G_isn)

        self.logger.info("@createSubstrateNetworkInstance(..): Substrate Network Initialized")


    def generateVNERequest(self, G_in = None):
        '''
        Generates Virtual Network Request in the form of a graph
        :param G_in: Provide a graph generated outside of the class for the time being
        :return: None
        '''
        if G_in:
            self.G_rvn = nx.Graph(G_in)

        self.logger.info("@generateVNERequest(..): Graph for Requested Virtual Network created")

    def computeNodeRanking(self, G_in,
                           attribute_name = 'capacity',
                           d = 0.85,
                           delta = 0.0001,
                           matrix_cut_off_dim = 100,
                           max_iter_count = 10000):
        '''
        This method ranks all nodes according to Markov-random walk models as described in Gong et. al 2014 INFOCOM paper..
        task:
        1. calculate matrix M
        2. calculate matrix C
        3. a) if matrix too large, do iteratively improve r(k+1) = (1-d)*c + d*  M* r(k) while improvement > delta
           b) else use direct formula r= (1 - d)*inv(I- d*M) * C
        :param G_in: Input graph, for which to compute the node ranking matrix, must be networkx object, also must have
        :param attribute_name : Name of the link/node attribute in the networkx dict object
        :param d : weight given to bandwidth capacity, value between 0 and 1 inclusive
        :param delta : the delta value for iterative improvement, iteration stops after the difference
                        between successive improvement is < this value
        :param matrix_cut_off_dim : used for large matrix, when no of nodes is > this value, iterative approx. is used
        :param max_iter_count : in case of iterative approximation of rank matrix, this limits the total iterations

        :return: a row-matrix R containing rank value for each node, i-th row contains rank value for i-th node
        '''

        # Step 1 -----------------
        n = G_in.number_of_nodes()
        M = np.zeros( (n, n) , dtype = np.float)

        for u, v in sorted(G_in.edges()):
            #first compute (i,j) entry
            for i, j in [ (u, v) , (v, u)]:
                bsum = 0.0
                for k in G_in.neighbors(j):
                    bsum += G_in[j][k][attribute_name]
                if bsum > 0.0: #find the ratio of denominator is greater than zero
                    M[i][j] =  1.0* G_in[i][j][attribute_name]/bsum
                else: #otherwise the matrix entry is zero
                    M[i][j] = 0.0 # total zero-link capacity

        # Step 2 ----------------------
        C = np.zeros( (n, 1) , dtype = np.float)
        csum = 0.0
        for u in G_in.nodes():
            csum+= G_in.node[u][attribute_name]
        if csum > 0.0: #no need to find the following ratio if total node capacity of the graph is zero
            for u in sorted(G_in.nodes()):
                C[u][0] = 1.0*G_in.node[u][attribute_name]/csum


        # Step 3---------------------
        R = np.array(C) #initialize R

        #---if  matrix  is too big, use iterative approximation
        if n> matrix_cut_off_dim:
            iter_improv = float('inf')
            iter_count = 1
            while iter_improv > delta and iter_count < max_iter_count:
                new_R = (1.0 - d) * R + d * np.dot(M, R)
                #-- find improvement in successive iterations
                iter_improv = np.linalg.norm(new_R - R)
                R = new_R
                iter_count += 1

        else: #--else use direct computation using matrix inverse
            I = np.identity(n, dtype = np.float)
            inv_I_dM = None
            try:
                inv_I_dM = np.linalg.inv(I - d*M )
            except np.linalg.LinAlgError:
                self.logger.warning( "@computeNodeRanking(..): Matrix Inverse Error")
                return None #return None as indicative of no rank matrix was computed
            R = (1 - d)* np.dot( inv_I_dM, C)

        graph_title = ''
        if 'title' in G_in.graph:
            graph_title = G_in.graph['title']
        self.logger.info("@computeNodeRanking(..): Node Rank computed for the graph: %s", graph_title)
        return R

    def mapVirtualNetworkNodesToSubstrateNodes(self):
        '''

        :return: None, but class attribute rvn_to_rsn_node_mapping  is set to the node-mapping
        '''
        '''
        steps:
        1. compute node ranking for self.G_rsn and sort the nodes according to the ranking
        2. compute node ranking for self.G_rvn sort the nodes according to the ranking
        3. map each of  the G_rvn nodes to one of the self.G_rsn nodes as per the ranking
        '''
        if self.G_rsn is None:
            self.logger.warn("@mapVirtualNetworkNodesToSubstrateNodes(..): Residual Substrate Network Graph not found, skipping node mapping")
            return

        if self.G_rvn is None:
            self.logger.warn("@mapVirtualNetworkNodesToSubstrateNodes(..): Requested Virtual Network Graph not found, skipping node mapping")
            return

        #step 1:
        R_rsn = self.computeNodeRanking(self.G_rsn, attribute_name='capacity')


        I_rsn = list( np.argsort(R_rsn, axis = 0)[::-1].flatten() )

        #step 2:
        R_rvn = self.computeNodeRanking(self.G_rvn, attribute_name='request')

        I_rvn = list( np.argsort(R_rvn, axis = 0)[::-1].flatten() )

        #step 3:
        self.rvn_to_rsn_node_mapping = [-1 for i in range(len(I_rvn))]

        for n_rvn  in I_rvn:
            request_val = self.G_rvn.node[n_rvn]['request']
            chosen_substrate_node = None
            for n_rsn in I_rsn:
                rem_capacity = self.G_rsn.node[n_rsn]['capacity']
                if rem_capacity >= request_val:
                    chosen_substrate_node = n_rsn
                    break
            if not chosen_substrate_node is None:
                self.rvn_to_rsn_node_mapping[n_rvn]  = chosen_substrate_node
                self.logger.debug("@mapVirtualNetworkNodesToSubstrateNodes(..): node mapping created %d <--> %d:",n_rvn, chosen_substrate_node)
                I_rsn.remove(chosen_substrate_node)
            else:
                graph_title = ''
                if 'title' in self.G_rvn.graph:
                    graph_title =self.G_rvn.graph['title']
                self.logger.warning("@mapVirtualNetworkNodesToSubstrateNodes(..): Node mapping failed for the Requested Virtual Network Graph: %s", graph_title)
                self.rvn_to_rsn_node_mapping = None
                break
        if not self.rvn_to_rsn_node_mapping is None:
            self.logger.debug("@mapVirtualNetworkNodesToSubstrateNodes(..): End of node mapping.. all virtual nodes mapped")

    def mapVirtualLinksToSubstratePaths(self):
        '''
        Computes a shortest path, with requested capacity, if exists, in the substrate network for each edge in the virtual network
        :return: None
        '''
        '''
        tasks:
        1. for each edge (u,v) in the G_rvn graph,
            i) find the matched substate network nodes (n1, n2)
            ii) copy the G_rsn network to G_temp, and remove all the edges with capacity < request from G_temp
            iii) find a shortest path from n1 to n2 in G_temp, if path exist, save the path in the link mapping dictionary in key (u,v)

        '''
        self.rvn_to_rsn_link_mapping = {}
        for u1, u2, req_val in self.G_rvn.edges(data = 'request'):
            n1 = self.rvn_to_rsn_node_mapping[u1]
            n2 = self.rvn_to_rsn_node_mapping[u2]
            if n1== -1  or n2 ==  -1:
                self.logger.warning("@mapVirtualLinksToSubstratePaths: virtual to substrate network node mapping not found for edge %d-%d", n1, n2)
                return #terminate immediately

            #build the G_temp graph
            G_temp = nx.Graph()
            G_temp.add_nodes_from( self.G_rsn.nodes() )
            for v1, v2, cap_val in self.G_rsn.edges(data='capacity'):
                if cap_val >= req_val:
                    G_temp.add_edge(v1, v2)
            cur_shortest_path = nx.shortest_path(G=G_temp, source= n1, target=n2)
            if cur_shortest_path:
                self.rvn_to_rsn_link_mapping[( u1, u2)] = list(cur_shortest_path)
            else:
                self.rvn_to_rsn_link_mapping[( u1, u2)] = []
                self.logger.warning("@mapVirtualLinksToSubstratePaths: Shortest path between vnodes %d and %d not found", u1, u2)

    def decrementCapacityOfPathInSubstrateNetwork(self, sn_path, req_val):
        '''
        decrements the capacity of each node on the path in the substrate network by the requested value
        :param sn_path: a list of nodes, a path on the substrate network
        :param req_val: a floating point value, the capacity of each edge will be decremented by this
        :return:
        '''
        for idx_path_node in range( len(sn_path)-1 ):
            n1 = sn_path[idx_path_node]
            n2 = sn_path[idx_path_node+1]
            self.G_rsn.edge[n1][n2]['capacity'] = self.G_rsn.edge[n1][n2]['capacity']  - req_val
            if self.G_rsn.edge[n1][n2]['capacity'] < 0.0:
                self.logger.error("@updateResidualSubstrateNetwork(..): <<link>> residualcapacity = %f, case of overprovisioned, plz DEBUG node/link matching", self.G_rsn.edge[n1][n2]['capacity'])
                return  # return immediately

    def decrementNodeCapacityInSubstrateNetwork(self, snode, vnode):
        '''
        decrement the node capacity of substrate network node i.e. snode by the request value of virtual node i.e. vnode
        :param snode: the node in the substrate network
        :param vnode: the node in the virtual network
        :return:
        '''
        self.G_rsn.node[snode]['capacity'] = self.G_rsn.node[snode]['capacity'] - self.G_rvn.node[vnode]['request']
        if self.G_rsn.node[snode]['capacity'] < 0.0:
            self.logger.error(
                "@decrementNodeCapacityInSubstrateNetwork(..): <<node>> residual capacity = %f for snode: %d, vnode: %d, case of overprovisioned, plz debug node/link matching", self.G_rsn.node[snode]['capacity'], snode, vnode)
            return  # return immediately

    def mapVirtualLinksToSubstratePathswithCapacityUpdate(self):
        '''
        Computes a shortest path, with requested capacity, if exists, in the substrate network for each edge in the virtual network
        :return: None
        '''
        '''
        tasks:
        1. for each edge (u,v) in the G_rvn graph,
            i) find the matched substate network nodes (n1, n2)
            ii) copy the G_rsn network to G_temp, and remove all the edges with capacity < request from G_temp
            iii) find a shortest path from n1 to n2 in G_temp, if path exist, decrement link-'request' value from the link-'capacity' of
                each edge on this path  in substrate network, also decrement node 'capacity' by node'request' value

        '''
        self.rvn_to_rsn_link_mapping = {}
        self.virtual_nodes_covered = []
        for u1, u2, req_val in self.G_rvn.edges(data='request'):
            #----------------------path-finding part-----------------------------------------------
            n1 = self.rvn_to_rsn_node_mapping[u1]
            n2 = self.rvn_to_rsn_node_mapping[u2]

            if n1 == -1 or n2 == -1:
                self.logger.warning(
                    "@mapVirtualLinksToSubstratePaths: virtual to substrate network node mapping not found for edge %d-%d", n1, n2)
                return  # terminate immediately

            # build the G_temp graph
            G_temp = nx.Graph()
            G_temp.add_nodes_from(self.G_rsn.nodes())
            for v1, v2, cap_val in self.G_rsn.edges(data='capacity'):
                if cap_val >= req_val:
                    G_temp.add_edge(v1, v2)

            cur_shortest_path = None
            try:
                cur_shortest_path = nx.shortest_path(G=G_temp, source=n1, target=n2)
            except nx.NetworkXNoPath:
                self.rvn_to_rsn_link_mapping[(u1, u2)] = []
                self.logger.warning("@mapVirtualLinksToSubstratePaths: Shortest path between vnodes %d and %d not found", u1, u2)
            if cur_shortest_path:
                self.rvn_to_rsn_link_mapping[(u1, u2)] = list(cur_shortest_path)
                #-----------udpate the residual graph------------------
                #---link capacity udpate----------------------------------
                self.decrementCapacityOfPathInSubstrateNetwork(cur_shortest_path, req_val)
                #-----also mark the nodes as part of covered-----#
                self.virtual_nodes_covered.extend([ u1, u2])

        #now update the node capacities
        self.virtual_nodes_covered = list( set(self.virtual_nodes_covered)) #<--you could use this too
        for vnode, snode  in enumerate(self.rvn_to_rsn_node_mapping):
            self.decrementNodeCapacityInSubstrateNetwork(snode, vnode)


if __name__ == '__main__':
    pass

