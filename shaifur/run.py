'''
author: Md Shaifur Rahman (shaifur.at.buet@gmail.com)
date: Feb 27, 2017
Project: Virtual Network Embedding in FSO-linked Data-centers
'''
import random
import networkx as nx
import logging


from vne_embedding import VirtualNetworkEmbedding

#--------------------------utility methods-----------------------------------------------------

def createLogger(logLevel = logging.WARNING, loggerName = 'Virtual Network Embedding', add_log_file=False, log_file_name='vne_run.log'):
    '''
    Creates a console-output logger for  the given logLevel
    :param logLevel: must be one of the 5 log-level enums of logging class: DEBUG, INFO, WARNING, ERROR, CRITICAL
    :return:
    '''
    # create an instance of the logger
    logger = logging.getLogger(loggerName)
    logger.setLevel(logLevel)

    # create console handler and set level t
    ch = logging.StreamHandler()
    ch.setLevel(logLevel)

    # create file handler and set level
    fh = logging.FileHandler(log_file_name)
    fh.setLevel(logLevel)

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add formatter to all the handlers
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)

    # add handlers to the logger object
    logger.addHandler(ch)
    if add_log_file:  # <--set file-log during final run, currently no file-logging for fast turnaround
        logger.addHandler(fh)
    return logger


def generateSubstrateGraph(default_node_deg = 3):
    '''
    method to generate a custom substrate graph for test, currently a graph of 8 nodes
    :return: a networkx graph for the substrate network
    '''

    G = nx.Graph(title='ISN')
    for i in range(8):
        G.add_node(i, capacity = random.randint(5, 15), max_deg = default_node_deg)


    G.add_edge(0, 1, capacity = 10)
    G.add_edge(0, 2, capacity = 10)
    G.add_edge(1, 2, capacity = 10)

    G.add_edge(3, 4, capacity = 10)
    G.add_edge(3, 5, capacity = 10)
    G.add_edge(4, 5, capacity = 10)

    G.add_edge(6, 7, capacity = 10)

    G.add_edge(1, 3, capacity = 10)

    G.add_edge(5, 6, capacity = 10)

    return G

def generateVNERequest(no_of_nodes = 4, graph_title = 'VNE-Req', min_node_req = 0, max_node_req = 5, min_link_req = 0, max_link_req = 5):
    '''
    method to generate a custom VNE-Request graph for test, currently a complete graph of 4 nodes
    :param no_of_nodes: total no. of nodes in the VNE-Request graph
    :param graph_title: a graph title, might be used for request hashing etc. later
    :param min_link_req: a random link-capacity request generated >= this val
    :param max_link_req: a random link-capacity request generated <= this val
    :return: a networkx graph, a complete graph with no_of_nodes nodes and each edge
             has a request field int value between [min_link_req, max_link_req]
    '''
    G = nx.Graph(title = graph_title)
    for i in range(no_of_nodes):
        G.add_node(i, request = random.randint(min_node_req, max_node_req))


    for i in range(no_of_nodes-1):
        for j in range(i+1, no_of_nodes):
            G.add_edge(i, j, request = random.randint(min_link_req, max_link_req))

    return G
#-----------------------------------------------------------------------end of utility methods-----------------


if __name__ == '__main__':
    random.seed(1013397)

    logLevel = logging.WARNING
    loggerName = 'Virtual Network Embedding'
    log_file_name = 'vne_run.log'
    add_log_file = False
    logger = createLogger(logLevel = logLevel, loggerName = loggerName, add_log_file = add_log_file, log_file_name = log_file_name)

    for n in range(1):
        print "Iteration %d", n+1,"++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        vnee = VirtualNetworkEmbedding(ext_logger = logger)
        vnee.createSubstrateNetworkInstance(generateSubstrateGraph())
        vnee.generateVNERequest(generateVNERequest())

        # print vnee.G_rsn.nodes(data = 'capacity')
        # print vnee.G_rvn.nodes(data = 'request')

        vnee.mapVirtualNetworkNodesToSubstrateNodes()
        # print vnee.rvn_to_rsn_node_mapping

        vnee.mapVirtualLinksToSubstratePathswithCapacityUpdate()
        # print vnee.rvn_to_rsn_link_mapping
