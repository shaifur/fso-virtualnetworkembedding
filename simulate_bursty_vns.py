
import bursty_vn_embedding as MVN
import physical_network as PN
import vn_requests as VNR
from profiler import Profiler, addProfilerArgs, getProfilerArgs

from heapq import *
from random import random, shuffle, randint, seed
from math import log
from datetime import datetime
from collections import defaultdict
import argparse
import gc
import re


# TODO : reconfig delay, and short flow backbone

# TEMP
def waitForUser():
	pass
	# raw_input('Waiting... ')

class PoissonDistribution:
	def __init__(self, poisson_lambda):
		self.poisson_lambda = poisson_lambda

	def getDelta(self):
		return -log(1.0 - random()) / self.poisson_lambda

class DiscreteInterArrivalDistribution:
	# inter_arrival_probs is a list of 2-tuples, where each tuple is (p, t) and p is the probability of having an inter-arrival time of t
	def __init__(self, inter_arrival_probs):
		self.inter_arrival_probs = inter_arrival_probs

	def sum_weights(self):
		return sum(p for p, t in self.inter_arrival_probs)

	def getDelta(self):
		r = random() * self.sum_weights()
		for p, t in self.inter_arrival_probs:
			r -= p
			if r <= 0.0:
				return t
		raise Exception('Error generated random inter arrival time')

class DiscreteFlowSizeDistribution:
	# flow_size_probs is a list of 2-tuples, where each tuple is (p, s) and p is the probability of a flow having size s 
	def __init__(self, flow_size_probs):
		self.flow_size_probs = flow_size_probs

	def sum_weights(self):
		return sum(p for p,s in self.flow_size_probs)

	def getSize(self):
		r = random() * self.sum_weights()
		for p,s in self.flow_size_probs:
			r -= p
			if r <= 0.0:
				return s
		raise Exception('Error generated random flow size')

	def getDuration(self):
		r = random() * self.sum_weights()
		for p,s in self.flow_size_probs:
			r -= p
			if r <= 0.0:
				return s
		raise Exception('Error generated random flow size')

class RoutingScheme:
	use_all_paths = 'all'
	use_only_mapped_paths = 'map'
	prioritize_mapped_paths = 'pri'
	all_algos = [use_all_paths, use_only_mapped_paths, prioritize_mapped_paths]

	def __init__(self, str_value):
		if str_value not in RoutingScheme.all_algos:
			raise Exception('Inavlid routing scheme')
		self.algo_type = str_value

	def __str__(self):
		return self.algo_type

	def __eq__(self, other):
		if isinstance(other, self.__class__):
			return self.algo_type == other.algo_type
		else:
			return False

	def __ne__(self, other):
		return not self.__eq__(other)

	def __hash__(self):
		return hash((self.algo_type,))

class RealWorldData:
	def __init__(self, filename = ""):
		self.filename = (filename if filename != "default" else "")
		self.large_flow_size = None

		self.num_large_flow = None
		self.num_small_flow = None

		self.large_iat_avg = None
		self.small_iat_avg = None
		self.all_iat_avg = None

		self.large_iat_hist = None
		self.small_iat_hist = None
		self.all_iat_hist = None

		self.large_fs_avg = None
		self.small_fs_avg = None
		self.all_fs_avg = None

		self.large_fs_hist = None
		self.small_fs_hist = None
		self.all_fs_hist = None

	def convertHist(self, val):
		if val == 'poisson':
			return 'poisson'
		# BMIN->BMAX|NUM,...
		return map(lambda x: (tuple(map(float, x.split('|')[0].split('->'))), int(x.split('|')[1])), val.split(','))

	def process(self, default_flow_size = None, use_constant_flow_size = False):
		Profiler.start('RealWorldData_process')
		if self.filename != "":
			f = open(self.filename)
			
			for line in f:
				spl = line.split()
				if len(spl) == 0:
					continue
				if spl[0] == 'large_flow_size':
					self.large_flow_size = int(spl[1])
				elif spl[0] == 'num_large':
					self.num_large_flow = int(spl[1])
				elif spl[0] == 'avg_large_size':
					self.large_fs_avg = float(spl[1])
				elif spl[0] == 'large_size_hist':
					self.large_fs_hist = self.convertHist(spl[1])
				elif spl[0] == 'avg_large_iat':
					self.large_iat_avg = float(spl[1])
				elif spl[0] == 'large_iat_hist':
					self.large_iat_hist = self.convertHist(spl[1])
				elif spl[0] == 'num_small':
					self.num_small_flow = int(spl[1])
				elif spl[0] == 'avg_small_size':
					self.small_fs_avg = float(spl[1])
				elif spl[0] == 'small_size_hist':
					self.small_fs_hist = self.convertHist(spl[1])
				elif spl[0] == 'avg_small_iat':
					self.small_iat_avg = float(spl[1])
				elif spl[0] == 'small_iat_hist':
					self.small_iat_hist = self.convertHist(spl[1])
				elif spl[0] == 'avg_all_size':
					self.all_fs_avg = float(spl[1])
				elif spl[0] == 'all_size_hist':
					self.all_fs_hist = self.convertHist(spl[1])
				elif spl[0] == 'avg_all_iat':
					self.all_iat_avg = float(spl[1])
				elif spl[0] == 'all_iat_hist':
					self.all_iat_hist = self.convertHist(spl[1])

			# TODO use_constant_flow_size = TRUE
			# self.testGetDelta()
		else:
			self.flow_sizes = [default_flow_size]

			self.num_large_flow = 1
			self.num_small_flow = 0

			self.large_iat_hist = 'poisson'
			self.small_iat_hist = 'poisson'
			self.all_iat_hist = 'poisson'

			self.large_fs_avg = default_flow_size
			self.small_fs_avg = 0.0
			self.all_fs_avg = default_flow_size

			self.large_fs_hist =[((default_flow_size, default_flow_size), 1.0)]
			self.small_fs_hist =[((0.0, 0.0), 1.0)]
			self.all_fs_hist =[((default_flow_size, default_flow_size), 1.0)]

		Profiler.end('RealWorldData_process')

	def getAverageFlowDuration(self, link_rate_in_bytes_per_second):
		return self.all_fs_avg / link_rate_in_bytes_per_second

	def getAverageLongFlowDuration(self, link_rate_in_bytes_per_second):
		return self.large_fs_avg / link_rate_in_bytes_per_second

	def getLongFlowDistribution(self, link_rate_in_bytes_per_second):
		return lambda: self.getLargeFlowSize() / link_rate_in_bytes_per_second

	def getSmallFlowDistribution(self, link_rate_in_bytes_per_second):
		return lambda: self.getSmallFlowSize() / link_rate_in_bytes_per_second

	def getAllFlowDistribution(self, link_rate_in_bytes_per_second):
		return lambda: self.getAllFlowSize() / link_rate_in_bytes_per_second

	def getAverageSmallFlowDuration(self, link_rate_in_bytes_per_second):
		return self.small_fs_avg / link_rate_in_bytes_per_second

	def getPercentLargeFlows(self):
		if self.num_large_flow == None or self.num_small_flow == None:
			raise Exception('ERR with RWD')
		return float(self.num_large_flow) / float(self.num_small_flow + self.num_large_flow)

	def testGetDelta(self):
		nt = 10000
		avg_desired_iat = .003
		sum_iat = 0.0
		for i in range(nt):
			sum_iat += self.getLargeFlowDelta(average_inter_arrival_time = avg_desired_iat)

		if abs(avg_desired_iat - sum_iat / float(nt)) / avg_desired_iat > .01:
			print 'Desired avg:', avg_desired_iat
			print 'Actual avg: ', sum_iat / float(nt)
			raise Exception('Tests had a percent difference of: ' + str(abs(avg_desired_iat - sum_iat / float(nt)) / avg_desired_iat))

	def getLargeFlowDelta(self, average_inter_arrival_time = None):
		if self.large_iat_hist == 'poisson':
			return -log(1.0 - random()) * average_inter_arrival_time
		else:
			rand = randint(1, self.num_large_flow - 1)
			for (b_min, b_max), num in self.large_iat_hist:
				rand -= num
				if rand <= 0:
					return (random() * (b_max - b_min) + b_min) / self.large_iat_avg * average_inter_arrival_time

	def getLargeFlowSize(self):
		rand = randint(1, self.num_large_flow)

		for (b_min, b_max), num in self.large_fs_hist:
			rand -= num
			if rand <= 0:
				rv = random() * (b_max - b_min) + b_min

				return rv

		print self.num_large_flow, sum(num for (b_min, b_max), num in self.large_fs_hist)
		raise Exception('PROBLEM WITH RANDOM NUM GENERATION')

	def getSmallFlowSize(self):
		rand = randint(1, self.num_small_flow)

		for (b_min, b_max), num in self.small_fs_hist:
			rand -= num
			if rand <= 0:
				rv = random() * (b_max - b_min) + b_min

				return rv

		print self.num_large_flow, sum(num for (b_min, b_max), num in self.large_fs_hist)
		raise Exception('PROBLEM WITH RANDOM NUM GENERATION')

	def getAllFlowSize(self):
		r = randint(1, self.num_small_flow + self.num_large_flow)
		if r <= self.num_small_flow:
			return self.getSmallFlowSize()
		else:
			return self.getLargeFlowSize()

	def __eq__(self, other):
		if isinstance(other, self.__class__):
			return self.filename == other.filename
		elif isinstance(other, str):
			return self.filename == other
		else:
			return False

	def __ne__(self, other):
		return not self.__eq__(other)

	def __hash__(self):
		return hash((self.filename,))

	def __str__(self):
		if self.filename == "":
			return "default"
		else:
			return self.filename[self.filename.rfind('/') + 1:]

class Flow:
	def __init__(self, virtual_edge, flow_id, start_time, flow_size):
		self.virtual_edge = virtual_edge
		self.flow_id = flow_id

		self.start_time = start_time
		self.end_time = None

		self.flow_size = flow_size

		self.is_active = False
		self.was_active = False
		self.was_ended = False
		self.is_waiting_to_be_retried = False
		self.was_retried = False

		self.current_congestion = None
		self.current_bottleneck = []
		self.current_end_time = None
		self.current_flow_rate = None

		self.current_mapped_path = None
		self.path_used_length = None

	def calcCurrentBottleneck(self):
		return set(pe for pe in self.current_mapped_path.edges if pe.current_congestion == self.current_congestion)

	def __str__(self):
		s = '(VN:' + str(self.virtual_edge.node1.network.virtual_network_request.vn_request_id) + \
				', FID:' + str(self.flow_id) + \
				', ST:' + str(self.start_time) + \
				', FS:' + str(self.flow_size) + \
				', CC:' + str(self.current_congestion) + \
				', ET:' + str(self.current_end_time) + \
				', FR:' + str(self.current_flow_rate) + \
				', MP:' + str(self.current_mapped_path) + ')'
		return s

def generateLargeFlows(start_time, duration, virtual_edge, real_world_data, new = False):
	Profiler.start('generateFlows')
	if new:
		cur_time = start_time
		flows = []
		flow_id = 0

		while True:
			delta = real_world_data.getLargeFlowDelta(average_inter_arrival_time = virtual_edge.large_flow_inter_arrival_time)
			flow_size = real_world_data.getLargeFlowSize()
			next_start = cur_time + delta
			if next_start >= start_time + duration:
				break
			flows.append(Flow(virtual_edge, flow_id, next_start, flow_size))
			flow_id += 1
			cur_time = next_start
		Profiler.end('generateFlows')
		return flows
	else:
		raise Exception('OUT OF DATE')

# This version filters out paths that can't be realized AND that would result in a useless FSO
# Output:
#   (was_mapped_to_congestion_free_path, was_mapped_to_any_path)
def tryToMapFlowWithBandwidthSharing(time, flow, mappings, physical_network, longest_path_length, current_physical_edge_set_to_virtual_edge_mapping, active_flows, path_choice_by_length, edges_that_changed_congestion, avoid_impossible_edge_set, routing_scheme):
	Profiler.start('tryToMapFlowWithBandwidthSharing')
	# TODO use all possible paths, not just mapped paths
	if routing_scheme.algo_type == RoutingScheme.use_all_paths:
		physical_node1 = mappings[flow.virtual_edge.node1.network].virtual_node_to_physical_server_mapping[flow.virtual_edge.node1].node
		physical_node2 = mappings[flow.virtual_edge.node1.network].virtual_node_to_physical_server_mapping[flow.virtual_edge.node2].node
		all_pos_paths = list(physical_network.findPathsBetween(physical_node1, physical_node2, longest_path_length))
		all_pos_paths.sort(key = lambda x: x.length() + (random() * .25))
	
	if routing_scheme.algo_type == RoutingScheme.use_only_mapped_paths:	
		# List of paths that a virtual link was mapped to
		all_pos_paths = [p for p, w in mappings[flow.virtual_edge.node1.network].virtual_edge_to_physical_paths_mapping[flow.virtual_edge]]
		all_pos_paths.sort(key = lambda x: x.length() + (random() * .25))
	
	if routing_scheme.algo_type == RoutingScheme.prioritize_mapped_paths:
		# Check that those dicts exist and are up to date
		mapped_paths = mappings[flow.virtual_edge.node1.network].mapped_paths[flow.virtual_edge]
		mapped_paths.sort(key = lambda x: x.length() + (random() * .25))
		
		unmapped_paths = mappings[flow.virtual_edge.node1.network].unmapped_paths[flow.virtual_edge]
		unmapped_paths.sort(key = lambda x: x.length() + (random() * .25))

		all_pos_paths = mapped_paths + unmapped_paths
	else:
		unmapped_paths = None
		mapped_paths = None
	
	all_edges = set(edge for path in all_pos_paths for edge in path.edges)

	# An edge can make some other FSO useless IFF, it's not already active but could become active, and by doing so would make some edge_set have no valid options for active edges
	if avoid_impossible_edge_set:
		# TODO track this with each change instead of recomputing everytime
		Profiler.start('tryToMapFlowWithBandwidthSharing_would_edge_make_some_edge_set_useless')
		would_edge_make_some_edge_set_useless = {edge:(edge.can_edge_become_active and not edge.is_edge_already_active and
														any(adjacent_edge.getNotSharedEdgeSet(edge) not in current_physical_edge_set_to_virtual_edge_mapping and 
																all(tertiary_edge.getOtherEdgeSet(adjacent_edge.getNotSharedEdgeSet(edge)) in current_physical_edge_set_to_virtual_edge_mapping or
																	tertiary_edge.getOtherEdgeSet(adjacent_edge.getNotSharedEdgeSet(edge)) == edge.getNotSharedEdgeSet(adjacent_edge)
																		for tertiary_edge in adjacent_edge.getNotSharedEdgeSet(edge).edges
																			if tertiary_edge != adjacent_edge)
															for adjacent_edge in edge.getRelatedEdges()
																if adjacent_edge != edge))
													for edge in all_edges}
		Profiler.end('tryToMapFlowWithBandwidthSharing_would_edge_make_some_edge_set_useless')
	else:
		would_edge_make_some_edge_set_useless = {edge:False for edge in all_edges}

	was_mapped_to_congestion_free_path = False
	was_mapped_to_any_path = False

	# Goal of this path is to maximize the flow rate (by minimize congestion) given to this flow (greedy approach)
	path_to_map_to = None
	min_possible_congestion = None
	for p in all_pos_paths:
		if all((physical_edge.can_edge_become_active or physical_edge.is_edge_already_active) and not would_edge_make_some_edge_set_useless[physical_edge]
				for physical_edge in p.edges):
			this_path_congestion = (max(pe.current_congestion for pe in p.edges) + 1 if len(p.edges) > 0 else 1)
			if path_to_map_to == None or min_possible_congestion > this_path_congestion:
				path_to_map_to = p
				min_possible_congestion = this_path_congestion

	was_mapped_to_congestion_free_path = (min_possible_congestion == 1)
	was_mapped_to_mapped_path = (mapped_paths != None and path_to_map_to in mapped_paths)

	if path_to_map_to != None:
		Profiler.start('tryToMapFlowWithBandwidthSharing_actuallyMapFlowToPath')
		was_mapped_to_any_path = True
		path_choice_by_length[path_to_map_to.length()] += 1

		# Adds where the new virtual edge is mapped to
		active_flows.append(flow)
		flow.is_active = True
		flow.was_active = True
		flow.current_mapped_path = path_to_map_to
		any_edge_made_active = False
		for edge in path_to_map_to.edges:
			for edge_set in (edge.edge_set1, edge.edge_set2):
				# Set this edge to active
				if edge_set in current_physical_edge_set_to_virtual_edge_mapping:
					# This edge is already active
					if current_physical_edge_set_to_virtual_edge_mapping[edge_set][0] != edge:
						# TODO was raised once
						raise Exception('MULTIPLE EDGES IN EDGE SET IN USE')
					# Update the edge_set.tim
					edge_set.time_per_num_flows[len(current_physical_edge_set_to_virtual_edge_mapping[edge_set][1])] += time - edge_set.prev_update_time
					edge_set.prev_update_time = time

					# Add this flow to the set of mapped flows
					current_physical_edge_set_to_virtual_edge_mapping[edge_set][1].append(flow)
				else:
					# Update edge_set.time_per_num_flows
					edge_set.time_per_num_flows[0] += time - edge_set.prev_update_time
					edge_set.prev_update_time = time

					# Makes this edge the active edge
					current_physical_edge_set_to_virtual_edge_mapping[edge_set] = (edge, [flow])
					any_edge_made_active = True
					
					# Since this edge_set now has an active edge, none of it's edges can be set to active
					for e in edge_set.edges:
						e.can_edge_become_active = False

					# Mark this edge as active
					edge.is_edge_already_active = True


			edge.current_congestion += 1
			edge.current_mapped_flows.append(flow)
			if len(edge.current_mapped_flows) != len(current_physical_edge_set_to_virtual_edge_mapping[edge_set][1]):
				raise Exception('AAAAAA!!!!!')
			edges_that_changed_congestion.add(edge)

		Profiler.end('tryToMapFlowWithBandwidthSharing_actuallyMapFlowToPath')					
	
	Profiler.end('tryToMapFlowWithBandwidthSharing')
	return was_mapped_to_congestion_free_path, was_mapped_to_any_path, was_mapped_to_mapped_path

# TODO comment
def updateFlowAttributes(time, active_flows, heap, edges_that_changed_congestion, new_flows_with_path_length_0, physical_network_link_rate, reverse_event_types):
	Profiler.start('simulatevirtualNetworksWithBandwidthSharing_updateFlowLinkRate')
	updated_flows = []

	# Gets set of flows that need to have their attributes updated
	Profiler.start('simulatevirtualNetworksWithBandwidthSharing_updateFlowLinkRate_filterFlows')
	# This may be inefficient
	flows_to_check = set(flow for edge in edges_that_changed_congestion for flow in edge.current_mapped_flows
							if edge.current_congestion > flow.current_congestion or
									all(e in edges_that_changed_congestion for e in flow.current_bottleneck)) \
						| new_flows_with_path_length_0
	Profiler.end('simulatevirtualNetworksWithBandwidthSharing_updateFlowLinkRate_filterFlows')

	# CHECK that all new flows are in flows_to_check
	for k in active_flows:
		if k.current_congestion == None and k not in flows_to_check:
			raise Exception('New Flow not in flows_to_check')

	# Find the indexes of flows that are not-new
	Profiler.start('simulatevirtualNetworksWithBandwidthSharing_updateFlowLinkRate_findHeapIndex')
	current_heap_index_per_flow = {flow:(-1 if flow.current_congestion != None else None) for flow in flows_to_check}
	num_flows_left_to_index = len([flow for flow in flows_to_check if flow.current_congestion != None])
	for i, (t, et_id, vs) in enumerate(heap):
		if num_flows_left_to_index == 0:
			break
		if et_id == reverse_event_types['flow_end'] and vs[0] in flows_to_check:
			current_heap_index_per_flow[vs[0]] = i
			num_flows_left_to_index -= 1
	Profiler.end('simulatevirtualNetworksWithBandwidthSharing_updateFlowLinkRate_findHeapIndex')

	num_updated_flows = 0
	for flow in flows_to_check:
		Profiler.start('simulatevirtualNetworksWithBandwidthSharing_updateFlowLinkRate_computeCongestion')
		if flow.current_mapped_path.length() >= 1:
			this_path_congestion = max(pe.current_congestion for pe in flow.current_mapped_path.edges)
		else:
			this_path_congestion = 1
		Profiler.end('simulatevirtualNetworksWithBandwidthSharing_updateFlowLinkRate_computeCongestion')

		Profiler.start('simulatevirtualNetworksWithBandwidthSharing_updateFlowLinkRate_updateFlowAttributes')
		# heap_elements_to_add = []
		if flow.current_congestion == None or flow.current_congestion != this_path_congestion:
			this_flow_rate = physical_network_link_rate / float(this_path_congestion)
			if flow.current_end_time != None:
				this_remaining_size = (flow.current_end_time - time) * flow.current_flow_rate
			else:
				this_remaining_size = flow.flow_size
			this_flow_end_time = time + this_remaining_size / this_flow_rate

			is_new_flow = (flow.current_congestion == None)

			flow.current_flow_rate = this_flow_rate
			flow.current_end_time = this_flow_end_time
			flow.current_congestion = this_path_congestion
			flow.current_bottleneck = [pe for pe in flow.current_mapped_path.edges if pe.current_congestion == flow.current_congestion]

			Profiler.start('simulatevirtualNetworksWithBandwidthSharing_heap.update')
			if current_heap_index_per_flow[flow] != None:
				num_updated_flows += 1
				heap[current_heap_index_per_flow[flow]] = (flow.current_end_time, reverse_event_types['flow_end'], (flow,))
			else:
				if not is_new_flow:
					raise Exception('Adding element to heap for non-new_flow')
				num_updated_flows += 1
				heap.append((flow.current_end_time, reverse_event_types['flow_end'], (flow,)))
			Profiler.end('simulatevirtualNetworksWithBandwidthSharing_heap.update')
		else:
			# This can happen when a flow ends, which causes a retry flow to begin. In this case just update current_bottleneck
			flow.current_bottleneck = [pe for pe in flow.current_mapped_path.edges if pe.current_congestion == flow.current_congestion]
		Profiler.end('simulatevirtualNetworksWithBandwidthSharing_updateFlowLinkRate_updateFlowAttributes')

	if num_updated_flows > 0:
		Profiler.start('simulatevirtualNetworksWithBandwidthSharing_updateFlowLinkRate_heapify')
		heapify(heap)
		Profiler.end('simulatevirtualNetworksWithBandwidthSharing_updateFlowLinkRate_heapify')
	Profiler.end('simulatevirtualNetworksWithBandwidthSharing_updateFlowLinkRate')

	# Update the current_bottleneck fo flows that were partially affected
	flows_to_update_bottleneck = set(flow for edge in edges_that_changed_congestion for flow in edge.current_mapped_flows
										if flow not in flows_to_check and (
												edge.current_congestion == flow.current_congestion or
												edge in flow.current_bottleneck))
	for flow in flows_to_update_bottleneck:
		flow.current_bottleneck = [pe for pe in flow.current_mapped_path.edges if pe.current_congestion == flow.current_congestion]

# TODO comment
# TODO add in reconfiguration delay for FSOs
def simulatevirtualNetworksWithBandwidthSharing(schedule, physical_network, virtual_network_requests, longest_path_length, avoid_impossible_edge_set, routing_scheme, real_world_data, no_flow_completion_times = False, status_file = None):
	Profiler.start('simulatevirtualNetworksWithBandwidthSharing')
	Profiler.start('simulation-setup')
	print 'RUNNING NEW SIMULATOR EXPERIMENTAL'
	# Used to sort events that occur at the same time
	event_types = {0: 'flow_start', 1: 'flow_end', 2: 'end', 3: 'accept', 4: 'change', 5: 'reject'}
	reverse_event_types = {event_types[k]:k for k in event_types}

	# Heap with values of (time, type, vals), where
	#   time - float - time of event
	#   type - int (that's a key in event_types) - type of event, use event_types for more info
	#   vals - tuple - a tuple with related values to this event. For the following events:
	#       accept - 2-tuple, (vnr_id, single_mapping)
	#		change - 2-tuple, (vnr_id, changed_single_mapping)
	#       reject - 1-tuple, (vnr_id)
	#       end - 1-tuple, (vnr_id)
	#       flow_start - 2-tuple, (Flow) the virtual edge where a flow is starting and flow_id is a unique identifier for the flow for this virtual edge
	#       flow_end - 2-tuple, (Flow) the virutal edge where a flow is ending and flow_id is a unique identifier for the flow for this virtual edge
	# Builds the initial heap
	heap = [(t, reverse_event_types[et], ((vnr_id, mapping) if et in ('accept', 'change') else (vnr_id,))) for t, et, vnr_id, mapping in schedule.events]
	heapify(heap)

	# Dict where key is virtual_network, and value is mapping of that virtual_network to the physical_network
	mappings = {}

	physical_network_link_rate = physical_network.getLinkRateInBytesPerSecond()

	# At current time, contains the physical edge sets that are being used (along with the associated virtual edges)
	# Keys are PhysicalEdgeSet, Value is 2-tuple (PhysicalEdge, list of Flows)
	current_physical_edge_set_to_virtual_edge_mapping = {}

	# List of all flos in the network
	all_flows = []
	
	# Elements are Flow that are currently active in the network
	active_flows = []
	
	num_accepted_flows = 0
	num_rejected_flows = 0

	# Only matter when RoutingAlgo is prioritize_mapped_paths
	num_flows_mapped_to_mapped_paths = 0
	num_flows_mapped_to_unmapped_paths = 0

	# List of flows that were ended early due to their VN ending
	preempted_flows = []
	
	# Start and end time of the test
	start_measure_time = 0.0
	end_measure_time = 0.0
	
	# Holds the number of times a flow is mapped to a path of a certain length
	path_choice_by_length = defaultdict(int)
	
	# Retry flows that have absolutely no options
	flows_to_retry = []
	
	# Key is VirtualEdge
	flow_size_by_edge = {}

	# Key is (VirtualEdge, flow_id)
	current_path_congestion = {}
	current_flow_end_time = {}
	current_flow_rate = {}

	flow_start_and_end_times = defaultdict(dict)


	# Count useless edge_sets
	calculate_utility_of_edge_sets = False
	if calculate_utility_of_edge_sets:
		# Statistics on overall congestion levels and network utilization
		prev_time = 0.0
		utility_per_edge_set = {edge_set:{'impossible_to_use':0.0, 'possible_to_use':0.0, 'in_use':0.0} for edge_set in physical_network.getAllPhysicalEdgeSets()}

	for physical_edge in physical_network.getAllPhysicalEdges():
		# True if edge isn't active, but could be
		physical_edge.can_edge_become_active = True
		
		# True if this edge is active
		physical_edge.is_edge_already_active = False

		# Current number of flows mapped to the edge
		physical_edge.current_congestion = 0

		# List of currently mapped flows to the PhysicalEdge
		physical_edge.current_mapped_flows = []

	# List of PhysicalEdges that changed in congestion. This is used when updating the rate given to any flow's
	edges_that_changed_congestion = set()

	# List of newly mapped flows that are mapped with a path of length 0
	new_flows_with_path_length_0 = set()

	# Updates The percent congestion on the physical network
	for edge_set in physical_network.getAllPhysicalEdgeSets():
		edge_set.time_per_num_flows = defaultdict(int)
		edge_set.prev_update_time = 0.0

	Profiler.end('simulation-setup')
	status_file.write('start simulation')

	# Performs the 'next' event in the heap
	while len(heap) > 0:
		time, event_type_id, vals = heappop(heap)
		event_type = event_types[event_type_id]

		# Clears the list without wasting memory
		edges_that_changed_congestion.clear()
		new_flows_with_path_length_0.clear()

		if time > end_measure_time:
			end_measure_time = time
			status_file.write('Advancing to time %d' % time)

		# TODO clean this up, because it is the current bottleneck!!!!!
		Profiler.start('simulation-pre_iter_check')
		if calculate_utility_of_edge_sets and time > prev_time:
			delta_time = time - prev_time
			prev_time = time

			# Compute 'utilization' of edge_sets
			for edge_set in physical_network.getAllPhysicalEdgeSets():
				if edge_set in current_physical_edge_set_to_virtual_edge_mapping and len(current_physical_edge_set_to_virtual_edge_mapping[edge_set][1]) > 0:
				 	utility_per_edge_set[edge_set]['in_use'] += delta_time
				else:
				 	possible_to_use = False
				 	for edge in edge_set.edges:
				 		other_edge_set = edge.getOtherEdgeSet(edge_set)
				 		if other_edge_set not in current_physical_edge_set_to_virtual_edge_mapping or len(current_physical_edge_set_to_virtual_edge_mapping[other_edge_set][1]) <= 0:
				 			possible_to_use = True
				 	if possible_to_use:
				 		utility_per_edge_set[edge_set]['possible_to_use'] += delta_time
				 	else:
				 		utility_per_edge_set[edge_set]['impossible_to_use'] += delta_time

		Profiler.end('simulation-pre_iter_check')

		if event_type == 'accept':
			Profiler.start('simulation-accept')
			# When a new VN is accepted, generate flows
			vnr_id, mapping = vals
			vnr = schedule.vn_requests[vnr_id]
			virtual_network = vnr.virtual_network
			mappings[virtual_network] = mapping
			this_real_world_data = vnr.real_world_data

			if routing_scheme.algo_type == RoutingScheme.prioritize_mapped_paths:
				mappings[virtual_network].computePrioritizedPaths()

			for virtual_edge in virtual_network.getAllVirtualEdges():
				flows = generateLargeFlows(time, vnr.duration, virtual_edge, this_real_world_data, new = True)
				for flow in flows:
					heappush(heap, (flow.start_time, reverse_event_types['flow_start'], (flow,)))
					all_flows.append(flow)
			Profiler.end('simulation-accept')
		elif event_type == 'change':
			Profiler.start('simulation-change')
			# All this does is change the mapping for this virtual network. Any existing flows don't have their paths changed.
			vnr_id, mapping = vals
			vnr = schedule.vn_requests[vnr_id]
			virtual_network = vnr.virtual_network

			mappings[virtual_network] = mapping

			if routing_scheme.algo_type == RoutingScheme.prioritize_mapped_paths:
				mappings[virtual_network].computePrioritizedPaths()
			Profiler.end('simulation-change')
		elif event_type == 'end':
			Profiler.start('simulation-end')
			# Finds all flows associated with this VNR, then updates the flow_end such that they end at the same time as the VNR
			vnr_id, = vals
			vnr = schedule.vn_requests[vnr_id]

			# Remove active Flows
			this_flows = [flow for flow in active_flows if flow.virtual_edge.node1.network == vnr.virtual_network]
			if len(this_flows) > 0:

				# Finds the index of the flow_end events for this VNR
				current_heap_index_per_flow = {flow:None for flow in this_flows}
				num_flows_left_to_index = len(this_flows)
				for i, (t, et_id, vs) in enumerate(heap):
					if num_flows_left_to_index == 0:
						break
					if et_id == reverse_event_types['flow_end'] and vs[0] in this_flows:
						current_heap_index_per_flow[vs[0]] = i
						num_flows_left_to_index -= 1

				# Updates the flow's flow_end events
				for flow in this_flows:
					preempted_flows.append(flow)

					# Update the flow size to only the amount that was fufilled
					remaining_size = (flow.current_end_time - time) * flow.current_flow_rate
					flow.flow_size -= remaining_size
					flow.current_end_time = time
					
					if current_heap_index_per_flow[flow] != None:
						# print 'UPDATING FLOW:', flow
						heap[current_heap_index_per_flow[flow]] = (flow.current_end_time, reverse_event_types['flow_end'], (flow,))
					else:
						print next(((t, et, vs) for t, et, vs in heap if event_types[et] == 'flow_end' and vs[0] == flow), None)
						print 'VE:', flow.virtual_edge, '\nFID:',flow.flow_id, '\nST:', flow.start_time, '\nET:', flow.end_time, '\nFS:', flow.flow_size, '\nis_active:', flow.is_active, '\nwas_active:', flow.was_active, '\nwas_ended:', flow.was_ended, '\ncur_congestion:', flow.current_congestion, '\ncur_bottleneck:', flow.current_bottleneck, '\ncurrent_end_time:', flow.current_end_time, '\ncurrent_flow_rate:', flow.current_flow_rate, '\ncurrent_mapped_path:', flow.current_mapped_path
						raise Exception('missing element')
				heapify(heap)

			# Remove Flows waiting to be retried
			flows_to_remove = [flow for flow in flows_to_retry if flow.virtual_edge.node1.network == vnr.virtual_network]
			for flow in flows_to_remove:
				flows_to_retry.remove(flow)
			Profiler.end('simulation-end')
		elif event_type == 'reject':
			# Currently nothing happens when a VN is rejected
			pass
		elif event_type == 'flow_start':
			Profiler.start('simulation-flow_start')
			# Try to find a path to map this flow to
			flow, = vals
			was_mapped_to_congestion_free_path, was_mapped_to_any_path, was_mapped_to_mapped_path = \
						tryToMapFlowWithBandwidthSharing(time, flow, mappings, physical_network, longest_path_length,
								current_physical_edge_set_to_virtual_edge_mapping,
								active_flows,
								path_choice_by_length,
								edges_that_changed_congestion,
								avoid_impossible_edge_set,
								routing_scheme)

			if was_mapped_to_congestion_free_path:
				num_accepted_flows += 1
			else:
				num_rejected_flows += 1

			if was_mapped_to_any_path:
				if was_mapped_to_mapped_path:
					num_flows_mapped_to_mapped_paths += 1
				else:
					num_flows_mapped_to_unmapped_paths += 1

			if not was_mapped_to_any_path:
				flows_to_retry.append(flow)
				flow.is_waiting_to_be_retried = True
			elif flow.current_mapped_path.length() == 0:
				new_flows_with_path_length_0.add(flow)
			updateFlowAttributes(time, active_flows, heap, edges_that_changed_congestion, new_flows_with_path_length_0, physical_network_link_rate, reverse_event_types)
			Profiler.end('simulation-flow_start')
		elif event_type == 'flow_end':
			Profiler.start('simulation-flow_end')
			# If the flow was successfully mapped, then it removes the mapping so the resources can be reused
			flow, = vals
			any_edges_switched_to_inactive = False
			if flow.current_mapped_path != None:
				flow.end_time = time
				active_flows.remove(flow)
				flow.is_active = False
				flow.was_ended = True

				# Don't delete this because 1) it doesn't affect other mappings, and 2) it could be useful to look a what paths where actually used at the end
				path = flow.current_mapped_path
				for edge in path.edges:
					for edge_set in (edge.edge_set1, edge.edge_set2):
						# Confirm this edge_set is being used by this virtual_edge
						if flow not in current_physical_edge_set_to_virtual_edge_mapping[edge_set][1]:
							raise Exception('Problem with mapping')
						
						# Update edge_set.time_per_num_flows
						edge_set.time_per_num_flows[len(current_physical_edge_set_to_virtual_edge_mapping[edge_set][1])] += time - edge_set.prev_update_time
						edge_set.prev_update_time = time

						# Removes this flow
						current_physical_edge_set_to_virtual_edge_mapping[edge_set][1].remove(flow)

						# If that was the last flow mapped to it, then the edge_set no longer has an active edge
						if len(current_physical_edge_set_to_virtual_edge_mapping[edge_set][1]) == 0:
							del current_physical_edge_set_to_virtual_edge_mapping[edge_set]
							
							# Updates status of edges
							for e in edge_set.edges:
								# We know that edge_set isn't in current_physical_edge_set_to_virtual_edge_mapping, so we only have to check the other one
								if e.getOtherEdgeSet(edge_set) not in current_physical_edge_set_to_virtual_edge_mapping:
									e.can_edge_become_active = True
									any_edges_switched_to_inactive = True

							# Mark this edge as no longer active
							edge.is_edge_already_active = False
					edge.current_congestion -= 1
					edge.current_mapped_flows.remove(flow)
					if len(edge.current_mapped_flows) != len((current_physical_edge_set_to_virtual_edge_mapping[edge_set][1] if edge_set in current_physical_edge_set_to_virtual_edge_mapping else [])):
						raise Exception('NOOOOOO')
					edges_that_changed_congestion.add(edge)
				# Saves the length this path was mapped to
				flow.path_used_length = flow.current_mapped_path.length()
				flow.current_mapped_path = None
			
				updateFlowAttributes(time, active_flows, heap, edges_that_changed_congestion, new_flows_with_path_length_0, physical_network_link_rate, reverse_event_types)
			# Retry flows that had to wait
			# Only retry flows once SOME edge was switched from active to inactive, because if the topology doesn't changed we know that all flows in flows_to_retry can't possibly be mapped
			# TODO only retry flows that could be affected by edges that were switched from inactive to active
			if len(flows_to_retry) > 0 and any_edges_switched_to_inactive:
				# Only retry flows that share an edge with the flow that ended
				Profiler.start('simulatevirtualNetworksWithBandwidthSharing_retryFlowMap')
				flows_that_were_mapped = []
				edges_that_changed_congestion.clear()
				for flow in flows_to_retry:
					was_mapped_to_congestion_free_path, was_mapped_to_any_path, was_mapped_to_mapped_path = \
						tryToMapFlowWithBandwidthSharing(time, flow, mappings, physical_network, longest_path_length,
								current_physical_edge_set_to_virtual_edge_mapping,
								active_flows,
								path_choice_by_length,
								edges_that_changed_congestion,
								avoid_impossible_edge_set, routing_scheme)				
					if was_mapped_to_any_path:
						
						if was_mapped_to_mapped_path:
							num_flows_mapped_to_mapped_paths += 1
						else:
							num_flows_mapped_to_unmapped_paths += 1

						flows_that_were_mapped.append(flow)
						flow.is_waiting_to_be_retried = False
						flow.was_retried = True
						if flow.current_mapped_path.length() == 0:
							new_flows_with_path_length_0.add(flow)
						updateFlowAttributes(time, active_flows, heap, edges_that_changed_congestion, new_flows_with_path_length_0, physical_network_link_rate, reverse_event_types)
				for flow in flows_that_were_mapped:
					flows_to_retry.remove(flow)
				Profiler.end('simulatevirtualNetworksWithBandwidthSharing_retryFlowMap')
			Profiler.end('simulation-flow_end')
		else:
			print 'ERROR IN SIMULATOR, UNEXPECTED EVENT_TYPE:', event_type

		# Update any flows that may have had their rates changed (this includes flows that were just added and technically don't have a rate yet)
		Profiler.start('simulation-post_flow_update')
		if event_type in ('flow_start', 'flow_end'):
			updateFlowAttributes(time, active_flows, heap, edges_that_changed_congestion, new_flows_with_path_length_0, physical_network_link_rate, reverse_event_types)
		Profiler.end('simulation-post_flow_update')

	status_file.write('end simulation')

	# Make sure all edge.time_per_num_flows are updated to end_measure_time
	for edge_set in physical_network.getAllPhysicalEdgeSets():
		if edge_set.prev_update_time < end_measure_time:
			if edge_set in current_physical_edge_set_to_virtual_edge_mapping:
				edge_set.time_per_num_flows[len(current_physical_edge_set_to_virtual_edge_mapping[edge_set][1])] += end_measure_time - edge_set.prev_update_time
			else:
				edge_set.time_per_num_flows[0] += end_measure_time - edge_set.prev_update_time
			edge_set.prev_update_time = end_measure_time
			

	Profiler.start('simulation-metric_evalulation')
	# Flow Completion time
	# TODO filter out those that started before reaching a 'steady' state
	flow_completion_times = [flow.end_time - flow.start_time
								for flow in all_flows
									if flow.was_active and flow.was_ended]
	# Sample some fixed number of completion times at regular intervals
	print 'Num Flows', len(flow_completion_times)
	print 'Num Negative Flows:', len([v for v in flow_completion_times if v <= 0.0])
	flow_completion_times.sort()

	# Overall Average Throughput for the network (i.e. the amount of data transfered divided by length of the test)

	total_physical_network_throughput = sum(flow.flow_size * flow.path_used_length for flow in all_flows if flow.was_active and flow.was_ended)
	total_virtual_network_throughput = sum(flow.flow_size for flow in all_flows if flow.was_active and flow.was_ended)
	if (end_measure_time - start_measure_time) != 0.0:
		average_vn_throughput_per_second = total_virtual_network_throughput / (end_measure_time - start_measure_time)
		average_pn_throughput_per_second = total_physical_network_throughput / (end_measure_time - start_measure_time)
	else:
		average_vn_throughput_per_second = 0.0
		average_pn_throughput_per_second = 0.0
	print 'Average VN Throughput:', physical_network.convertFromBytes(average_vn_throughput_per_second)
	print 'Average PN Throughput:', physical_network.convertFromBytes(average_pn_throughput_per_second)

	# New Congestion calculation
	total_time_with_no_congestion = sum(v for edge_set in physical_network.getAllPhysicalEdgeSets() for k, v in edge_set.time_per_num_flows.iteritems() if k <= 1)
	total_time_with_congestion = sum(v for edge_set in physical_network.getAllPhysicalEdgeSets() for k, v in edge_set.time_per_num_flows.iteritems() if k > 1)
	if (total_time_with_congestion + total_time_with_no_congestion) != 0.0:
		percent_congestion = total_time_with_congestion / (total_time_with_congestion + total_time_with_no_congestion)
		percent_no_congestion = total_time_with_no_congestion / (total_time_with_congestion + total_time_with_no_congestion)
	else:
		percent_congestion = 0
		percent_no_congestion = 0

	print 'Congestion%:', percent_congestion
	print 'No Congestion%:', percent_no_congestion

	if calculate_utility_of_edge_sets:
		# Compute Edge Set utilization
		total_impossible_to_use_time = sum(utility_per_edge_set[edge_set]['impossible_to_use'] for edge_set in utility_per_edge_set)
		total_possible_to_use_time = sum(utility_per_edge_set[edge_set]['possible_to_use'] for edge_set in utility_per_edge_set)
		total_in_use_time = sum(utility_per_edge_set[edge_set]['in_use'] for edge_set in utility_per_edge_set)
		
		if (total_impossible_to_use_time + total_possible_to_use_time + total_in_use_time) != 0.0:
			percent_impossible_to_use = total_impossible_to_use_time / (total_impossible_to_use_time + total_possible_to_use_time + total_in_use_time)
			percent_possible_to_use = total_possible_to_use_time / (total_impossible_to_use_time + total_possible_to_use_time + total_in_use_time)
			percent_in_use = total_in_use_time / (total_impossible_to_use_time + total_possible_to_use_time + total_in_use_time)
		else:
			percent_impossible_to_use = 0.0
			percent_possible_to_use = 0.0
			percent_in_use = 0.0

		print 'Impossible%:', percent_impossible_to_use
		print 'Possible%:', percent_possible_to_use
		print 'In Use%:', percent_in_use

	if sum(flow.end_time - flow.start_time for flow in all_flows if flow.was_active and flow.was_ended) != 0.0:
		avg_flow_rate = sum(flow.flow_size for flow in all_flows if flow.was_active and flow.was_ended) \
						 / sum(flow.end_time - flow.start_time for flow in all_flows if flow.was_active and flow.was_ended)
	else:
		avg_flow_rate = 0.0	
	print 'Avg Flow Rate:', avg_flow_rate

	if routing_scheme.algo_type == RoutingScheme.prioritize_mapped_paths:
		percent_flows_using_mapped_paths = 0
		if num_flows_mapped_to_mapped_paths + num_flows_mapped_to_unmapped_paths > 0:
			percent_flows_using_mapped_paths = float(num_flows_mapped_to_mapped_paths) / float(num_flows_mapped_to_mapped_paths + num_flows_mapped_to_unmapped_paths)
		print 'Flows using mapped paths%:', percent_flows_using_mapped_paths

	simulator_data = {'num_flows_accepted': num_accepted_flows,
						'num_flows_rejected': num_rejected_flows,
						'path_choice_by_length': ','.join([str(l) + ':' + str(path_choice_by_length[l]) for l in sorted(path_choice_by_length)]),
						
						# VN throughput is the amount of data sent over virtual links (so it is affected by path_length)
						'total_vn_throughput': total_virtual_network_throughput,
						'average_vn_throughput_per_second': average_vn_throughput_per_second,

						# PN throughput is the amount of data transfered over physical links (i.e. physical network utilization)
						'total_pn_throughput': total_physical_network_throughput,
						'average_pn_throughput_per_second': average_pn_throughput_per_second,

						# percent_congestion is the % of time that physical_edges had 2 or more flows active on them
						'percent_congestion': percent_congestion,
						# percent_no_congestion is the % of time that physcial_edges had no or exactly 1 flow active on them
						'percent_no_congestion': percent_no_congestion,

						# percent_impossible_to_use is the % of time where an edge_set didn't have an active edge AND couldn't have an active edge (because all neighboring edge_sets already had an active edge)
						# 'percent_impossible_to_use': percent_impossible_to_use,
						# percent_possible_to_use is the % of time where an edge_set didn't have an active edge but could if necessary
						# 'percent_possible_to_use': percent_possible_to_use,
						# percent_in_use is the % of time where an edge_set was actively in use
						# 'percent_in_use': percent_in_use,

						'avg_flow_rate': avg_flow_rate}
	simulator_col_hdrs = ['num_flows_accepted', 'num_flows_rejected',
							'path_choice_by_length',
							'total_vn_throughput', 'average_vn_throughput_per_second',
							'total_pn_throughput', 'average_pn_throughput_per_second',
							'percent_congestion', 'percent_no_congestion',
							'avg_flow_rate']
	
	if not no_flow_completion_times:
		simulator_data['flow_completion_times'] = ','.join(map(str, flow_completion_times))
		simulator_col_hdrs.append('flow_completion_times')

	if routing_scheme.algo_type == RoutingScheme.prioritize_mapped_paths:
		simulator_data['percent_flows_using_mapped_paths'] = percent_flows_using_mapped_paths
		simulator_col_hdrs.append('percent_flows_using_mapped_paths')

	Profiler.end('simulation-metric_evalulation')
	Profiler.end('simulatevirtualNetworksWithBandwidthSharing')
	return simulator_data, simulator_col_hdrs

# TODO comment
def simulatePhysicalEdgeCollision(schedule, physical_network, virtual_network_requests, user_physical_edge_collision_rate, flow_size, physical_edge_bandwidth):
	# Used to sort events that occur at the same time
	Profiler.start('simulatePhysicalEdgeCollision')
	event_types = {0:'flow_end', 1:'flow_start', 2: 'end', 3: 'accept', 4: 'reject'}
	reverse_event_types = {event_types[k]:k for k in event_types}

	# Heap with values of (time, type, vals), where
	#   time - float - time of event
	#   type - int (that's a key in event_types) - type of event, use event_types for more info
	#   vals - tuple - a tuple with related values to this event. For the following events:
	#       accept - 2-tuple, (vnr_id, single_mapping)
	#       reject - 1-tuple, (vnr_id)
	#       end - 1-tuple, (vnr_id)
	#       flow_start - 1-tuple, (VirtualEdge, flow_id) the virtual edge where a flow is starting and flow_id is a unique identifier for the flow for this virtual edge
	#       flow_end - 1-tuple, (VirtualEdge, flow_id) the virutal edge where a flow is ending and flow_id is a unique identifier for the flow for this virtual edge
	# Builds the initial heap
	heap = [(t, reverse_event_types[et], ((vnr_id, mapping) if et == 'accept' else (vnr_id,))) for t, et, vnr_id, mapping in schedule.events]
	heapify(heap)

	# Dict where key is virtual_network, and value is mapping of that virtual_network to the physical_network
	mappings = {}

	physical_network_link_rate = physical_network.getLinkRate()

	# At the current time, the mapping of virtual edges with an active flow to physical paths
	# Keys are Virtual Edge, Values are dict with key of int (that are flow_ids) and values of PhysicalPath
	current_virtual_edge_to_path_mapping = defaultdict(dict)

	# At current time, contains the physical edge sets that are being used (along with the associated virtual edges)
	# Keys are PhysicalEdgeSet, Values are list of 2-tuple (VirtualEdge, flow_id) (each of which is using the key edge set)
	current_physical_edge_set_to_virtual_edge_mapping = defaultdict(list)

	# Key is physical_edge, value is time (either with collision or without collision)
	time_per_edge_no_congestion = defaultdict(float)
	time_per_edge_congestion = defaultdict(float)

	# Times that flows start on a physical edge
	# Key is physical_edge, value is list of when flows arrive
	arrival_times_per_edge = defaultdict(list)

	prev_time = 0.0

	# Performs the 'next' event in the heap
	while len(heap) > 0:
		time, event_type_id, vals = heappop(heap)

		# Increment time_per_edge_no_congestion and time_per_edge_congestion
		if time != prev_time:
			for physical_edge in physical_network.getAllPhysicalEdges():
				if len(set(current_physical_edge_set_to_virtual_edge_mapping[physical_edge.edge_set1] + current_physical_edge_set_to_virtual_edge_mapping[physical_edge.edge_set2])) <= 1:
					# No congestion
					time_per_edge_no_congestion[physical_edge] += time - prev_time
				else:
					# Congestion
					time_per_edge_congestion[physical_edge] += time - prev_time

		event_type = event_types[event_type_id]
		# When a new VN is accepted, generate flows
		if event_type == 'accept':
			vnr_id, mapping = vals
			vnr = schedule.vn_requests[vnr_id]
			virtual_network = vnr.virtual_network
			mappings[virtual_network] = mapping
			flow_size_distribution = DiscreteFlowSizeDistribution([(p, s / physical_network_link_rate) for p,s in vnr.flow_size_distribution])

			for virtual_edge in virtual_network.getAllVirtualEdges():
				flow_times = generateFlows(time, vnr.duration, PoissonDistribution(1.0 / virtual_edge.inter_arrival_time), flow_size_distribution)
				for t, ft, fid in flow_times:
					heappush(heap, (t, reverse_event_types[ft], (virtual_edge, fid)))
		elif event_type == 'end':
			# Currently nothing happens when a VN is removed (since flow_starts and ends are done separately)
			pass
		elif event_type == 'reject':
			# Currently nothing happens when a VN is rejected
			pass
		elif event_type == 'flow_start':
			# Try to find a path to map this flow to
			virtual_edge, flow_id = vals
			sum_weights = sum(w for p, w in mappings[virtual_edge.node1.network].virtual_edge_to_physical_paths_mapping[virtual_edge])
			random_weight = random() * sum_weights

			path_to_map_to = None
			for p, w in mappings[virtual_edge.node1.network].virtual_edge_to_physical_paths_mapping[virtual_edge]:
				if random_weight < w:
					path_to_map_to = p
					break
				random_weight -= w

			if path_to_map_to == None:
				raise Exception('Couldn\'t find a path to map to')
			else:
				# Accepted flow
				current_virtual_edge_to_path_mapping[virtual_edge][flow_id] = path_to_map_to
				for edge in path_to_map_to.edges:
					arrival_times_per_edge[edge].append(time)
					for edge_set in (edge.edge_set1, edge.edge_set2):
						current_physical_edge_set_to_virtual_edge_mapping[edge_set].append((virtual_edge, flow_id))
		elif event_type == 'flow_end':
			# If the flow was successfully mapped, then it removes the mapping so the resources can be reused
			virtual_edge, flow_id = vals
			if virtual_edge in current_virtual_edge_to_path_mapping and flow_id in current_virtual_edge_to_path_mapping[virtual_edge]:
				# Don't delete this because 1) it doesn't affect other mappings, and 2) it could be useful to look a what paths where actually used at the end
				path = current_virtual_edge_to_path_mapping[virtual_edge][flow_id]
				for edge in path.edges:
					for edge_set in (edge.edge_set1, edge.edge_set2):
						# Confirm this edge_set is being used by this virtual_edge
						if (virtual_edge, flow_id) not in current_physical_edge_set_to_virtual_edge_mapping[edge_set]:
							raise Exception('Problem with mapping')
						current_physical_edge_set_to_virtual_edge_mapping[edge_set].remove((virtual_edge, flow_id))

			# Retry adding flows that were rejected
		else:
			print 'Unhandled event_type:', event_type
		prev_time = time

	total_congestion_time = sum(time_per_edge_congestion[pe] for pe in time_per_edge_congestion)
	total_no_congestion_time = sum(time_per_edge_no_congestion[pe] for pe in time_per_edge_no_congestion)

	
	inter_arrival_times = [sum(arrival_times_per_edge[pe][i + 1] - arrival_times_per_edge[pe][i] for i in range(len(arrival_times_per_edge[pe]) - 1)) / (len(arrival_times_per_edge[pe]) - 1) for pe in arrival_times_per_edge]
	print 'Congestion Percent:      ', total_congestion_time / (total_congestion_time + total_no_congestion_time)
	print 'User Congestion Percent: ', user_physical_edge_collision_rate
	# print 'Max average edge arrival rate:', min(inter_arrival_times)
	# print 'Calculated maximum arrival rate:', MVN.computeMaxEdgeInterArrivalTime(user_physical_edge_collision_rate, flow_size / physical_edge_bandwidth)

	# arrival_no_congestion, arrival_congestion = 0.0, 0.0
	# for pe in arrival_times_per_edge:
	# 	no_congestion, congestion = computeCongestionFromArrivalTimes(arrival_times_per_edge[pe], flow_size / physical_edge_bandwidth, prev_time)
	# 	arrival_no_congestion += no_congestion
	# 	arrival_congestion += congestion

	# print 'Congestion percent based arrival times ONLY:', arrival_congestion / (arrival_congestion + arrival_no_congestion)
	Profiler.end('simulatePhysicalEdgeCollision')

def computeCongestionFromArrivalTimes(arrival_times, flow_duration, end_time):
	Profiler.start('computeCongestionFromArrivalTimes')
	events = sorted([(t + (0.0 if e == 'start' else flow_duration), e, i) for i, t in enumerate(arrival_times) for e in ('start', 'end')] + [(end_time, 'end_test', -1)])
	num_events = defaultdict(float)
	cur_events = set()
	prev_t = 0.0
	total_time = 0.0
	for t, e, i in events:
		num_events[len(cur_events)] += t - prev_t
		if e == 'start':
			cur_events.add(i)
		if e == 'end':
			cur_events.remove(i)
		if e == 'end_test':
			break
		prev_t = t
	Profiler.end('computeCongestionFromArrivalTimes')
	return num_events[0] + num_events[1], sum(num_events[k] for k in num_events if k >= 2)

def debug_main():
	schedule, physical_network, virtual_network_requests, params = MVN.readSchedule('schedule_1.txt')

	simulatevirtualNetworksWithBandwidthSharing(schedule, physical_network, virtual_network_requests)

def addSimulatorArgs(parser):
	# TODO add in reconfiguration delay
	parser.add_argument('-k', '--file_name_identifier', metavar = 'FILE_NAME_IDENTIFIER', type = str, nargs = 1, default = [''], help = 'string that is put repaces \'K\' in the different generated files')
	parser.add_argument('-sim_out','--simulator_out_file', metavar = 'SIMULATOR_OUT_FILE', type = str, nargs = 1, default = ['data/simulator_data_K.txt'],help = 'File to write simulator test results to. Data is appended to the end of the file, so the same file can be reused for multiple tests')
	parser.add_argument('-avoid_impos_es', '--avoid_impossible_edge_set', action = 'store_true', help = 'If given, then the simulator will avoid choosing an edge which directly causes some edge set to become useless')
	parser.add_argument('-routing','--routing_scheme', metavar = 'ROUTING_SCHEME', type = RoutingScheme, nargs = 1, default = [RoutingScheme('pri')],help = 'Scheme used to map flows to physical paths. Must be a value from (' + ', '.join(RoutingScheme.all_algos) + ')')
	parser.add_argument('-rwd', '--real_world_data', metavar = 'REAL_WORLD_DATA', type = RealWorldData, nargs = '+', default = [RealWorldData()], help = 'Real data to use when simulating (contains both flow sizes and arrival_times)')
	parser.add_argument('-ucfs', '--use_constant_flow_size', action = 'store_true', help = 'If given, will not use real world flow sizes, but instead will use the supplied constant flow_size')
	parser.add_argument('-no_fct', '--do_not_report_flow_completion_times', action = 'store_true', help = 'If given, will not record flow completion times')


def getSimulatorArgs(args, default_flow_size = None):
	file_name_identifier = args.file_name_identifier[0]
	simulator_out_file = args.simulator_out_file[0]
	avoid_impossible_edge_set = args.avoid_impossible_edge_set
	routing_scheme = args.routing_scheme[0]
	real_world_data = args.real_world_data
	use_constant_flow_size = args.use_constant_flow_size
	no_flow_completion_times = args.do_not_report_flow_completion_times

	for rwd in real_world_data:
		rwd.process(default_flow_size = flow_size, use_constant_flow_size = use_constant_flow_size)

	this_params = {'file_name_identifier': ('str', file_name_identifier),
					'simulator_out_file': ('str', simulator_out_file),
					'avoid_impossible_edge_set': ('bool', str(avoid_impossible_edge_set)),
					'routing_scheme': ('RoutingScheme', str(routing_scheme)),
					'real_world_data': ('list(RealWorldData)', ','.join(map(str, real_world_data))),
					'large_flow_size': ('float', str(rwd.large_flow_size)),
					'use_constant_flow_size': ('bool', str(use_constant_flow_size)),
					'no_flow_completion_times': ('bool', str(no_flow_completion_times))}

	return this_params, file_name_identifier, simulator_out_file, avoid_impossible_edge_set, routing_scheme, real_world_data, rwd.large_flow_size, use_constant_flow_size, no_flow_completion_times

# TODO comment
def writeSimulatorData(simulator_out_file, args_data, schedule_data, simulator_data, all_col_hdrs, test_num, params, this_physical_out_file, this_virtual_out_file, this_schedule_out_file, ilp_simulator_data = None):
	Profiler.start('writeSimulatorData')
	f = open(simulator_out_file, 'a')

	args_data, args_hdrs = args_data
	schedule_data, schedule_hdrs = schedule_data
	simulator_data, simulator_hdrs = simulator_data

	if ilp_simulator_data != None:
		ilp_simulator_data, ilp_simulator_hdrs = ilp_simulator_data


	if len(all_col_hdrs) == 0:
		# write params
		f.write('\nTEST_PARAMS ' + ' '.join(['|'.join([var, params[var][0], params[var][1]]) for var in params]) + '\n')

		# write '--------------------------------------------------------------------------------'
		f.write('-' * 80 + '\n')

		# write col_hdrs (and also set it) for simulator data
		all_col_hdrs += args_hdrs + schedule_hdrs + simulator_hdrs + ([] if ilp_simulator_data == None else ilp_simulator_hdrs) + ['pn_file', 'vnr_file', 'schedule_file']

		f.write(' '.join(all_col_hdrs) + '\n')

	# write data in order of the col_hdrs
	vs = []
	for k in all_col_hdrs:
		if k in ['pn_file', 'vnr_file', 'schedule_file']:
			if k == 'pn_file':
				vs.append((this_physical_out_file if this_physical_out_file != '' else 'None'))
			elif k == 'vnr_file':
				vs.append((this_virtual_out_file if this_virtual_out_file != '' else 'None'))
			elif k == 'schedule_file':
				vs.append((this_schedule_out_file if this_schedule_out_file != '' else 'None'))
			else:
				raise Exception('ERRORRR!!!!!!!!!')
		elif k in args_hdrs:
			vs.append(str(args_data[k]))
		elif k in schedule_hdrs:
			vs.append(str(schedule_data[k]))
		elif k in simulator_hdrs:
			vs.append(str(simulator_data[k]))
		elif ilp_simulator_data != None and k in ilp_simulator_hdrs:
			vs.append(str(ilp_simulator_data[k]))
		else:
			raise Exception('ERROR!!!! Unknown column header: ' + str(k))

	f.write(' '.join(vs) + '\n')
	f.close()
	Profiler.end('writeSimulatorData')

# TODO comment
def convertValue(val_type, val):
	if val_type == 'int':
		if val in ('None', ''):
			return None
		else:
			return int(val)
	elif val_type == 'float':
		if val == 'None':
			return None
		return float(val)
	elif val_type == 'bool':
		return bool(val)
	elif val_type == 'str':
		return val
	elif val_type == 'ConvertVirtualEdgeCollisionToPhysicalEdgeCollisionAlgorithm':
		return MVN.ConvertVirtualEdgeCollisionToPhysicalEdgeCollisionAlgorithm(val)
	elif val_type == 'PathWeightingScheme':
		return MVN.PathWeightingScheme(val)
	elif val_type == 'list(PathWeightingScheme)':
		if ',' in val:
			return map(MVN.PathWeightingScheme, val.split(','))
		else:
			return MVN.PathWeightingScheme(val)
	elif val_type == 'ComputeMaxInterArrivalTimeScheme':
		return MVN.ComputeMaxInterArrivalTimeScheme(val)
	elif val_type == 'list(int)':
		if ',' in val:
			return map(int, val.split(','))
		else:
			return int(val)
	elif val_type == 'list(float)':
		if ',' in val:
			return map(float, val.split(','))
		else:
			return float(val)
	elif val_type == 'list(str)':
		if ',' in val:
			return val.split(',')
		else:
			return val
	elif val_type == 'RoutingScheme':
		return RoutingScheme(val)
	elif val_type == 'list(RealWorldData)':
		if ',' in val:
			return map(RealWorldData, val.split(','))
		else:
			return RealWorldData(val)
	elif val_type == 'list(PreemptionScheme)':
		if ',' in val:
			return map(MVN.PreemptionScheme, val.split(','))
		else:
			return MVN.PreemptionScheme(val)
	elif val_type == 'list(NodeMappingScheme)':
		if ',' in val:
			return map(MVN.NodeMappingScheme, val.split(','))
		else:
			return MVN.NodeMappingScheme(val)
	elif val_type == 'list(ILPScheme)':
		if ',' in val:
			return map(MVN.ILPScheme, val.split(','))
		else:
			return MVN.ILPScheme(val)
	elif val_type == 'RealVirtualTopologyData':
		return VNR.RealVirtualTopologyData(val)
	elif val_type == 'EmbeddingScheme':
		return MVN.EmbeddingScheme(val)
	elif val_type == 'list(LinkMappingScheme)':
		if ',' in val:
			return map(MVN.LinkMappingScheme, val.split(','))
		else:
			return MVN.LinkMappingScheme(val)
	elif val_type == 'list(PNSize)':
		return map(lambda x: tuple(map(int, x.split(','))), val.split(';'))
	else:
		print 'Error in convertvalue:', val_type, val

def try_pws_scheme(val):
	try:
		MVN.PathWeightingScheme(val)
		return True
	except:
		return False

def try_toy_network_param(val):
	if any(re.match(regex, val) for regex in PN.toy_physical_network_params_acceptable_values[2:]):
		return True

def try_convert(val):
	if val == 'None':
		return None
	elif val in ['univ1_data.txt', 'default']:
		return RealWorldData(val)
	elif val[-4:] == '.txt':
		return val
	elif try_pws_scheme(val):
		return MVN.PathWeightingScheme(val)
	elif try_toy_network_param(val):
		return val
	elif val in MVN.ILPScheme.all_algos:
		return MVN.ILPScheme(val)
	elif val in MVN.LinkMappingScheme.all_algos:
		return MVN.LinkMappingScheme(val)
	elif ':' in val:
		return {try_convert(x.split(':')[0]):try_convert(x.split(':')[1]) for x in val.split(',')}
	elif ',' in val:
		return [try_convert(x) for x in val.split(',')]
	elif '.' in val:
		return float(val)
	else:
		return int(val)

# TODO comment
def readInSimulatorDataFile(filename):
	f = open(filename)

	params = []
	data = []

	this_params = None
	this_data = None
	this_col_hdrs = None

	next_line_col_hdrs = False

	for line in f:
		if line == '-' * 80 + '\n':
			next_line_col_hdrs = True
			continue
		spl = line.split()
		if len(spl) == 0:
			continue

		if spl[0] == 'TEST_PARAMS':
			if this_params != None and this_data != None:
				params.append(this_params)
				data.append(this_data)

			this_data = []
			this_params = {v_name:convertValue(v_type, v_val) for v_name, v_type, v_val in map(lambda x: x.split('|'), spl[1:])}
		elif next_line_col_hdrs:
			next_line_col_hdrs = False
			this_col_hdrs = spl
		else:
			# TEMP
			curly_start = next((i for i,v in enumerate(spl) if '{' in v), None)
			curly_end = next((i for i,v in enumerate(spl) if '}' in v), None)
			if curly_start != None and curly_end != None:
				spl = spl[:curly_start] + [''.join(spl[curly_start:(curly_end + 1)])[1:-1]] + spl[(curly_end + 1):]

			this_case = {v_name:try_convert(val) for v_name, val in zip(this_col_hdrs, spl)}
			this_data.append(this_case)

	if this_params != None and this_data != None:
		params.append(this_params)
		data.append(this_data)

	return params, data

class StatusFile:
	def __init__(self, fname):
		if fname != '':
			self.f = open(fname, 'w')
			self.write('Opened!!!')
		else:
			self.f = None

	def write(self, str_val):
		if self.f != None:
			self.f.write('[' +str(datetime.now()) + '] ' + str_val + '\n')
			self.f.flush()

	def close(self):
		self.f.close()
		self.f = None

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description = 'Embeds a series of virtual network requests onto a shared physical network using a Poisson model based VN, then simulates the VNs with the embedding')
	
	# Allows for experiments to be repeated (especially in the case of a bug)
	rseed = randint(1,1000000)
	print 'Random seed is', rseed
	seed(seed)

	# Physical Network params
	PN.addPhysicalNetworkArgs(parser)

	# Virtual Network Request params
	VNR.addVirtualNetworkRequestArgs(parser)

	# Schedule Params
	MVN.addScheduleArgs(parser)

	# Simulator Params
	addSimulatorArgs(parser)
	addProfilerArgs(parser)

	# General Params
	parser.add_argument('-nt', '--num_tests', metavar = 'NUM_TESTS', type = int, nargs = 1, default = [1], help = 'The number of tests to run')

	args = parser.parse_args()

	# Physical params
	# physical_num_nodes, num_servers, num_edge_sets, physical_num_edges
	physical_params, physical_network_sizes, server_cpu, physical_node_to_node_bandwidth, server_to_node_bandwidth, toy_physical_network_params, physical_out_file, num_backbone_trees = PN.getPhysicalNetworkArgs(args)

	# Virtual params
	virtual_params, num_vns, virtual_num_nodes, virtual_link_percent, vn_arrival_gap, vn_duration, virtual_cpu_requirement, virtual_edge_bandwidth, flow_size, real_vn_data, real_vn_sizes, test_duration, virtual_network_arrival_rate, virtual_out_file = VNR.getVirtualNetworkRequestArgs(args)

	# Schedule params
	schedule_params, max_virtual_edge_collision, conversion_algorithm, max_physical_edge_collision, embedding_scheme, link_mapping_scheme, node_mapping_scheme, path_weighting_scheme, preemption_scheme, ilp_scheme, ilp_max_size, compute_max_inter_arrival_time_scheme, longest_path_length, schedule_out_file = MVN.getScheduleArgs(args)
	
	# Simulator params
	simulator_params, file_name_identifier, simulator_out_file, avoid_impossible_edge_set, routing_scheme, real_world_data, large_flow_size, use_constant_flow_size, no_flow_completion_times = getSimulatorArgs(args, default_flow_size = flow_size)
	
	status_file = StatusFile('data/status/status_K.txt'.replace('K', file_name_identifier))
	status_file.write('start')
	for this_ilp_scheme in ilp_scheme:
		this_ilp_scheme.status_file = status_file

	profiler_params, profile_code = getProfilerArgs(args)
	if profile_code:
		Profiler.turnOn()
	Profiler.start('simulate_bursty_vns.main')

	# Test params
	num_tests = args.num_tests[0]

	params = {'num_tests': ('int', str(num_tests))}
	params.update(physical_params)
	params.update(virtual_params)
	params.update(schedule_params)
	params.update(simulator_params)

	overall_col_hdrs = []

	args = [(this_physical_num_nodes, this_num_edge_sets, this_num_servers, this_physical_num_edges,
					this_virtual_num_nodes, this_real_vn_size, this_virtual_network_arrival_rate, this_max_virtual_edge_collision, this_max_physical_edge_collision, this_longest_path_length,
					this_link_mapping_scheme, this_node_mapping_scheme, this_path_weighting_scheme, this_preemption_scheme, this_ilp_scheme, this_real_world_data, this_num_backbone_trees,
					this_toy_physical_network_params)
				for this_physical_num_nodes, this_num_edge_sets, this_num_servers, this_physical_num_edges in physical_network_sizes
				for this_virtual_num_nodes in virtual_num_nodes
				for this_real_vn_size in real_vn_sizes
				for this_virtual_network_arrival_rate in virtual_network_arrival_rate
				for this_max_virtual_edge_collision in max_virtual_edge_collision
				for this_max_physical_edge_collision in max_physical_edge_collision
				for this_longest_path_length in longest_path_length
				for this_link_mapping_scheme in link_mapping_scheme
				for this_node_mapping_scheme in node_mapping_scheme
				for this_path_weighting_scheme in path_weighting_scheme
				for this_preemption_scheme in preemption_scheme
				for this_ilp_scheme in ilp_scheme
				for this_real_world_data in real_world_data
				for this_num_backbone_trees in num_backbone_trees
				for this_toy_physical_network_params in toy_physical_network_params]
	
	test_id = 1
	start_time = datetime.now()
	for i in range(num_tests):
		for this_physical_num_nodes, this_num_edge_sets, this_num_servers, this_physical_num_edges, \
				this_virtual_num_nodes, this_real_vn_size, this_virtual_network_arrival_rate, this_max_virtual_edge_collision, this_max_physical_edge_collision, this_longest_path_length, \
				this_link_mapping_scheme, this_node_mapping_scheme, this_path_weighting_scheme, this_preemption_scheme, this_ilp_scheme, this_real_world_data, this_num_backbone_trees, \
				this_toy_physical_network_params in args:
			print '\nRunning test', test_id, 'out of', num_tests * len(args)
			this_args = {}
			this_args_col_hdrs = []
			if len(physical_network_sizes) > 1:
				this_args['physical_num_nodes'] = this_physical_num_nodes
				this_args_col_hdrs.append('physical_num_nodes')

				this_args['num_edge_sets'] = this_num_edge_sets
				this_args_col_hdrs.append('num_edge_sets')

				this_args['num_servers'] = this_num_servers
				this_args_col_hdrs.append('num_servers')

				this_args['physical_num_edges'] = this_physical_num_edges
				this_args_col_hdrs.append('physical_num_edges')
			if len(virtual_num_nodes) > 1:
				this_args['virtual_num_nodes'] = this_virtual_num_nodes
				this_args_col_hdrs.append('virtual_num_nodes')
			if len(real_vn_sizes) > 1:
				this_args['real_vn_sizes'] = this_real_vn_size
				this_args_col_hdrs.append('real_vn_sizes')
			if len(virtual_network_arrival_rate) > 1:
				this_args['virtual_network_arrival_rate'] = this_virtual_network_arrival_rate
				this_args_col_hdrs.append('virtual_network_arrival_rate')
			if len(max_virtual_edge_collision) > 1:
				this_args['max_virtual_edge_collision'] = this_max_virtual_edge_collision
				this_args_col_hdrs.append('max_virtual_edge_collision')
			if len(max_physical_edge_collision) > 1:
				this_args['max_physical_edge_collision'] = this_max_physical_edge_collision
				this_args_col_hdrs.append('max_physical_edge_collision')
			if len(longest_path_length) > 1:
				this_args['longest_path_length'] = this_longest_path_length
				this_args_col_hdrs.append('longest_path_length')
			if len(link_mapping_scheme) > 1:
				this_args['link_mapping_scheme'] = this_link_mapping_scheme
				this_args_col_hdrs.append('link_mapping_scheme')
			if len(node_mapping_scheme) > 1:
				this_args['node_mapping_scheme'] = this_node_mapping_scheme
				this_args_col_hdrs.append('node_mapping_scheme')
			if len(path_weighting_scheme) > 1:
				this_args['path_weighting_scheme'] = this_path_weighting_scheme
				this_args_col_hdrs.append('path_weighting_scheme')
			if len(preemption_scheme) > 1:
				this_args['preemption_scheme'] = this_preemption_scheme
				this_args_col_hdrs.append('preemption_scheme')
			if len(ilp_scheme) > 1:
				this_args['ilp_scheme'] = this_ilp_scheme
				this_args_col_hdrs.append('ilp_scheme')
			if len(real_world_data) > 1:
				this_args['real_world_data'] = this_real_world_data
				this_args_col_hdrs.append('real_world_data')
			if len(num_backbone_trees) > 1:
				this_args['num_backbone_trees'] = this_num_backbone_trees
				this_args_col_hdrs.append('num_backbone_trees')
			if len(toy_physical_network_params) > 1:
				this_args['toy_physical_network_params'] = this_toy_physical_network_params
				this_args_col_hdrs.append('toy_physical_network_params')

			if len(this_args) > 0:
				print ', '.join([k + ': ' + str(this_args[k]) for k in this_args])
				status_file.write('args: ' + ', '.join([k + ': ' + str(this_args[k]) for k in this_args]))

			if this_physical_num_nodes > 64:
				this_path_weighting_scheme.random_num_paths = 5
			else:
				this_path_weighting_scheme.random_num_paths = 50

			this_virtual_link_percent = virtual_link_percent
			if this_virtual_num_nodes == 2 and virtual_link_percent > 0:
				this_virtual_link_percent = 1.0


			physical_network = PN.generatePhysicalNetwork(this_physical_num_nodes, this_num_servers, this_num_edge_sets, this_physical_num_edges, server_cpu, physical_node_to_node_bandwidth, server_to_node_bandwidth, make_backbone = this_num_backbone_trees, toy_param = this_toy_physical_network_params)
			status_file.write('generated physical network')

			vn_requests = VNR.generate_vn_requests(num_vns, this_virtual_num_nodes, this_virtual_link_percent, vn_arrival_gap, vn_duration, virtual_cpu_requirement, virtual_edge_bandwidth, this_real_world_data, real_vn_data = real_vn_data, real_vn_size = this_real_vn_size, test_duration = test_duration, vn_arrival_rate = this_virtual_network_arrival_rate)
			status_file.write('generated vnrs')

			this_mpec = MVN.getMaxPhysicalEdgeCollision(this_max_virtual_edge_collision, conversion_algorithm, this_max_physical_edge_collision, physical_network, this_longest_path_length)

			schedule, schedule_data = MVN.createVnEmbeddingSchedule(physical_network, vn_requests, this_mpec, embedding_scheme, this_link_mapping_scheme, this_node_mapping_scheme, this_path_weighting_scheme, this_preemption_scheme, this_ilp_scheme, compute_max_inter_arrival_time_scheme, this_longest_path_length, this_real_world_data, return_data = True, status_file = status_file)

			# Write physical_network
			this_physical_out_file = physical_out_file.replace('X', str(test_id)).replace('K', file_name_identifier)
			PN.writePhysicalNetwork(physical_network, this_physical_out_file)

			# Write vn_requests
			this_virtual_out_file = virtual_out_file.replace('X', str(test_id)).replace('K', file_name_identifier)
			VNR.write_vn_requests(vn_requests, this_virtual_out_file)

			# schedule
			this_schedule_out_file = schedule_out_file.replace('X', str(test_id)).replace('K', file_name_identifier)
			MVN.writeSchedule(schedule, this_schedule_out_file, this_physical_out_file, this_virtual_out_file, MVN.algorithm_version, params)

			# Run simulation
			simulator_data = simulatevirtualNetworksWithBandwidthSharing(schedule, physical_network, vn_requests, this_longest_path_length, avoid_impossible_edge_set, routing_scheme, this_real_world_data, no_flow_completion_times = no_flow_completion_times, status_file = status_file)

			if this_ilp_scheme.schedule != None and this_ilp_scheme.shouldRunILPFlowSimulation(num_racks = physical_network.getNumberOfNodes()):
				# Run simulation using ILP embedding
				ilp_simulator_data = simulatevirtualNetworksWithBandwidthSharing(this_ilp_scheme.schedule, physical_network, vn_requests, this_longest_path_length, avoid_impossible_edge_set, routing_scheme, this_real_world_data, no_flow_completion_times = no_flow_completion_times)
				
				# Prepend 'ilp_' to all value names in ilp_simulator_data
				ilp_simulator_data, ilp_simulator_hdrs = ilp_simulator_data
				ilp_simulator_data = {(this_ilp_scheme.algo_type + '_' + k): ilp_simulator_data[k] for k in ilp_simulator_data}
				ilp_simulator_hdrs = [this_ilp_scheme.algo_type + '_' + v for v in ilp_simulator_hdrs]
				ilp_simulator_data = (ilp_simulator_data, ilp_simulator_hdrs)
			else:
				ilp_simulator_data = None

			this_simulator_out_file = simulator_out_file.replace('K', file_name_identifier)
			writeSimulatorData(this_simulator_out_file, (this_args, this_args_col_hdrs), schedule_data, simulator_data, overall_col_hdrs, test_id, params, this_physical_out_file, this_virtual_out_file, this_schedule_out_file, ilp_simulator_data = ilp_simulator_data)
			
			del physical_network
			del vn_requests
			del schedule
			gc.collect()

			cur_time = datetime.now()
			time_per_test = (cur_time - start_time) / test_id

			print 'Average test length ->', time_per_test

			test_id += 1

	status_file.close()
	Profiler.end('simulate_bursty_vns.main')

	# output Profiler statistics
	Profiler.stats()
