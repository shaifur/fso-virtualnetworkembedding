
from profiler import Profiler

from random import shuffle, sample, choice, random
import argparse
from collections import defaultdict
import math
import re

# Container of all Objects related to a Physical network
class PhysicalNetwork:
	def __init__(self, num_nodes):
		# Dictionary of nodes, indexed by id_num
		if num_nodes != None:
			self.nodes = {i:PhysicalNode(self, i) for i in range(num_nodes)}
		else:
			self.nodes = {}

		# BURSTY max edge constraint
		self.max_edge_inter_arrival_time = None

		# Dictionary of all paths, indexed by (source_node_id, dest_node_id). Contains all possible paths of up to some maximum length
		self.all_paths = defaultdict(list)
		# self.all_paths2 = defaultdict(list)
		
		# Units for the link_rate given for the physical network. Currently in Gigabits
		self.is_bandwidth_bytes = False
		self.is_bandwidth_bits = True

		# K is 1000
		# M is 1000 * 1000
		# G is 1000 * 1000 * 1000
		self.bandwidth_units = 1000 * 1000 * 1000

		# BURSTY related to backbone network
		self.has_backbone = False
		self.all_backbone_paths = None

	# Returns the networks link rate in Bytes/Sec
	def getLinkRateInBytesPerSecond(self):
		lr = float(self._getLinkRate())
		if self.is_bandwidth_bits:
			lr = lr / 8.0
		return int(lr * self.bandwidth_units)

	# Converts v from bytes to whatever unit the link rate of the network is
	def convertFromBytes(self, v):
		if self.is_bandwidth_bits:
			v = v * 8.0
		return v / self.bandwidth_units

	# PRIVATE - returns the lowest bandwidth of edges in the network. 
	def _getLinkRate(self):
		return min(e.bandwidth for nid in self.nodes for esid in self.nodes[nid].edge_sets for e in self.nodes[nid].edge_sets[esid].edges)

	# Returns the link rate between PhysicalServers and PhysicalNodes in Bytes/Sec
	def getServerToRackLinkRateInBytesPerSecond(self):
		v = self._getServerToRackLinkLinkRate()
		if v == None:
			return None
		lr = float(v)
		if self.is_bandwidth_bits:
			lr = lr / 8.0
		return int(lr * self.bandwidth_units)

	# PRIVATE - returns the lowest bandwidth between server and node.
	def _getServerToRackLinkLinkRate(self):
		return min(server.bandwidth_to_node for server in self.getAllServers())

	# Returns a generator for all servers in the network
	def getAllServers(self):
		return (self.nodes[i].servers[j] for i in self.nodes for j in self.nodes[i].servers)

	def setMaxEdgeInterArrivalTime(self, max_edge_inter_arrival_time):
		self.max_edge_inter_arrival_time = max_edge_inter_arrival_time

	def setMaxBackboneEdgeInterArrivalTime(self, max_backbone_edge_inter_arrival_time):
		self.max_backbone_edge_inter_arrival_time = max_backbone_edge_inter_arrival_time

	def setMaxServerToNodeEdgeInterArrivalTime(self, max_server_to_node_edge_inter_arrival_time):
		self.max_server_to_node_edge_inter_arrival_time = max_server_to_node_edge_inter_arrival_time

	# Performs a breadth-first search from both node1 and node2, and then combines paths from each search that share a destination to make paths from node1 to node2.
	def computePathsBetween(self, node1, node2, longest_path):
		Profiler.start('PhysicalNetwork.computePathsBetween')
		if node1 == node2:
			Profiler.end('PhysicalNetwork.computePathsBetween')
			return [PhysicalPath([node1], [])]
		# List of nodes that start at node1
		from_node1 = [PhysicalPath([node1], [])]

		# List of nodes that start at node2
		from_node2 = [PhysicalPath([node2], [])]

		# List of nodes from node1 to node2 
		node1_to_node2 = []

		for j in range(1, longest_path + 1):
			# Figures out which side to expand from
			if j % 2 == 1:
				this_from = from_node1
			else:
				this_from = from_node2

			new_paths = []

			# Expands all paths in this_from by one hop
			for p in this_from:
				last_node = p.getLastNode()
				last_edge_set = p.getLastEdgeSet()
				if last_node == None:
					raise Exception('Unexpected "None" node')

				for edge_set_id in last_node.edge_sets:
					next_edge_set = last_node.edge_sets[edge_set_id]

					# Can't have two edges from the edge_set in a path
					if next_edge_set == last_edge_set:
						continue
					for next_edge in next_edge_set.edges:
						next_node = next_edge.getOtherNode(last_node)

						# Can't have loops
						if p.nodeInPath(next_node):
							continue

						new_paths.append(p.expandPath(next_node, next_edge))

			# Updates the path lists
			if j % 2 == 1:
				from_node1 = new_paths
			else:
				from_node2 = new_paths

			# TODO come up with a better algorithm to merge the lists
			# Finds pairs of paths in each list that share the same last node, and the combines them to make a path from node1 to node2
			from_node1_by_last_node_id = defaultdict(list)
			for p in from_node1:
				from_node1_by_last_node_id[p.getLastNode().id_num].append(p)

			from_node2_by_last_node_id = defaultdict(list)
			for p in from_node2:
				from_node2_by_last_node_id[p.getLastNode().id_num].append(p)

			for k in from_node1_by_last_node_id:
				if k in from_node2_by_last_node_id:
					for p1 in from_node1_by_last_node_id[k]:
						for p2 in from_node2_by_last_node_id[k]:
							# Makes sure that combining the two paths doesn't make a cycle, and that the path doesn't use the same edge set twice
							if p1.getLastEdgeSet() != p2.getLastEdgeSet() and all(p1_node not in p2.nodes[:-1] for p1_node in p1.nodes):
								new_path = PhysicalPath(p1.nodes + p2.nodes[:-1][::-1],p1.edges + p2.edges[::-1])
								node1_to_node2.append(new_path)
		Profiler.end('PhysicalNetwork.computePathsBetween')
		return node1_to_node2

	def computePathsFrom(self, node1, longest_path):
		Profiler.start('PhysicalNetwork.computePathsFrom')
		from_node1 = [PhysicalPath([node1], [])]

		self.all_paths[(node1, node1)].append(from_node1[0])

		for j in range(1, longest_path + 1):
			new_paths = []

			# Expands all paths in this_from by one hop
			for p in from_node1:
				last_node = p.getLastNode()
				last_edge_set = p.getLastEdgeSet()
				if last_node == None:
					raise Exception('Unexpected "None" node')

				for _, next_edge_set in last_node.edge_sets.iteritems():

					# Can't have two edges from the edge_set in a path
					if next_edge_set == last_edge_set:
						continue
					for next_edge in next_edge_set.edges:
						next_node = next_edge.getOtherNode(last_node)

						# Can't have loops
						if p.nodeInPath(next_node):
							continue

						new_paths.append(p.expandPath(next_node, next_edge))
			from_node1 = new_paths
			for p in new_paths:
				self.all_paths[(node1, p.getLastNode())].append(p)
		Profiler.end('PhysicalNetwork.computePathsFrom')

	def computeAllPaths(self, longest_path):
		Profiler.start('PhysicalNetwork.computeAllPaths')
		# for n1 in self.nodes:
		# 	node1 = self.nodes[n1]
		# 	for n2 in self.nodes:
		# 		node2 = self.nodes[n2]
		# 		self.all_paths[(node1,node2)] = self.computePathsBetween(node1, node2, longest_path)

		for _, node1 in self.nodes.iteritems():
			self.computePathsFrom(node1, longest_path)

		# for n1, n2 in self.all_paths:
		# 	ps1 = self.all_paths[(n1, n2)]
		# 	ps2 = self.all_paths2[(n1, n2)]

		# 	if len(ps1) != len(ps2):
		# 		print len(ps1), len(ps2)
		# 		raise Exception('A')

		# 	for p1 in ps1:
		# 		if len([p2 for p2 in ps2 if p1.compare(p2)]) != 1:
		# 			raise Exception('B')
		Profiler.end('PhysicalNetwork.computeAllPaths')

	def findPathsBetween(self, node1, node2, longest_path = None):
		if len(self.all_paths) == 0:
			if longest_path == None:
				raise Exception('Must provide lpl the first time paths are needed')
			self.computeAllPaths(longest_path)
		return self.all_paths[(node1, node2)]

	def computeBackbonePathsBetween(self, node1, node2, only_shortest_paths = True):
		Profiler.start('PhysicalNetwork.computeBackbonePathsBetween')
		if node1 == node2:
			Profiler.end('PhysicalNetwork.computeBackbonePathsBetween')
			return [PhysicalPath([node1], [])]

		# List of nodes that start from node1 and node2 respectively
		from_node1 = [PhysicalPath([node1], [])]
		from_node2 = [PhysicalPath([node2], [])]

		node1_to_node2 = []

		j = 0
		while (not only_shortest_paths and not(len(from_node1) == 0 and len(from_node2) == 0)) or (only_shortest_paths and len(node1_to_node2) == 0):
			j += 1
			if j % 2 == 1:
				this_from = from_node1
			else:
				this_from = from_node2

			new_paths = []
			# Expands all paths in this_from by one hop
			for p in this_from:
				last_node = p.getLastNode()
				last_backbone_edge_set = p.getLastEdgeSet()
				if last_node == None:
					raise Exception('Unexpected "None" node')

				for backbone_edge_set_id in last_node.backbone_edge_sets:
					next_backbone_edge_set = last_node.backbone_edge_sets[backbone_edge_set_id]

					# Can't have two edges from the edge_set in a path
					if next_backbone_edge_set == last_backbone_edge_set:
						continue
					for next_backbone_edge in next_backbone_edge_set.edges:
						next_node = next_backbone_edge.getOtherNode(last_node)

						# Can't have loops
						if p.nodeInPath(next_node):
							continue

						new_paths.append(p.expandPath(next_node, next_backbone_edge))

			# Updates the path lists
			if j % 2 == 1:
				from_node1 = new_paths
			else:
				from_node2 = new_paths

			# Merge paths from the two lists that end on the same node
			from_node1_by_last_node_id = defaultdict(list)
			for p in from_node1:
				from_node1_by_last_node_id[p.getLastNode().id_num].append(p)

			from_node2_by_last_node_id = defaultdict(list)
			for p in from_node2:
				from_node2_by_last_node_id[p.getLastNode().id_num].append(p)

			for k in from_node1_by_last_node_id:
				if k in from_node2_by_last_node_id:
					for p1 in from_node1_by_last_node_id[k]:
						for p2 in from_node2_by_last_node_id[k]:
							# Makes sure that combining the two paths doesn't make a cycle, and that the path doesn't use the same edge set twice
							if p1.getLastEdgeSet() != p2.getLastEdgeSet() and all(p1_node not in p2.nodes[:-1] for p1_node in p1.nodes):
								new_path = PhysicalPath(p1.nodes + p2.nodes[:-1][::-1],p1.edges + p2.edges[::-1])
								node1_to_node2.append(new_path)

		for path in node1_to_node2:
			for i in range(len(path.nodes)):
				if path.nodes[i] in path.nodes[(i+1):]:
					print path
					raise Exception('THERE ARE LOOPS!!!!')
			if not all(edge.is_backbone for edge in path.edges):
				raise Exception('Some Not backbone edges')
		Profiler.end('PhysicalNetwork.computeBackbonePathsBetween')
		return node1_to_node2

	def computeAllBackbonePaths(self):
		Profiler.start('PhysicalNetwork.computeAllBackbonePaths')
		for n1 in self.nodes:
			node1 = self.nodes[n1]
			for n2 in self.nodes:
				node2 = self.nodes[n2]
				self.all_backbone_paths[(node1, node2)] = self.computeBackbonePathsBetween(node1, node2)
		Profiler.end('PhysicalNetwork.computeAllBackbonePaths')

	def findBackbonePathsBetween(self, node1, node2):
		if not self.has_backbone:
			raise Exception('Physical Network Has No Backbone!')
		if len(self.all_backbone_paths) == 0:
			self.computeAllBackbonePaths()
		return self.all_backbone_paths[(node1, node2)]

	def getAllPhysicalEdges(self):
		Profiler.start('PhysicalNetwork.getAllPhysicalEdges')
		rv = (pe for node_id in self.nodes for edge_set_id in self.nodes[node_id].edge_sets for pe in self.nodes[node_id].edge_sets[edge_set_id].edges if self.nodes[node_id].edge_sets[edge_set_id] == pe.edge_set1)
		Profiler.end('PhysicalNetwork.getAllPhysicalEdges')
		return rv

	def getAllPhysicalBackboneEdges(self):
		Profiler.start('PhysicalNetwork.getAllPhysicalBackboneEdges')
		rv = (pe for node_id in self.nodes for edge_set_id in self.nodes[node_id].backbone_edge_sets for pe in self.nodes[node_id].backbone_edge_sets[edge_set_id].edges if self.nodes[node_id].backbone_edge_sets[edge_set_id] == pe.edge_set1)
		Profiler.end('PhysicalNetwork.getAllPhysicalBackboneEdges')
		return rv

	def getAllPhysicalEdgeSets(self):
		Profiler.start('PhysicalNetwork.getAllPhysicalEdgeSets')
		rv = (self.nodes[node_id].edge_sets[edge_set_id] for node_id in self.nodes for edge_set_id in self.nodes[node_id].edge_sets)
		Profiler.end('PhysicalNetwork.getAllPhysicalEdgeSets')
		return rv

	def getNodeClass(self):
		return PhysicalNode

	def getServerClass(self):
		return PhysicalServer

	def getCandidateEdgeClass(self):
		return PhysicalEdge

	def getMaxCandidateEdgeArrivalRate(self):
		return 1.0 / self.max_edge_inter_arrival_time

	def getMaxServerToNodeArrivalRate(self):
		return 1.0 / self.max_server_to_node_edge_inter_arrival_time

	def getNumberOfNodes(self):
		return len(self.nodes)

	def getServerCPU(self):
		val = None
		for server in self.getAllServers():
			if val == None:
				val = server.cpu
			elif val != server.cpu:
				raise Exception('Inconsistent Server CPU')
		return val

	def getNumberOfServersPerNode(self):
		val = None
		for _, node in self.nodes.iteritems():
			if val == None:
				val = len(node.servers)
			elif val != len(node.servers):
				raise Exception('Inconsistent Servers per Node')
		return val

	def getNumberOfEdgeSetsPerNode(self):
		val = None
		for _, node in self.nodes.iteritems():
			if val == None:
				val = len(node.edge_sets)
			elif val != len(node.edge_sets):
				raise Exception('Inconsistent Edge Sets per Node')
		return val

# Represents a ToR switch
class PhysicalNode:
	def __init__(self, network, id_num):
		# Reference to containing network
		self.network = network

		# Unique number given to node within network
		self.id_num = id_num

		# Dictionary of servers belonging to this node, indexed by server's id_nums
		self.servers = {}

		# Dictionary of edge_sets belonging to this node, indexed by edge_set's id_nums
		self.edge_sets = {}

		# BURSTY - the backbone edge_sets
		# TODO Output the Backbone
		self.backbone_edge_sets = None

# Server in a rack
class PhysicalServer:
	def __init__(self, node, id_num, cpu, bandwidth_to_node):
		# Node this belongs to
		self.node = node

		# Id unique id across node's servers
		self.id_num = id_num

		# CPU of the Server
		self.cpu = cpu

		# Bandwidth between this server and it's node
		self.bandwidth_to_node = bandwidth_to_node

		self.neighbor_servers = None

	def getNeighborServers(self):
		Profiler.start('PhysicalServer.getNeighborServers')
		if self.neighbor_servers == None:
			self.neighbor_servers = [self.node.servers[same_node_server_ids]
											for same_node_server_ids in self.node.servers] \
				 					+ [edge.getOtherNode(self.node).servers[other_node_server_id]
				 							for es_id in self.node.edge_sets
				 								for edge in self.node.edge_sets[es_id].edges
				 									for other_node_server_id in edge.getOtherNode(self.node).servers]
		Profiler.end('PhysicalServer.getNeighborServers')
		return self.neighbor_servers

# Represents a FSO in FireFly
class PhysicalEdgeSet:
	def __init__(self, node, id_num):
		# Node this belongs to 
		self.node = node

		# Id that is unique across node's edge_sets
		self.id_num = id_num

		# Set of candidate edges associated with this edge_Set
		self.edges = set()

		# Current active edge. Must be either None or edge in self.edges
		self.active_edge = None

		# BURSTY indicates whether the edge_Set is part of the backbone network or not
		self.is_backbone = None

	# Gets the edge between this edge_set and other_edge_set. Raises exception if there is no edge between the two edge_sets.
	def getEdge(self, other_edge_set):
		for e in self.edges:
			if e.getOtherEdgeSet(self) == other_edge_set:
				return e
		raise Exception('Invalid input to getEdge function')

	def hasEdgeToEdgeSet(self, other_edge_set):
		return any(edge.getOtherEdgeSet(self) == other_edge_set for edge in self.edges)

# A candidate edge
class PhysicalEdge:
	def __init__(self, edge_set1, edge_set2, bandwidth):
		# The two edge_sets this edge goes between. No assumptions can be made about which edge_set is which.
		self.edge_set1 = edge_set1
		self.edge_set2 = edge_set2

		# bandwidth of the edge (should be identical to all edgesthat share at least one edge_set)
		self.bandwidth = bandwidth

		# set of "related" edges (i.e. any edge that shares at least one edge_set)
		self.related_edges = None
		# self.adjacent_edges = None

		# Indicateds if the edge is a part of the backbone or not
		self.is_backbone = None

	def getOtherNode(self, node_or_edge_set):
		if node_or_edge_set == self.edge_set1 or node_or_edge_set == self.edge_set1.node:
			return self.edge_set2.node
		if node_or_edge_set == self.edge_set2 or node_or_edge_set == self.edge_set2.node:
			return self.edge_set1.node
		raise Exception('Value matches neither!')

	def getOtherNodeId(self, node_or_edge_set):
		if node_or_edge_set == self.edge_set1 or node_or_edge_set == self.edge_set1.node:
			return self.edge_set2.node.id_num
		if node_or_edge_set == self.edge_set2 or node_or_edge_set == self.edge_set2.node:
			return self.edge_set1.node.id_num
		raise Exception('Value matches neither!')

	def getOtherEdgeSet(self, node_or_edge_set):
		if node_or_edge_set == self.edge_set1 or node_or_edge_set == self.edge_set1.node:
			return self.edge_set2
		if node_or_edge_set == self.edge_set2 or node_or_edge_set == self.edge_set2.node:
			return self.edge_set1
		raise Exception('Value matches neither!')

	def getOtherEdgeSetId(self, node_or_edge_set):
		if node_or_edge_set == self.edge_set1 or node_or_edge_set == self.edge_set1.node:
			return self.edge_set2.id_num
		if node_or_edge_set == self.edge_set2 or node_or_edge_set == self.edge_set2.node:
			return self.edge_set1.id_num
		raise Exception('Value matches neither!')
	
	def getThisEdgeSetId(self, node_or_edge_set):
		if node_or_edge_set == self.edge_set1 or node_or_edge_set == self.edge_set1.node:
			return self.edge_set1.id_num
		if node_or_edge_set == self.edge_set2 or node_or_edge_set == self.edge_set2.node:
			return self.edge_set2.id_num
		raise Exception('Value matches neither!')

	def getThisEdgeSet(self, node_or_edge_set):
		if node_or_edge_set == self.edge_set1 or node_or_edge_set == self.edge_set1.node:
			return self.edge_set1
		if node_or_edge_set == self.edge_set2 or node_or_edge_set == self.edge_set2.node:
			return self.edge_set2
		raise Exception('Value matches neither!')

	# Returns the edge_set that THIS edge doesn't have in common with other_edge
	def getNotSharedEdgeSet(self, other_edge):
		# TODO add checks that the edge's aren't essentially the same
		if self.edge_set1 in (other_edge.edge_set1, other_edge.edge_set2):
			return self.edge_set2
		if self.edge_set2 in (other_edge.edge_set1, other_edge.edge_set2):
			return self.edge_set1
		raise Exception('Edge\'t share an Edge Set!!!')

	def getRelatedEdges(self):
		Profiler.start('PhysicalEdge.getRelatedEdges')
		if self.related_edges == None:
			self.related_edges = self.edge_set1.edges | self.edge_set2.edges
		Profiler.end('PhysicalEdge.getRelatedEdges')
		return self.related_edges

	def __str__(self):
		return '(' + str(self.edge_set1.node.id_num) + ', ' + str(self.edge_set1.id_num) + ') -> (' + str(self.edge_set2.node.id_num) + ', ' + str(self.edge_set2.id_num) + ')'

# A path in the physical network. Contains a list of nodes and edges. The edge-sets involved must be infered.
class PhysicalPath:
	def __init__(self, nodes, edges):
		self.nodes = nodes
		self.edges = edges

	def getLastNode(self):
		if len(self.nodes) >= 1:
			return self.nodes[-1]
		else:
			return None

	def getLastEdge(self):
		if len(self.edges) >= 1:
			return self.edges[-1]
		else:
			return None

	def getLastEdgeSet(self):
		last_node = self.getLastNode()
		last_edge = self.getLastEdge()
		if last_node == None or last_edge == None:
			return None

		if last_edge.edge_set1.node == last_node:
			return last_edge.edge_set1
		if last_edge.edge_set2.node == last_node:
			return last_edge.edge_set2

	def nodeInPath(self, node):
		return node in self.nodes

	def expandPath(self, next_node, next_edge):
		return PhysicalPath(self.nodes + [next_node], self.edges + [next_edge])

	def length(self):
		return len(self.edges)

	def node_str(self):
		if self.length() == 0:
			return '()'
		return '->'.join(map(lambda x: str(x.id_num), self.nodes))

	def compare(self, other_path):
		# Checks if the paths go through the same nodes and edges (ignoring direction)
		Profiler.start('PhysicalPath.compare')
		if not isinstance(other_path, self.__class__) or len(self.nodes) != len(other_path.nodes) or len(self.edges) != len(other_path.edges):
			Profiler.end('PhysicalPath.compare')
			return False
		rv = (all(n1 == n2  for n1, n2 in zip(self.nodes, other_path.nodes)) and all(e1 == e2  for e1, e2 in zip(self.edges, other_path.edges))) or \
				(all(n1 == n2  for n1, n2 in zip(self.nodes, reversed(other_path.nodes))) and all(e1 == e2  for e1, e2 in zip(self.edges, reversed(other_path.edges))))
		Profiler.end('PhysicalPath.compare')
		return rv

	def __str__(self):
		if self.length() == 0:
			return '()'

		return '|'.join('(' + str(this_n.id_num) + ',' + str(e.getThisEdgeSetId(this_n)) + ')->(' + str(e.getOtherNodeId(this_n)) + ',' + str(e.getOtherEdgeSetId(this_n)) + ')' for e, this_n in zip(self.edges, self.nodes[:-1]))

# Makes a two node network, with two edge_sets each, and eitther 1 or 2 edges
toy_physical_network_params_acceptable_values = ['1', '2', # These values will create a 2 rack, 2 FSO, 1 server network with the specified # of edges per FSO
													'small_(\d+)_([sd])']
def makeToyNetwork(param):
	re_matches = [re.match(regex, param) for regex in toy_physical_network_params_acceptable_values]

	if all(match == None for match in re_matches):
		print param
		raise Exception("WRONG INPUT")
	if any(match != None for match in re_matches[:2]):
		print 'Making a physical network with 2 racks, 1 server per rack, 2 FSOs, and', num_edges, 'edge' + ('s' if num_edges > 1 else '')+ ' per FSO'
		num_edges = int(param)
		network = PhysicalNetwork(2)
		for node_id in network.nodes:
			node = network.nodes[node_id]
			node.servers = {0: PhysicalServer(node, 0, 1.0, 1.0)}
			node.edge_sets = {0: PhysicalEdgeSet(node, 0), 1:PhysicalEdgeSet(node, 1)}
			
		if num_edges == 1 or num_edges == 2:
			edge00 = PhysicalEdge(network.nodes[0].edge_sets[0], network.nodes[1].edge_sets[0], 1.0)
			edge11 = PhysicalEdge(network.nodes[0].edge_sets[1], network.nodes[1].edge_sets[1], 1.0)
			network.nodes[0].edge_sets[0].edges.add(edge00)
			network.nodes[1].edge_sets[0].edges.add(edge00)
			network.nodes[0].edge_sets[1].edges.add(edge11)
			network.nodes[1].edge_sets[1].edges.add(edge11)

		if num_edges == 2:
			edge01 = PhysicalEdge(network.nodes[0].edge_sets[0], network.nodes[1].edge_sets[1], 1.0)
			edge10 = PhysicalEdge(network.nodes[0].edge_sets[1], network.nodes[1].edge_sets[0], 1.0)
			network.nodes[0].edge_sets[0].edges.add(edge01)
			network.nodes[1].edge_sets[1].edges.add(edge01)
			network.nodes[0].edge_sets[1].edges.add(edge10)
			network.nodes[1].edge_sets[0].edges.add(edge10)
	elif re_matches[2] != None:
		num_fsos = int(re_matches[2].group(1))
		type_network = re_matches[2].group(2)

		num_edges = (1 if type_network == 's' else num_fsos)

		print 'Making a physical network with 2 racks, 1 server per rack', num_fsos, 'FSOs, and', num_edges, 'edge' + ('s' if num_edges > 1 else '')+ ' per FSO'

		network = PhysicalNetwork(2)
		for node_id in network.nodes:
			node = network.nodes[node_id]
			node.servers = {0: PhysicalServer(node, 0, 1.0, 1.0)}
			node.edge_sets = {k: PhysicalEdgeSet(node, k) for k in range(num_fsos)}

		for edge_set_id in network.nodes[0].edge_sets:
			edge_set = network.nodes[0].edge_sets[edge_set_id]

			if num_edges == 1:
				search_range = [edge_set_id]
			elif num_edges == num_fsos:
				search_range = (k for k in range(num_fsos))
			for other_edge_set_id in search_range:
				other_edge_set = network.nodes[1].edge_sets[other_edge_set_id]

				edge = PhysicalEdge(edge_set, other_edge_set, 1.0)
				edge_set.edges.add(edge)
				other_edge_set.edges.add(edge)

	return network

# Generates a random graph network based on 
# Input:
#   num_nodes - int in [1,inf) - number of physical nodes (racks) in the network
#   num_servers - int in [1,inf) - number of servers per node (rack)
#   num_edge_sets - int in [1, inf) - number of edge sets (FSO) per node (rack). Essentially the degree of a node (rack).
#   num_edges - int in [1,inf) - number of edges per edge set (FSO). A value of 1 will cause a static network, and a value of 2 or greater will cause a dynamic network
#   server_cpu - float in (0,inf) - The amount of CPU each PhysicalServer has
#   node_to_node_bandwidth - float in (0,inf) - The amount of bandwidth of PhysicalEdges between PhysicalNodes
#   server_to_node_bandwidth - float in (0,inf) or -1 - The bandwidth between a PhysicalServer and PhysicalNode. A value of -1 indicates infinite bandwidth
#   make_backbone - None or int in [1, inf) - Determines the number of trees to make for the backbone. If None, then no backbone is made.
# Output:
#   Returns a PhysicalNetwork object with all the appropriate characteristics
def generatePhysicalNetwork(num_nodes, num_servers, num_edge_sets, num_edges, server_cpu, node_to_node_bandwidth, server_to_node_bandwidth, make_backbone = None, toy_param = None):
	Profiler.start('generatePhysicalNetwork')
	if toy_param != None:
		network = makeToyNetwork(toy_param)
		Profiler.end('generatePhysicalNetwork')
		return network
	# Create higher level container for network. Constructor also creates emtpy PhysicalNodes
	network = PhysicalNetwork(num_nodes)

	# Fill the PhysicalNodes with data
	for node_id in network.nodes:
		node = network.nodes[node_id]

		# Create the PhysicalServers associated with each PhysicalNode
		node.servers = {server_id:PhysicalServer(node, server_id, server_cpu, server_to_node_bandwidth) for server_id in range(num_servers)}

		# Create the PhysicalEdgeSets associated with each PhysicalNode
		node.edge_sets = {edge_set_id:PhysicalEdgeSet(node, edge_set_id) for edge_set_id in range(num_edge_sets)}
	
	# Create a random graph over the nodes, such that each node has num_edge_sets*num_edges edges
	possible_edges = [(i,j) for i in network.nodes for j in network.nodes if i < j]
	shuffle(possible_edges)
	edges_per_node = {i:[] for i in network.nodes}
	total_edges = []

	num_random_edges = (num_edge_sets * num_edges) % (num_nodes - 1)
	for a, b in possible_edges:
		if len(edges_per_node[a]) >= num_random_edges or len(edges_per_node[b]) >= num_random_edges:
			continue
		edges_per_node[a].append(b)
		edges_per_node[b].append(a)
		total_edges.append((a,b))
	
		# Exits early if all nodes have the correct number of edges
		if all(len(edges_per_node[i]) == num_random_edges for i in edges_per_node):
			break

	# If the random process didn't yield the right number of edges per node, then swap out edges until all nodes have the right number of edges.
	while any(len(edges_per_node[i]) < num_random_edges for i in edges_per_node):
		nodes_with_missing_edges = [i for i in edges_per_node for n in range(num_random_edges - len(edges_per_node[i]))]
		a, b = sample(nodes_with_missing_edges, 2)

		# Finds the sets of edges that can be swapped out and two edges added between the delted edges endpoints and a and b
		possible_swap_edges = [(i,j) for i, j in total_edges
								if i not in (a,b) and j not in (a,b) and
								((i not in edges_per_node[a] and j not in edges_per_node[b]) or (i not in edges_per_node[b] and j not in edges_per_node[a]))]

		if len(possible_swap_edges) == 0:
			raise Exception("Couldn't find any edges to swap out!")
		
		i, j = choice(possible_swap_edges)
		pos_orders = []
		if i not in edges_per_node[a] and j not in edges_per_node[b]:
			pos_orders.append('keep')
		if i not in edges_per_node[b] and j not in edges_per_node[a]:
			pos_orders.append('swap')
		order = choice(pos_orders)

		if order == 'keep':
			x = i
			y = j
		if order == 'swap':
			x = j
			y = i

		# Delete the edge (i,j)
		total_edges.remove((i,j))
		edges_per_node[i].remove(j)
		edges_per_node[j].remove(i)

		# Add the edge (x,a) and (y,b)
		total_edges.append((x,a))
		edges_per_node[x].append(a)
		edges_per_node[a].append(x)

		total_edges.append((y,b))
		edges_per_node[y].append(b)
		edges_per_node[b].append(y)

	# Add in a complete graph
	if num_edges * num_edge_sets >= (num_nodes - 1):
		for k in range((num_edges * num_edge_sets) / (num_nodes - 1)):
			for i, j in possible_edges:
				edges_per_node[i].append(j)
				edges_per_node[j].append(i)
				total_edges.append((i,j))

		if not all(len(edges_per_node[i]) == num_edge_sets * num_edges for i in edges_per_node):
			raise Exception('WHY?!?!?!?!?!?!?!?!')

	# Separate the edges for each node into separate edge_sets
	for node_id in edges_per_node:
		this_edge_sets = [[] for i in range(num_edge_sets)]
		num_edges_by_other = {v: 0 for v in edges_per_node[node_id]}
		for v in edges_per_node[node_id]:
			num_edges_by_other[v] += 1

		edges_per_node[node_id].sort(key = lambda x: num_edges_by_other[x])
		edges_per_node[node_id].reverse()

		for v in edges_per_node[node_id]:
			pos_edge_sets = [es for es in this_edge_sets if len(es) < num_edges and v not in es]

			if len(pos_edge_sets) == 0:
				pos_edge_sets = [(es, swap_v, swap_es)
									for es in this_edge_sets if v not in es
										for swap_v in es
											for swap_es in this_edge_sets if swap_es != es and swap_v not in swap_es and len(swap_es) < num_edges]
				
				if len(pos_edge_sets) == 0:
					raise Exception('NO POSSIBLE SWAP')

				es, swap_v, swap_es = choice(pos_edge_sets)
				es.remove(swap_v)
				es.append(v)
				swap_es.append(swap_v)
			else:
				es = choice(pos_edge_sets)
				es.append(v)
		edges_per_node[node_id] = this_edge_sets

	# Creates edges in a tuple of ids format
	edges_by_ids = []
	for node_id in network.nodes:
		for edge_set_id in network.nodes[node_id].edge_sets:
			edge_set_edges = edges_per_node[node_id][edge_set_id]
			for other_node_id in edge_set_edges:
				pos_other_edge_set_ids = [other_edge_set_id for other_edge_set_id, other_edge_set_edges in enumerate(edges_per_node[other_node_id]) if node_id in other_edge_set_edges]
				other_edge_set_id = choice(pos_other_edge_set_ids)
				edges_per_node[other_node_id][other_edge_set_id].remove(node_id)
				edges_by_ids.append(((node_id, edge_set_id), (other_node_id, other_edge_set_id)))

			# Clear out this nodes edges
			edges_per_node[node_id][edge_set_id] = []

	# Create the physical Edges
	for n1_descriptor, n2_descriptor in edges_by_ids:
		node1_id, edge_set1_id = n1_descriptor
		node2_id, edge_set2_id = n2_descriptor
		edge_set1 = network.nodes[node1_id].edge_sets[edge_set1_id]
		edge_set2 = network.nodes[node2_id].edge_sets[edge_set2_id]

		edge = PhysicalEdge(edge_set1, edge_set2, node_to_node_bandwidth)
		edge_set1.edges.add(edge)
		edge_set2.edges.add(edge)

	# Backbone
	if make_backbone != None and make_backbone >= 1:
		network.has_backbone = True
		network.all_backbone_paths = {}
		for node_id in network.nodes:
			network.nodes[node_id].backbone_edge_sets = {}
			for edge_set_id in network.nodes[node_id].edge_sets:
				network.nodes[node_id].edge_sets[edge_set_id].is_backbone = False
				for edge in network.nodes[node_id].edge_sets[edge_set_id].edges:
					edge.is_backbone = False

		for i in range(make_backbone):
			# Select Root node, and divide remaining equally between R + L sets, co
			tree_edges = []
			tree_sets = []
			while True:
				if len(tree_sets) == 0:
					root = choice(network.nodes.keys())
					num_right = (len(network.nodes) - 1.0) / 2.0
					num_right = int(math.floor(num_right) if random() < .5 else math.ceil(num_right))
					set_right = sample([node_id for node_id in network.nodes if node_id != root], num_right)
					set_left = [node_id for node_id in network.nodes if node_id != root and node_id not in set_right]
					tree_sets.append((root, set_right, set_left))
				else:
					next_tree_sets = []
					for root, set_right, set_left in tree_sets:
						for node_set in (set_right, set_left):
							if len(node_set) == 0:
								continue
							new_root = choice(node_set)
							tree_edges.append((root, new_root))
							num_right = (len(node_set) - 1.0) / 2.0
							num_right = int(math.floor(num_right) if random() < .5 else math.ceil(num_right))
							new_set_right = sample([node_id for node_id in node_set if node_id != new_root], num_right)
							new_set_left = [node_id for node_id in node_set if node_id != new_root and node_id not in new_set_right]
							next_tree_sets.append((new_root, new_set_right, new_set_left))
					tree_sets = next_tree_sets
					if len(tree_sets) == 0:
						break

			# Create the Backbone Edge Sets and Edges
			for node1_id, node2_id in tree_edges:
				node1, node2 = network.nodes[node1_id], network.nodes[node2_id]
				# Add new edge_sets
				edge_set1_id = (max(node1.backbone_edge_sets.keys()) + 1 if len(node1.backbone_edge_sets) > 0 else max(node1.edge_sets.keys()) + 1)
				edge_set2_id = (max(node2.backbone_edge_sets.keys()) + 1 if len(node2.backbone_edge_sets) > 0 else max(node2.edge_sets.keys()) + 1)
				edge_set1 = PhysicalEdgeSet(node1, edge_set1_id)
				edge_set2 = PhysicalEdgeSet(node2, edge_set2_id)
				edge_set1.is_backbone = True
				edge_set2.is_backbone = True

				node1.backbone_edge_sets[edge_set1_id] = edge_set1
				node2.backbone_edge_sets[edge_set2_id] = edge_set2

				# Add edge between those two edge_sets
				edge = PhysicalEdge(edge_set1, edge_set2, node_to_node_bandwidth)
				edge.is_backbone = True
				edge_set1.edges.add(edge)
				edge_set2.edges.add(edge)

		print 'Max # of Added of FSOs:', max(len(network.nodes[node_id].backbone_edge_sets) for node_id in network.nodes)

	Profiler.end('generatePhysicalNetwork')
	return network

# Constant values used for reading and writing PhysicalNetworks to disk
class PhysicalNetworkKeywords:
	physical_node = 'Node'
	physical_server = 'Server'
	physical_edge_set = 'EdgeSet'
	physical_edge = 'Edges'

# Writes the given PhysicalNetwork to the specified file. The format of the file is declarative with each line being a separate object.
def writePhysicalNetwork(network, filename):
	if filename == '':
		return
		
	Profiler.start('writePhysicalNetwork')
	f = open(filename, 'w')
	for node_id in network.nodes:
		f.write(PhysicalNetworkKeywords.physical_node + ' ' + str(node_id) + '\n')
		for server_id in network.nodes[node_id].servers:
			f.write(PhysicalNetworkKeywords.physical_server + ' ' + str(server_id) + ' ' + str(network.nodes[node_id].servers[server_id].cpu) + ' ' + str(network.nodes[node_id].servers[server_id].bandwidth_to_node) + '\n')
		for edge_set_id in network.nodes[node_id].edge_sets:
			f.write(PhysicalNetworkKeywords.physical_edge_set + ' ' + str(edge_set_id) + '\n')
			for edge in network.nodes[node_id].edge_sets[edge_set_id].edges:
				f.write(PhysicalNetworkKeywords.physical_edge + ' ' + str(edge.getOtherNodeId(network.nodes[node_id])) + ' ' + str(edge.getOtherEdgeSetId(network.nodes[node_id])) + ' ' + str(edge.bandwidth) + '\n')
	f.close()
	Profiler.end('writePhysicalNetwork')

# Reads in a Physical network written to file via the above function and returns the PhysicalNetwork.
def readPhysicalNetwork(filename):
	Profiler.start('readPhysicalNetwork')
	f = open(filename)

	network = PhysicalNetwork(None)
	this_node = None
	this_edge_set = None
	for line in f:
		spl = line.split()
		if spl[0] == PhysicalNetworkKeywords.physical_node and len(spl) == 2:
			this_node_id = int(spl[1])
			this_node = PhysicalNode(network, this_node_id)
			network.nodes[this_node_id] = this_node
		elif spl[0] == PhysicalNetworkKeywords.physical_server and len(spl) == 4:
			server_id = int(spl[1])
			cpu = float(spl[2])
			bw = float(spl[3])
			this_server = PhysicalServer(this_node, server_id, cpu, bw)
			this_node.servers[server_id] = this_server
		elif spl[0] == PhysicalNetworkKeywords.physical_edge_set and len(spl) == 2:
			edge_set_id = int(spl[1])
			this_edge_set = PhysicalEdgeSet(this_node, edge_set_id)
			this_node.edge_sets[edge_set_id] = this_edge_set
		elif spl[0] == PhysicalNetworkKeywords.physical_edge and len(spl) == 4:
			other_node_id = int(spl[1])
			other_edge_set_id = int(spl[2])
			bandwidth = float(spl[3])
			if other_node_id in network.nodes:
				this_edge = PhysicalEdge(network.nodes[other_node_id].edge_sets[other_edge_set_id], this_edge_set, bandwidth)
				network.nodes[other_node_id].edge_sets[other_edge_set_id].edges.add(this_edge)
				this_edge_set.edges.add(this_edge)
	Profiler.end('readPhysicalNetwork')
	return network

def addPhysicalNetworkArgs(parser):
	parser.add_argument('-pns', '--physical_network_size', metavar = 'SIZE_STRING', type = str, nargs = '+', default = [None], help = 'Each var should be "(Num Nodes),(Num ES per Node),(Num Server per Node),(Num Edges per ES)"')
	parser.add_argument('-sc', '--server_cpu', metavar = 'SERVER_CPU', type = float, nargs = 1, default = [1.0], help = 'The CPU of each server')
	parser.add_argument('-pnnbw', '--physical_node_to_node_bandwidth', metavar = 'NODE_TO_NODE_BANDWIDTH', type = float, nargs = 1, default = [1.0], help = 'The bandwidth of candidate edges between two nodes (racks)')
	parser.add_argument('-snbw', '--server_to_node_bandwidth', metavar = 'SERVER_TO_NODE_BANDWIDTH', type = float, nargs = 1, default = [None], help = 'The bandwidth between a server and node (rack). Providing a value of "inf" results in this check being ignored')
	parser.add_argument('-nbb', '--num_backbone_trees', metavar = 'NUM_BACKBONE_TREES', type = int, nargs = '+', default = [0], help = 'The number of backbone trees to create (If value < 1 given, then no backbone is created')

	parser.add_argument('-tpn', '--toy_physical_network_params', metavar = 'TOY_PN_PARAM', type = str, nargs = '+', default = [None], help = 'If a value of 1 or 2 are given then a "toy" physical network is used')

	parser.add_argument('-pout','--physical_out_file',metavar = 'PHYSICAL_OUT_FILE',type = str,nargs = 1,default = ['data/pn_data/physical_network_X_K.txt'],help = 'File to write physical networks to. The first \'X\' in the filename is replaced by a unique identifier')

def getPhysicalNetworkArgs(args):
	physical_network_size_strs = args.physical_network_size
	physical_network_sizes = []

	pn_size_re = '(?P<num_nodes>\d+),(?P<num_edge_sets>\d+),(?P<num_servers>\d+),(?P<num_edges>\d+)'
	for pn_size_str in physical_network_size_strs:
		pn_size_match = re.match(pn_size_re, pn_size_str)
		if pn_size_match == None:
			raise Exception('Invalid physical network size input!!!')

		physical_network_sizes.append(tuple(map(int, pn_size_str.split(','))))


	server_cpu = args.server_cpu[0]
	physical_node_to_node_bandwidth = args.physical_node_to_node_bandwidth[0]
	server_to_node_bandwidth = args.server_to_node_bandwidth[0]
	num_backbone_trees = args.num_backbone_trees

	if server_to_node_bandwidth == float('inf'):
		server_to_node_bandwidth = None

	toy_physical_network_params = args.toy_physical_network_params

	physical_out_file = args.physical_out_file[0]

	this_params = {'physical_network_size': ('list(PNSize)', ';'.join(map(lambda x: ','.join(map(str, x)) ,physical_network_sizes))),
					'server_cpu': ('float', str(server_cpu)),
					'physical_node_to_node_bandwidth': ('float', str(physical_node_to_node_bandwidth)),
					'server_to_node_bandwidth': ('float', str(server_to_node_bandwidth)),
					'physical_out_file': ('str', physical_out_file),
					'toy_physical_network_params': ('list(str)', ','.join(map(str, toy_physical_network_params))),
					'num_backbone_trees': ('list(int)', ','.join(map(str, num_backbone_trees)))}

	return this_params, physical_network_sizes, server_cpu, physical_node_to_node_bandwidth, server_to_node_bandwidth, toy_physical_network_params, physical_out_file, num_backbone_trees

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description = 'Generates a physical network and writes it to file')

	# Physical Network Args
	parser.add_argument('-nn', '--num_nodes', metavar = 'NODES', type = int, nargs = 1, default = [1], help = 'Number of nodes (racks) in the physical network')
	parser.add_argument('-ns', '--num_servers', metavar = 'SERVERS', type = int, nargs = 1, default = [1], help = 'Number of servers per node (rack)')
	parser.add_argument('-nes', '--num_edge_sets', metavar = 'EDGE_SETS', type = int, nargs = 1, default = [1], help = 'Number of edge sets (FSOs) per node (rack)')
	parser.add_argument('-ne', '--num_edges', metavar = 'EDGES', type = int, nargs = 1, default = [1.0], help = 'Number of candidate edges per edge set (FSO)')
	parser.add_argument('-sc', '--server_cpu', metavar = 'SERVER_CPU', type = float, nargs = 1, default = [1.0], help = 'The CPU of each server')
	parser.add_argument('-nnbw', '--node_to_node_bandwidth', metavar = 'NODE_TO_NODE_BANDWIDTH', type = float, nargs = 1, default = [1.0], help = 'The bandwidth of candidate edges between two nodes (racks)')
	parser.add_argument('-snbw', '--server_to_node_bandwidth', metavar = 'SERVER_TO_NODE_BANDWIDTH', type = float, nargs = 1, default = [1.0], help = 'The bandwidth between a server and node (rack)')

	# Output
	parser.add_argument('-out','--out_file',metavar = 'OUT_FILE',type = str,nargs = 1,default = ['temp.txt'],help = 'File to output vn requests to')

	args = parser.parse_args()

	num_nodes = args.num_nodes[0]
	num_servers = args.num_servers[0]
	num_edge_sets = args.num_edge_sets[0]
	num_edges = args.num_edges[0]

	server_cpu = args.server_cpu[0]
	node_to_node_bandwidth = args.node_to_node_bandwidth[0]
	server_to_node_bandwidth = args.server_to_node_bandwidth[0]

	out_file = args.out_file[0]

	network = generatePhysicalNetwork(num_nodes, num_servers, num_edge_sets, num_edges, server_cpu, node_to_node_bandwidth, server_to_node_bandwidth)
	# writePhysicalNetwork(network, out_file)

	num_paths_by_len = defaultdict(int)
	for node_id1 in network.nodes:
		for node_id2 in network.nodes:
			if node_id1 != node_id2:
				paths = network.findPathsBetween(network.nodes[node_id1], network.nodes[node_id2], 3)
				for p in paths:
					num_paths_by_len[p.length()] += 1
	print num_paths_by_len
