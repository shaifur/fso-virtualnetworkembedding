
import physical_network as PN
import vn_requests as VNR
import simulate_bursty_vns as SIM

import networkx as nx

def getServerTuple(node_id, server_id):
	return (node_id, 's' + str(server_id))

def getToRTuple(node_id):
	return (node_id, 't')

def getFSOTuple(node_id, edge_set_id):
	return (node_id, 'f' + str(edge_set_id))

# Takes in a PhysicalNetwork (from physical_network.py) Object, and outputs a corresponding nx.Graph
def translatePhysicalNetwork(physical_network):
	# Create rack topology (For now is a NX1 grid, where N is the number of racks)
	dcn_g = nx.Graph(name = 'DCN',
						server_capacity = physical_network.getServerCPU(),
						link_capacity = physical_network.getLinkRateInBytesPerSecond(),
						server_link_capacity = physical_network.getServerToRackLinkRateInBytesPerSecond(),
						no_of_server_per_rack = physical_network.getNumberOfServersPerNode(),
						no_of_fso_per_rack = physical_network.getNumberOfEdgeSetsPerNode())

	# First Creates All Nodes for each Rack. For each rack add ToR node, Server Nodes, and FSO nodes. Also creates the candidate graph(???)
	# Then Creates all intra-rack edges (server-to-ToR, and ToR-to-FSO)
	for physical_node_id, physical_node in physical_network.nodes.iteritems():
		# Server nodes
		for server_id, server in physical_node.servers.iteritems():
			dcn_g.add_node(getServerTuple(physical_node_id, server_id),
							rcap = server.cpu,
							ntype = 's')
		
		# ToR node
		dcn_g.add_node(getToRTuple(physical_node_id),
						rcap = 0.0,
						ntype = 't')

		# FSO Nodes
		for edge_set_id, edge_set in physical_node.edge_sets.iteritems():
			# This is pressumably the candidate edges of the FSO(???)
			f_i_clist = [getFSOTuple(edge.getOtherNodeId(edge_set), edge.getOtherEdgeSetId(edge_set)) for edge in edge_set.edges],

			dcn_g.add_node(getFSOTuple(physical_node_id, edge_set_id),
							rcap = 0,
							ntype = 'f',
							clist = f_i_clist,
							isUsed = False)

		# ----------------------------------------
		# |  LINK DEMAND IS IN BYTES PER SECOND  |
		# ----------------------------------------


		# Server-to-ToR edges
		for server_id, server in physical_node.servers.iteritems():
			dcn_g.add_edge(getServerTuple(physical_node_id, server_id),
							getToRTuple(physical_node_id),
							rcap = server.bandwidth_to_node)

		# ToR-to-FSO edges
		for edge_set_id, edge_set in physical_node.edge_sets.iteritems():
			dcn_g.add_edge(getFSOTuple(physical_node_id, edge_set_id),
							getToRTuple(physical_node_id),
							rcap = physical_network.getLinkRateInBytesPerSecond())

	return dcn_g

# Takes in a list of VirtualNetworkRequest(see vn_request.py), and outputs a list of (nx.Graph, arrival of vn, duration of vn)
# bandwidth_slack_factor:
# 		For any given virtual edge, it has a iat, which when combined with it's flow size distribution,
# 		yields a minimum required bandwidth, call it B, to avoid an infinite queue problem. We set the
# 		bandwidth requirement of the virtual edge in the traditional model to B * (1 + bandwidth_slack_factor)
def translateVirtualNetworkRequests(virtual_network_requests, bandwidth_slack_factor = 0.0):
	req_list = []

	for vnr in virtual_network_requests:
		# Ensures that the virtual network strucutre is made
		vnr.safeBuildVirtualNetwork()
		virtual_network = vnr.virtual_network

		this_graph = nx.Graph(name = 'VN' + str(vnr.vn_request_id))

		# Add Nodes and demands
		for node_id, node in virtual_network.nodes.iteritems():
			this_graph.add_node(node_id)

			# Node demand
			this_graph.node[node_id]['d'] = node.cpu_requirement

		# ----------------------------------------
		# |  LINK DEMAND IS IN BYTES PER SECOND  |
		# ----------------------------------------

		# Add Edges and demands
		for edge in virtual_network.getAllVirtualEdges():
			this_graph.add_edge(edge.node1.id_num, edge.node2.id_num)

			# edge.getTotalArrivalRate() is in flow/sec
			# vnr.real_world_data.all_fs_avg is in bytes/flow
			# Therefore bandwidth is in bytes per second
			this_edge_required_bandwidth = (edge.getTotalArrivalRate() * vnr.real_world_data.all_fs_avg) * (1.0 + bandwidth_slack_factor)
			this_graph[edge.node1.id_num][edge.node2.id_num]['d'] = this_edge_required_bandwidth

		req_list.append((this_graph, vnr.arrival_time, vnr.duration))

	return req_list

if __name__ == '__main__':
	print 'TESTING PHYSICAL NETWORK TRANSLATION'

	# Generic Args
	num_nodes = 16
	num_servers = 4
	num_edge_sets = 4
	num_edges = 4
	server_cpu = 1.0
	node_to_node_bandwidth = 10.0
	server_to_node_bandwidth = -1
	# A value of -1 means infinite bandwidth between servers and nodes (this is just to make sure that any VNs rejected are from the rack-to-rack network).
	# In this case the "link_capacity" of those edges will be None

	# make_backbone is only relevant for bursty VNs
	# toy_param creates simple deterministic physical_networks
	physical_network = PN.generatePhysicalNetwork(num_nodes,
													num_servers,
													num_edge_sets,
													num_edges,
													server_cpu,
													node_to_node_bandwidth,
													server_to_node_bandwidth,
													make_backbone = None, 
													toy_param = None)

	translatePhysicalNetwork(physical_network)

	print 'TESTING VIRTUAL NETWORK REQUEST TRANSLATION'

	# Generic Args
	num_vns = 100
	num_nodes = 4 # TODO allow this to be a range
	link_percent = .5
	vn_arrival_gap = (0.0, 20.0)
	vn_duration = (0.0, 100.0)
	cpu_requirement = (0.0, 0.1)
	inter_arrival_time = (.005,)
	real_world_data = SIM.RealWorldData('real_world_data/vl2_approx.flow_iat_hist')
	real_world_data.process()

	verbose = False # Set to True, and generate_vn_request will print the range of bandwidth required for bandwidth_slack_factor = 0.0
	
	vnrs = VNR.generate_vn_requests(num_vns = num_vns,
									num_nodes = num_nodes,
									link_percent = link_percent,
									vn_arrival_gap = vn_arrival_gap,
									vn_duration = vn_duration,
									cpu_requirement = cpu_requirement,
									inter_arrival_time = inter_arrival_time,
									real_world_data = real_world_data,
									verbose = verbose)

	translateVirtualNetworkRequests(vnrs)

	# Translate VNRs generated using real VN data

	print 'TESTING REAL TOPOLOGY VN TRANSLATION'

	num_vns = 100
	vn_arrival_gap = (0.0, 20.0)
	vn_duration = (0.0, 100.0)
	cpu_requirement = (0.0, 0.1)
	inter_arrival_time = (.005,)
	real_world_data = SIM.RealWorldData('real_world_data/vl2_approx.flow_iat_hist')
	real_world_data.process()

	verbose = False # Set to True, and generate_vn_request will print the range of bandwidth required for bandwidth_slack_factor = 0.0
	
	real_vn_data = VNR.RealVirtualTopologyData('real_world_data/emulab_data.vn_top')

	vnrs_real = VNR.generate_vn_requests(num_vns = num_vns,
											vn_arrival_gap = vn_arrival_gap,
											vn_duration = vn_duration,
											cpu_requirement = cpu_requirement,
											inter_arrival_time = inter_arrival_time,
											real_world_data = real_world_data,
											verbose = verbose,
											real_vn_data = real_vn_data)

	translateVirtualNetworkRequests(vnrs_real)

	# Translate VNRs, when VN arrivals modeled as a Poisson Process

	print 'TESTING POISSON ARRIVAL VNS TRANSLATION'

	test_duration = 100.0
	vn_arrival_rate = 5.0
	vn_duration = (0.0, 100.0)
	cpu_requirement = (0.0, 0.1)
	inter_arrival_time = (.005,)
	real_world_data = SIM.RealWorldData('real_world_data/vl2_approx.flow_iat_hist')
	real_world_data.process()

	verbose = False # Set to True, and generate_vn_request will print the range of bandwidth required for bandwidth_slack_factor = 0.0
	
	real_vn_data = VNR.RealVirtualTopologyData('real_world_data/emulab_data.vn_top')
	vnrs_real = VNR.generate_vn_requests(vn_duration = vn_duration,
											cpu_requirement = cpu_requirement,
											inter_arrival_time = inter_arrival_time,
											real_world_data = real_world_data,
											verbose = verbose,
											real_vn_data = real_vn_data,
											test_duration = test_duration,
											vn_arrival_rate = vn_arrival_rate)

	translateVirtualNetworkRequests(vnrs_real)

