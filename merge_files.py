
import argparse

def mergeFiles(files, out_file):
	fout = open(out_file, 'w')

	for fname in files:
		fin = open(fname)
		fout.write(fin.read())
		fin.close()
	fout.close()

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description = 'Merges selected files')

	parser.add_argument('-fs', '--files', metavar = 'FILE', type = str, nargs = '+', default = [], help = 'List of files to merge')
	parser.add_argument('-out', '--out_file', metavar = 'OUT_FILE', type = str, nargs = 1, default = [None], help = 'File to write merged data')

	args = parser.parse_args()
	
	files = args.files
	out_file = args.out_file[0]

	if len(files) < 1:
		raise Exception('Must specifiy at least one file')

	if out_file == None:
		raise Exception('Must specifiy an out file')

	mergeFiles(files, out_file)
