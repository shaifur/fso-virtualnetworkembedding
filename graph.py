
import simulate_bursty_vns as SIM
import bursty_vn_embedding as MVN
import physical_network as PN
import matplotlib.pyplot as plt

import collections
import re
import math

import argparse

def avg(vals):
	if len(vals) == 0:
		return 0.0
	return sum(vals) / len(vals)

class EqualFilter:
	# If val is basic type checks equality
	# If val is iterable, checks that v in val
	def __init__(self, val_name, val):
		self.val_name = val_name
		self.val = val

	def check(self, case, param):
		v = getValue(case, param, self.val_name)
		return (v in self.val if isinstance(self.val, collections.Iterable) else v == self.val)

	def checkValue(self, val):
		return (val in self.val if isinstance(self.val, collections.Iterable) else val == self.val)

def getValue(test_case, overall_params, val_name):
	if val_name in test_case:
		return test_case[val_name]
	if val_name in overall_params:
		return overall_params[val_name]

	if val_name in ('physical_num_nodes', 'num_edge_sets', 'num_servers', 'physical_num_edges'):
		if 'physical_network_size' in overall_params:
			return overall_params['physical_network_size'][0][('physical_num_nodes', 'num_edge_sets', 'num_servers', 'physical_num_edges').index(val_name)]

	if val_name.startswith("lp_") or val_name.startswith("mip_"):
		return None

	if val_name == "average_revenue_to_cost_ratio":
		return None

	raise Exception('Unable to get parameter: ' + str(val_name))

# Hard maximum that cannot be outdone by any test case
def getMaximumPhysicalNetworkThroughput(params, data):
	max_throughput = None
	first = True
	for p, d in zip(params, data):
		for c in d:
			num_nodes = getValue(c, p, 'physical_num_nodes')
			num_edge_sets = getValue(c, p, 'num_edge_sets')
			edge_bandwidth = getValue(c, p, 'physical_node_to_node_bandwidth')
			this_max_throughput = num_nodes * num_edge_sets * edge_bandwidth / 2.0
			if first and max_throughput == None:
				max_throughput = this_max_throughput
				first = False
			if not first and max_throughput != this_max_throughput:
				max_throughput = None
	return max_throughput

# Expected maximum case, but a test could have a higher value since traffic is randomly generated
def getMaximumVirtualNetworkThroughput(params, data):
	for p, d in zip(params, data):
		for c in d:
			num_virtual_links_accepted = getValue(c, p, 'num_virtual_edges_accepted')
			average_arrival_rate = getValue(c, p, 'arrival_rate')
			average_flow_size = getValue(c, p, 'flow_size')
			average_throughput_per_edge = average_flow_size / average_arrival_rate
			this_expected_virtual_network
	return 1.0

# TODO comment
def groupData(params, data, x_value, y_values, group_value, filters = [], group_func = None):
	if group_func == None:
		group_func = [None] * len(group_value)

	x_filter = next((f for f in filters if f.val_name == x_value), None)

	if x_value == 'physical_num_edges':
		x_values = (ne for p in params for nn, nes, ns, ne in p['physical_network_size'])
	elif x_value == 'physical_num_nodes':
		x_values = (nn for p in params for nn, nes, ns, ne in p['physical_network_size'])
	else:
		x_values = (v for p in params for v in (p[x_value] if isinstance(p[x_value], collections.Iterable) else (p[x_value],)))
	x_value_range = set(v for v in x_values if x_filter == None or x_filter.checkValue(v))
	
	group_filters = {gv:next((f for f in filters if f.val_name == gv), None) for gv in group_value}
	group_value_range = set(tuple((getValue(case, param, gv) if gf == None else gf(getValue(case, param, gv))) for gv,gf in zip(group_value, group_func)) for test, param in zip(data, params) for case in test if all(group_filters[gv] == None or group_filters[gv].check(case, param) for gv in group_filters))

	vals_grouped = {gv:{xv:[] for xv in x_value_range} for gv in group_value_range}
	for test, param in zip(data, params):
		for case in test:
			if all(f.check(case, param) for f in filters):
				vals_grouped[tuple((getValue(case, param, gv) if gf == None else gf(getValue(case, param, gv))) for gv,gf in zip(group_value, group_func))][getValue(case, param, x_value)].append(tuple(getValue(case, param, y_value) for y_value in y_values))

	return vals_grouped

x_label_table = {'physical_num_edges': '# of Links per FSO',
					'max_physical_edge_collision': 'User Supplied Slack Factor',
					'physical_num_nodes': '# of Racks',
					'virtual_network_arrival_rate': 'Arrival Rate of Virtual Networks (VN/sec)',
					'virtual_num_nodes': 'Size of Virtual Networks',
					'real_vn_sizes': 'Size of Virtual Networks'}

def getXLabel(x_dimension):
	if x_dimension in x_label_table:
		return x_label_table[x_dimension]
	else:
		print 'No x-label specified for x-dimension:', x_dimension
		return ''

y_label_table = {('average_vn_throughput_per_second',): 'Average Throughput of Network (Gb) per sec',
					('average_pn_throughput_per_second', 'physical_num_nodes', 'num_edge_sets', 'physical_node_to_node_bandwidth'): 'Average Utilization of the physical network',
					('num_virtual_edges_accepted', 'num_virtual_edges_rejected'): 'Ratio of Virtual Links Accepted',
					('num_vns_accepted', 'num_vns_rejected'): 'Ratio of Virtual Networks Accepted',
					('percent_congestion',): 'Percent Congestion at Runtime',
					('percent_impossible_to_use',): 'Percent of time FSOs have no possible active links',
					('avg_flow_rate',): 'Average bandwidth (byte/second) given to flows',
					('average_vn_throughput_per_second', 'num_virtual_edges_accepted', 'arrival_rate', 'flow_size'): 'Throughput relative to expected maximum throughput of Virtual Networks',
					('flow_completion_times',): 'Flow Completion Time (s)',
					'flow_completion_times': 'Flow Completion Time (s)',
					('max_edge_arrival_rate',): 'Max Inter-Arrival Allowed per Edge (s)',
					('average_accepted_revenue_ar_formula',): 'Average accepted revenue per second',
					('average_accepted_revenue_data_formula',): 'Average accepted revenue per second',
					('average_accepted_revenue_normalized_data_formula',): 'Average accepted revenue per second',
					('average_embedding_time_in_seconds',): 'Average time per embedding (s)'}

def getYLabel(y_dimensions):
	rv = ''
	for yd in y_dimensions:
		if all(k.startswith('lp_') for k in yd):
			yd = tuple(k[3:] for k in yd)
		if all(k.startswith('mip_') for k in yd):
			yd = tuple(k[4:] for k in yd)
		if yd in y_label_table:
			if rv == '':
				rv = y_label_table[yd]
			elif rv != y_label_table[yd]:
				print 'Mismatched y-dimension:', y_dimensions
		else:
			print 'No y-label specified for y-dimension:', yd
	return rv

def legend_formatter_rwd(v):
	if str(v) == 'default':
		return 'Synthetic'
	elif str(v) == 'univ1_data.txt':
		return 'Wisconsin'
	else:
		print 'Unknown RWD value'
		return str(v)

def legend_formatter_toy(v):
	if v == 1:
		return 'Static 2 rack, 2 FSO'
	if v == 2:
		return 'Dynamic 2 rack, 2 FSO'
	match = re.match(PN.toy_physical_network_params_acceptable_values[2], v)
	if match != None:
		return ('Dynamic' if match.group(2) == 'd' else 'Static') + ' 2 rack, ' + match.group(1) + ' FSO'

def legend_formatter_pne(v):
	if v == 'static':
		return 'SN'
	elif v == 'dynamic':
		return 'DN'
	else:
		return str(v) + ' Link' + ('s' if x != 1 else '') +' per FSO'

legend_formatter_table = {'max_physical_edge_collision': lambda x: str(x * 100) + '% Congestion',
							'path_weighting_scheme': str,
							'physical_num_edges': legend_formatter_pne,
							'real_world_data': legend_formatter_rwd,
							'toy_physical_network_params': legend_formatter_toy,
							'link_mapping_scheme': lambda x: ('DE' if x.algo_type == 'dyn' else 'SE')}

def getYLegendFormat(y):
	if all(k.startswith('mip_') for k in y):
		return 'ILP'
	if all(k.startswith('lp_') for k in y):
		return 'LP'
	return 'Backtracking Algorithm'

def getLegendFormater(group_by, ys = None):
	if ys == None:
		if any(gv not in legend_formatter_table for gv in group_by):
			print 'No legend formatter specified for:', group_by
			return str
		return lambda k, _: ', '.join(legend_formatter_table[gv](k[i]) for i, gv in enumerate(group_by))
	else:
		if any(gv not in legend_formatter_table for gv in group_by):
			print 'No legend formatter specified for:', group_by
			return str
		return lambda k, y: ', '.join(legend_formatter_table[gv](k[i]) for i, gv in enumerate(group_by)) + getYLegendFormat(y)

def graphData(params, data, x_dimension, y_dimensions, y_func, group_by, legend_loc = 0, filters = [], title = '', include_fliers = None, include_y_errbars = False, group_func = None, output_file = None):
	
	if len(y_dimensions) == 1:
		this_legend_formatter = getLegendFormater(group_by)
	elif len(y_dimensions) > 1:
		this_legend_formatter = getLegendFormater(group_by, ys = y_dimensions)

	if output_file != None:
		xy_data = []

	handles = []
	legend_label = []
	for yd, yf in zip(y_dimensions, y_func):
		grouped_vals = groupData(params, data, x_dimension, yd, group_by, filters = filters, group_func = group_func)
		grouped_vals = {k:{xv:[yf(*yv) for yv in grouped_vals[k][xv] if all(tmp != None for tmp in yv)] for xv in grouped_vals[k]} for k in grouped_vals}

		for k in grouped_vals:
			x_vals = sorted([xv for xv in grouped_vals[k] if len(grouped_vals[k][xv]) > 0])
			y_vals = [avg(grouped_vals[k][xv]) for xv in x_vals]
			if include_y_errbars:
				yerr_low = [yv - min(grouped_vals[k][xv]) for xv, yv in zip(x_vals, y_vals)]
				yerr_high = [max(grouped_vals[k][xv]) - yv for xv, yv in zip(x_vals, y_vals)]

			if len(x_vals) > 0 and len(y_vals) > 0:
				if include_y_errbars:
					if output_file == None:
						# PLOTS DATA
						h, a, b = plt.errorbar(x_vals, y_vals, yerr = [yerr_low, yerr_high])
					else:
						raise Exception('Outputting data to file not implmented when using y_errbars yet!!!')
				else:
					# PLOTS DATA
					h, = plt.plot(x_vals, y_vals)
					if output_file != None:
						xy_data.append((x_vals, y_vals))
				handles.append(h)

				legend_label.append(this_legend_formatter(k, yd))
	
	if output_file != None:
		f = open(output_file, 'w')
		f.write('TITLE ' + title + '\n')
		f.write('X ' + getXLabel(x_dimension) + '\n')
		f.write('Y ' + getYLabel(y_dimensions) + '\n')
		for label, (x_vals, y_vals) in zip(legend_label, xy_data):
			s = label + ' ' + ' '.join(map(lambda x: str(x[0]) + ',' + str(x[1]) ,zip(x_vals, y_vals)))
			f.write(s + '\n')
		f.close()
	plt.xlabel(getXLabel(x_dimension))
	plt.ylabel(getYLabel(y_dimensions))
	plt.title(title)
	if legend_loc >= 0 and legend_loc <= 10:
		plt.legend(handles, legend_label, loc = legend_loc)
	plt.show()

# TODO Add new way to add more colors
pos_colors = ['r', 'g', 'b', 'c', 'm', 'y', 'k', '0.5']
def getColor(k, colors):
	if k in colors:
		return colors[k]
	for c in pos_colors:
		if all(colors[x] != c for x in colors):
			colors[k] = c
			return c
	raise Exception('NO MORE COLORS LEFT!!')

def makeBoxPlot(params, data, x_dimension, y_dimensions, y_func, group_by, legend_loc = 0, filters = [], title = '', include_fliers = True, include_y_errbars = None, group_func = None, output_file = None):
	this_legend_formatter = getLegendFormater(group_by)

	if len(y_dimensions) != 1:
		raise Exception('Can only make Box Plot for exactly 1 y-dimension')
	y_dimensions = y_dimensions[0]

	if len(y_func) != 1:
		raise Exception('Can only make Box Plot for exactly 1 y-func')
	y_func = y_func[0]

	grouped_vals = groupData(params, data, x_dimension, y_dimensions, group_by, filters = filters, group_func = group_func)
	grouped_vals = {k:{xv:[y_func(*yv) for yv in grouped_vals[k][xv]] for xv in grouped_vals[k]} for k in grouped_vals}
	
	handles = []
	legend_label = []

	fig = plt.figure()
	ax = plt.axes()

	# Figures Out x-coords for the inidividual box-plots
	# intra_buffer and inter_buffer are constants that determine the layout
	intra_buffer = .1
	inter_buffer = .3
	num_plots = len(grouped_vals)
	plot_width = (1.0 - inter_buffer - intra_buffer * (num_plots - 1)) / num_plots
	positions = {k:[] for k in grouped_vals}
	for i, k in enumerate(sorted(grouped_vals)):
		start_position = .5 + inter_buffer / 2.0 + plot_width / 2.0 + i * (plot_width + intra_buffer)
		positions[k] = [start_position + i for i in range(len(grouped_vals[k]))]

	if output_file != None:
		xy_data = []

	x_labels = {}
	colors = {}
	for k in sorted(grouped_vals):
		x_vals = sorted([xv for xv in grouped_vals[k] if len(grouped_vals[k][xv]) > 0])
		for i, xv in enumerate(x_vals):
			if (i + 1) in x_labels and x_labels[(i + 1)] != xv:
				if output_file == None:
					raise Exception('AAAA')
			x_labels[(i + 1)] = xv
		y_vals = [[yv for yvs in grouped_vals[k][xv] for yv in yvs if yv >=0] for xv in x_vals]
		# PLOTS DATA
		print len(y_vals), len(positions[k])
		try:
			bp = plt.boxplot(y_vals, positions = positions[k], widths = plot_width, sym = ('+' if include_fliers else ''))
		except ValueError:
			bp = None

		this_color = getColor(k, colors)
		if bp != None:
			for elem in ('boxes','caps','whiskers', 'fliers', 'medians'):
				[plt.setp(bp[elem][idx], color = this_color) for idx in range(len(bp[elem]))]
		if output_file != None:
			xy_data.append((this_legend_formatter(k, None), x_vals, y_vals))

	# if output_file != None:
	# 	f = open(output_file, 'w')
	# 	for label, x_vals, y_vals in xy_data:
	# 		#print "debug:label:",label,x_vals
	# 		s = label + ' ' + ' '.join(map(lambda x: str(x[0]) + ',' + '|'.join(map(str, x[1])), zip(x_vals, y_vals)))
	# 		f.write(s + '\n')
	# 	f.close()

	if output_file != None:
		for label, x_vals, y_vals in xy_data:
			# print "debug:label:",label,x_vals
			for x_val,y_val in zip(x_vals, y_vals):
				f = open(str(output_file)+'_'+str(label)+'_'+str(x_val)+'.txt', 'w')
				f.write("#"+str(x_val)+'\n')
				for y in y_val:
					f.write(str(y)+'\n')
				f.close()

	plt.xlabel(getXLabel(x_dimension))
	plt.xlim([.5 - inter_buffer / 2.0, len(x_labels) + .5 + inter_buffer / 2.0])
	ax.set_xticklabels([x_labels[i] for i in sorted(x_labels)])
	ax.set_xticks(sorted(x_labels.keys()))
	plt.ylabel(getYLabel(y_dimensions))
	# plt.ylim([-1.0, 5])
	plt.title(title)

	# Make legend
	if legend_loc >= 0 and legend_loc <= 10:
		for k in sorted(grouped_vals):
			h, = plt.plot([1,1], color = colors[k], linestyle = '-')
			handles.append(h)
			legend_label.append(this_legend_formatter(k, None))
		plt.legend(handles, legend_label, loc = legend_loc)
		for h in handles:
			h.set_visible(False)
	plt.show()

def printData(params, data, x_dimension, y_dimensions, y_func, group_by):
	for yd, yf in zip(y_dimensions, y_func):
		grouped_vals = groupData(params, data, x_dimension, yd, group_by, filters = filters, group_func = group_func)
		grouped_vals = {k:{xv:[yf(*yv) for yv in grouped_vals[k][xv] if all(tmp != None for tmp in yv)] for xv in grouped_vals[k]} for k in grouped_vals}

		for k in grouped_vals:
			x_vals = sorted([xv for xv in grouped_vals[k] if len(grouped_vals[k][xv]) > 0])
			y_vals = [grouped_vals[k][xv] for xv in x_vals]
			if include_y_errbars:
				yerr_low = [yv - min(grouped_vals[k][xv]) for xv, yv in zip(x_vals, y_vals)]
				yerr_high = [max(grouped_vals[k][xv]) - yv for xv, yv in zip(x_vals, y_vals)]

			if len(x_vals) > 0 and len(y_vals) > 0:
				for xv, yvs in zip(x_vals, y_vals):
					print 'G' + str(tuple(map(str,group_by))) + '=' + str(tuple(map(str,k))) + ' X(' + str(x_dimension) + ')=' + str(xv) + ' Y(' + str(yd) + ')=' + str(yvs)

def getDataFromFiles(filenames):
	params = []
	data = []
	for filename in filenames:
		p, d = SIM.readInSimulatorDataFile(filename)
		params += p
		data += d
	return params, data

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description = 'Graph bursty VN Embedding output')

	# Input file
	parser.add_argument('-df', '--data_file_name', metavar = 'FILE_NAME', type = str, nargs = '+', default = [None], help = 'File to plot')

	# Graph params
	parser.add_argument('-ll', '--legend_loc', metavar = 'LL', type = int, nargs = 1, default = [0], help = 'Location of legend, default\'s to using matplotlib\'s auto  location')

	# X-dimension (specify exactly one)
	parser.add_argument('-x_pne','--use_physical_num_edges_for_x_dimension', help = 'Use # of Links per FSO as the x-dimension', action = 'store_true')
	parser.add_argument('-x_mpec','--use_max_physical_edge_collision_for_x_dimension', help = 'Use Maximum Congestion as the x-dimension', action = 'store_true')
	parser.add_argument('-x_nr','--use_number_of_racks_for_x_dimension', help = 'Use Number of racks for x-dimension', action = 'store_true')
	parser.add_argument('-x_vn_ar','--use_vn_arrival_rate_for_x_dimension', help = 'Use VN Arrival Rate for x-dimension', action = 'store_true')
	parser.add_argument('-x_nvn','--use_num_vn_nodes_for_x_dimension', help = 'Use Number of VN Nodes for x-dimension', action = 'store_true')
	parser.add_argument('-x_real_nvn','--use_real_num_vn_nodes_for_x_dimension', help = 'Use Real Number of VN Nodes for x-dimension', action = 'store_true')

	# Y-dimension (Specify exactly one)
	parser.add_argument('-y_vnt', '--use_vn_throughput_for_y_dimension', help = 'Use Average VN Throughput as the y-dimension', action = 'store_true')
	parser.add_argument('-y_vnt_gb', '--use_vn_throughput_in_giga_bits_for_y_dimension', help = 'Use Average VN Throughput (Converting from Bytes to Giga Bits) as the y-dimension', action = 'store_true')
	parser.add_argument('-y_rel_vnt', '--use_relative_vn_throughput_for_y_dimension', help = 'Use Relative Average VN Throughput as the y-dimension', action = 'store_true')
	parser.add_argument('-y_pnu', '--use_pn_utilization_for_y_dimension', help = 'Use Average PN Utilization as the y-dimension', action = 'store_true')
	parser.add_argument('-y_rla', '--use_ratio_of_links_accepted_for_y_dimension', help = 'Use Ratio of Links Accepted as the y-dimension', action = 'store_true')
	parser.add_argument('-y_rna', '--use_ratio_of_networks_accepted_for_y_dimension', help = 'Use Ratio of Network Accepted as the y-dimension', action = 'store_true')
	parser.add_argument('-y_avg_rev_ar', '--use_average_revenue_arrival_rate_for_y_dimension', help = 'Use Average accepted revenue (arrival rate formulation) as the y-dimension', action = 'store_true')
	parser.add_argument('-y_avg_rev_data', '--use_average_revenue_data_for_y_dimension', help = 'Use Average accepted revenue (data formulation) as the y-dimension', action = 'store_true')
	parser.add_argument('-y_avg_rev_norm', '--use_average_revenue_normalized_for_y_dimension', help = 'Use Average accepted revenue (normalized formulation) as the y-dimension', action = 'store_true')
	parser.add_argument('-y_pc', '--use_percent_congestion_for_y_dimension', help = 'Use Percent Congestion as the y-dimension', action = 'store_true')
	parser.add_argument('-y_piu', '--use_percent_impossible_to_use_for_y_dimension', help = 'Use Percent FSO Impossible to use as the y-dimension', action = 'store_true')
	parser.add_argument('-y_afr', '--use_average_flow_rate_for_y_dimension', help = 'Use Average Flow Rate as the y-dimension', action = 'store_true')
	parser.add_argument('-y_fct', '--use_flow_completion_time_for_y_dimension', help = 'Use Flow Completion Time as the y-dimension', action = 'store_true')
	parser.add_argument('-y_fct_no', '--use_flow_completion_time_no_outliers_for_y_dimension', help = 'Use Flow Completion Time (ignoring outliers beyond the 10th and 90th percentile) as the y-dimension', action = 'store_true')
	parser.add_argument('-y_mar', '--use_max_arrival_rate_for_y_dimension', help = 'Use Max Arrival Rate as the y-dimension', action = 'store_true')

	parser.add_argument('-y_spa', '--use_shortest_path_accept_for_y_dimension', help = 'Use Shortest Path accept as the y-dimension', action = 'store_true')
	parser.add_argument('-y_spr', '--use_shortest_path_reject_for_y_dimension', help = 'Use Shortest Path reject as the y-dimension', action = 'store_true')
	parser.add_argument('-y_tru', '--use_total_resource_usage_per_node_for_y_dimension', help = 'Use total resource usage per node as the y-dimension', action = 'store_true')
	parser.add_argument('-y_rbu', '--use_reject_because_useless_for_y_dimension', help = 'Use reject because useless as the y-dimension', action = 'store_true')

	parser.add_argument('-y_rtc', '--use_revenue_to_cost_ratio_for_y_dimension', help = 'Use revenute to cost ratio as the y-dimension', action = 'store_true')

	# Y-dimension ILP
	parser.add_argument('-y_ilp_rla', '--use_ratio_of_links_accepted_ilp_for_y_dimension', help = 'Use Ratio of Links Accepted as the y-dimension. Shows using our algo and ILP algo.', action = 'store_true')
	parser.add_argument('-y_ilp_rna', '--use_ratio_of_network_accepted_ilp_for_y_dimension', help = 'Use Ratio of Networks Accepted as the y-dimension. Shows using our algo and ILP algo.', action = 'store_true')
	parser.add_argument('-y_ilp_avg_rev_norm', '--use_average_revenue_normalized_ilp_for_y_dimension', help = 'Use average_revenue_normalized as the y-dimension. Shows using our algo and ILP algo.', action = 'store_true')
	parser.add_argument('-y_ilp_vnt', '--use_virtual_network_throughput_ilp_for_y_dimension', help = 'Use virtual_network_throughput as the y-dimension. Shows using our algo and ILP algo.', action = 'store_true')
	
	# Embedding time
	parser.add_argument('-y_emb', '--use_embedding_time_for_y_dimension', help = 'Use embedding time as the y-dimension.', action = 'store_true')

	# Group Values (For now only one, but should allow any number)
	parser.add_argument('-g_mpec','--use_max_physical_edge_collision_for_grouping', help = 'Use Maximum Congestion to group data', action = 'store_true')
	parser.add_argument('-g_pne','--use_physical_num_edges_for_grouping', help = 'Use # of Links per FSO to group data', action = 'store_true')
	parser.add_argument('-g_svd','--use_static_or_dynamic_for_grouping', help = 'Use whether the network was static or dynamic to group data', action = 'store_true')
	parser.add_argument('-g_pws','--use_path_weighting_scheme_for_grouping', help = 'Use Path Weighting Scheme to group data', action = 'store_true')
	parser.add_argument('-g_rwd','--use_real_world_data_for_grouping', help = 'Use the Real World Data to group data', action = 'store_true')
	parser.add_argument('-g_toy','--use_toy_network_params_for_grouping', help = 'Uses the type of toy network to group data', action = 'store_true')
	parser.add_argument('-g_lms','--use_link_mapping_scheme_for_grouping', help = 'Uses link mapping scheme to group data', action = 'store_true')

	# Filter
	parser.add_argument('-f_pws','--filter_by_path_weighting_scheme', metavar = 'PWS', type = MVN.PathWeightingScheme, nargs = '+', default = [], help = 'Only use test cases that use one of the supplied PWSs')
	parser.add_argument('-f_pne','--filter_by_physical_num_edges', metavar = 'PNE', type = int, nargs = '+', default = [], help = 'Only use test cases that use one of the supplied PNEs')
	parser.add_argument('-f_rwd','--filter_by_real_world_data', metavar = 'RWD', type = SIM.RealWorldData, nargs = '+', default = [], help = 'Only use test cases that use one of the supplied RWD')

	parser.add_argument('-err','--include_error_bars', help = 'Includes Error Bars in the plot', action = 'store_true')
	parser.add_argument('-print','--print_data', help = 'Doesn\'t draw a plot, instead just prints the data to stdout', action = 'store_true')

	parser.add_argument('-t','--title', metavar = 'TITLE', type = str, nargs = 1, default = [''], help = 'Title of the Graph')

	parser.add_argument('-out', '--output_file', metavar = 'OUT_FILE', type = str, nargs = 1, default = [None], help = 'If a file is given, then no graph is displayed, and the XY data is written to supplied file.')

	args = parser.parse_args()

	params, data = getDataFromFiles(args.data_file_name)
	
	# Parse x-dimension flags
	x_dimension = []
	if args.use_physical_num_edges_for_x_dimension:
		x_dimension.append('physical_num_edges')
	if args.use_max_physical_edge_collision_for_x_dimension:
		x_dimension.append('max_physical_edge_collision')
	if args.use_number_of_racks_for_x_dimension:
		x_dimension.append('physical_num_nodes')
	if args.use_vn_arrival_rate_for_x_dimension:
		x_dimension.append('virtual_network_arrival_rate')
	if args.use_num_vn_nodes_for_x_dimension:
		x_dimension.append('virtual_num_nodes')
	if args.use_real_num_vn_nodes_for_x_dimension:
		x_dimension.append('real_vn_sizes')

	if len(x_dimension) != 1:
		raise Exception('Must supply exactly 1 x-dimension flag')
	x_dimension = x_dimension[0]

	# Parse y-dimension flags
	y_dimension = []
	y_func = [] # Must have the same number of arguments as the length of the corresponding element in y_dimension, and must return a float
	graph_func = graphData
	include_fliers = None
	if args.use_vn_throughput_for_y_dimension:
		y_dimension.append(('average_vn_throughput_per_second',))
		y_func.append(lambda x: x / 1000.0 / 1000.0 / 1000.0 * 8.0)
	if args.use_vn_throughput_in_giga_bits_for_y_dimension:
		y_dimension.append(('average_vn_throughput_per_second',))
		y_func.append(lambda x: x / 1000.0 / 1000.0 / 1000.0 * 8.0)
	if args.use_relative_vn_throughput_for_y_dimension:
		y_dimension.append(('average_vn_throughput_per_second', 'num_virtual_edges_accepted', 'arrival_rate', 'flow_size'))
		y_func.append(lambda t, ls, ar, fs: t / (ls * fs / ar))
	if args.use_pn_utilization_for_y_dimension:
		y_dimension.append(('average_pn_throughput_per_second', 'physical_num_nodes', 'num_edge_sets', 'physical_node_to_node_bandwidth'))

		# x is in bytes/sec, and ebw is in gigabits/sec
		y_func.append(lambda x, nn, nes, ebw: float(x) / (nn * nes * (ebw * 1000.0 * 1000.0 * 1000.0 / 8.0) / 2.0))
	if args.use_ratio_of_links_accepted_for_y_dimension:
		y_dimension.append(('num_virtual_edges_accepted', 'num_virtual_edges_rejected'))
		y_func.append(lambda a, r: float(a) / float(a + r))
	if args.use_ratio_of_networks_accepted_for_y_dimension:
		y_dimension.append(('num_vns_accepted', 'num_vns_rejected'))
		y_func.append(lambda a, r: float(a) / float(a + r))
	if args.use_average_revenue_arrival_rate_for_y_dimension:
		y_dimension.append(('average_accepted_revenue_ar_formula',))
		y_func.append(float)
	if args.use_average_revenue_data_for_y_dimension:
		y_dimension.append(('average_accepted_revenue_data_formula',))
		y_func.append(float)
	if args.use_average_revenue_normalized_for_y_dimension:
		y_dimension.append(('average_accepted_revenue_normalized_data_formula',))
		y_func.append(float)
	if args.use_revenue_to_cost_ratio_for_y_dimension:
		y_dimension.append(('average_revenue_to_cost_ratio',))
		y_func.append(float)
	if args.use_percent_congestion_for_y_dimension:
		y_dimension.append(('percent_congestion',))
		y_func.append(float)
	if args.use_percent_impossible_to_use_for_y_dimension:
		y_dimension.append(('percent_impossible_to_use',))
		y_func.append(float)
	if args.use_average_flow_rate_for_y_dimension:
		y_dimension.append(('avg_flow_rate',))
		y_func.append(float)
	if args.use_flow_completion_time_for_y_dimension:
		y_dimension.append(('flow_completion_times',))
		y_func.append(lambda x: x)
		graph_func = makeBoxPlot
		include_fliers = True
	if args.use_flow_completion_time_no_outliers_for_y_dimension:
		y_dimension.append(('flow_completion_times',))
		y_func.append(lambda x: x)
		graph_func = makeBoxPlot
		include_fliers = False
	if args.use_max_arrival_rate_for_y_dimension:
		y_dimension.append(('max_edge_arrival_rate',))
		y_func.append(float)

	if args.use_ratio_of_links_accepted_ilp_for_y_dimension:
		y_dimension.append(('num_virtual_edges_accepted', 'num_virtual_edges_rejected'))
		y_func.append(lambda a, r: float(a) / float(a + r))

		y_dimension.append(('mip_num_virtual_edges_accepted', 'mip_num_virtual_edges_rejected'))
		y_func.append(lambda a, r: float(a) / float(a + r))

		y_dimension.append(('lp_num_virtual_edges_accepted', 'lp_num_virtual_edges_rejected'))
		y_func.append(lambda a, r: float(a) / float(a + r))

	if args.use_ratio_of_network_accepted_ilp_for_y_dimension:
		y_dimension.append(('num_vns_accepted', 'num_vns_rejected'))
		y_func.append(lambda a, r: float(a) / float(a + r))

		y_dimension.append(('mip_num_vns_accepted', 'mip_num_vns_rejected'))
		y_func.append(lambda a, r: float(a) / float(a + r))

		y_dimension.append(('lp_num_vns_accepted', 'lp_num_vns_rejected'))
		y_func.append(lambda a, r: float(a) / float(a + r))
	if args.use_average_revenue_normalized_ilp_for_y_dimension:
		y_dimension.append(('average_accepted_revenue_normalized_data_formula',))
		y_func.append(float)

		y_dimension.append(('mip_average_accepted_revenue_normalized_data_formula',))
		y_func.append(float)

		y_dimension.append(('lp_average_accepted_revenue_normalized_data_formula',))
		y_func.append(float)
	if args.use_virtual_network_throughput_ilp_for_y_dimension:
		y_dimension.append(('average_vn_throughput_per_second',))
		y_func.append(lambda x: x / 1000.0 / 1000.0 / 1000.0 * 8.0)

		y_dimension.append(('mip_average_vn_throughput_per_second',))
		y_func.append(lambda x: x / 1000.0 / 1000.0 / 1000.0 * 8.0)

	if args.use_shortest_path_accept_for_y_dimension:
		y_dimension.append(('shortest_path_accept_str',))
		y_func.append(lambda x: x)
	if args.use_shortest_path_reject_for_y_dimension:
		y_dimension.append(('shortest_path_reject_str',))
		y_func.append(lambda x: x)
	if args.use_total_resource_usage_per_node_for_y_dimension:
		y_dimension.append(('total_resource_usage_per_node',))

		def process_total_resource_usage_per_node(x):
			return sorted([v for k, v in x.iteritems()])

		y_func.append(process_total_resource_usage_per_node)
	if args.use_reject_because_useless_for_y_dimension:
		y_dimension.append(('reject_because_useless',))
		y_func.append(float)

	if args.use_embedding_time_for_y_dimension:
		y_dimension.append(('average_embedding_time_in_seconds',))
		y_func.append(float)


	# if len(y_dimension) != 1 or len(y_func) != 1:
	# 	raise Exception('Must supply exactly 1 y-dimension flag')
	# y_dimensions = y_dimension[0]
	# y_func = y_func[0]

	# Parse group-by flags
	group_by = []
	group_func = []
	if args.use_max_physical_edge_collision_for_grouping:
		group_by.append('max_physical_edge_collision')
		group_func.append(None)
	if args.use_physical_num_edges_for_grouping:
		group_by.append('physical_num_edges')
		group_func.append(None)
	if args.use_static_or_dynamic_for_grouping:
		group_by.append('physical_num_edges')
		group_func.append(lambda x: ('static' if x == 1 else 'dynamic'))
	if args.use_path_weighting_scheme_for_grouping:
		group_by.append('path_weighting_scheme')
		group_func.append(None)
	if args.use_real_world_data_for_grouping:
		group_by.append('real_world_data')
		group_func.append(None)
	if args.use_toy_network_params_for_grouping:
		group_by.append('toy_physical_network_params')
		group_func.append(None)
	if args.use_link_mapping_scheme_for_grouping:
		group_by.append('link_mapping_scheme')
		group_func.append(None)

	# if len(group_by) != 1:
	# 	raise Exception('Must supply exactly 1 group-by flag for now')
	group_by = tuple(group_by)

	# Parse Filter Flags
	filters = []
	if len(args.filter_by_path_weighting_scheme) > 0:
		filters.append(EqualFilter('path_weighting_scheme', args.filter_by_path_weighting_scheme))
	if len(args.filter_by_physical_num_edges) > 0:
		filters.append(EqualFilter('physical_num_edges', args.filter_by_physical_num_edges))
	if len(args.filter_by_real_world_data) > 0:
		filters.append(EqualFilter('real_world_data', args.filter_by_real_world_data))

	include_y_errbars = args.include_error_bars

	output_file = args.output_file[0]

	# Make the graph
	if args.print_data:
		printData(params, data, x_dimension, y_dimension, y_func, group_by)
	else:
		graph_func(params, data, x_dimension, y_dimension, y_func, group_by, legend_loc = args.legend_loc[0], filters = filters, title = args.title[0], include_fliers = include_fliers, include_y_errbars = include_y_errbars, group_func = group_func, output_file = output_file)
