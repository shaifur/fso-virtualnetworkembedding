from ilp_non_bursty import ILP_Non_Bursty

if __name__ == '__main__':
    #---set the filepaths-------#
    phy_network_file_path = './network_data/test_phy_net.txt' #<--set path to phy. network description
    vn_request_file_path='./network_data/test_vn_reqs.txt' #<--set path to vn-requests description
    modelFile = './debug.lp' #<--set path to the file where model equations are dumped as .lp file, use '' to skip file write
    #----------------------------#

    ilp_non_bursty = ILP_Non_Bursty()
    modeltype = ilp_non_bursty.MIP #<--set modeltype: can be IP, MIP, LP
    '''
    modeltype description:
    IP: All variables binaries, the slowest to run
    MIP: All variables except flow variables are binaries, reflect the case of splittable flows
    LP: All variables continuous in the interval [0,1], gives a very loose lowerbound on cost, the fastest to run
    '''


    ilp_non_bursty.readPhysicalNetwork(filepath=phy_network_file_path)
    ilp_non_bursty.readVNRequests(filepath=vn_request_file_path)

    cost_of_embedding = ilp_non_bursty.run(modelType = modeltype,
                                           modelFile = modelFile)
    '''
    cost_of_embedding is: -1 for infeasible solution, any value >=0 for a feasible solution (the revenue is the sum of vn requests)
    '''
    print "Cost val:", cost_of_embedding