from gurobipy import *
import logging

class ILP_Non_Bursty(object):
    '''

    '''
    def __init__(self):
        '''

        '''
        self.LP = 1
        self.IP = 2
        self.MIP = 3

        self.setClassLogger()
        self.logger.setLevel(logging.INFO) #<--change logging level
        self.logger.debug("ILP Non Bursty Initialized..")


    def setClassLogger(self):
        self.logger = logging.getLogger('ILP_Non_Bursty_Logger')
        self.logger.setLevel(logging.DEBUG)

        # create console handler and set level to debug
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)

        # create formatter
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # add formatter to ch
        ch.setFormatter(formatter)

        # add ch to logger
        self.logger.addHandler(ch)

    def readPhysicalNetwork(self, filepath):
        '''
        from the file pointed to by filepath, read
         i) no. of  phy. nodes
         ii) list of capacity of each nodes (if skipped, capacity can be the same for each node)
         ii) candidate links as node pairs and its capacity
               (if skipped, capacity can be the same for each candidate link)

        :param filepath:
        :return:
        '''
        self.pnode_capacity = []
        self.pnode_max_deg = []
        self.plink = []
        self.plink_capacity = []

        with open(filepath, "r") as f:
            #read the no. of nodes
            fline = f.readline().split(' ')
            self.pnode_count = int(fline[0])
            #store physical node capacities and max_deg
            for n in range(self.pnode_count):
                fline = f.readline().split(' ')
                cap = float(fline[1])
                deg = int(fline[2])
                self.pnode_capacity.append(cap)
                self.pnode_max_deg.append(deg)
            #read the no. of links
            fline = f.readline().split(' ')
            self.plink_count = int(fline[0])

            #store list  of candidate links with corresponding capacities
            for i in range(self.pnode_count):
                self.plink_capacity.append({})
            for lc in range(self.plink_count):
                fline = f.readline().split(' ')
                i = int(fline[0])
                j = int(fline[1])
                c = float(fline[2])
                #for each pair of values (i, j, c) read from the file..
                #add both i and j to each other's adjacency list
                self.plink_capacity[i][j] = c
                self.plink_capacity[j][i] = c
                self.plink.append( (i, j) )
        self.logger.info("Physical network loaded, total nodes:%d, total (undir) links:%d", self.pnode_count, self.plink_count)

    def readVNRequests(self, filepath):
        '''
        read virtual network request from file pointed to by filepath
        :param filepath:
        :return:
        '''
        self.vnode_demand = []
        self.vlink = []
        with open(filepath, "r") as f:
            fline = f.readline().split(' ')
            self.vnreq_count = int(fline[0])

            for r in range(self.vnreq_count):
                #read no. of nodes in this request
                fline = f.readline().split(' ')
                cur_vnode_count = int(fline[0])

                #read the node demands in this request and store
                cur_vnode_demand = []
                for n in range(cur_vnode_count):
                    fline = f.readline().split(' ')
                    cur_vnode_demand.append(float(fline[1]))
                self.vnode_demand.append(cur_vnode_demand)


                #read no. of links in this request
                fline = f.readline().split(' ')
                cur_vlink_count = int(fline[0])

                #read the link demands in this request and store
                cur_vlink_demand = [] # attn: unidir links considered if u <v, (u,v) is stored..
                for l in range(cur_vlink_count):
                    fline = f.readline().split(' ')
                    u = int(fline[0])
                    v = int(fline[1])
                    d = float(fline[2])
                    cur_vlink_demand.append(  (u, v, d)   )
                self.vlink.append(cur_vlink_demand)

        self.vnode_count_per_request = [len(self.vnode_demand[r]) for r in range(self.vnreq_count)]


    def getVariableIndices(self):
        '''

        :return:
        '''
        # ----variable x indexed by (r, u, m)---is 1 if request r's node u is mapped to phy. node m
        # ---enumerate the variable x indices----
        x_indx = tuplelist()
        for r in range(self.vnreq_count):
            for u in range(self.vnode_count_per_request[r]):
                for m in range(self.pnode_count):
                    x_indx.append((r, u, m))
        # ----variable f indexed by (r, u, v, m, n)
        # ---is 1 if request r's link (u,v) is mapped to phy. link (m,n). Both directed edges
        # ---enumerate the variable f indices----
        f_indx = tuplelist()
        for r in range(self.vnreq_count):
            for (u, v, d) in self.vlink[r]:
                for (m, n) in self.plink:
                    f_indx.append((r, u, v, m, n))
                    f_indx.append((r, u, v, n, m))
                    f_indx.append((r, v, u, m, n))
                    f_indx.append((r, v, u, n, m))


        #----variable y indexed by (m, n)---is 1 if m,n is a candidate link
        # ---enumerate variable y indices-------
        y_indx = tuplelist()
        for m in range(self.pnode_count - 1):
            for n in range(m + 1, self.pnode_count):
                y_indx.append((m, n))
                y_indx.append((n, m))

        return x_indx, f_indx, y_indx

    def generateILPInstance(self, modelType):
        '''

        :return:
        '''
        #----declare the Gurboi Model------#
        self.ilp_model = Model("ILPModel")
        #build the indices
        x_indx, f_indx, y_indx = self.getVariableIndices()
        #----------------------------variable type determination----------------------------------#
        #default model: LP
        x_var_type = GRB.CONTINUOUS
        f_var_type = GRB.CONTINUOUS
        y_var_type = GRB.CONTINUOUS

        if modelType == self.IP:
            x_var_type = GRB.BINARY
            f_var_type = GRB.BINARY
            y_var_type = GRB.BINARY
        elif modelType == self.MIP:
            x_var_type = GRB.BINARY
            f_var_type = GRB.CONTINUOUS
            y_var_type = GRB.BINARY
        else:
            self.logger.info("Unknown Model Type (must be IP, LP or MIP)...defaulting to LP..")
        #------------------------------------------------------------------------------------------#
        #---now add the variables-------#
        x = self.ilp_model.addVars(x_indx, lb = 0, ub = 1, vtype=x_var_type, name="x")
        f = self.ilp_model.addVars(f_indx, lb = 0, ub = 1, vtype=f_var_type, name="f")
        y = self.ilp_model.addVars(y_indx, lb = 0, ub = 1, vtype=y_var_type, name="y")

        #----constr 1: node mapping completeness constraint-----#
        self.ilp_model.addConstrs(
            (x.sum(r, u, '*') == 1 for r in range(self.vnreq_count) for u in range( self.vnode_count_per_request[r] )),
            "const_1_node_mapping_completeness")

        #-------constr 2: node uniqueness constraint-------#
        self.ilp_model.addConstrs(
            (x.sum(r, '*', m) <= 1 for r in range(self.vnreq_count) for m in range( self.pnode_count)  ),
            "const_2_node_mapping_uniqueness")
        #-------constr 3: node capacity constraint---------#
        for m in range(self.pnode_count):
            self.ilp_model.addConstr( quicksum( (x[r, u, m]* self.vnode_demand[r][u])
                                           for r in range(self.vnreq_count)
                                           for u in range(self.vnode_count_per_request[r]))
                                 <=self.pnode_capacity[m],
                                 name="capacity_constr_node_"+str(m))

        # -------constr 4a: multi-commodity-flow-constraint---------#
        for r in range(self.vnreq_count):
            for u_un, v_un,_ in self.vlink[r]:
                for u,v in [(u_un, v_un),(v_un, u_un)]: #consider directed_edge
                    for m in range(self.pnode_count):
                        source_expr =  quicksum( f[r, u, v, m, n] for n in self.plink_capacity[m].keys())
                        sink_expr =    quicksum( f[r, u, v, n, m] for n in self.plink_capacity[m].keys())
                        self.ilp_model.addConstr( source_expr - sink_expr == x[r,u,m] - x[r, v, m], name="mcf_constr_r_uv_m_"+str(r)+"_"+str(u)+str(v)+"_"+str(m))

        # -------constr 4b: flow_variable limited by candidate link variable---------#
        for r in range(self.vnreq_count):
            for u_un, v_un,_ in self.vlink[r]:
                for u,v in [( u_un, v_un),(v_un , u_un)]:
                    for m in range(self.pnode_count):
                        for n in self.plink_capacity[m].keys():
                            self.ilp_model.addConstr(f[r,u,v,m,n] <= y[m,n], name = "mcf_flimit_f_y__r_uv_mn"+str(r)+"_"+str(u)+str(v)+"_"+str(m)+str(n))

        #-----constr 5: link capacity constr----------#
        for m in range(self.pnode_count):
            for n, mn_cap in self.plink_capacity[m].iteritems(): #both m,n and n,m edges are considered in the loop
                f_exp_mn = quicksum(
                     (f[r, u, v, m, n]*b_uv + f[r, v, u, m, n]*b_uv)
                        for r in range(self.vnreq_count)
                            for u, v, b_uv in self.vlink[r]
                    )
                self.ilp_model.addConstr(f_exp_mn <= mn_cap * y[m, n],
                                    name="link_capacity_constr_mn_" + str(m) + str(n))

        # -----constr 6a: candidate link constr----------#
        for m in range(self.pnode_count-1):
            for n in range(m+1, self.pnode_count):
                if (m,n) in self.plink:
                    self.ilp_model.addConstr(y[m,n]  <= 1, name = "clink_constr_mn_" + str(m) + str(n))
                    self.ilp_model.addConstr(y[n, m] <= 1, name = "clink_constr_mn_" + str(n) + str(m))
                else:
                    self.ilp_model.addConstr(y[m,n]  == 0, name = "clink_constr_mn_" + str(m) + str(n))
                    self.ilp_model.addConstr(y[n, m] == 0, name = "clink_constr_mn_" + str(n) + str(m))


        # -----constr 6b: candidate link constr----------#
        for m in range(self.pnode_count):
            self.ilp_model.addConstr(
                quicksum(
                    y[m, n] for n in self.plink_capacity[m].keys()
                ) <= self.pnode_max_deg[m], name = "node_deg_constr_"+str(m)
            )
        #--------set objective-----------------------#
        cost_expr = quicksum(
                     (x[r, u, m])*self.vnode_demand[r][u]
                        for r in range(self.vnreq_count)
                            for u in range(self.vnode_count_per_request[r])
                                for m in range(self.pnode_count)
                    )+quicksum(
                     (f[r, u, v, m, n]*b_uv + f[r, v, u, m, n]*b_uv)
                        for r in range(self.vnreq_count)
                            for u, v, b_uv in self.vlink[r]
                                for m,n in self.plink
                    )
        self.ilp_model.setObjective(cost_expr, GRB.MINIMIZE)


    def run(self, modelType, modelFile):
        '''

        :return:
        '''
        self.generateILPInstance(modelType)
        self.ilp_model.update()
        self.logger.info("Total no. of int. variables: %d, total no. of model variables: %d", self.ilp_model.numintvars,
                         self.ilp_model.numvars)
        if modelFile:
            self.ilp_model.write(modelFile)

        obj_val = -1
        self.ilp_model.optimize()
        if self.ilp_model.status == GRB.Status.OPTIMAL:
            obj_val = self.ilp_model.objVal
            self.logger.info("optimal solution found, min. cost of embedding: %0.2f", obj_val)
        elif self.ilp_model.status == GRB.Status.SUBOPTIMAL:
            obj_val = self.ilp_model.objVal
            self.logger.info("suboptimal solution found, cost of embedding: %0.2f", obj_val)
        elif self.ilp_model.status == GRB.INFEASIBLE:
            self.logger.info("No Feasible Solution Found")
        elif self.ilp_model.status == GRB.UNBOUNDED:
            self.logger.error("Model is Unbounded, recheck..")
        else:
            self.logger.error("Gurobi Error code:%d", self.ilp_model.status)
        return obj_val


if __name__ == '__main__':
    #---set the filepaths-------#
    phy_network_file_path = './network_data/test_phy_net.txt'
    vn_request_file_path='./network_data/test_vn_reqs.txt'
    modelFile = 'debug.lp'
    #----------------------------#


    ilp_non_bursty = ILP_Non_Bursty()
    ilp_non_bursty.readPhysicalNetwork(filepath=phy_network_file_path)
    ilp_non_bursty.readVNRequests(filepath=vn_request_file_path)

    cost_of_embedding = ilp_non_bursty.run(modelType=ilp_non_bursty.MIP,
                                           modelFile = modelFile)
    print "Cost val:", cost_of_embedding
