
import gurobipy as GB
from collections import defaultdict

import re

class ILPBurstySolver:
	# Constants representing different models for the solver
	LP = 1
	IP = 2
	MIP = 3

	all_types = [LP, IP, MIP]

	def __init__(self, model_type = None, status_file = None):
		self.model_type = (model_type if model_type != None else ILPBurstySolver.MIP)

		if self.model_type not in ILPBurstySolver.all_types:
			raise Exception('Invalid model type selected')

		self.status_file = status_file

	def parseVirtualNetworkRequests(self, virtual_network_requests):
		# Group necessary data from virtual_network_requests
		
		# List of VirtualNetworkRequests. The vnr will be refered to by their index in this list
		self.vnrs = list(virtual_network_requests)

		# List of lists of VirtualNodes. The ith sublist are the Virtual Nodes of the ith virtual network requests in self.vnrs. The Virtual Nodes will be referenced by their index in their sublist
		self.virtual_nodes = [[vnr.virtual_network.nodes[vn_id]
									for vn_id in vnr.virtual_network.nodes]
							for vnr in self.vnrs]

		# List of lists of 3-tuple (vn1_id, vn2_id, VirtualEdge). Each 3-tuple (a, b, ve) in the ith sublist are an edge in the ith virtual network between nodes self.virtual_nodes[i][a] and self.virtual_nodes[i][b], and the ve object holds data associated with the edge. The edges are bidirectional.
		self.virtual_edges = [[(i, j, vn1.getConnectingEdge(vn2))
									for i, vn1 in enumerate(self.virtual_nodes[r])
										for j, vn2 in enumerate(self.virtual_nodes[r])
											if i < j and vn1.hasNeighbor(vn2)]
								for r in range(len(self.vnrs))]

	def parsePhysicalNetwork(self, physical_network):
		# Group necessary data from physical_network
		
		# The classes of the two different types of objects that are considered "nodes." These will be used to differentiate the constraints on each type of "node"
		self.physical_rack_class = physical_network.getNodeClass()
		self.physical_server_class = physical_network.getServerClass()
		
		# List of all "nodes", this includes PhysicalNodes (aka Racks) and PhysicalServers
		self.physical_nodes = [physical_network.nodes[node_id] for node_id in physical_network.nodes] + list(physical_network.getAllServers())

		physical_node_by_index = {pn: i for i, pn in enumerate(self.physical_nodes)}

		# The classes of the two different types of objects that are considered "edges"
		self.physical_candidate_edge_class = physical_network.getCandidateEdgeClass()
		self.physical_server_to_node_edge_class = physical_network.getServerClass() 
		
		# List of all "edges", this includes PhysicalEdges (between two FSOs), and edges between PhysicalServers and PhysicalNodes (which don't have their own object, and are stored as PhysicalServers).
		# Each element is a 3-tuple ("Edge" object, source_node_index, destination_node_index), each edge is therefore directional
		self.physical_edges = [(pe, physical_node_by_index[pe.edge_set1.node], physical_node_by_index[pe.edge_set2.node]) for pe in physical_network.getAllPhysicalEdges()] + \
								[(pe, physical_node_by_index[pe.edge_set2.node], physical_node_by_index[pe.edge_set1.node]) for pe in physical_network.getAllPhysicalEdges()] + \
								[(ps, physical_node_by_index[ps], physical_node_by_index[ps.node]) for ps in physical_network.getAllServers()] + \
								[(ps, physical_node_by_index[ps.node], physical_node_by_index[ps]) for ps in physical_network.getAllServers()]

		# Key is physical_node index, value is list of physical_edge indexes where the key value is the src/dst
		self.src_physical_edges = defaultdict(list)
		self.dst_physical_edges = defaultdict(list)

		# Key is physical_node index (that refers to a PhysicalNode), value is dict with key of PhysicalEdgeSet and value of list of physical_edge ids (The physical_edge has the source of keyed physical_node and is associated with the keyed PhysicalEdgeSet)
		self.edges_by_edge_sets = defaultdict(lambda: defaultdict(list))

		# Key is physical_node index (that refers to a PhysicalServer), values are list of incoming and outgoing physical_edge indexes
		self.edges_by_server = defaultdict(list)
		for i, (edge_object, src_node, dst_node) in enumerate(self.physical_edges):
			self.src_physical_edges[src_node].append(i)
			self.dst_physical_edges[dst_node].append(i)

			if isinstance(edge_object, self.physical_candidate_edge_class):
				self.edges_by_edge_sets[src_node][edge_object.getThisEdgeSet(self.physical_nodes[src_node])].append(i)
			if isinstance(edge_object, self.physical_server_to_node_edge_class):
				if isinstance(self.physical_nodes[src_node], self.physical_server_class):
					self.edges_by_server[src_node].append(i)
				elif isinstance(self.physical_nodes[dst_node], self.physical_server_class):
					self.edges_by_server[dst_node].append(i)
				else:
					raise Exception('Expected at least one to be a PhysicalServer')

	def getVariableIndices(self, physical_network, virtual_network_requests):
		# Input
		# 	physical_network - PhysicalNetwork object (see physical_network.py)
		# 	virtual_network_requests - list of VirtualNetworkRequest objects(see vn_requests.py)
		self.parseVirtualNetworkRequests(virtual_network_requests)
		self.parsePhysicalNetwork(physical_network)

		# variable x, indexed by (r, u, m), is 1 if request r's node u is mapped to physical node m
		x_indx = GB.tuplelist()
		for r in range(len(self.vnrs)):
			for u in range(len(self.virtual_nodes[r])):
				for m, physical_node in enumerate(self.physical_nodes):
					# Can only map virtual nodes to PhysicalServers, so we can ignore "nodes" that are PhysicalNodes
					if isinstance(physical_node, self.physical_server_class):
						x_indx.append((r, u, m))

		# variable f, indexed by (r, u, v, pe_id),
		# 	is 1 if the request r's link (u, v) is mapped to physical link pe_id
		f_indx = GB.tuplelist()
		for r in range(len(self.vnrs)):
			for u, v, ve in self.virtual_edges[r]:
				for pe_id in range(len(self.physical_edges)):
					f_indx.append((r, u, v, pe_id))
					f_indx.append((r, v, u, pe_id))
		return x_indx, f_indx

	def generateILPInstance(self, physical_network, virtual_network_requests, mip_gap = None):
		# Create Gurobi Model
		self.ilp_model = GB.Model("ILPModel")
		self.ilp_model.setParam( 'OutputFlag', False )
		self.ilp_model.setParam("Threads", 10)

		if mip_gap != None:
			self.ilp_model.setParam("TimeLimit", mip_gap)


		# Create indices
		x_indx, f_indx = self.getVariableIndices(physical_network, virtual_network_requests)

		# Determine the variable types
		if self.model_type == ILPBurstySolver.LP:
			x_var_type = GB.GRB.CONTINUOUS
			f_var_type = GB.GRB.CONTINUOUS
		elif self.model_type == self.IP:
			x_var_type = GB.GRB.BINARY
			f_var_type = GB.GRB.BINARY
		elif self.model_type == self.MIP:
			x_var_type = GB.GRB.BINARY
			f_var_type = GB.GRB.CONTINUOUS

		# Create the variables
		x = self.ilp_model.addVars(x_indx, lb = 0, ub = 1, vtype = x_var_type, name = 'x')
		f = self.ilp_model.addVars(f_indx, lb = 0, ub = 1, vtype = f_var_type, name = 'f')

		# Add Constraints
		# Constraint 1: Node Mapping Completeness (All virtual nodes are mapped to exactly one physical node)
		self.ilp_model.addConstrs(
				(x.sum(r, u, '*') == 1 for r in range(len(self.vnrs)) for u in range(len(self.virtual_nodes[r]))),
				'c1_node_mapping_completeness')

		# Constraint 2: Node Mapping Uniqueness (There is at most one virtual node from each request mapped to a physical node)
		self.ilp_model.addConstrs(
				(x.sum(r, '*', m) <= 1
						for r in range(len(self.vnrs))
							for m, pn in enumerate(self.physical_nodes)
								if isinstance(pn, self.physical_server_class)),
				'c2_node_mapping_uniqueness')

		# Constraint 3: Node Capacity Constraint (The mapped virtual nodes to a physical node don't exceed the physical node's CPU capacity)
		for m, physical_node in enumerate(self.physical_nodes):
			if isinstance(physical_node, self.physical_server_class):
				self.ilp_model.addConstr(GB.quicksum(x[r, u, m] * virtual_node.cpu_requirement
														for r in range(len(self.vnrs))
															for u, virtual_node in enumerate(self.virtual_nodes[r]))
												<= physical_node.cpu,
											name = 'c3_node_capacity_node:' + str(m))

		# Constraint 4: Multi-Commidity Flow (Essentially ensures that virtual edges are mapped to physical paths)
		for r in range(len(self.vnrs)):
			for a, b, virtual_edge in self.virtual_edges[r]:
				for u, v in ((a, b), (b, a)):
					for m, physical_node in enumerate(self.physical_nodes):
						source_expr = GB.quicksum(f[r, u, v, pe_id] for pe_id in self.src_physical_edges[m])
						sink_expr =   GB.quicksum(f[r, u, v, pe_id] for pe_id in self.dst_physical_edges[m])
						if isinstance(physical_node, self.physical_server_class):
							self.ilp_model.addConstr(source_expr - sink_expr == x[r, u, m] - x[r, v, m],
														name = 'c4_mcf_constraint:(' + str(r) + ', ' + str(u) + ', ' + str(v) + ', ' + str(m) + ')')
						if isinstance(physical_node, self.physical_rack_class):
							self.ilp_model.addConstr(source_expr - sink_expr == 0,
														name = 'c4_mcf_constraint:(' + str(r) + ', ' + str(u) + ', ' + str(v) + ', ' + str(m) + ')')

		for m, physical_node in enumerate(self.physical_nodes):
			# Constraint 5a: FSO Capacity Constraint
			if isinstance(physical_node, self.physical_rack_class):
				for edge_set, edge_ids in self.edges_by_edge_sets[m].iteritems():
					flow_expr = GB.quicksum(f[r, u, v, pe_id] * virtual_edge.getLargeFlowArrivalRate() +
											f[r, v, u, pe_id] * virtual_edge.getLargeFlowArrivalRate()
												for pe_id in edge_ids
													for r in range(len(self.vnrs))
														for u, v, virtual_edge in self.virtual_edges[r])
					self.ilp_model.addConstr(flow_expr <= physical_network.getMaxCandidateEdgeArrivalRate(),
												name = 'c5a_edge_set_capacity:(' + str(m) + ', ' + str(edge_set.id_num) + ')')
			# Constraint 5b: Server-to-Rack Capacity Constraint 
			if physical_network.max_server_to_node_edge_inter_arrival_time != None and isinstance(physical_node, self.physical_server_class):
				# Make sure that the flow mapped to the edge from and to the physical_server is less than constraint. If there is no constraint for this, skip this.
				for pe_id in self.edges_by_server[m]:
					flow_expr = GB.quicksum(f[r, u, v, pe_id] * virtual_edge.getTotalArrivalRateTime() +
											f[r, v, u, pe_id] * virtual_edge.getTotalArrivalRateTime()
													for r in range(len(self.vnrs))
														for u, v, virtual_edge in self.virtual_edges[r])
					self.ilp_model.addConstr(flow_expr <= physical_network.getMaxServerToNodeArrivalRate(),
												name = 'c5a_edge_set_capacity:(' + str(m) + ', ' + str(edge_set.id_num) + ')')

		# Set Objective
		cost_expr = GB.quicksum(x[r, u, m] * virtual_node.cpu_requirement
									for r in range(len(self.vnrs))
										for u, virtual_node in enumerate(self.virtual_nodes[r])
											for m, physical_node in enumerate(self.physical_nodes)
												if isinstance(physical_node, self.physical_server_class)) + \
					GB.quicksum(f[r, u, v, pe_id] * virtual_edge.large_flow_inter_arrival_time +
								f[r, v, u, pe_id] * virtual_edge.large_flow_inter_arrival_time
									for r in range(len(self.vnrs))
										for u, v, virtual_edge in self.virtual_edges[r]
											for pe_id, (physical_edge, src_node, dst_node) in enumerate(self.physical_edges)
												if isinstance(physical_edge, self.physical_candidate_edge_class))

		self.ilp_model.setObjective(cost_expr, GB.GRB.MINIMIZE)

	def run(self, physical_network, virtual_network_requests, k = None, mip_gap = None):
		# Generates the ILP Model
		self.generateILPInstance(physical_network, virtual_network_requests, mip_gap = mip_gap)
		
		self.ilp_model.update()

		if k != None:
			self.ilp_model.write('data/lp/out_%d.lp' % k)

		obj_val = None
		self.ilp_model.optimize()
		if self.ilp_model.status == GB.GRB.Status.OPTIMAL:
			obj_val = self.ilp_model.objVal
			self.status_file.write('Optimal, %f' % self.ilp_model.MIPGap)
		elif self.ilp_model.status == GB.GRB.Status.SUBOPTIMAL:
			obj_val = self.ilp_model.objVal
			self.status_file.write('Suboptimal, %f' % self.ilp_model.MIPGap)
		elif self.ilp_model.status == GB.GRB.TIME_LIMIT:
			if self.ilp_model.MIPGap < 1.0:
				obj_val = self.ilp_model.objVal
			self.status_file.write(('Time_limit, %f ' % self.ilp_model.MIPGap) + str(obj_val))
		elif self.ilp_model.status == GB.GRB.INFEASIBLE:
			self.status_file.write('Infeasible, %f' % self.ilp_model.MIPGap)
		elif self.ilp_model.status == GB.GRB.UNBOUNDED:
			self.status_file.write('Unbounded, %f' % self.ilp_model.MIPGap)
			raise Exception('Model is Unbounded, recheck..')
		else:
			raise Exception('Gurobi Error code: ' + str(self.ilp_model.status))
		return obj_val

	def computeILPMapping(self, physical_network, virtual_network_requests, k = None, mip_gap = None):
		rv = self.run(physical_network, virtual_network_requests, k = k, mip_gap = mip_gap)

		if rv != None and self.model_type in [ILPBurstySolver.MIP, ILPBurstySolver.IP]:
			# Dict where Key is VirtualNode, and value is the PhysicalServer it is mapped to
			node_mapping = {}

			# Dict where Key is VirtualEdge, and value is a list of 2-tuple (PhysicalEdge, total_weight)
			edge_mapping = defaultdict(list)
			extra_edge_mapping = defaultdict(list)
			for var in self.ilp_model.getVars():
				node_map_match = re.match('x\[(\d+),(\d+),(\d+)\]', var.VarName)
				flow_map_match = re.match('f\[(\d+),(\d+),(\d+),(\d+)\]', var.VarName)
				if node_map_match != None:
					r = int(node_map_match.group(1))
					u = int(node_map_match.group(2))
					m = int(node_map_match.group(3))

					if var.X == 1:
						node_mapping[self.virtual_nodes[r][u]] = self.physical_nodes[m]
				elif flow_map_match != None:
					r = int(flow_map_match.group(1))
					u = int(flow_map_match.group(2))
					v = int(flow_map_match.group(3))
					pe_id = int(flow_map_match.group(4))

					# Only use the mapping for one direction of the virtual edge just in case the mappings are different.
					if u < v and var.X > 0.0:
						virtual_edge = next((ve for i, j, ve in self.virtual_edges[r] if (i == u and j == v) or (i == v and j == u)))
						edge_object, src_node, dst_node = self.physical_edges[pe_id]
						
						# Only care about candidate edges, the other "edge" in the ILP is implicityly handled by the node mapping
						if isinstance(edge_object, self.physical_candidate_edge_class):
							edge_mapping[virtual_edge].append((edge_object, var.X))
						if isinstance(edge_object, self.physical_server_to_node_edge_class):
							extra_edge_mapping[virtual_edge].append((edge_object, var.X))

				else:
					raise Exception('Unrecongized ILP Var: ' + var.VarName)

			# if not (all(node in node_mapping for request_nodes in self.virtual_nodes for node in request_nodes) and \
			# 		all((virtual_edge in edge_mapping and node_mapping[virtual_edge.node1].node != node_mapping[virtual_edge.node2].node) or \
			# 				(virtual_edge not in edge_mapping and node_mapping[virtual_edge.node1].node == node_mapping[virtual_edge.node2].node) \
			# 			for request_edges in self.virtual_edges for a, b, virtual_edge in request_edges)):
			# 	raise Exception('ILP Mapping incomplete!!!!!!!!')

			# Returns the mappings if sucessful
			return node_mapping, edge_mapping
		elif rv != None and self.model_type in [ILPBurstySolver.LP]:
			# Dict where Key is VirtualNode, and value is the PhysicalServer it is mapped to
			node_mapping = defaultdict(list)

			# Dict where Key is VirtualEdge, and value is a list of 2-tuple (PhysicalEdge, total_weight)
			edge_mapping = defaultdict(list)
			for var in self.ilp_model.getVars():
				node_map_match = re.match('x\[(\d+),(\d+),(\d+)\]', var.VarName)
				flow_map_match = re.match('f\[(\d+),(\d+),(\d+),(\d+)\]', var.VarName)
				if node_map_match != None:
					r = int(node_map_match.group(1))
					u = int(node_map_match.group(2))
					m = int(node_map_match.group(3))

					if var.X > 0.0:
						node_mapping[self.virtual_nodes[r][u]].append((self.physical_nodes[m], var.X))
				elif flow_map_match != None:
					r = int(flow_map_match.group(1))
					u = int(flow_map_match.group(2))
					v = int(flow_map_match.group(3))
					pe_id = int(flow_map_match.group(4))

					# Only use the mapping for one direction of the virtual edge just in case the mappings are different.
					if u < v and var.X > 0.0:
						virtual_edge = next((ve for i, j, ve in self.virtual_edges[r] if (i == u and j == v) or (i == v and j == u)))
						edge_object, src_node, dst_node = self.physical_edges[pe_id]
						
						# Only care about candidate edges, the other "edge" in the ILP is implicityly handled by the node mapping
						if isinstance(edge_object, self.physical_candidate_edge_class):
							edge_mapping[virtual_edge].append((edge_object, var.X))

				else:
					raise Exception('Unrecongized ILP Var: ' + var.VarName)
			# Returns the mappings if sucessful
			return node_mapping, edge_mapping
		else:
			return None, None
