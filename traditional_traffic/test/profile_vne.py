from traditional_traffic.util import GenerateVNEProblemInstance
from traditional_traffic.vne_traditional import VNETraditional
import cProfile
from guppy import hpy
import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
import matplotlib.pyplot as plt
import random


def run_all():
    # --------Generate problem instance------#
    random.seed(101938)
    vnep = GenerateVNEProblemInstance()
    vnep.generateRackTopology(grid_x=2, grid_y=2, radius=1)
    vnep.generateDCNGraph(no_of_server_per_rack=4, server_capacity=10, no_of_fso_per_rack=2, link_capacity=10)
    vnep.generateVNRequest(no_of_vnreqs= 1,
                           min_node=2,
                           max_node=4,
                           min_node_demand=1,
                           max_node_demand=10,
                           min_edge=1,
                           max_edge=6,
                           min_link_demand=1,
                           max_link_demand=10,
                           min_duration=1,
                           max_duration=10,
                           min_iat=1,
                           max_iat=5)

    vnet = VNETraditional(phy_net=vnep.getPhyNetwork(),
                               vnreqs=vnep.getVReqList())
    vnet.run()

if __name__ == '__main__':
    profileRun = False #<---set True to check memory/CPU use

    if profileRun:
        cProfile.run('run_all()')
        h = hpy()
        print h.heap()
    else:
        run_all()
