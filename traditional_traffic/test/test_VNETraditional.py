import unittest
from traditional_traffic.vne_traditional import VNETraditional
from traditional_traffic.util import GenerateVNEProblemInstance
import random
import sys

class TestVNETraditional(unittest.TestCase):
    def setUp(self):
        #--------Generate problem instance------#
        random.seed(101938)
        vnep = GenerateVNEProblemInstance()
        vnep.generateRackTopology(grid_x=2, grid_y=2, radius=1)
        vnep.generateDCNGraph(no_of_server_per_rack=4, server_capacity=10, no_of_fso_per_rack=2, link_capacity=10)
        vnep.generateVNRequest(no_of_vnreqs=5,
                               min_node=2,
                               max_node=4,
                               min_node_demand=1,
                               max_node_demand=10,
                               min_edge=4,
                               max_edge=4,
                               min_link_demand=1,
                               max_link_demand=10,
                               min_duration=1,
                               max_duration=10,
                               min_iat=1,
                               max_iat=5)


        self.vnet = VNETraditional( phy_net=vnep.getPhyNetwork(),
                                    vnreqs=vnep.getVReqList() )


    #@unittest.skip("Code incomplete")
    def test_run(self):
        #self.fail()
        self.vnet.run()

    @unittest.skip("Code incomplete")
    def test_getResults(self):
        self.fail()

    @unittest.skip("Code incomplete")
    def test__computePNodeRank(self):
        self.fail()

    @unittest.skip("Code incomplete")
    def test__computeVNodeRank(self):
        self.fail()

    @unittest.skip("Code incomplete")
    def test__buildBFSTree(self):
        self.fail()

    @unittest.skip("Code incomplete")
    def test__mapNode(self):
        self.fail()

    @unittest.skip("Code incomplete")
    def test__mapLink(self):
        self.fail()

    @unittest.skip("Code incomplete")
    def test__unmapNode(self):
        self.fail()

    @unittest.skip("Code incomplete")
    def test__unmapLink(self):
        self.fail()

    @unittest.skip("Code incomplete")
    def test__findPPath(self):
        self.fail()
