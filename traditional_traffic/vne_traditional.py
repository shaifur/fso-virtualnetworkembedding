__author__ =  "Md Shaifur Rahman"
__date__ =    "May 10, 2017"
__license__ = "GPL"
__email__ =   "mdsrahman@cs.stonybrook.edu"
__status__ =  "Production"

import networkx as nx
import numpy as np
import heapq

from traditional_traffic.util import EventQueue
class VNETraditional(object):
    '''
    high level task:
    A. init phy. network
    B. read next event from event-Q
    C. if event = vn-req, try to embed,
        i) embedding step:
            I) Without re-config:
                1. compute node ranking on residual phy. network
                2. compute node ranking of vn-req
                3. compute bfs-tree on vn-graph
                4. starting at the root of bfs-vn, map each node at each level
                5. if can't be mapped, backtrack (up to max. depth), 
        ii) if successful, update phy network and current
    D. if event = finish, update phy network
    Organization:
    1. class constructor initializes class data-structures and simulator eventqueue
    2. run() method starts the simulation till all the events are processed
    3. getResults() computes and returns values such as revenue, cost, utilization, cost-to-revenue etc.
    '''


    def __init__(self, phy_net, vnreqs, backtrack_depth = 6, fso_link_capacity = 10 ):
        '''
        :param phy_net: networkx (undirected) graph with the following property:
            1. each node has following (networkx) node attributes :
                i)  capacity: float, remaining capacity of the node
                ii) max_capacity (optional): float, max capacity, used if nodes have different capacity
                iii) ntype: either 'f', 't', or 's' depending on whether fso, tor-switch or server
                iv) clist (optional): list of string/int/node-label,  list of connectible candidate nodes 
                v) isUsed (optional): if FSO node then true if connected to one of the clist node, else false
                vi) vmap (optional): dict of (node, demand) tuples keyed by vn-request number i.e.
                                    {1: (3, 10.0), 3: (1, 3.5)}
                                    initially may not have it          
            2. each link has the following (networkx) edge attributes:
                i) capacity: float, remaining capacity of the link
                ii) max_capacity (optional): float, max capacity, used if nodes have different capacity
                iii) vmap (optional): dict of (vn-edge, demand) tuples keyed by vn-request number i.e.
                                    {1: ( (3, 5), 4.0), 3: ( (1, 4), 0.5)} 
                                    initially may not have it
                                    **Edge tuples are ordered by (low, high), for node-labels
        :param vnreqs:  
            a list of tuples (arrival_time, duration, vn_req)
            pre-recorded list of vn-requests sorted by the arrival time
            i) arrival_time: float/int, simulation timestamp
            ii) duration: float/int, duration of vn-ruquest
            iii) vn_req: pointer to networkx (undirected) graph with the following property:
               1) each node has the following (networkx) node attributes:
                    i) demand: float, node demand of this virtual node
                    ii) pmap (optional): int/string/phy-node-label,label of the phy. node this node has been mapped to.
               1) each edge has the following (networkx) edge attributes:
                    i) demand: float, node demand of this virtual link
                    ii) pmap (optional): tuple (p1, p2), phy. edge label this virtual link has been mapped to.
         :param ext_logger:
            externally supplied logger, used mostly for debugging
        tasks:
            1. initialize  self.pnet with phy_net
            2. initialize simulator eventq with vnreqs
        '''
        self.phy_net = phy_net
        self.no_of_vn_reqs = 0
        self.eventq = EventQueue()
        for req_tuple in vnreqs:
            arrival_t, duration, vn_g = req_tuple
            self.no_of_vn_reqs += 1
            vn_g.graph['id'] = self.no_of_vn_reqs
            self.eventq.schedule(event_time=arrival_t, isArrival=True, vn_req=vn_g, arrival_time =arrival_t, duration=duration)
        self.backtrack_depth = backtrack_depth
        self.fso_link_capacity = fso_link_capacity
        self.phy_net = self._computePNodeRank(self.phy_net) #initially compute phy. network node rank
        # print "DEBUG: P-ranks"
        # for u,rank in self.phy_net.nodes(data='rank'):
        #     print "DEBUG:u,rank:",u,rank

    def run(self):
        '''
        task list:
        while the evenque is not empty
            1. fetch the next event
            2. if event type arrival:
                i. try to embed
                ii). if embedding successful, update phy network and 
                    insert finishing event into eventqueue and
                    store/comnpute results
            3. if event type finishing, update phy. network (by releasing resources) and store/compute results      
        :return: 
        '''
        while True:
            event_time, isArrival, vn_req, arrival_time, duration = self.eventq.getNextEvent()
            if event_time is None:
                break
            #else:
            # print "DEBUG: eventq pop data:", event_time, isArrival, vn_req.nodes(), arrival_time, duration
            if isArrival:
                if self._embed(vn_req=vn_req):
                    self.eventq.schedule(event_time=(event_time+duration), isArrival=False, vn_req=vn_req, arrival_time= arrival_time, duration=duration)
            else: #finishing event
                self._disembed( phy_net= self.phy_net, vn_req=vn_req)

    def getResults(self):
        '''
        compute avg. reveneue, admission-ratio, cost, utilization and cost-to-reveneue ratio and return
        :return: 
        '''

    def _getCandidatePNodes(self, phy_net, pnode_list, vn_req, vnode):
        '''
        for each node p in pnode_list, if capacity is  greater than demand and pnode is not already mapped to the current vn-req
        return the list
        :param phy_net: same as constructor
        :param pnode_list: sorted liste of phy node according to rank
        :param vn_req: same as constructor
        :param vnode: vnode label
        :return: 
        '''
        demand = vn_req.node[vnode]['demand']
        req_id = vn_req.graph['id']
        candidate_pnodes = []
        for pnode in pnode_list:
            if 'vmap' in phy_net.node[pnode].keys():
                if vnode in phy_net.node[pnode]['vmap'].keys():
                    continue
            if phy_net.node[pnode]['capacity'] < demand:
                continue
            candidate_pnodes.append(pnode)
        return candidate_pnodes

    def _embed(self, vn_req):
        '''
        try to embed the VN-Request vn_req into the current physical network, if successful, return True, else False
        phy. network is updated as part of the process
        Tasks:
            1. rank the vn nodes, phy nodes (only embedding is successful)
            2. make the bfs-tree based on vn-nodes rank, also candidate phy nodes for each vnode
            3. init backtrack-counter
            4. while NOT all the vnodes have been mapped:
                i) unmap vlinks, if any, between this vnode and all previously mapped vnodes
                ii) if candidate phy node list empty (all pnodes popped),
                    A. if max_depth_vnode visited, return False (embedding failed)
                iii) else, re-initialize candidate phy node list for current vnode
                iv) try to pop the next available candidate phy node pnode for current vnode
                iv) if no such phy node, then backtrack (cur_bfs_node -- )
                v) map vnode<->pnode
                vi) for each vlink to vnode to all the previously embedded node:
                       A. (recursive call) find a path on  phy network for vlink
                vii) if not successful to find a single of the paths unmap vnode<->pnode
                viii) else move on to the next vnode in the bfs-traversal (cur_bfs_node++)

        :param vn_req: same as the spec. of virtual request network in class constructor
        :return: True if embedding successful, else False 
        '''
        self.cur_vn_req = vn_req
        self.cur_vn_req = self._computeVNodeRank(self.cur_vn_req)

        self.bfs_nodes = self._buildBFSTree(self.cur_vn_req)
        self.candidate_pnodes = self._getSortedPNode(self.phy_net)
        self.isVnodeExpanded = {}
        for u in self.cur_vn_req.nodes():
            self.isVnodeExpanded[u] = False

        self.cur_candidate_pnodes = {}
        for u in self.bfs_nodes:
            self.cur_candidate_pnodes[u] = []

        #bfs_root = self.bfs_nodes[0]
        # self.cur_candidate_pnodes[bfs_root] = self._getCandidatePNodes(self.phy_net,
        #                                                                             self.candidate_pnodes,
        #                                                                             self.cur_vn_req,
        #                                                                             bfs_root)
        self.cur_bfs_indx = self.bfs_nodes[0]
        self.max_bfs_indx = len(self.bfs_nodes)
        self.min_bfs_index = self.cur_bfs_indx

        while self.cur_bfs_indx < self.max_bfs_indx:
            self.min_bfs_index = max(self.min_bfs_index, self.cur_bfs_indx - self.backtrack_depth) #update the min-backtrackable index
            cur_vnode  = self.bfs_nodes[self.cur_bfs_indx]
            print "cur_bfs_index:", self.cur_bfs_indx
            #unmap all mapped vlinks
            for u, v in self.cur_vn_req.edges( cur_vnode):
                ppath = []
                if "pmap" in self.cur_vn_req[u][v].keys():
                    ppath = self.cur_vn_req[u][v]["pmap"]
                if ppath:
                    print "DEBUG:@_embed: phy_net, vn_req, vlink:", phy_net.edges(data=True), vn_req.edges(data=True), u, v
                    self._unmapLink(self.phy_net, self.cur_vn_req, (u, v))

            #if cur-candidate-pnode is emtpy
                # check if cur-vnode is expanded,
                    # if backtrack depth is reached, return false
                    #else set cur-node-expanded false and backtrack
                #else set cur-node-expanded as True and initialize the cur-candidate-pnode
            if not self.cur_candidate_pnodes[ cur_vnode ]:
                if self.isVnodeExpanded[cur_vnode]:
                    self.isVnodeExpanded[cur_vnode]  = False
                    if self.min_bfs_index >= self.cur_bfs_indx:
                        return False
                    else: #backtrack
                        self.cur_bfs_indx = max(0, self.cur_bfs_indx - 1)  # no pnode available to map, backtrack
                        continue

                else: #else this node is re-expanded, so initialized candidate pnode list
                    self.isVnodeExpanded[cur_vnode] = True
                    self.cur_candidate_pnodes[cur_vnode] = self._getCandidatePNodes(self.phy_net,
                                                                                    self.candidate_pnodes,
                                                                                    self.cur_vn_req,
                                                                                    cur_vnode)

            candidate_pnode = self.cur_candidate_pnodes[cur_vnode].pop(0)
            self._mapNode(self.phy_net, self.cur_vn_req, cur_vnode, candidate_pnode)
            if self._recursiveLinkEmbed( self.phy_net, self.cur_vn_req, cur_vnode):
                self.cur_bfs_indx = self.cur_bfs_indx + 1
                continue
            else:
                #same cur_bfs_index, but should try next pnode in the candidate_pnode_list
                self._unmapNode(self.phy_net, self.cur_vn_req, cur_vnode)
                continue
        return True

    def _getSortedPNode(self, phy_net):
        '''
        returns phy. network nodes sorted by the non-increasing order of their ranks
        :param phy_net: as described in constructor
        :return: 
        '''
        nodes = []
        ranks = []
        sorted_node_indx = []
        sorted_nodes = []
        for u in phy_net.nodes():
            nodes.append(u)
            ranks.append(phy_net.node[u]['rank'])
        sorted_node_indx = np.array(ranks).argsort()[::-1]
        sorted_nodes = [nodes[indx] for indx in sorted_node_indx]

        return sorted_nodes

    def _recursiveLinkEmbed(self, phy_net, vn_req, vnode):
        '''
        1. finds one of the unmapped vlink between vnode and previously mapped node
        2. if no such link found, return True
        2. else, maps vlink to ppath
        3. if successful call _recursiveLinkEmbed(vnode)
        4. if the above call returns falls, unmap vlink
        5. else, return True
        :param phy_net: phy_net as spec in constructor
        :param vn_req: virtual network graph as spec. in constructor, must have 'pmap' set for all the edges/nodes already mapped 
        :param vnode: 
        :return: 
        '''
        all_neighbors = vn_req.neighbors(vnode)
        mapped_neighbor = None
        for u in all_neighbors:
            if "pmap" in vn_req.node[u].keys():
                if vn_req.node[u]["pmap"]:
                    mapped_neighbor = u
                    break
        if mapped_neighbor is None: #if no mapped neighbor, still return true
            return True
        p1 = vn_req.node[mapped_neighbor]["pmap"]
        p2 = vn_req.node[vnode]["pmap"]
        print "Debug p1,p2:",p1, p2
        ppath = self._findPPath(self.phy_net, p1, p2, vn_req[vnode][mapped_neighbor]['demand'])
        if not ppath: #no path found
            return False
        self._mapLink(self.phy_net, vn_req, (vnode, mapped_neighbor), ppath)
        hasLinkEmbedded = self._recursiveLinkEmbed(self.phy_net, vn_req, vnode)
        if not hasLinkEmbedded:
            print "DEBUG:@_recursiveLinkEmbed: phy_net, vn_req, vlink:", phy_net.edges(data=True), vn_req.edges(data=True), vnode, mapped_neighbor
            self._unmapLink(self.phy_net, vn_req, (vnode, mapped_neighbor))
        return hasLinkEmbedded


    def _disembed(self, phy_net, vn_req):
        '''
        try to remove embedding of the VN-Request vn_req from the current physical network,
        phy. network is updated as part of the process
        :param vn_req: same as the spec. of virtual request network in class constructor
        :return: 
        '''
        #--disembed all nodes
        vn_req_id = vn_req.graph['id']
        for vnode in vn_req.nodes():
            self._unmapNode(phy_net,vn_req, vnode)
        for vlink in vn_req.edges():
            print "DEBUG:@_disembed: phy_net, vn_req, vlink:",phy_net.edges(data=True),vn_req.edges(data=True),vlink
            self._unmapLink(phy_net, vn_req, vlink)



    def _computePNodeRank(self,
                          G_in,
                          d = 0.5,
                          delta = 0.0001,
                          matrix_cut_off_dim = 100,
                          max_iter_count = 10000):
        '''
        ***same is _computeVNodeRank(..) except check if the adjacent nodes has, clist and if dyn. link is active***
        In particular, connect the inactive fso-links, compute on the ranks on the new graph,
        then copy over the values to the input graph
        This method ranks all nodes according to Markov-random walk models as described in Gong et. al 2014 INFOCOM paper..
        task:
        1. calculate matrix M
        2. calculate matrix C
        3. a) if matrix too large, do iteratively improve r(k+1) = (1-d)*c + d*  M* r(k) while improvement > delta
           b) else use direct formula r= (1 - d)*inv(I- d*M) * C
        :param G_f_in: Input graph, for which to compute the node ranking matrix, must be networkx object, also must have
        :param attribute_name : Name of the link/node attribute in the networkx dict object
        :param d : weight given to bandwidth capacity, value between 0 and 1 inclusive
        :param delta : the delta value for iterative improvement, iteration stops after the difference
                        between successive improvement is < this value
        :param matrix_cut_off_dim : used for large matrix, when no of nodes is > this value, iterative approx. is used
        :param max_iter_count : in case of iterative approximation of rank matrix, this limits the total iterations

        :return: a row-matrix R containing rank value for each node, i-th row contains rank value for i-th node
        '''
        #the following connects all the connectible nodes via fso
        G_f_in = nx.Graph(G_in)
        for u in G_f_in.nodes():
            if G_f_in.node[u]['ntype'] == 'f':
                if not G_f_in.node[u]['isUsed']:
                    for v in G_f_in.node[u]['clist']:
                        if not G_f_in.node[v]['isUsed']:
                            n1 = len( G_f_in.node[u]['clist'] )
                            n2 = len( G_f_in.node[v]['clist'] )
                            G_f_in.add_edge(u, v, capacity = 1.0*self.fso_link_capacity/(n1+n2))
                            G_f_in.node[u]['isUsed'] = True
                            G_f_in.node[v]['isUsed'] = True

        # Step 1 -----------------
        n = G_f_in.number_of_nodes()
        i_s = G_f_in.nodes()
        s_i = {}
        for indx, s in enumerate(i_s):
            s_i[s] = indx

        M = np.zeros( (n, n) , dtype = np.float)

        for u, v in G_f_in.edges():
            #first compute (i,j) entry
            for i, j in [ (u, v) , (v, u)]:
                bsum = 0.0
                for k in G_f_in.neighbors(j):
                    bsum += G_f_in[j][k]['capacity']
                if bsum > 0.0: #find the ratio of denominator is greater than zero
                    M[ s_i[i] ][ s_i[j] ] = 1.0 * G_f_in[i][j]['capacity'] / bsum
                else: #otherwise the matrix entry is zero
                    M[s_i[i]][s_i[j]] = 0.0 # total zero-link capacity

        # Step 2 ----------------------
        C = np.zeros( (n, 1) , dtype = np.float)
        csum = 0.0
        for u in G_f_in.nodes():
            csum+= G_f_in.node[u]['capacity']
        if csum > 0.0: #no need to find the following ratio if total node capacity of the graph is zero
            for u in sorted(G_f_in.nodes()):
                C[ s_i[u] ][0] = 1.0 * G_f_in.node[u]['capacity'] / csum


        # Step 3---------------------
        R = np.array(C) #initialize R

        #---if  matrix  is too big, use iterative approximation
        if n> matrix_cut_off_dim:
            iter_improv = float('inf')
            iter_count = 1
            while iter_improv > delta and iter_count < max_iter_count:
                new_R = (1.0 - d) * R + d * np.dot(M, R)
                #-- find improvement in successive iterations
                iter_improv = np.linalg.norm(new_R - R)
                R = new_R
                iter_count += 1

        else: #--else use direct computation using matrix inverse
            I = np.identity(n, dtype = np.float)
            inv_I_dM = None
            try:
                inv_I_dM = np.linalg.inv(I - d*M )
            except np.linalg.LinAlgError:
                print "@VNETraditional._computeVNodeRank(..): Matrix Inverse Error!"
                return None #return None as indicative of no rank matrix was computed
            R = (1 - d)* np.dot( inv_I_dM, C)

        for i in range(n):
            G_in.node[i_s[i]]['rank'] = round(R[i][0], 2)
        return G_in
        #return R


    def _computeVNodeRank( self, G_in,
                           attribute_name = 'demand',
                           d = 0.5,
                           delta = 0.0001,
                           matrix_cut_off_dim = 100,
                           max_iter_count = 10000):
        '''
        This method ranks all nodes according to Markov-random walk models as described in Gong et. al 2014 INFOCOM paper..
        task:
        1. calculate matrix M
        2. calculate matrix C
        3. a) if matrix too large, do iteratively improve r(k+1) = (1-d)*c + d*  M* r(k) while improvement > delta
           b) else use direct formula r= (1 - d)*inv(I- d*M) * C
        :param G_in: Input graph, for which to compute the node ranking matrix, must be networkx object, also must have
        :param attribute_name : Name of the link/node attribute in the networkx dict object
        :param d : weight given to bandwidth capacity, value between 0 and 1 inclusive
        :param delta : the delta value for iterative improvement, iteration stops after the difference
                        between successive improvement is < this value
        :param matrix_cut_off_dim : used for large matrix, when no of nodes is > this value, iterative approx. is used
        :param max_iter_count : in case of iterative approximation of rank matrix, this limits the total iterations

        :return: a row-matrix R containing rank value for each node, i-th row contains rank value for i-th node
        '''

        # Step 1 -----------------
        n = G_in.number_of_nodes()
        i_s = G_in.nodes()
        s_i = {}
        for indx, s in enumerate(i_s):
            s_i[s] = indx

        M = np.zeros( (n, n) , dtype = np.float)

        for u, v in sorted(G_in.edges()):
            #first compute (i,j) entry
            for i, j in [ (u, v) , (v, u)]:
                bsum = 0.0
                for k in G_in.neighbors(j):
                    bsum += G_in[j][k][attribute_name]
                if bsum > 0.0: #find the ratio of denominator is greater than zero
                    M[ s_i[i] ][ s_i[j] ] =  1.0* G_in[i][j][attribute_name]/bsum
                else: #otherwise the matrix entry is zero
                    M[s_i[i]][s_i[j]] = 0.0 # total zero-link capacity

        # Step 2 ----------------------
        C = np.zeros( (n, 1) , dtype = np.float)
        csum = 0.0
        for u in G_in.nodes():
            csum+= G_in.node[u][attribute_name]
        if csum > 0.0: #no need to find the following ratio if total node capacity of the graph is zero
            for u in sorted(G_in.nodes()):
                C[ s_i[u] ][0] = 1.0*G_in.node[u][attribute_name]/csum


        # Step 3---------------------
        R = np.array(C) #initialize R

        #---if  matrix  is too big, use iterative approximation
        if n> matrix_cut_off_dim:
            iter_improv = float('inf')
            iter_count = 1
            while iter_improv > delta and iter_count < max_iter_count:
                new_R = (1.0 - d) * R + d * np.dot(M, R)
                #-- find improvement in successive iterations
                iter_improv = np.linalg.norm(new_R - R)
                R = new_R
                iter_count += 1

        else: #--else use direct computation using matrix inverse
            I = np.identity(n, dtype = np.float)
            inv_I_dM = None
            try:
                inv_I_dM = np.linalg.inv(I - d*M )
            except np.linalg.LinAlgError:
                print "@VNETraditional._computeVNodeRank(..): Matrix Inverse Error!"
                return None #return None as indicative of no rank matrix was computed
            R = (1 - d)* np.dot( inv_I_dM, C)

        for i in range(n):
            G_in.node[i_s[i]]['rank'] = round(R[i][0], 2)

        return G_in

    def _buildBFSTree(self, v_net):
        '''
        builds a bfs tree with root being the highest ranked node, and list of descendants sorted in non-increasing 
        order of ranks; also builds, for each virtual node,
        a list of candidate physical nodes sorted in the non-increasing order of their ranks
        associated with each virutal node 
        :param v_net: same as the spec. of virtual request network in class constructor 
                      except must have 'rank' networkx node attribute (pre-computed)
        :return bfs_node_list, candidate_plist
            bfs_node_list: list of virutal nodes as per bfs-traversal such that 
                            the bfs-tree has the highest ranked node as the root and 
                           bfs-successors are sorted in non-increasing order of their node ranks
        '''
        #find the root-node
        bfs_root = None
        max_demand =  - float('inf')
        for u in v_net.nodes():
            if max_demand < v_net.node[u]['rank']:
                max_demand =  v_net.node[u]['rank']
                bfs_root = u
        bfs_succ = nx.bfs_successors(v_net, bfs_root)


        bfs_nodes = []
        cur_level_nodes = [ bfs_root ]

        while cur_level_nodes:
            bfs_leaf_nodes = []
            bfs_leaf_sorted = []
            bfs_leaf_ranks = []

            for cur_node in cur_level_nodes:
                if cur_node in bfs_succ.keys():
                    cur_succ = bfs_succ[ cur_node ]
                    bfs_leaf_nodes = bfs_leaf_nodes + cur_succ
                    bfs_leaf_ranks = bfs_leaf_ranks + [v_net.node[u]['rank'] for u in cur_succ]

            if bfs_leaf_nodes:
                bfs_leaf_indx = np.array(bfs_leaf_ranks).argsort()[::-1]
                bfs_leaf_sorted = [ bfs_leaf_nodes[indx] for indx in bfs_leaf_indx  ]

            bfs_nodes = bfs_nodes + cur_level_nodes
            cur_level_nodes = bfs_leaf_sorted
        return bfs_nodes


    def _mapNode(self, phy_net, v_net, vnode, pnode):
        '''
         maps the vnode of v_net to the pnode of phy_net, by
         1. decreasing the capacity of pnode as per the demand of vnode
         2. adding pnode and vnode to the  pmap of vnode and vmap of pnode respectively
        :param phy_net: same as the spec. for phy_net in class constructor
        :param v_net: same as the spec. of virtual request network in class constructor
        :param vnode: label of the vnode to be mapped
        :param pnode: label of the pnode to which to map vnode
        :return: 
        '''
        phy_net.node[pnode]['capacity'] = phy_net.node[pnode]['capacity'] - v_net.node[vnode]['demand']

        if 'vmap' not in phy_net.node[pnode].keys():
            phy_net.node[pnode]['vmap'] = {}
        vreq_id = v_net.graph['id']
        phy_net.node[pnode]['vmap'][vreq_id] = vnode
        v_net.node[vnode]['pmap'] = pnode

    def _unmapNode(self, phy_net, v_net, vnode):
        '''
         un-maps the vnode of v_netto the pnode of phy_net, by
         1. increasing the capacity of pnode as per the demand of vnode 
         2. removing pnode and vnode from the  pmap of vnode and vmap of pnode respectively
        :param phy_net: same as the spec. for phy_net in class constructor but must have vmap
        :param v_net: same as the spec. of virtual request network in class constructor but must have pmap
        :param vnode: label of the vnode to be mapped
        :return: 
        '''
        pnode = v_net.node[vnode]['pmap']
        phy_net.node[pnode]['capacity'] = phy_net.node[pnode]['capacity'] + v_net.node[vnode]['demand']
        vreq_id = v_net.graph['id']
        del phy_net.node[pnode]['vmap'][vreq_id]
        del v_net.node[vnode]['pmap']

    def _mapLink(self, phy_net, v_net, vlink, phy_path):
        '''
         maps the vlink of v_net to the path phy_path of phy_net, by
            for each phy. link plink on phy_path:
             1. setting up the link if does not exist already
             1. decreasing the capacity of plink as per the demand of vlink
             2. updating vmap of plink and pmap of vlink (by adding vlink and plink respectively)
        :param phy_net: same as the spec. for phy_net in class constructor
        :param v_net: same as the spec. of virtual request network in class constructor
        :param vnode: label of the vnode to be mapped
        :param pnode: label of the pnode to which to map vnode
        :return: 
        '''
        vreq_id = v_net.graph['id']
        v1, v2 = vlink
        for (p1,p2) in phy_path:
            if phy_net.node[p1]['ntype'] == 'f' and phy_net.node[p2]['ntype'] == 'f':
                phy_net.add_edge(p1, p2, capacity = self.fso_link_capacity)
                phy_net.node[p1]['isUsed'] = True
                phy_net.node[p2]['isUsed'] = True

            phy_net[p1][p2]['capacity'] = phy_net[p1][p2]['capacity'] - v_net[v1][v2]['demand']
            if 'vmap' not in phy_net[p1][p2].keys():
                phy_net[p1][p2]['vmap'] = {}
            phy_net[p1][p2]['vmap'][vreq_id] = (v1, v2)
        v_net[v1][v2]['pmap'] = list(phy_path)


    def _unmapLink(self, phy_net, v_net, vlink):
        '''
         un-maps the vlink of v_net to the path phy_path of phy_net, by
         for each phy. link plink on phy_path:
             1. increasing the capacity of plink as per the demand of vlink
             2. updating vmap of plink and pmap of vlink (by removing vlink and plink respectively)
                also, if plink is dynamic and vmap is emtpy, mark the adjacent nodes attribute isActive as 'false'
        :param phy_net: same as the spec. for phy_net in class constructor
        :param v_net: same as the spec. of virtual request network in class constructor
        :param vlink: label of the vnode to be mapped
        :return: 
        '''
        vreq_id = v_net.graph['id']
        v1, v2 = vlink
        phy_path = v_net[v1][v2]['pmap']
        for (p1, p2) in phy_path:
            if phy_net.node[p1]['ntype'] == 'f' and phy_net.node[p2]['ntype'] == 'f':
                if not phy_net[p1][p2]['vmap'].keys(): #free up/break the fso-link if no mapped vlinks
                    phy_net.remove_edge(p1, p2)
                    phy_net.node[p1]['isUsed'] = False
                    phy_net.node[p2]['isUsed'] = False
                    continue
            phy_net[p1][p2]['capacity'] = phy_net[p1][p2]['capacity'] + v_net[v1][v2]['demand']
            del phy_net[p1][p2]['vmap'][vreq_id]
        del v_net[v1][v2]['pmap']



    def _findPPathPrev(self, phy_net, p1, p2, demand):
        '''
        finds the shortest path on the physical network phy_net between nodes p1 and p2 
        with link capacity of at least demand
        :param phy_net: same as the spec. for phy_net in class constructor
        :param p1: phy. node label
        :param p2: phy. node label
        :param demand: float, link demand to be met on the shortest path
        :return list of phy nodes [p1,.., p2] or None if no such path found: 
        '''
        #add inactive fso connection
        sphy_net = nx.Graph(phy_net)
        for u in sphy_net.nodes():
            if sphy_net.node[u]['ntype'] == 'f':
                if not sphy_net.node[u]['isUsed']:
                    clist = sphy_net.node[u]['clist']
                    for v in clist:
                        if not sphy_net.node[v]['isUsed']:
                            sphy_net.add_edge(u, v, capacity = self.fso_link_capacity)
        for (u,v) in sphy_net.edges():
            if sphy_net[u][v]['capacity'] < demand:
                sphy_net.remove_edge(u, v)

        print "Debug:"
        ppath = nx.shortest_path(sphy_net, source=p1, target=p2)
        print "Debug: ppath:",ppath
        return ppath

    def _findPPath(self, phy_net, src, tgt, demand):
        '''
        
        :param phy_net: same as constructor
        :param src: source phy node label
        :param tgt: dest phy node label
        :param demand: demand to be met by link
        :return: 
        '''
        #pre-process so that all unused candidate links are included
        #and then all links having capacity < demand are discarded
        sphy_net = nx.Graph(phy_net)
        for u in sphy_net.nodes():
            if sphy_net.node[u]['ntype'] == 'f':
                if not sphy_net.node[u]['isUsed']:
                    clist = sphy_net.node[u]['clist']
                    for v in clist:
                        if not sphy_net.node[v]['isUsed']:
                            sphy_net.add_edge(u, v, capacity = self.fso_link_capacity)
        for (u,v) in sphy_net.edges():
            if sphy_net[u][v]['capacity'] < demand:
                sphy_net.remove_edge(u, v)
        #--now run Dijsktra
        prev = {}
        fring_nodes =[src]
        visited_nodes = []
        for u in sphy_net.nodes():
            prev[u] = []
        while fring_nodes:
            cur_node = fring_nodes.pop(0)
            if cur_node == tgt:
                break
            for nbr in sphy_net.neighbors(cur_node):
                if nbr not in visited_nodes:
                    if sphy_net.node[cur_node]['ntype'] == 'f' and sphy_net.node[nbr]['ntype'] == 'f':
                        #if it's f-f link, remove all the candidate edges of prev-f node
                        for v in sphy_net.node[cur_node]['clist']:
                            if v!=nbr:
                                if (cur_node, v) in sphy_net.edges():
                                    sphy_net.remove_edge(cur_node, v)
                    prev[nbr].append(cur_node)
                    fring_nodes.append(nbr)
            visited_nodes.append(cur_node)

        ppath = []
        cur_node = tgt
        while cur_node != src:
            if not prev[cur_node]:
                return []
            prev_cur_node = prev[cur_node].pop(0)
            ppath = [(prev_cur_node, cur_node)] + ppath
            cur_node = prev_cur_node
        return ppath

