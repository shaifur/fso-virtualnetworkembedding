__author__ =  "Md Shaifur Rahman"
__date__ =    "May 1, 2017"
__license__ = "GPL"
__email__ =   "mdsrahman@cs.stonybrook.edu"
__status__ =  "Production"

'''
Set of utility classes to be used in VNE classes for traditional traffic
'''

import networkx as nx
import random
from matplotlib import pyplot as plt
import heapq
import logging
import sys

def createLogger(logLevel = logging.WARNING, loggerName = 'Virtual Network Embedding', add_log_file=False, log_file_name='vne_run.log'):
    '''
    Creates a console-output logger for  the given logLevel
    :param logLevel: must be one of the 5 log-level enums of logging class: DEBUG, INFO, WARNING, ERROR, CRITICAL
    :return:
    '''
    # create an instance of the logger
    logger = logging.getLogger(loggerName)
    logger.setLevel(logLevel)

    # create console handler and set level t
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logLevel)

    # create file handler and set level
    fh = logging.FileHandler(log_file_name)
    fh.setLevel(logLevel)

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add formatter to all the handlers
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)

    # add handlers to the logger object
    logger.addHandler(ch)
    if add_log_file:  # <--set file-log during final run, currently no file-logging for fast turnaround
        logger.addHandler(fh)
    return logger

#--------------********************************************************************-----------------------------------
class EventQueue(object):
    '''
    utility class to store events in a priority queue
    '''
    def __init__(self):
        self.queue = []


    def schedule(self, event_time, isArrival, vn_req, arrival_time, duration ):
        """
        Schedules VN request evnet arrival and expiration events
        here vn_req_no is a global index of events and isArrival is True/False for arrival/finish events
        """
        event = [event_time, isArrival, vn_req, arrival_time, duration ]
        heapq.heappush(self.queue, event)


    def getNextEvent(self):
        """
        Returns the next event in the priority queue, None tuple, if queue is empty
        """
        if self.queue:
            event_time, isArrival, vn_req, arrival_time, duration = heapq.heappop(self.queue)
            return (event_time, isArrival, vn_req, arrival_time, duration)
        else:
            return (None, False, None,  None, None)


#--------------********************************************************************-----------------------------------
class GenerateVNEProblemInstance(object):
    '''
    i) generates physical network instance
    ii) generate series of vn requests 
    iii) save them to files (optional)
    iv) returnt them as separate objects

    '''

    def generateRackTopology(self, grid_x, grid_y, radius=1):
        '''
        generate the dynamic graph showing possible connections between racks
        :return: 
        '''

        self.rack_topo_g = nx.grid_graph([grid_x, grid_y])
        self.rack_topo_g.graph['grid_x'] = grid_x
        self.rack_topo_g.graph['grid_y'] = grid_y
        # TODO : Implement adding edge upto radius
        # print self.rack_topo_g.nodes()
        # print self.rack_topo_g.edges()

    def generateDCNGraph(self, no_of_server_per_rack, server_capacity, no_of_fso_per_rack, link_capacity):
        '''
        given the # of servers, fso per rack, generate the DC graph with appropriate link and node capacity
        then save the graph
        :return: 
        '''
        self.dcn_g = nx.Graph(name='DCN',
                              grid_x=self.rack_topo_g.graph['grid_x'],
                              grid_y=self.rack_topo_g.graph['grid_y'],
                              server_capacity=server_capacity,
                              link_capacity=link_capacity,
                              no_of_server_per_rack=no_of_server_per_rack,
                              no_of_fso_per_rack=no_of_fso_per_rack)
        # print self.dcn_g.graph
        '''
        for each node in rack_topo_g graph:
            i) add all the server nodes, fso-nodes, a switch node
            ii) add edges between servers and switch; and between fso's and switches
        for each edge in rack_topo_g graph: add endopints to the corresponding fso attribute
        '''
        for n in self.rack_topo_g.nodes():
            u, v = n
            nbrs = self.rack_topo_g.neighbors(n)
            # -------------add nodes-------------------#
            for i in range(no_of_server_per_rack):  # add servers
                self.dcn_g.add_node((u, v, "s" + str(i + 1)), capacity=server_capacity, ntype='s')

            self.dcn_g.add_node((u, v, 't'), capacity=0, ntype='t')  # add tor switch

            for i in range(no_of_fso_per_rack):  # add fso's
                f_i_clist = []
                for nbr in nbrs:
                    x, y = nbr
                    for j in range(no_of_fso_per_rack):
                        f_i_clist.append((x, y, "f" + str(j + 1)))
                self.dcn_g.add_node((u, v, "f" + str(i + 1)), capacity=0, ntype='f', clist=f_i_clist, isUsed=False)

            # -------------add edges---------------------------#
            for i in range(no_of_server_per_rack - 1):  # add edges between servers
                for j in range(i + 1, no_of_server_per_rack):
                    self.dcn_g.add_edge((u, v, "s" + str(i + 1)),
                                        (u, v, "s" + str(j + 1)),
                                        capacity=link_capacity)

            for i in range(no_of_server_per_rack):  # add edges between servers and the tor switch
                self.dcn_g.add_edge((u, v, "s" + str(i + 1)),
                                    (u, v, "t"),
                                    capacity=link_capacity)

            for i in range(no_of_fso_per_rack):  # add edges between fso's and the tor switch
                self.dcn_g.add_edge((u, v, "f" + str(i + 1)),
                                    (u, v, "t"),
                                    capacity=link_capacity)
                #
                # for u in sorted(self.dcn_g.nodes()):
                #     print u,"<--->",v,"  rem-cap:", self.dcn_g.node[u]['capacity']," node-type:",self.dcn_g.node[u]['ntype'],
                #     if self.dcn_g.node[u]['ntype'] =='f':
                #         print "\t\tclist:",self.dcn_g.node[u]['clist']," isUsed",self.dcn_g.node[u]['isUsed']
                #     else:
                #         print ""
                #
                # for u,v in sorted(self.dcn_g.edges()):
                #     print u,"<--->",v,"  rem-cap:", self.dcn_g[u][v]['capacity']

    def generateVNRequest(self, no_of_vnreqs,
                          min_node,
                          max_node,
                          min_node_demand,
                          max_node_demand,
                          min_edge,
                          max_edge,
                          min_link_demand,
                          max_link_demand,
                          min_duration,
                          max_duration,
                          min_iat,
                          max_iat):
        '''
        generates a list of tuples (nxGraph-annotated-with-demand, starting_time, duration)
        here time is integer

        :return: 
        '''
        self.req_list = []
        arrival_t = 0
        for n in range(no_of_vnreqs):
            vn_g = nx.gnm_random_graph(n=random.randint(min_node, max_node),
                                       m=random.randint(min_edge, max_edge))
            # --add node-demand---
            for u in vn_g.nodes():
                vn_g.node[u]['demand'] = random.randint(min_node_demand, max_node_demand)

            # # ----find the isolates and add an edge to non-isolates
            # vn_g_isolates = nx.isolates(vn_g)
            # vn_g_non_isolates = list(set(vn_g.nodes()) - set(vn_g_isolates))
            # for iso_n in vn_g_isolates:
            #     vn_g.add_edge(iso_n, random.choice(vn_g_non_isolates))

            #---find the connected components and put edges between them

            vn_connected_comp = nx.connected_components(vn_g)
            nodes_to_connect =[]
            for c_comp in vn_connected_comp:
                u =  c_comp.pop()
                nodes_to_connect.append(u)

            for indx in range(len(nodes_to_connect) - 1):
                u = nodes_to_connect[indx]
                v = nodes_to_connect[indx+1]
                vn_g.add_edge(u, v)


            # ---add link demand randomly
            for u, v in vn_g.edges():
                vn_g[u][v]['demand'] = random.randint(min_link_demand, max_link_demand)
            duration = random.randint(min_duration, max_duration)
            self.req_list.append( (arrival_t, duration, vn_g) )
            iat = random.randint(min_iat, max_iat)
            arrival_t += iat

            # event_counter = 0
            # for e in self.req_list:
            #     a, d, g = e
            #     event_counter += 1
            #     print "event# ", event_counter," arrival_t: ",a," duration: ",d
            #     pos = nx.spring_layout(g)
            #     nx.draw(g, pos)
            #     node_labels = nx.get_node_attributes(g, 'd')
            #     nx.draw_networkx_labels(g, pos, labels=node_labels)
            #     edge_labels = nx.get_edge_attributes(g, 'd')
            #     nx.draw_networkx_edge_labels(g, pos, labels=edge_labels)
            #     plt.show()

    def getPhyNetwork(self):
        return self.dcn_g
    def getVReqList(self):
        return self.req_list
